#!/usr/bin/env bash

wget http://download.redis.io/releases/redis-5.0.3.tar.gz
tar -xvf redis-5.0.3.tar.gz
cd redis-5.0.3
make

src/redis-server --daemonize yes --protected-mode no
