#!/bin/bash

# this should be run from the project root (e.g., ~/RubymineProjects/meta)

mkdir public/av 2>/dev/null && echo "Created empty av folder in public/av"
cd public/av
AV_DIR="$(pwd)"
cd ../..
[ -e ../dlg/public ] && ln -fhs "$AV_DIR" ../dlg/public/dlg_av && echo "Linked dlg/public/dlg_av to public/av folder in this project"
