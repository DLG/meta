#!/bin/bash
set -e

docker compose build --build-arg SOLRCONFIG_BRANCH=${SOLRCONFIG_BRANCH:-master}
docker compose up -d

SOLR_TIMEOUT_WAIT=30
while [[ $(docker compose exec solr /opt/solr/bin/solr status) =~ "No Solr nodes are running" ]]
do
  if [ $SOLR_TIMEOUT_WAIT -le 0 ]; then
    echo 'Solr never started :('
    break
  fi
  echo "Waiting for Solr..."
  sleep 1
  ((SOLR_TIMEOUT_WAIT--))
done

docker compose exec solr /opt/solr/bin/solr create_core -c meta-dev -d /opt/solr/server/solr/configsets/meta || echo 'Skipped.'
docker compose exec solr /opt/solr/bin/solr create_core -c meta-test -d /opt/solr/server/solr/configsets/meta || echo 'Skipped.'
docker compose exec solr /opt/solr/bin/solr create_core -c dlg-test -d /opt/solr/server/solr/configsets/meta || echo 'Skipped.'
docker compose exec solr /opt/solr/bin/solr create_core -c crdl-test -d /opt/solr/server/solr/configsets/meta || echo 'Skipped.'
bundle config jobs $(nproc 2>/dev/null || sysctl -n hw.ncpu 2>/dev/null || getconf _NPROCESSORS_ONLN 2>/dev/null)
RAILS_ENV=development bundle exec rake db:setup

bash provision/av_directory.sh