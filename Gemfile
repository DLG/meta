# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.2.2'

gem "activerecord-import"
gem 'addressable'
gem 'aws-sdk-s3'
gem 'activemodel-serializers-xml'
gem 'blacklight', '7.33.1'
gem 'blacklight-gallery', '>= 1.0'
gem 'leaflet-rails', '0.7.7' # 9/8: Downgraded back to where this was in Master to fix "Cannot read property 'trim' of undefined" bug
gem 'blacklight_advanced_search', '7.0.0'
gem 'blacklight_range_limit', '~> 7.0.0'
gem 'bootsnap', require: false
gem 'bootstrap', '~> 4.0'
gem 'bootstrap_form', '~> 4.0'
gem 'cancancan'
gem 'carrierwave', '~> 2.2.5'
gem 'chosen-rails'
gem "cocoon"
gem 'coffee-rails'
gem 'devise'
gem 'devise_invitable'
gem 'diffy'
gem 'exception_notification'
gem 'filterrific'
gem 'font-awesome-sass', '~> 6.2', '>= 6.2.1'
gem 'geocoder'
gem 'gretel'
gem 'httparty'
gem 'image_processing'
gem 'jbuilder'
gem 'jquery-rails'
gem 'jquery-turbolinks'
gem 'json'
gem 'multi-select-rails'
gem 'mock_redis'
gem 'net-ftp', require: false
gem 'nokogiri'
gem 'openseadragon'
gem 'paper_trail'
gem 'pagy'
gem 'pg'
gem 'pg_search'
# popper needed for bootstrap dropdown buttons
gem 'popper_js', '>= 1.14.3', '< 2'
gem 'puma', '~> 6.3'
gem 'rails', '~> 7.0'
gem 'redis'
gem 'resque'
gem 'rsolr'
gem 'rubyzip'
gem 'sass-rails'
gem "sentry-ruby"
gem "sentry-rails"
gem 'slack-notifier'
gem "sprockets-rails"
gem 'sunspot_rails'
gem 'turbolinks'
gem 'twitter-typeahead-rails', '0.11.1.pre.corejavascript'
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw]
gem 'uglifier'

group :test do
  gem 'capybara', '~> 3'
  gem 'capybara-screenshot'
  gem "cuprite"
  gem 'fabrication'
  gem 'rails-controller-testing'
  gem 'resque_spec'
  gem 'rspec-mocks'
  gem 'simplecov'
  gem 'webdrivers'
end

group :development do
  gem 'listen'
  gem 'web-console'
end

group :development, :test do
  gem 'bullet'
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'rspec-rails'
end

