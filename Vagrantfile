# Install vagrant-disksize to allow resizing the vagrant box disk.
unless Vagrant.has_plugin?("vagrant-disksize")
  raise  Vagrant::Errors::VagrantError.new, "vagrant-disksize plugin is missing. Please install it using 'vagrant plugin install vagrant-disksize' and rerun 'vagrant up'"
end

Vagrant.configure('2') do |config|
  config.disksize.size = '50GB'
  config.vm.box = 'ubuntu/xenial64'
  config.vm.network :forwarded_port, guest: 8983, host: 4983
  config.vm.network :forwarded_port, guest: 5432, host: 4432
  config.vm.network :forwarded_port, guest: 6379, host: 4379
  config.vm.provider :virtualbox do |v, _|
    v.cpus = 2
    v.memory = 2048
  end
  config.vm.provision 'shell', inline: <<-SHELL
    sudo date > /etc/vagrant_provisioned_at
    apt-get update
    apt-get upgrade
    apt-get -y -q install git-core make gcc default-jdk
  SHELL
  config.vm.provision :shell, path: 'provision/postgres.sh'
  config.vm.provision :shell, path: 'provision/redis.sh'
  config.vm.provision :shell, path: 'provision/solr.sh', privileged: false

  #   Start up Applications
  config.vm.provision :shell,
                      path: 'provision/startup.sh',
                      run: 'always'
end