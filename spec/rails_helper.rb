# This file is copied to spec/ when you run 'rails generate rspec:install'
require 'spec_helper'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
# Prevent database truncation if the environment is production
abort('The Rails environment is running in production mode!') if Rails.env.production?
require 'rspec/rails'
require 'capybara/rspec'
require "capybara/cuprite"
require 'paper_trail/frameworks/rspec'
require 'capybara-screenshot/rspec'
require 'chosen-rails/rspec'

Capybara.javascript_driver = :cuprite
Capybara.register_driver(:cuprite) do |app|
  Capybara::Cuprite::Driver.new(app,
                                js_errors: true,
                                window_size: [1200, 900],
                                browser_options: { 'no-sandbox': nil },
                                timeout: 30,           # Timeout for commands (default is 5 seconds)
                                process_timeout: 60,
                                )
end


Webdrivers.install_dir = "#{Rails.root}/.webdrivers"

# Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
#
# The following line is provided for convenience purposes. It has the downside
# of increasing the boot-up time by auto-requiring all files in the support
# directory. Alternatively, in the individual `*_spec.rb` files, manually
# require only the support files necessary.
#
# Dir[Rails.root.join('spec', 'support', '**', '*.rb')].each { |f| require f }

# Checks for pending migrations and applies them before tests are run.
# If you are not using ActiveRecord, you can remove these lines.
begin
  ActiveRecord::Migration.maintain_test_schema!
rescue ActiveRecord::PendingMigrationError => e
  puts e.to_s.strip
  exit 1
end

RSpec::Matchers.define :have_normalized_text do |expected|
  match do |actual|
    return false if actual.nil? || expected.nil?
    normalized_page_text = actual.text&.gsub(/\s+/, ' ')
    normalized_page_text.include? expected.strip.gsub(/\s+/, ' ')
  end
end

RSpec.configure do |config|
  config.use_transactional_fixtures = true
  config.infer_spec_type_from_file_location!
  config.filter_rails_from_backtrace!

  config.include Devise::Test::ControllerHelpers, type: :controller

  # clean up DB & Solr index before test suite
  config.before(:suite) do
    Dir[Rails.root.join('app/models/*.rb').to_s].each do |filename|
      klass = File.basename(filename, '.rb').camelize.constantize
      next unless klass.ancestors.include?(ActiveRecord::Base)
      next if klass.abstract_class?
      ActiveRecord::Base.connection.execute("TRUNCATE TABLE #{klass.table_name} RESTART IDENTITY CASCADE")
    end
  end

  config.before(:all) do
    SolrService.delete_all
  end

  config.before(:each) do
    $redis.flushdb
  end
end
