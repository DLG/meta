Fabricator :batch_item do
  batch
  slug { sequence(:slug) { |n| "slug-#{n}" }}
  collection { Fabricate :empty_collection }
  portals do |attrs|
    attrs[:collection].portals
  end
  dcterms_title { sequence(:dcterms_title) { |n| %W[Title#{n} Alternate#{n}] }}
  dcterms_type { [%w(StillImage Text).sample] }
  dcterms_subject { [%w(Athens Atlanta Augusta Macon).sample, 'Georgia'] }
  geographic_locations(count: 1)
  dc_date ['1999-2000']
  dc_right [I18n.t('meta.rights.zero.uri')]
  dcterms_contributor ['DLG']
  edm_is_shown_at ['http://dlg.galileo.usg.edu']
  edm_is_shown_by ['http://dlg.galileo.usg.edu']
  iiif_partner_url {'http://dlg.galileo.usg.edu'}
  iiif_partner_url_enabled false
  fulltext { 'Plaid bicycle rights selfies tumeric. Fam actually messenger bag, tbh intelligentsia thundercats migas semiotics asymmetrical.' }
  holding_institutions(count: 1)
  source_data { '' }
  external_identifiers { [] }
end

Fabricator :batch_serial_item, from: :batch_item do
  serial
  slug { '' }
  dcterms_title []
  dcterms_type []
  dcterms_subject []
  dcterms_contributor []
  dc_date { sequence(:dc_date) { |n| %W[#{n}] }}
  dc_right []
  geographic_locations []
  edm_is_shown_at ['http://dlg.galileo.usg.edu']
  edm_is_shown_by ['http://dlg.galileo.usg.edu']
end

Fabricator :batch_item_without_batch, from: :batch_item do
  batch nil
end
