Fabricator(:geographic_location) do
  component_placenames { sequence(:component_placenames) { |n| ['United States', "State#{n}", "County#{n}", "City#{n}"] }}
  geonames_id { sequence(:geonames_id) { |n| 1000 + n }}
  latitude do
    rand = Random.new
    rand.rand(30.0..35.0) #Roughly in or around GA
  end
  longitude do
    rand = Random.new
    -rand.rand(81.0..86.0)#Roughly in or around GA
  end
end