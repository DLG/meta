
Fabricator(:name) do
end

Fabricator(:valid_name, from: :name) do
  portals                   {[Fabricate(:portal, code: 'crdl')]}
  name_type                 "person"    # person|corpent
  slug                      "kennedy_john_f_john_fitzgerald_1917_1963"
  additional_slugs          "jfk"
  legacy_slug               "000000358"
  authoritative_name        "Kennedy, John F. (John Fitzgerald), 1917-1963"
  related_names             "Onassis, Jacqueline Kennedy, 1929-1994"
  bio_history               "35th President of the United States"
  authoritative_name_source "LCNAF"
  source_uri                "https://authorities.loc.gov/cgi-bin/Pwebrecon.cgi?AuthRecID=2354935&v1=1&PID=FgwmwUeaKWrSiEo6r0cJaGtx-DBzCDe"
  oclc_number               "00288416"
  public                    true
end
