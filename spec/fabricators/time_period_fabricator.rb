Fabricator(:time_period) do
  name { sequence(:name) { |n| "Name#{n}" }}
  start {  (DateTime.now - 20.years).year }
  finish { (DateTime.now - 18.years).year }
end
