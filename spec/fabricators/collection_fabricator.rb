Fabricator(:collection) do
  slug { sequence(:slug) { |n| "slug-#{n}" }}
  display_title { sequence(:display_title) { |n| "Display Title #{n}" } }
  short_description { 'Swag pug tilde kogi synth hammock cold-pressed hell of occupy craft beer food truck man bun woke biodiesel.' }
  dcterms_title { sequence(:dcterms_title) { |n| %W[Title#{n} Alternate#{n}] }}
  dc_date ['1999-2000']
  dc_right [I18n.t('meta.rights.zero.uri')]
  edm_is_shown_at ['http://dlg.galileo.usg.edu']
  edm_is_shown_by ['http://dlg.galileo.usg.edu']
  sponsor_note 'Health goth iceland pug shoreditch'
  sponsor_image File.open("#{Rails.root}/spec/files/snickers.jpg")
  dcterms_type ['Collection']
  holding_institutions(count: 1)
  geographic_locations(count: 1)
end

Fabricator(:empty_collection, from: :collection) do
  repository(fabricator: :empty_repository)
  portals { |attrs| attrs[:repository].portals }
end

Fabricator(:empty_collection_with_resource, from: :collection) do
  repository(fabricator: :empty_repository)
  portals { |attrs| attrs[:repository].portals }
  collection_resources(count:1)
end

Fabricator(:collection_with_repo_and_item, from: :collection) do
  repository(fabricator: :empty_repository)
  portals { |attrs| attrs[:repository].portals }
  items(count: 1) do |attrs|
    Fabricate.build(:robust_item, portals: attrs[:repository].portals)
  end
end

Fabricator(:collection_with_public_repo_and_item, from: :collection) do
  repository(fabricator: :empty_public_repository)
  portals { |attrs| attrs[:repository].portals }
  items(count: 1) do |attrs|
    Fabricate.build(:robust_item, portals: attrs[:repository].portals)
  end
end

Fabricator(:collection_with_repo_and_robust_item, from: :collection) do
  repository(fabricator: :empty_repository)
  portals { |attrs| attrs[:repository].portals }
  items(count: 1) do |attrs|
    Fabricate.build(:robust_item, portals: attrs[:repository].portals)
  end
end