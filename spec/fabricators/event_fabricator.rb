Fabricator(:event) do
end

Fabricator(:valid_event, from: :event) do
  slug                         "uga_integration"
  title                        "University of Georgia Integration"
  dcterms_temporal             ['1961']
  description                  "On January 6, 1961, Judge W. A. Bootle ordered the admission of Hamilton Holmes and Charlayne Hunter to the University of Georgia"
  canned_search                '_su: "College integration--Georgia--Athens" or "University of Georgia"'
  public                       true
  dcterms_subject_matches      ["College integration--Georgia--Athens","University of Georgia"]
  dlg_subject_personal_matches ["Hamilton Holmes","Charlayne Hunter"]
  legacy_data                  '{
    "link": "http://crdl.usg.edu/events/uga_integration",
    "Record ID": "uga_integration",
    "Record Type": "event",
    "Event ID": "uga_integration",
    "Public": "yes",
    "DC Title": "University of Georgia Integration"
  }'
end
