Fabricator(:pending_invitation, from: :user) do
  email { sequence(:email) { |n| "user#{n}@uga.edu" }}
  invitation_created_at { Time.now }
  invitation_sent_at { Time.now }
end
