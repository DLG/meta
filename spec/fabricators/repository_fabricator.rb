Fabricator(:repository) do
  slug { sequence(:slug) { |n| "slug-#{n}" }}
  title { sequence(:title) { |n| "Repo Title #{n}" }}
  portals(count: 1)
  collections(count: 1) do |attrs|
    Fabricate.build(:collection, portals: attrs[:portals],
                    items: [Fabricate.build(:item, portals: attrs[:portals])]
    )
  end
end

Fabricator(:empty_repository, from: :repository) do
  collections []
end

Fabricator(:empty_public_repository, from: :repository) do
  collections []
  public true
  portals {[Fabricate(:portal, code: 'georgia')]}
end

Fabricator(:empty_crdl_repository, from: :repository) do
  collections []
  portals {[Fabricate(:portal, code: 'crdl')]}
end

Fabricator(:repository_public_true, from: :repository) do
  public true
end
