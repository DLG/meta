# frozen_string_literal: true
Fabricator(:serial) do
  slug { sequence(:slug) { |n| "slug-#{n}" }}
  dcterms_title { sequence(:dcterms_title) { |n| %W[Title#{n} Alternate#{n}] }}
  dcterms_type { %w(Text) }
  dcterms_subject { ['Georgia'] }
  dcterms_description { ['Serial Description'] }
  dcterms_medium { ['Serial Medium'] }
  dcterms_contributor { ['Serial Contributor'] }
  dcterms_creator { ['Serial Creator'] }
  dc_date ['1999-2000']
  dc_right [I18n.t('meta.rights.zero.uri')]
  geographic_locations(count: 1)
  source_data { '' }
  external_identifiers { [] }
  dcterms_publisher { %w(Publisher) }
  dlg_subject_personal {['Serial Subject']}
  dcterms_language {['Serial Language']}
end