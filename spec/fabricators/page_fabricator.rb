Fabricator(:page) do
  item
  number { Fabricate.sequence(:pages, 1) }
  media_type 'image'
  file_type 'jpg'
  title { sequence(:title) { |n| "Title #{n}" } }
end

Fabricator(:page_with_item, from: :page) do
  item { Fabricate(:item_with_parents) }
end

Fabricator(:page_with_item_and_text, from: :page_with_item) do
  fulltext { 'Raw denim flannel artisan dreamcatcher' }
end
