Fabricator(:feature) do
  image File.open("#{Rails.root}/spec/files/aarl.gif")
  title { sequence(:title) { |n| "Title #{n}" } }
  title_link { 'https://example.com' }
  institution { sequence(:institution) { |n| "Institution #{n}" } }
  institution_link { 'https://example.com' }
  area 'carousel'
  primary false
  public true
  portals(count: 1)
end

Fabricator(:external_feature, from: :feature) do
  external_link { 'https://example.com' }
end

Fabricator(:tab_feature, from: :feature) do
  short_description { 'Letterpress williamsburg ramps distillery hella small batch swag enamel pin vaporware hexagon.' }
  area 'tabs'
end

Fabricator(:primary_tab_feature, from: :feature) do
  large_image File.open("#{Rails.root}/spec/files/aarl.gif")
  short_description { 'Normcore fixie woke tacos tousled venmo kogi migas organic retro vegan gentrify.' }
  area 'tabs'
  primary true
end