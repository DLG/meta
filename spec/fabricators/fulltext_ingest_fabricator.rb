Fabricator(:fulltext_ingest) do
  title { sequence(:title) { |n| "Title #{n}" } }
  description { 'Copper mug thundercats everyday carry health goth.' }
  user { Fabricate :super }
  file File.open("#{Rails.root}/spec/files/fulltext.zip")
end

Fabricator(:completed_fulltext_ingest_success, from: :fulltext_ingest) do
  queued_at { DateTime.now - 1.day }
  finished_at { DateTime.now }
  results do
    {
      status: 'success',
      message: 'Hooray',
      files: {
        succeeded: [Fabricate(:item_with_parents).id, Fabricate(:item_with_parents).id],
        failed: {}
      }
    }
  end
end

Fabricator(:completed_fulltext_ingest_with_errors, from: :fulltext_ingest) do
  queued_at { DateTime.now - 1.day }
  finished_at { DateTime.now }
  results do
    {
      status: 'partial failure',
      message: 'Message',
      files: {
        succeeded: [Fabricate(:item_with_parents).id],
        failed: {
          r1_c1_i2: 'Exception message'
        }
      }
    }
  end
end

Fabricator(:completed_fulltext_ingest_total_failure, from: :fulltext_ingest) do
  queued_at { DateTime.now - 1.day }
  finished_at { DateTime.now }
  results do
    {
      status: 'failed',
      message: 'Overall failure',
      files: {
        succeeded: [],
        failed: {
          r1_c1_i1: 'Exception message',
          r1_c1_i2: 'Exception message'
        }
      }
    }
  end
end

Fabricator(:queued_fulltext_ingest, from: :fulltext_ingest) do
  queued_at { DateTime.now - 1.day }
end

Fabricator(:undone_fulltext_ingest, from: :fulltext_ingest) do
  queued_at { DateTime.now - 1.day }
  undone_at { DateTime.now }
end

Fabricator(:completed_fulltext_ingest_for_undoing, from: :fulltext_ingest) do
  queued_at { DateTime.now - 1.day }
  finished_at { DateTime.now }
  results do
    {
      status: 'success',
      message: 'Hooray',
      files: {
        succeeded: [Fabricate(:item_with_parents).id],
        failed: {}
      }
    }
  end
end