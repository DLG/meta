Fabricator(:collection_resource) do
  slug { sequence(:slug) { |n| "slug-#{n}" }}
  position { sequence(:position, 1) }
  title { 'Title' }
  use_external_link { false }
  external_link { '' }
  content { 'Succulents umami venmo craft beer selvage. Vexillologist tousled YOLO gastropub.' }
  collection { Fabricate :empty_collection }
end
