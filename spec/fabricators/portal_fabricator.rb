Fabricator :portal do
  name { sequence(:name) { |n| "Name #{n}" }}
  code { sequence(:code) { |n| "code#{n}" }}
end