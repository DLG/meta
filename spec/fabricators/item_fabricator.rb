Fabricator(:item) do
  slug { sequence(:slug) { |n| "slug-#{n}" }}
  dcterms_title { sequence(:dcterms_title) { |n| %W[Title#{n} Alternate#{n}] }}
  dcterms_type { [%w[StillImage Text].sample] }
  dcterms_subject { [%w(Athens Atlanta Augusta Macon).sample, 'Georgia'] }
  dcterms_description { ['Item Description'] }
  dcterms_medium { ['Item medium'] }
  dcterms_creator { ['Item Creator'] }
  dc_date ['1999-2000']
  dc_right [I18n.t('meta.rights.zero.uri')]
  dcterms_contributor ['DLG']
  edm_is_shown_at ['http://dlg.galileo.usg.edu']
  edm_is_shown_by ['http://dlg.galileo.usg.edu']
  iiif_partner_url {'http://dlg.galileo.usg.edu'}
  iiif_partner_url_enabled false
  fulltext { 'Tousled vaporware single-origin coffee hoodie.' }
  holding_institutions(count: 1)
  legacy_dcterms_provenance {  |attrs| attrs[:holding_institutions].collect(&:authorized_name) }
  geographic_locations(count: 1)
  source_data { '' }
  external_identifiers { [] }
  dlg_subject_personal {['Item Subject']}
  dcterms_language {['Item Language']}

end

Fabricator(:item_with_parents, from: :item) do
  repository(fabricator: :empty_repository)
  collection do |attrs|
    Fabricate(
      :empty_collection,
      repository: attrs[:repository],
      portals: attrs[:repository].portals
    )
  end
  portals do |attrs|
    attrs[:repository].portals
  end
  holding_institutions(count: 1)
end

Fabricator(:crdl_item, from: :item) do
  repository(fabricator: :empty_crdl_repository)
  collection do |attrs|
    Fabricate(
      :empty_collection,
      repository: attrs[:repository],
      portals: attrs[:repository].portals
    )
  end
  portals do |attrs|
    attrs[:repository].portals
  end
  holding_institutions(count: 1)
end

Fabricator(:item_with_parents_and_pages, from: :item_with_parents) do
  pages(count: 3)
end

Fabricator(:invalid_item) do
  slug { sequence(:slug) { |n| "slug-#{n}" }}
  dcterms_title { ['Title 1', 'Title 2'] }
end

Fabricator(:robust_item, from: :item) do
  dcterms_publisher { ['The University of Georgia'] }
  edm_is_shown_at { ['http://dlg.galileo.usg.edu'] }
  edm_is_shown_by { ['http://dlg.galileo.usg.edu'] }
  dcterms_identifier { ['http://dlg.galileo.usg.edu'] }
  dlg_subject_personal { ['Art Decco'] }
  geographic_locations(count: 2)
end

Fabricator(:item_with_serial, from: :item_with_parents) do
  serial { Fabricate(:serial) }
end
