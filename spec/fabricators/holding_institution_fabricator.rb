Fabricator(:holding_institution) do
  slug { sequence(:slug) { |n| "slug-#{n}" }}
  authorized_name { sequence(:authorized_name) { |n| "Authorized Name #{n}" } }
  short_description { 'Baby typewriter unicorn literally, kinfolk messenger bag semiotics.' }
  description { 'Mustache messenger bag pinterest vegan, sartorial semiotics tbh retro pok pok meggings mixtape mumblecore adaptogen marfa.' }
  homepage_url { 'https://www.example.com' }
  coordinates '31.978987, -81.161760'
  strengths { 'Next level pinterest' }
  public_contact_address { '320 S Jackson St, Athens, GA 30602' }
  public_contact_email { 'user@uga.edu' }
  public_contact_phone { '555-555-5555' }
  institution_type 'Museum'
  galileo_member false
  contact_name { 'Contact Name' }
  contact_email { 'user@uga.edu' }
  harvest_strategy 'OAI'
  oai_urls { 'https://www.example.com' }
  ignored_collections 'None'
  last_harvested_at { DateTime.now - 1.year }
  analytics_emails { %w[user1@uga.edu user2@uga.edu] }
  subgranting { 'franzen stumptown' }
  grant_partnerships { 'thundercats humblebrag' }
  training { 'Cold-pressed snackwave swag post-ironic bitters brunch mumblecore portland live-edge.' }
  site_visits { 'Sustainable kale chips lo-fi edison bulb godard umami cornhole.' }
  consultations { 'Put a bird on it wolf YOLO semiotics' }
  impact_stories { 'Pork belly cardigan fingerstache vaporware' }
  newspaper_partnerships { 'Next level kitsch af everyday carry try-hard' }
  committee_participation { 'Pickled vape ennui flannel fingerstache' }
  other { 'woke palo santo green' }
  notes { 'Cronut locavore snackwave, photo booth cray' }
  parent_institution { 'The University of Georgia' }
  image File.open("#{Rails.root}/spec/files/aarl.gif")
end
