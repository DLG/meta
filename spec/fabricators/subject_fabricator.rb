Fabricator(:subject) do
  name { sequence(:name) { |n| "Name #{n}" }}
end
