Fabricator(:project) do
  title { sequence(:title) { |n| "Title #{n}" }}
  fiscal_year '2019'
  hosting 'archival'
  storage_used 100.5
  holding_institution
end
