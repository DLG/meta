require 'rails_helper'

RSpec.describe Serial, type: :model do
  context 'validations' do
    it 'is not valid without a slug' do
      i = Fabricate.build(:item, slug: '')
      i.valid?
      expect(i.errors).to have_key :slug
    end
    it 'is not valid without a dcterms_title' do
      i = Fabricate.build(:item, dcterms_title: [])
      i.valid?
      expect(i.errors).to have_key :dcterms_title
    end
  end
end
