require 'rails_helper'

RSpec.describe GeographicLocation, type: :model do
  context 'attributes' do
    context 'code' do
      it 'is invalid if code, which is derived from component_placenames, is empty' do
        geographic_location = Fabricate.build(:geographic_location, component_placenames: [])
        geographic_location.valid?
        expect(geographic_location.errors).to have_key :code
      end
      it 'is invalid if code is not unique and Geonames ID is nil' do
        Fabricate(:geographic_location,
                  component_placenames: ['United States', 'Georgia', 'Dummy County', 'Dummy City'],
                  geonames_id: nil)
        geographic_location = Fabricate.build(:geographic_location,
                                              component_placenames: ['United States', 'Georgia', 'Dummy County', 'Dummy City'],
                                              geonames_id: nil)
        geographic_location.valid?
        expect(geographic_location.errors).to have_key :code
        Fabricate(:geographic_location,
                  component_placenames: ['United States', 'Georgia', 'Dummy County', 'Dummy Hamlet'],
                  geonames_id: 5)
        geographic_location = Fabricate.build(:geographic_location,
                                              component_placenames: ['United States', 'Georgia', 'Dummy County', 'Dummy Hamlet'],
                                              geonames_id: nil)
        geographic_location.valid?
        expect(geographic_location.errors).to have_key :code
      end
      it 'is valid if two records with different non-nil Geonames IDs have the same code' do
        Fabricate(:geographic_location,
                  component_placenames: ['United States', 'Georgia', 'Dummy County', 'Dummy City'],
                  geonames_id: 1)
        geographic_location = Fabricate.build(:geographic_location,
                                              component_placenames: ['United States', 'Georgia', 'Dummy County', 'Dummy City'],
                                              geonames_id: 2)
        expect(geographic_location.valid?).to be_truthy
      end
    end
    context 'geonames_id' do
      it 'is valid if geonames_id is an integer' do
        geographic_location = Fabricate.build(:geographic_location,  geonames_id: 5)
        expect(geographic_location.valid?).to be_truthy
      end
      it 'is valid if geonames_id is nil' do
        geographic_location = Fabricate.build(:geographic_location,  geonames_id: nil)
        expect(geographic_location.valid?).to be_truthy
      end
      it 'is valid if geonames_id is supplied as a blank string, which will be saved as nil' do
        geographic_location = Fabricate.build(:geographic_location,  geonames_id: '')
        expect(geographic_location.valid?).to be_truthy
        expect(geographic_location.geonames_id).to be_nil
      end
      it 'is invalid if geonames_id has a decimal' do
        geographic_location = Fabricate.build(:geographic_location,  geonames_id: 12.4)
        geographic_location.valid?
        expect(geographic_location.errors).to have_key :geonames_id
      end
      it 'is invalid if geonames_id is non-numeric' do
        geographic_location = Fabricate.build(:geographic_location,  geonames_id: 'A')
        geographic_location.valid?
        expect(geographic_location.errors).to have_key :geonames_id
      end
    end
    context 'latitude' do
      it 'is valid if latitude is a number between -90 and 90' do
        geographic_location = Fabricate.build(:geographic_location,  latitude: 15.3)
        expect(geographic_location.valid?).to be_truthy
        geographic_location = Fabricate.build(:geographic_location,  latitude: -85)
        expect(geographic_location.valid?).to be_truthy
      end
      it 'is valid if latitude is nil' do
        geographic_location = Fabricate.build(:geographic_location,  latitude: nil)
        expect(geographic_location.valid?).to be_truthy
      end
      it 'is valid if latitude is supplied as a blank string, which will be saved as nil' do
        geographic_location = Fabricate.build(:geographic_location,  latitude: '')
        expect(geographic_location.valid?).to be_truthy
        expect(geographic_location.latitude).to be_nil
      end
      it 'is invalid if latitude is a number greater than 90 or less than -90' do
        geographic_location = Fabricate.build(:geographic_location,  latitude: 91)
        geographic_location.valid?
        expect(geographic_location.errors).to have_key :latitude
        geographic_location = Fabricate.build(:geographic_location,  latitude: -90.4)
        geographic_location.valid?
        expect(geographic_location.errors).to have_key :latitude
      end
      it 'is invalid if latitude is non-numeric' do
        geographic_location = Fabricate.build(:geographic_location,  latitude: 'A')
        geographic_location.valid?
        expect(geographic_location.errors).to have_key :latitude
      end
    end
    context 'longitude' do
      it 'is valid if longitude is a number between -180 and 180' do
        geographic_location = Fabricate.build(:geographic_location,  longitude: 179)
        expect(geographic_location.valid?).to be_truthy
        geographic_location = Fabricate.build(:geographic_location,  longitude: -85.3)
        expect(geographic_location.valid?).to be_truthy
      end
      it 'is valid if longitude is nil' do
        geographic_location = Fabricate.build(:geographic_location,  longitude: nil)
        expect(geographic_location.valid?).to be_truthy
      end
      it 'is valid if longitude is supplied as a blank string, which will be saved as nil' do
        geographic_location = Fabricate.build(:geographic_location,  longitude: '')
        expect(geographic_location.valid?).to be_truthy
        expect(geographic_location.longitude).to be_nil
      end
      it 'is invalid if longitude is a number greater than 180 or less than -180' do
        geographic_location = Fabricate.build(:geographic_location,  longitude: 180.1)
        geographic_location.valid?
        expect(geographic_location.errors).to have_key :longitude
        geographic_location = Fabricate.build(:geographic_location,  longitude: -190)
        geographic_location.valid?
        expect(geographic_location.errors).to have_key :longitude
      end
      it 'is invalid if longitude is non-numeric' do
        geographic_location = Fabricate.build(:geographic_location,  longitude: 'A')
        geographic_location.valid?
        expect(geographic_location.errors).to have_key :longitude
      end
    end
  end
  context 'class methods' do
    describe '#parse_location_string' do
      it 'Takes extracts a trailing latitude and longitude from a comma-separated location string' do
        code, lat, lon =
          GeographicLocation.parse_location_string 'United States, Testsylvannia, Test County, 30.123, 80.456'
        expect(code).to eq 'United States, Testsylvannia, Test County'
        expect(lat).to eq 30.123
        expect(lon).to eq 80.456
      end
      it 'Can deal with negative latitudes or longitudes' do
        code, lat, lon =
          GeographicLocation.parse_location_string 'United States, Testsylvannia, Test County, -30.123, 80.456'
        expect(code).to eq 'United States, Testsylvannia, Test County'
        expect(lat).to eq -30.123
        expect(lon).to eq 80.456

        code, lat, lon =
          GeographicLocation.parse_location_string 'United States, Testsylvannia, Test County, 30.123, -80.456'
        expect(code).to eq 'United States, Testsylvannia, Test County'
        expect(lat).to eq 30.123
        expect(lon).to eq -80.456
      end
      it 'Does not extract anything if there is no latitude and longitude present' do
        code, lat, lon =
          GeographicLocation.parse_location_string 'United States, Testsylvannia, Test County, Test City'
        expect(code).to eq 'United States, Testsylvannia, Test County, Test City'
        expect(lat).to be_nil
        expect(lon).to be_nil

        code, lat, lon =
          GeographicLocation.parse_location_string 'United States, Testsylvannia, Test County, 123-45-6789, 10'
        expect(code).to eq 'United States, Testsylvannia, Test County, 123-45-6789, 10'
        expect(lat).to be_nil
        expect(lon).to be_nil
      end
      it 'Does not extract anything if only one trailing number is present' do
        code, lat, lon =
          GeographicLocation.parse_location_string 'United States, Testsylvannia, Test County, 30.123'
        expect(code).to eq 'United States, Testsylvannia, Test County, 30.123'
        expect(lat).to be_nil
        expect(lon).to be_nil
      end
      it 'Deletes additional trailing numbers if more than two comma-separated trailing numbers are present' do
        code, lat, lon =
          GeographicLocation.parse_location_string 'United States, Testsylvannia, Test, 11.11, 22.22, 33.33, 44.44'
        expect(code).to eq 'United States, Testsylvannia, Test'
        expect(lat).to eq 11.11
        expect(lon).to eq 22.22

        code, lat, lon =
          GeographicLocation.parse_location_string 'United States, Testsylvannia, Test, 11.11, -22.22, 33.33, -44.44'
        expect(code).to eq 'United States, Testsylvannia, Test'
        expect(lat).to eq 11.11
        expect(lon).to eq -22.22
      end
      it 'All works the same even if the place name only has one component (i.e., no commas)' do
        code, lat, lon =
          GeographicLocation.parse_location_string 'Testsylvannia, 30.123, 80.456'
        expect(code).to eq 'Testsylvannia'
        expect(lat).to eq 30.123
        expect(lon).to eq 80.456

        code, lat, lon =
          GeographicLocation.parse_location_string 'Testsylvannia, 30.123, -80.456'
        expect(code).to eq 'Testsylvannia'
        expect(lat).to eq 30.123
        expect(lon).to eq -80.456

        code, lat, lon =
          GeographicLocation.parse_location_string 'Testsylvannia'
        expect(code).to eq 'Testsylvannia'
        expect(lat).to be_nil
        expect(lon).to be_nil

        code, lat, lon =
          GeographicLocation.parse_location_string 'Testsylvannia, 123-45-6789, 10'
        expect(code).to eq 'Testsylvannia, 123-45-6789, 10'
        expect(lat).to be_nil
        expect(lon).to be_nil

        code, lat, lon =
          GeographicLocation.parse_location_string 'Testsylvannia, 30.123'
        expect(code).to eq 'Testsylvannia, 30.123'
        expect(lat).to be_nil
        expect(lon).to be_nil

        code, lat, lon =
          GeographicLocation.parse_location_string 'Testsylvannia, 11.11, -22.22, 33.33, -44.44'
        expect(code).to eq 'Testsylvannia'
        expect(lat).to eq 11.11
        expect(lon).to eq -22.22
      end
    end
  end
end
