require 'rails_helper'

RSpec.describe Event, type: :model do

  it 'has none to begin with' do
    expect(Event.count).to eq 0
  end

  it 'should be valid fabricator' do
    event = Fabricate.build(:valid_event)
    expect(event.valid?).to be_truthy
  end

  it 'should require title' do
    event = Fabricate.build(:valid_event, title: "")
    event.valid?
    expect(event.errors).to have_key :title
  end

  it 'should not be valid with non-unique slug' do
    event_saved = Fabricate(:valid_event)
    event = Fabricate.build(:valid_event)
    event.valid?
    expect(event.errors.map(&:attribute)).to include :slug
  end

  context 'item relation' do
    let(:item)  { Fabricate :crdl_item }
    let(:event) { Fabricate :valid_event }
    it 'connects to event via dcterms_subject match' do
      item.dcterms_subject << "Iron Horse"
      item.save
      event.dcterms_subject_matches << "Iron Horse"
      event.save
      expect(event.items).to include item
    end
    it 'connects to event via dcterms_subject wildcard match' do
      item.dcterms_subject << "Iron Horse--Vandalizing"
      item.save
      event.dcterms_subject_matches << "Iron Horse*"
      event.save
      expect(event.items).to include item
    end
    it 'connects to event via dlg_subject_personal match' do
      item.dlg_subject_personal << "Alma, Gil"
      item.save
      event.dlg_subject_personal_matches << "Alma, Gil"
      event.save
      expect(event.items).to include item
    end
    it 'connects to event via dlg_subject_personal wildcard match' do
      item.dlg_subject_personal << "Alma, Gil--Strange Duck"
      item.save
      event.dlg_subject_personal_matches << "Alma, Gil*"
      event.save
      expect(event.items).to include item
    end
  end

end
