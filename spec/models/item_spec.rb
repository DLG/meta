require 'rails_helper'

RSpec.describe Item, type: :model do
  it 'has none to begin with' do
    expect(Item.count).to eq 0
  end
  it 'has one after adding one' do
    Fabricate :item_with_parents
    expect(Item.count).to eq 1
  end
  context 'Page association' do
    let(:item_with_pages) { Fabricate(:item_with_parents_and_pages) }
    it 'can have Pages' do
      expect(item_with_pages.pages.first).to be_a Page
    end
  end
  it 'should require a Collection' do
    i = Fabricate.build(:item, collection: nil)
    i.valid?
    expect(i.errors).to have_key :collection
  end
  it 'should require a Holding Institution' do
    i = Fabricate.build(:item, holding_institutions: [])
    i.valid?
    expect(i.errors).to have_key :holding_institutions
  end
  it 'should require a dc_date value' do
    i = Fabricate.build(:item, dc_date: [])
    i.valid?
    expect(i.errors).to have_key :dc_date
  end
  it 'should require a dc_date value that is not blank' do
    i = Fabricate.build(:item, dc_date: [''])
    i.valid?
    expect(i.errors).to have_key :dc_date
  end
  it 'should require one of the dcterms_type values to be in a standardized
      set' do
    i = Fabricate.build(:item, dcterms_type: ['Some Random Silly Type'])
    i.valid?
    expect(i.errors).to have_key :dcterms_type
  end
  it 'should not require a dcterms_temporal value' do
    i = Fabricate.build(:item, dcterms_temporal: [])
    i.valid?
    expect(i.errors).not_to have_key :dcterms_temporal
  end
  it 'should require each of the dcterms_temporal values use a limited character
      set' do
    i = Fabricate.build(:item, dcterms_temporal: ['Text'])
    i.valid?
    expect(i.errors).to have_key :dcterms_temporal
  end
  it 'should require a dcterms_title value' do
    i = Fabricate.build(:item, dcterms_title: [])
    i.valid?
    expect(i.errors).to have_key :dcterms_title
  end
  it 'should require a subject in either subject field' do
    i = Fabricate.build(
      :item,
      dcterms_subject: [],
      dlg_subject_personal: []
    )
    i.valid?
    expect(i.errors).to have_key :dcterms_subject
  end
  it 'should not require a dcterms_subject if dlg_subject_personal is
      provided' do
    i = Fabricate.build(
      :item,
      dcterms_subject: [],
      dlg_subject_personal: ['Santa']
    )
    i.valid?
    expect(i.errors).not_to have_key :dcterms_subject
  end
  it 'should not require a dlg_subject_personal if dcterms_subject is
      provided' do
    i = Fabricate.build(
      :item,
      dcterms_subject: ['Santa'],
      dlg_subject_personal: []
    )
    i.valid?
    expect(i.errors).not_to have_key :dlg_subject_personal
  end
  it 'should not be valid if dc_right is empty' do
    i = Fabricate.build(:item, dc_right: [])
    i.valid?
    expect(i.errors).to have_key :dc_right
  end
  it 'should not be valid if dc_right has >1 value' do
    i = Fabricate.build :item, dc_right: %w(a b)
    i.valid?
    expect(i.errors).to have_key :dc_right
    expect(i.errors[:dc_right]).to include I18n.t('activerecord.errors.messages.item_type.dc_right_not_single_valued')
  end
  it 'should not be valid if dc_right is not an approved URI' do
    i = Fabricate.build :item, dc_right: ['http://not.approved.uri']
    i.valid?
    expect(i.errors).to have_key :dc_right
    expect(i.errors[:dc_right]).to include I18n.t('activerecord.errors.messages.dc_right_not_approved')
  end
  it 'should require a value in edm_is_shown_at for non-local item' do
    i = Fabricate.build(:item, edm_is_shown_at: [], local: false)
    i.valid?
    expect(i.errors).to have_key :edm_is_shown_at
  end
  it 'should require a valid URL in edm_is_shown_at' do
    i = Fabricate.build(:item, edm_is_shown_at: ['not/a.url'])
    i.valid?
    expect(i.errors).to have_key :edm_is_shown_at
  end
  it 'should be valid if there is a valid URL in edm_is_shown_at' do
    i = Fabricate.build(
      :item,
      edm_is_shown_at: ['http://dlg.galileo.usg.edu']
    )
    i.valid?
    expect(i.errors).not_to have_key :edm_is_shown_at
  end
  it 'should require a valid URL in edm_is_shown_by' do
    i = Fabricate.build(
      :item,
      edm_is_shown_by: ['not/a.url'],
      local: true
    )
    i.valid?
    expect(i.errors).to have_key :edm_is_shown_by
  end
  it 'should be valid if there is a valid URL in edm_is_shown_by' do
    i = Fabricate.build(
      :item,
      edm_is_shown_by: ['http://dlg.galileo.usg.edu'],
      local: true
    )
    i.valid?
    expect(i.errors).not_to have_key :edm_is_shown_by
  end
  it 'should be valid if there is a no value in edm_is_shown_by but the item is not local' do
    i = Fabricate.build(:item, edm_is_shown_by: [], local: false)
    i.valid?
    expect(i.errors).not_to have_key :edm_is_shown_by
  end
  it 'should only allow valid characters in the slug' do
    invalid1 = Fabricate.build(:item, slug: 'slug/')
    invalid2 = Fabricate.build(:item, slug: 'Slug')
    invalid3 = Fabricate.build(:item, slug: 'sl ug')
    invalid4 = Fabricate.build(:item, slug: 'slug/')
    invalid5 = Fabricate.build(:item, slug: 'slug:')
    valid = Fabricate.build(:item, slug: 'valid-slug')
    invalid1.valid?
    invalid2.valid?
    invalid3.valid?
    invalid4.valid?
    invalid5.valid?
    valid.valid?
    expect(invalid1.errors).to have_key :slug
    expect(invalid2.errors).to have_key :slug
    expect(invalid3.errors).to have_key :slug
    expect(invalid4.errors).to have_key :slug
    expect(invalid5.errors).to have_key :slug
    expect(valid.errors).not_to have_key :slug
  end
  context 'when created' do
    let(:item) { Fabricate :item_with_parents }
    it 'has a scope that returns items updated after a provided date' do
      item.updated_at = '2015-01-01'
      item.save
      i2 = Fabricate(:repository).items.first
      i2.updated_at = '2017-01-01'
      i2.save
      selected_items = Item.updated_since('2016-01-01')
      expect(selected_items).to include i2
      expect(selected_items).not_to include item
    end
    it 'belongs to a Repository' do
      expect(item.repository).to be_kind_of Repository
    end
    it 'belongs to a Collection' do
      expect(item.collection).to be_kind_of Collection
    end
    it 'has a String title' do
      expect(item.title).to be_kind_of String
    end
    it 'has an Array dcterms_title' do
      expect(item.dcterms_title).to be_kind_of Array
    end
    it 'has an Array dlg_subject_personal' do
      expect(item.dcterms_title).to be_kind_of Array
    end
    it 'has a boolean method has_thumbnail' do
      expect(item.has_thumbnail?).to be false
    end
    it 'has a slug' do
      expect(item.slug).not_to be_empty
    end
    it 'has a record_id' do
      expect(item.record_id).not_to be_empty
    end
    it 'has a record_id that tracks changes to collection' do
      expect(item.record_id).to include item.slug
      expect(item.record_id).to include item.collection.slug
      expect(item.record_id).to include item.repository.slug
    end
    it 'has a facet_years value that is an Array of years taken from dc_date' do
      item.dc_date = %w(991 1802 2001 1776-1791 1900/1901)
      expect(item.facet_years).to eq %w(1802 2001 1776 1791 1900 1901)
    end
    it 'returns an array for coordinates' do
      item.geographic_locations << Fabricate(:geographic_location, code: 'United States, Georgia, DeKalb County, Decatur', latitude: 33.7748275, longitude: -84.2963123)
      item.geographic_locations << Fabricate(:geographic_location, code: 'United States, Georgia, Fulton County', latitude:  33.7902836, longitude: -84.466986)
      item.dcterms_spatial = [
        'United States, Georgia, DeKalb County, Decatur, 33.7748275, -84.2963123',
        'United States, Georgia, Fulton County, 33.7902836, -84.466986'
      ]
      expect(item.coordinates).to include('33.7748275, -84.2963123', '33.7902836, -84.466986')
    end
    it 'returns an array containing parseable JSON strings geojson if
        coordinates are found present in the dcterms_spatial field' do
      item.dcterms_spatial = [
        'United States, Georgia, DeKalb County, Decatur, 33.7748275, -84.2963123',
        'United States, Georgia, Fulton County, 33.7902836, -84.466986'
      ]
      item.geojson.each do |g|
        expect(JSON.parse(g)).to be_a Hash
      end
    end
    context 'ensures scoped uniqueness of slug' do
      it 'by disallowing the creation of two items with the same slug related to
          the same collection' do
        i2 = Fabricate(:repository).items.first
        i2.collection = item.collection
        i2.slug = item.slug
        expect { i2.save! }.to raise_exception ActiveRecord::RecordInvalid
        expect(i2.errors).to include :slug
      end
      it 'when creating two items with the same slug related to different
          collection' do
        i2 = Fabricate(:repository).items.first
        i2.repository = item.repository
        expect { i2.save! }.not_to raise_exception
      end
    end
    context 'has other_* values' do
      let(:collection2) { Fabricate :empty_collection }
      let(:collection3) { Fabricate :empty_collection }
      before :each do
        item.other_collections = [collection2.id, collection3.id]
      end
      it 'that includes the titles of other_collections' do
        expect(item.collection_titles).to include item.collection.title
        expect(item.collection_titles).to include collection2.title
        expect(item.collection_titles).to include collection3.title
      end
      it 'that includes the repository titles associated with other_collections' do
        expect(item.repository_titles).to include item.repository.title
        expect(item.repository_titles).to include collection2.repository.title
        expect(item.repository_titles).to include collection3.repository.title
      end
    end
    context 'has validation status' do
      it 'of true if the item is valid' do
        item.valid?
        expect(item.valid_item).to be true
      end
      it 'of false if the item is invalid' do
        i1 = Fabricate.build :item
        i1.valid?
        expect(i1.valid_item).to be false
      end
    end
    context 'has a counties method' do
      before :each do
        item.geographic_locations << Fabricate(:geographic_location, component_placenames: ['United States', 'Georgia'])
        item.geographic_locations << Fabricate(:geographic_location, component_placenames: ['United States', 'Georgia', 'Muscogee County', 'Columbus'])
        item.geographic_locations << Fabricate(:geographic_location, component_placenames: ['United States', 'Georgia', 'Jeff Davis County', 'Test'])
      end
      it 'that extracts Georgia county values from dcterms_spatial' do
        expect(item.counties).to eq ['Muscogee', 'Jeff Davis']
      end
    end
    context 'has a display value' do
      context 'for a non-public Repository' do
        context 'and a non-public Collection' do
          context 'and a non-public Item' do
            it 'that returns false' do
              expect(item.display?).to eq false
            end
          end
          context 'and a public Item' do
            it 'that returns true' do
              item.public = true
              expect(item.display?).to eq false
            end
          end
        end
        context 'and a public Collection' do
          before :each do
            item.collection.public = true
          end
          context 'and a non-public Item' do
            it 'returns false' do
              expect(item.display?).to eq false
            end
          end
          context 'and a public Item' do
            it 'returns true' do
              item.public = true
              expect(item.display?).to eq false
            end
          end
        end
      end
      context 'for a public Repository' do
        before :each do
          item.repository.public = true
        end
        context 'and a non-public Collection' do
          context 'and a non-public Item' do
            it 'returns false' do
              expect(item.display?).to eq false
            end
          end
          context 'and a public Item' do
            it 'returns false' do
              item.public = true
              expect(item.display?).to eq false
            end
          end
        end
        context 'and a public Collection' do
          context 'and a non-public Item' do
            it 'returns false' do
              expect(item.display?).to eq false
            end
          end
          context 'and a public Item' do
            it 'returns true' do
              item.public = true
              expect(item.display?).to eq false
            end
          end
        end
      end
    end
    context 'fulltext handling' do
      let(:item) { Fabricate :item_with_parents_and_pages }
      it 'has a string for full text' do
        expect(item.fulltext).to be_kind_of String
      end
    end
  end
  context 'iiif_manifest_url' do
    it 'equals iiif_partner_url when that field is present and iiif_partner_url_enabled is true' do
      item = Fabricate(:item_with_parents_and_pages, iiif_partner_url: 'http://example.com', iiif_partner_url_enabled: true)
      expect(item.iiif_manifest_url).to eq item.iiif_partner_url
    end

    it 'does not equals iiif_partner_url when that field is present and iiif_partner_url_enabled is false' do
      item = Fabricate(:item_with_parents_and_pages, iiif_partner_url: 'http://example.com', iiif_partner_url_enabled: false)
      expect(item.iiif_manifest_url).to_not eq item.iiif_partner_url
    end

    it 'has a generated url when the item has pages and not using iiif_partner_url' do
      item = Fabricate(:item_with_parents_and_pages, iiif_partner_url: 'http://example.com', iiif_partner_url_enabled: false)
      expect(item.iiif_manifest_url).to include("/record/#{item.record_id}/presentation/manifest.json")
    end

    it 'is an empty string when it does not have page or iiif_partner_url_enabled is false' do
      item = Fabricate(:item_with_parents, iiif_partner_url: 'http://example.com', iiif_partner_url_enabled: false) do
        pages(count: 0)
      end
      expect(item.iiif_manifest_url).to be_nil
    end
  end

  context 'event relation' do
    let(:item)  { Fabricate :crdl_item }
    let(:event) { Fabricate :valid_event }
    it 'connects to event via dcterms_subject match' do
      event.dcterms_subject_matches << "Iron Horse"
      event.save
      item.dcterms_subject << "Iron Horse"
      item.save
      expect(item.events).to include event
    end
    it 'connects to event via dcterms_subject wildcard match' do
      event.dcterms_subject_matches << "Iron Horse*"
      event.save
      item.dcterms_subject << "Iron Horse--Vandalizing"
      item.save
      expect(item.events).to include event
    end
    it 'connects to event via dlg_subject_personal match' do
      event.dlg_subject_personal_matches << "Alma, Gil"
      event.save
      item.dlg_subject_personal << "Alma, Gil"
      item.save
      expect(item.events).to include event
    end
    it 'connects to event via dlg_subject_personal wildcard match' do
      event.dlg_subject_personal_matches << "Alma, Gil*"
      event.save
      item.dlg_subject_personal << "Alma, Gil--Strange Duck"
      item.save
      expect(item.events).to include event
    end
  end

  context 'name relation' do
    let(:item) { Fabricate :item_with_parents }
    let(:name) { Fabricate :valid_name }
    it 'connects to name via dlg_subject_personal/authoritative_name match after save' do
      item.dlg_subject_personal << name.authoritative_name
      item.save
      expect(item.names).to include name
    end
    it 'connects to name when dlg_subject_person has double hyphen suffix' do
      name.authoritative_name = "Bell, Mary Kate"
      name.save
      item.dlg_subject_personal << "Bell, Mary Kate--Imprisonment"
      item.save
      expect(item.names).to include name
    end
    it 'connects to name when dlg_subject_person has double hyphen suffix with space preceding' do
      name.authoritative_name = "Briggs, Fred, 1932-"
      name.save
      item.dlg_subject_personal << "Briggs, Fred, 1932- --Trials, litigation, etc."
      item.save
      expect(item.names).to include name
    end
  end

  context 'with serial' do
    it 'inherits values when blank/empty' do
        item = Fabricate(:item_with_serial, dcterms_publisher: [], dcterms_description: [], dcterms_subject:[],
                         dcterms_medium: [], dcterms_contributor: [], dcterms_creator: [], dc_right: [],
                         dlg_subject_personal: [],dcterms_language: [], dcterms_type: [], geographic_locations:[] )
        expect(item.dcterms_publisher).to include(*item.serial.dcterms_publisher)
        expect(item.dcterms_description).to include(*item.serial.dcterms_description)
        expect(item.dcterms_subject).to include(*item.serial.dcterms_subject)
        expect(item.dcterms_medium).to include(*item.serial.dcterms_medium)
        expect(item.dcterms_contributor).to include(*item.serial.dcterms_contributor)
        expect(item.dcterms_creator).to include(*item.serial.dcterms_creator)
        expect(item.dc_right).to include(*item.serial.dc_right)
        expect(item.dlg_subject_personal).to include(*item.serial.dlg_subject_personal)
        expect(item.dcterms_language).to include(*item.serial.dcterms_language)
        expect(item.dcterms_type).to include(*item.serial.dcterms_type)
        expect(item.geographic_locations).to include(*item.serial.geographic_locations)
    end
  end

end
