require 'rails_helper'

RSpec.describe BatchItem, type: :model do
  it 'has none to begin with' do
    expect(BatchItem.count).to eq 0
  end
  it 'has some after creating a batch' do
    Fabricate(:batch_item)
    expect(BatchItem.count).to be 1
  end
  it 'should require a Collection' do
    i = Fabricate.build(:batch_item)
    i.collection = nil
    i.valid?
    expect(i.errors).to have_key :collection
  end
  it 'should require a dc_date value' do
    i = Fabricate.build(:batch_item, dc_date: [])
    i.valid?
    expect(i.errors).to have_key :dc_date
  end
  it 'should require one of the dcterms_type values to be in a standardized set' do
    i = Fabricate.build(:batch_item, dcterms_type: ['Some Random Silly Type'])
    i.valid?
    expect(i.errors).to have_key :dcterms_type
  end
  it 'should require each of the dcterms_temporal values use a limited character set' do
    i = Fabricate.build(:batch_item, dcterms_temporal: ['Text'])
    i.valid?
    expect(i.errors).to have_key :dcterms_temporal
  end
  context 'when created' do
    let(:batch_item) { Fabricate :batch_item }
    it 'has a Batch' do
      expect(batch_item.batch).to be_kind_of Batch
    end
    it 'has a String title' do
      expect(batch_item.title).to be_kind_of String
    end
    it 'has a slug' do
      expect(batch_item.slug).not_to be_empty
    end
    it 'has a string for full text' do
      expect(batch_item.fulltext).to be_kind_of String
    end
    it 'is not an Item' do
      expect(batch_item).not_to be_kind_of Item
    end
    context 'has a commit method' do
      it 'creates an Item copy of itself using commit' do
        i = batch_item.commit
        expect(i).to be_an Item
      end
      it 'replaces an existing Item with its attributes using commit' do
        i = Fabricate(:repository).items.first
        batch_item.item = i
        batch_item.collection = i.collection
        batch_item.portals = i.portals
        ni = batch_item.commit
        expect(ni).to be_an Item
        expect(ni.slug).to eq batch_item.slug
      end
      it 'raises a validation exception when the commit result is saved' do
        Fabricate :repository
        batch_item.collection = Collection.first
        ni = batch_item.commit
        expect { ni.save! }.to raise_error ActiveRecord::RecordInvalid
      end
    end
    context 'has previous and next methods' do
      let(:batch) do
        batch = Fabricate(:batch) { batch_items(count: 3) }
        batch.batch_items = Fabricate.times(3, :batch_item, batch_id: batch.id)
        batch
      end
      it 'returns the next batch_item in a batch ordered by id' do
        n = batch.batch_items.first.next
        expect(n).to eq batch.batch_items[1]
      end
      it 'returns the previous batch_item in a batch ordered by id' do
        p = batch.batch_items.last.previous
        expect(p).to eq batch.batch_items[1]
      end
      it 'returns nil if there is no previous item' do
        p = batch.batch_items.first.previous
        expect(p).to eq nil
      end
      it 'returns nil if there is no next item' do
        n = batch.batch_items.last.next
        expect(n).to eq nil
      end
    end
  end
end
