require 'rails_helper'

RSpec.describe IndexDates, type: :model do
  context 'index years' do
    context 'single year dates' do
      it 'parses MM_DD_YYYY' do
        expect(IndexDates.index_years(%w[10-1-1999 10-12-1977])).to eq %w[1999 1977]
        expect(IndexDates.index_years(%w[1-1-2001 1-01-2002 01-1-2003 01-01-2004])).to eq %w[2001 2002 2003 2004]
        expect(IndexDates.index_years(%w[1/1/2001 1/01/2002 01/1/2003 01/01/2004])).to eq %w[2001 2002 2003 2004]
      end
      it 'parses MM_YYYY' do
        expect(IndexDates.index_years(["03-1957"])).to eq ["1957"]
        expect(IndexDates.index_years(%w[1-2001 01-2002])).to eq %w[2001 2002]
        expect(IndexDates.index_years(%w[1/2001 01/2002])).to eq %w[2001 2002]
      end
      it 'parses MM_DD_YY' do  # we never assume YY_MM_DD
        expect(IndexDates.index_years(%w[2-6-97 3-18-01])).to eq %w[1997 2001]
        expect(IndexDates.index_years(%w[1-1-09 1-1-10])).to eq %w[2009 1910] # see IndexDates::TWO_DIGIT_CUTOFF (10)
        expect(IndexDates.index_years(%w[1-1-01 1-01-02 01-1-03 01-01-04])).to eq %w[2001 2002 2003 2004]
        expect(IndexDates.index_years(%w[1/1/01 1/01/02 01/1/03 01/01/04])).to eq %w[2001 2002 2003 2004]
      end
      it 'parses YYYY' do
        expect(IndexDates.index_years(%w[1995 2014])).to eq %w[1995 2014]
        expect(IndexDates.index_years(%w[123 1300 4668])).to eq %w[0123 1300 4668]
      end
      it 'parses YYYY_MM_DD' do
        expect(IndexDates.index_years(%w[1999-02-05 2005-07-19 1993/01/06])).to eq %w[1999 2005 1993]
        expect(IndexDates.index_years(%w[2001-1-1 2002-1-01 2003-01-1 2004-01-01])).to eq %w[2001 2002 2003 2004]
        expect(IndexDates.index_years(%w[2001/1/1 2002/1/01 2003/01/1 2004/01/01])).to eq %w[2001 2002 2003 2004]
      end
      it 'parses YYYY_MM' do
        expect(IndexDates.index_years(%w[1999/12 2000-01])).to eq %w[1999 2000]
        expect(IndexDates.index_years(%w[2001-1 2002-01])).to eq %w[2001 2002]
        expect(IndexDates.index_years(%w[2001/1 2002/01])).to eq %w[2001 2002]
      end
      it 'parses DD_MON_YY' do
        expect(IndexDates.index_years(%w[1-Jan-09 31-Dec-10])).to eq %w[2009 1910] # see IndexDates::TWO_DIGIT_CUTOFF (10)
        expect(IndexDates.index_years(%w[02-Apr-03 24-May-05 31-May-07 16-May-15 17-May-27 16-May-50])).to eq %w[2003 2005 2007 1915 1927 1950]
      end
    end

    context 'year range dates' do
      now_plus_10 = IndexDates.current_year_plus_10_years.to_s
      it 'parses YYYY_YYYY and checks for max range' do
        expect(IndexDates.index_years(["1500/9999"])).to eq ("1500" .. now_plus_10).to_a
        expect(IndexDates.index_years(["0001/9999"])).to eq ("0001" .. now_plus_10).to_a
      end
      it 'parses YYYY_YYYY' do
        expect(IndexDates.index_years(["1300/9999"])).to eq ("1300" .. now_plus_10).to_a
        expect(IndexDates.index_years(["1300/1305"])).to eq %w[1300 1301 1302 1303 1304 1305]
        expect(IndexDates.index_years(["1635/1640"])).to eq %w[1635 1636 1637 1638 1639 1640]
        expect(IndexDates.index_years(%w[2001/2003 2005-2006])).to eq %w[2001 2002 2003 2005 2006]
      end
      it 'parses YYY_YYY' do
        expect(IndexDates.index_years(["500/9999"])).to eq ("0500" .. now_plus_10).to_a
        expect(IndexDates.index_years(["500/505"])).to eq %w[0500 0501 0502 0503 0504 0505]
        expect(IndexDates.index_years(["995/1000"])).to eq %w[0995 0996 0997 0998 0999 1000]
        expect(IndexDates.index_years(%w[995/999 2005-2006])).to eq %w[0995 0996 0997 0998 0999 2005 2006]
        expect(IndexDates.index_years(%w[995/1000 2005-2006])).to eq %w[0995 0996 0997 0998 0999 1000 2005 2006]
      end
      it 'parses YYYY_MM_YYYY_MM' do
        expect(IndexDates.index_years(["1823-01/1827-03"])).to eq %w[1823 1824 1825 1826 1827]
        expect(IndexDates.index_years(["2007/11/2003/12"])).to eq %w[2003 2004 2005 2006 2007]
        expect(IndexDates.index_years(%w[2001/01/2003/01 2001-01/2003-01])).to eq %w[2001 2002 2003]
        expect(IndexDates.index_years(%w[2001/1/2003/01 2005-1/2006-01])).to eq %w[2001 2002 2003 2005 2006]
        expect(IndexDates.index_years(%w[2001/01/2003/1 2005-01/2006-1])).to eq %w[2001 2002 2003 2005 2006]
        expect(IndexDates.index_years(%w[2001/1/2003/1 2005-1/2006-1])).to eq %w[2001 2002 2003 2005 2006]
      end
      it 'parses YYYY_MM_YYYY' do
        expect(IndexDates.index_years(["1854-04/1856"])).to eq %w[1854 1855 1856]
        expect(IndexDates.index_years(["1982-09/1983"])).to eq %w[1982 1983]
        expect(IndexDates.index_years(%w[2001/01/2003 2005-01/2006])).to eq %w[2001 2002 2003 2005 2006]
        expect(IndexDates.index_years(%w[2001/1/2003 2005-1/2006])).to eq %w[2001 2002 2003 2005 2006]
      end
      it 'parses YYYY_YYYY_MM' do
        expect(IndexDates.index_years(["1863/1864-04"])).to eq %w[1863 1864]
        expect(IndexDates.index_years(["1980/1982-12"])).to eq %w[1980 1981 1982]
        expect(IndexDates.index_years(%w[2001/2003/01 2005/2006-01])).to eq %w[2001 2002 2003 2005 2006]
        expect(IndexDates.index_years(%w[2001/2003/1 2005/2006-1])).to eq %w[2001 2002 2003 2005 2006]
      end
      it 'parses YYYY_MM_DD_YYYY_MM_DD' do
        expect(IndexDates.index_years(["1786-02-12/1788-08-24"])).to eq %w[1786 1787 1788]
        expect(IndexDates.index_years(%w[2001/01/01/2003/01/01 2005-01-01/2006-01-01])).to eq %w[2001 2002 2003 2005 2006]
        expect(IndexDates.index_years(%w[2001/1/01/2003/01/1 2005-1-01/2006-01-1])).to eq %w[2001 2002 2003 2005 2006]
      end
      it 'parses YYYY_MM_YYYY_MM_DD' do
        expect(IndexDates.index_years(["1981-01/1980-02-24"])).to eq %w[1980 1981]
        expect(IndexDates.index_years(%w[2001/01/2003/01/01 2005-01/2006-01-01])).to eq %w[2001 2002 2003 2005 2006]
        expect(IndexDates.index_years(%w[2001/1/2003/01/1 2005-1/2006-01-1])).to eq %w[2001 2002 2003 2005 2006]
      end
      it 'parses YYYY_MM_DD_YYYY_MM' do
        expect(IndexDates.index_years(["1861-05-01/1863-09"])).to eq %w[1861 1862 1863]
        expect(IndexDates.index_years(%w[2001/01/01/2003/01 2005-01-01/2006-01])).to eq %w[2001 2002 2003 2005 2006]
        expect(IndexDates.index_years(%w[2001/1/01/2003/01 2005-1-01/2006-01])).to eq %w[2001 2002 2003 2005 2006]
      end
      it 'parses YYYY_YYYY_MM_DD' do
        expect(IndexDates.index_years(["1976/1978-02-22"])).to eq %w[1976 1977 1978]
        expect(IndexDates.index_years(%w[2001/2003/01/01 2005/2006-01-01])).to eq %w[2001 2002 2003 2005 2006]
        expect(IndexDates.index_years(%w[2001/2003/01/1 2005/2006-01-1])).to eq %w[2001 2002 2003 2005 2006]
      end
      it 'parses YYYY_MM_DD_YYYY' do
        expect(IndexDates.index_years(["1970-06-27/1972"])).to eq %w[1970 1971 1972]
        expect(IndexDates.index_years(%w[2001/01/01/2003 2005-01-01/2006])).to eq %w[2001 2002 2003 2005 2006]
        expect(IndexDates.index_years(%w[2001/1/01/2003 2005-1-01/2006])).to eq %w[2001 2002 2003 2005 2006]
      end
      it 'parses YYYY_YYYY_YYYY' do
        expect(IndexDates.index_years(["1900; 1901; 1902; 1903;"])).to eq %w[1900 1901 1902 1903]
      end
    end

    context 'circa year dates' do
      it 'parses CIRCA_YYYY' do
        expect(IndexDates.index_years(["ca. 1863"])).to eq %w[1861 1862 1863 1864 1865]
        expect(IndexDates.index_years(["ca. 1900"])).to eq %w[1898 1899 1900 1901 1902]
        expect(IndexDates.index_years(["ca. 1910"])).to eq %w[1908 1909 1910 1911 1912]
      end
    end

    context 'last ditch year dates' do
      it 'parses LAST_DITCH_YYYY' do
        expect(IndexDates.index_years(["2001-01-20-"])).to eq ["2001"]
        expect(IndexDates.index_years(["2002-05-05-"])).to eq ["2002"]
        expect(IndexDates.index_years(["2005-02-015-"])).to eq ["2005"]
        expect(IndexDates.index_years(["2006/07/2016//09"])).to eq ["2006"]
      end
    end
  end

  context 'index yyyy_mm_dd for sort' do
    context 'simple dates' do
      it 'returns yyyy-mm-dd given yyyy-mm-dd' do
        expect(IndexDates.index_yyyy_mm_dd_sort(['1900-12-31'])).to eq '1900-12-31'
      end
      it 'returns yyyy-mm-01 given yyyy-mm' do
        expect(IndexDates.index_yyyy_mm_dd_sort(['1910-11'])).to eq '1910-11-01'
      end
      it 'returns yyyy-01-01 given yyyy' do
        expect(IndexDates.index_yyyy_mm_dd_sort(['1920'])).to eq '1920-01-01'
      end
      it 'returns yyyy-01-01 given yyyy-yyyy range' do
        expect(IndexDates.index_yyyy_mm_dd_sort(['1930-1940'])).to eq '1930-01-01'
      end
    end

    context 'year_month_day_match' do
      it 'parses YYYY' do
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[1995 2014])).to eq "1995-01-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[123 1300 4668])).to eq "0123-01-01"
      end
      it 'parses YYYY_MM_DD' do
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[1999-02-05 2005-07-19])).to eq "1999-02-05"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001-2-5 2002-1-01])).to eq "2001-02-05"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/5/7 2002/1/01])).to eq "2001-05-07"
      end
      it 'parses YYYY_MM' do
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[1999/12 2000-01])).to eq "1999-12-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001-4 2002-01])).to eq "2001-04-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/11 2002/01])).to eq "2001-11-01"
      end
      it 'parses YYYY_YYYY' do
        expect(IndexDates.index_yyyy_mm_dd_sort(["1300/9999"])).to eq "1300-01-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(["1300/1305"])).to eq "1300-01-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(["1635/1640"])).to eq "1635-01-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/2003 2005-2006])).to eq "2001-01-01"
      end
      it 'parses YYYY_MM_YYYY_MM' do
        expect(IndexDates.index_yyyy_mm_dd_sort(["1823-01/1827-03"])).to eq "1823-01-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(["2007/11/2003/12"])).to eq "2007-11-01"  # do we care (i.e., about a bad range)?
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/02/2003/02 2001-01/2003-01])).to eq "2001-02-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/2/2003/01 2005-1/2006-01])).to eq "2001-02-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/02/2003/1 2005-01/2006-1])).to eq "2001-02-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/2/2003/1 2005-1/2006-1])).to eq "2001-02-01"
      end
      it 'parses YYYY_MM_YYYY' do
        expect(IndexDates.index_yyyy_mm_dd_sort(["1854-04/1856"])).to eq "1854-04-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(["1982-09/1983"])).to eq "1982-09-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/02/2003 2005-01/2006])).to eq "2001-02-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/2/2003 2005-1/2006])).to eq "2001-02-01"
      end
      it 'parses YYYY_YYYY_MM' do
        expect(IndexDates.index_yyyy_mm_dd_sort(["1863/1864-04"])).to eq "1863-01-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(["1980/1982-12"])).to eq "1980-01-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/2003/01 2005/2006-01])).to eq "2001-01-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/2003/1 2005/2006-1])).to eq "2001-01-01"
      end
      it 'parses YYYY_MM_DD_YYYY_MM_DD' do
        expect(IndexDates.index_yyyy_mm_dd_sort(["1786-02-12/1788-08-24"])).to eq "1786-02-12"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/02/02/2003/01/01 2005-01-01/2006-01-01])).to eq "2001-02-02"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/2/02/2003/01/1 2005-1-01/2006-01-1])).to eq "2001-02-02"
      end
      it 'parses YYYY_MM_YYYY_MM_DD' do
        expect(IndexDates.index_yyyy_mm_dd_sort(["1981-01/1980-02-24"])).to eq "1981-01-01"  # bad range--simple answer
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/02/2003/01/01 2005-01/2006-01-01])).to eq "2001-02-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/2/2003/01/1 2005-1/2006-01-1])).to eq "2001-02-01"
      end
      it 'parses YYYY_MM_DD_YYYY_MM' do
        expect(IndexDates.index_yyyy_mm_dd_sort(["1861-05-01/1863-09"])).to eq "1861-05-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/02/02/2003/01 2005-01-01/2006-01])).to eq "2001-02-02"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/2/02/2003/01 2005-1-01/2006-01])).to eq "2001-02-02"
      end
      it 'parses YYYY_YYYY_MM_DD' do
        expect(IndexDates.index_yyyy_mm_dd_sort(["1976/1978-02-22"])).to eq "1976-01-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/2003/01/01 2005/2006-01-01])).to eq "2001-01-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/2003/01/1 2005/2006-01-1])).to eq "2001-01-01"
      end
      it 'parses YYYY_MM_DD_YYYY' do
        expect(IndexDates.index_yyyy_mm_dd_sort(["1970-06-27/1972"])).to eq "1970-06-27"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/02/02/2003 2005-01-01/2006])).to eq "2001-02-02"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2001/2/02/2003 2005-1-01/2006])).to eq "2001-02-02"
      end
      it 'parses YYYY_YYYY_YYYY' do
        expect(IndexDates.index_yyyy_mm_dd_sort(["1900; 1901; 1902; 1903;"])).to eq "1900-01-01"
      end
    end

    context 'month_day_year_match' do
      it 'parses MM_DD_YYYY' do
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[10-1-1999 10-12-1977])).to eq "1999-10-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2-2-2001 1-01-2002])).to eq "2001-02-02"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2/2/2001 1/01/2002])).to eq "2001-02-02"
      end
      it 'parses MM_DD_YY' do  # we never assume YY_MM_DD
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[12-6-97 3-18-01])).to eq "1997-12-06"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[3-2-09 1-1-10])).to eq "2009-03-02"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[10-2-01 1-01-02])).to eq "2001-10-02"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[9/2/01 1/01/02])).to eq "2001-09-02"
      end
    end

    context 'month_year_match' do
      it 'parses MM_YYYY' do
        expect(IndexDates.index_yyyy_mm_dd_sort(["03-1957"])).to eq "1957-03-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2-2001 01-2002])).to eq "2001-02-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[2/2001 01/2002])).to eq "2001-02-01"
      end
    end

    context 'day_mon_year_match' do
      it 'parses DD_MON_YY' do
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[5-Jan-09 31-Dec-10])).to eq "2009-01-05"
        expect(IndexDates.index_yyyy_mm_dd_sort(%w[02-Apr-03 24-May-05])).to eq "2003-04-02"
      end
    end

    # for last ditch, we only match year-ish
    context 'last_ditch_ymd_match' do
      it 'parses LAST_DITCH_YYYY' do
        expect(IndexDates.index_yyyy_mm_dd_sort(["2001-01-20-"])).to eq "2001-01-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(["2002-05-05-"])).to eq "2002-01-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(["2005-02-015-"])).to eq "2005-01-01"
        expect(IndexDates.index_yyyy_mm_dd_sort(["2006/07/2016//09"])).to eq "2006-01-01"
      end
    end
  end

  context 'older specs from IndexDates' do
    it 'returns an array of years given an array of values containing legit single
        dates as strings' do
      dc_dates = %w(1732-02-03 1921-02-03)
      expect(IndexDates.index_years(dc_dates)).to eq %w(1732 1921)
    end
    it 'returns an array of years given an array of values containing nonsense
        days that do not exist' do
      dc_dates = %w(1732-02-31 1921-03-32)
      expect(IndexDates.index_years(dc_dates)).to eq %w(1732 1921)
    end
    it 'returns an array of years given an array of values containing legit single
        dates as strings' do
      dc_dates = %w(garbage 3000 0123)
      expect(IndexDates.index_years(dc_dates)).to eq %w(3000 0123)
    end
    it 'returns an array of years given an array of values containing dates with
        some common errors' do
      dc_dates = %w(1999-00-00 2001--07-09 2004-03-00)
      expect(IndexDates.index_years(dc_dates)).to eq %w(1999 2001 2004)
    end
    it 'returns an array of years given an array of values containing dates in the
        ugly format' do
      dc_dates = %w(02/03/1732 2/3/1776 11/25/1916 9/15/2016)
      expect(IndexDates.index_years(dc_dates)).to(
        eq %w(1732 1776 1916 2016)
      )
    end
    it 'returns an array of years given an array of values containing an ugly date
        range and some text' do
      dc_dates = ['circa 1999-2001']
      expect(IndexDates.index_years(dc_dates)).to eq %w(1999)  # last-ditch
    end
    it 'returns an array of years given an array of values containing an normal
        date range' do
      dc_dates = ['1-1-1983/1-1-1990']
      expect(IndexDates.index_years(dc_dates)).to(
        eq %w(1983)  # we no longer have M_D_Y/M_D_Y ranges
      )
    end
    it 'returns an array of years given an array of values containing an normal
        date range without days' do
      dc_dates = ['1-1983/7-1990']
      expect(IndexDates.index_years(dc_dates)).to(
        eq %w(1983)  # we no longer have M_Y/M_Y ranges
      )
    end
    it 'returns an array of years given an array of values containing an normal
        date range without days or months' do
      dc_dates = ['1983/1990']
      expect(IndexDates.index_years(dc_dates)).to(
        eq %w(1983 1984 1985 1986 1987 1988 1989 1990)
      )
    end
    it 'returns an array of years given an array of values containing an ugly date
        range without days or months' do
      dc_dates = ['1983-1990']
      expect(IndexDates.index_years(dc_dates)).to(
        eq %w(1983 1984 1985 1986 1987 1988 1989 1990)
      )
    end
  end

end
