require 'rails_helper'

RSpec.describe Name, type: :model do

  it 'has none to begin with' do
    expect(Name.count).to eq 0
  end

  it 'should be valid fabricator' do
    name = Fabricate.build(:valid_name)
    expect(name.valid?).to be_truthy
  end

  it 'should require portals' do
    name = Fabricate.build(:valid_name, portals: [])
    name.valid?
    expect(name.errors).to have_key :portals
  end

  it 'should require a authoritative_name' do
    name = Fabricate.build(:valid_name, authoritative_name: nil)
    name.valid?
    expect(name.errors).to have_key :authoritative_name
  end

  it 'should require a name_type' do
    name = Fabricate.build(:valid_name, name_type: nil)
    name.valid?
    expect(name.errors).to have_key :name_type
  end

  it 'should not be valid with and invalid name_type' do
    name = Fabricate.build(:valid_name, name_type: "bogus_name_type")
    name.valid?
    expect(name.errors).to have_key :name_type
  end

  it 'should not be valid with non-unique slug/legacy_slug' do
    name_saved = Fabricate(:valid_name)
    name = Fabricate.build(:valid_name)
    name.valid?
    expect(name.errors.map(&:attribute)).to include(:slug, :legacy_slug)
  end

  it 'should create a slug from authoritative_name' do
    name = Fabricate.build(:valid_name, slug: nil, authoritative_name: "  Jim \"Bubba\" Garner  ")
    expect(name.valid?).to be_truthy
    expect(name.slug).to eql("jim_bubba_garner")
  end

  it 'should add slug to additional_slugs' do
    name = Fabricate.build(:valid_name)
    old_slug = name.slug
    name.authoritative_name = "New Name"
    expect(name.valid?).to be_truthy  # validation triggers update_slugs
    expect(name.slug).to eq("new_name")
    expect(name.additional_slugs).to include(old_slug)
  end

  it 'exercises normalized' do
    expect(Name.normalized(" ")).to eq("")
    expect(Name.normalized("-")).to eq("")
    expect(Name.normalized("NAME")).to eq("name")
    expect(Name.normalized("  NAME  ")).to eq("name")
    expect(Name.normalized("First Last")).to eq("first_last")
    expect(Name.normalized("Brezhnev, Leonid Ilʹich, 1906-1982")).to eq("brezhnev_leonid_il_ich_1906_1982")
  end

  it 'checks if slug is in other additional_slugs' do
    name_saved = Fabricate(:valid_name)
    name_saved.authoritative_name = "New Name"
    name_saved.save  # saved so name will collide

    name = Fabricate.build(:valid_name)
    name.slug_in_additional_slugs
    expect(name.errors).to have_key :slug
  end

  it 'checks if slug is in other legacy_slugs' do
    name_saved = Fabricate(:valid_name)
    name_saved.update_column('legacy_slug', name_saved.slug)
    name_saved.authoritative_name = "New Name"  # makes new slug
    name_saved.save  # saved so name will collide

    name = Fabricate.build(:valid_name)
    name.slug_in_legacy_slug
    expect(name.errors).to have_key :slug
  end

  it 'checks if any additional_slugs matches another slug' do
    name_saved = Fabricate(:valid_name)
    name_saved.authoritative_name = "New Name"  # makes new slug
    name_saved.save  # saved so name will collide

    name = Fabricate.build(:valid_name)
    name.additional_slugs = ['new_name']
    name.additional_slugs_in_slug
    expect(name.errors).to have_key :additional_slugs
  end

  it 'checks if any additional_slugs is in other additional_slugs' do
    name_saved = Fabricate(:valid_name)
    name_saved.additional_slugs = [name_saved.slug]
    name_saved.save  # saved so name will collide

    name = Fabricate.build(:valid_name)
    name.additional_slugs = [name.slug]
    name.additional_slugs_in_additional_slugs
    expect(name.errors).to have_key :additional_slugs
  end

  it 'checks if any additional_slugs is matches another legacy_slug' do
    name_saved = Fabricate(:valid_name)
    name_saved.legacy_slug = 'LEGACY_SLUG'
    name_saved.save  # saved so name will collide

    name = Fabricate.build(:valid_name)
    name.additional_slugs = ['LEGACY_SLUG']
    name.additional_slugs_in_legacy_slug
    expect(name.errors).to have_key :additional_slugs
  end

  it 'checks if legacy_slug matches any other slug' do
    name_saved = Fabricate(:valid_name)
    name_saved.authoritative_name = "New Name"  # makes new slug
    name_saved.save  # saved so name will collide

    name = Fabricate.build(:valid_name)
    name.legacy_slug = "new_name"  # slug: 'new_name'
    name.legacy_slug_in_slug
    expect(name.errors).to have_key :legacy_slug
  end

  it 'checks if legacy_slug is in other additional_slugs' do
    name_saved = Fabricate(:valid_name)
    name_saved.additional_slugs = [name_saved.slug]
    name_saved.authoritative_name = "New Name"  # makes new slug
    name_saved.save  # saved so name will collide

    name = Fabricate.build(:valid_name)
    name.legacy_slug = name.slug
    name.legacy_slug_in_additional_slugs
    expect(name.errors).to have_key :legacy_slug
  end

  it 'checks if legacy_slug is in this record\'s additional_slugs' do
    name = Fabricate.build(:valid_name)
    name.additional_slugs = ['LEGACY_SLUG']
    name.legacy_slug = 'LEGACY_SLUG'
    name.legacy_slug_in_these_additional_slugs
    expect(name.errors).to have_key :additional_slugs
  end

  context 'item relation' do
    let(:item) { Fabricate :item_with_parents }
    let(:name) { Fabricate :valid_name }
    it 'connects to item via dlg_subject_personal/authoritative_name match after save' do
      item.dlg_subject_personal << "Bill Blass"
      item.save
      name.authoritative_name = "Bill Blass"
      name.save
      expect(name.items).to include item
    end
    it 'connects to item when dlg_subject_person has double hyphen suffix' do
      item.dlg_subject_personal << "Bell, Mary Kate--Imprisonment"
      item.save
      name.authoritative_name = "Bell, Mary Kate"
      name.save
      expect(name.items).to include item
    end
    it 'connects to item when dlg_subject_person has double hyphen suffix with space preceding' do
      item.dlg_subject_personal << "Briggs, Fred, 1932- --Trials, litigation, etc."
      item.save
      name.authoritative_name = "Briggs, Fred, 1932-"
      name.save
      expect(name.items).to include item
    end
  end

end
