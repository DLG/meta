require 'rails_helper'

RSpec.describe 'API V2 for Features', type: :request do
  headers = { 'X-User-Token' => Rails.application.credentials.api_token }
  context 'can list using #index' do
    before(:each) do
      portals = Fabricate.times(1, :portal, code: 'georgia')
      Fabricate.times( 5, :feature, portals: portals) # carousel
      Fabricate.times( 5, :tab_feature, portals: portals )
      Fabricate :external_feature, portals: portals # carousel
      Fabricate :feature, primary: true, portals: portals # carousel
      Fabricate :primary_tab_feature, portals: portals
    end
    it 'returns an array of tab features, properly ordered' do
      get '/api/v2/features.json', params: { type: 'tab' }, headers: headers
      json = JSON.parse(response.body)
      expect(json.length).to eq 6
      expect(json.first['primary']).to be true
      expect(json[1]['created_at']).to be > json[5]['created_at']
    end
    it 'returns an array of carousel features, properly ordered' do
      get '/api/v2/features.json',
          params: { type: 'carousel' },
          headers: headers
      json = JSON.parse(response.body)
      expect(json.length).to eq 7
      expect(json.first['primary']).to be true
    end
    it 'returns error code for unsupported type' do
      get '/api/v2/features.json',
          params: { type: 'collection' },
          headers: headers
      expect(response.status).to eq 400
    end
  end
end