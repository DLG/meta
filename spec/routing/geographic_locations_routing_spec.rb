require "rails_helper"

RSpec.describe GeographicLocationsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/geographic_locations").to route_to("geographic_locations#index")
    end

    it "routes to #new" do
      expect(:get => "/geographic_locations/new").to route_to("geographic_locations#new")
    end

    it "routes to #show" do
      expect(:get => "/geographic_locations/1").to route_to("geographic_locations#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/geographic_locations/1/edit").to route_to("geographic_locations#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/geographic_locations").to route_to("geographic_locations#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/geographic_locations/1").to route_to("geographic_locations#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/geographic_locations/1").to route_to("geographic_locations#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/geographic_locations/1").to route_to("geographic_locations#destroy", :id => "1")
    end

  end
end
