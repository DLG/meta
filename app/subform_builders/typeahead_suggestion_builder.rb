# frozen_string_literal: true

class TypeaheadSuggestionBuilder

  def initialize(view)
    @view = view
  end
  attr_accessor :view

  def method_missing(symbol, *args, **options)
    data = {
      'suggest-fill' => symbol
    }
    data['suggest-format'] = options[:locale] if options[:locale].present?
    if options[:fallback].present?
      fallback = view.escape_once options[:fallback]
      fallback = CGI.escapeHTML fallback unless options[:fallback].html_safe?
      data['suggest-fallback'] = fallback
    end
    view.content_tag 'span', '', data: data
  end

end