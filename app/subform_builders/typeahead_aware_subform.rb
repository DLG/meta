# frozen_string_literal: true

class TypeaheadAwareSubform

  def self.render_subform(view, form, &inner)
    subform = TypeaheadAwareSubform.new(view, form, form.object)
    render_inner = Proc.new { inner.call subform }
    view.render partial: 'simple_subform/subform', locals: { form: form, render_inner: render_inner }
  end

  def initialize(view, form, related_object)
    @view = view
    @form = form
    @related_object = related_object
  end

  attr_accessor :view

  def object
    @related_object
  end

  def object_present?
    object&.id.present?
  end

  def form
    @form
  end

  def typeahead_input(field, suggestions_path, **options, &suggestion_block)
    if options.include? :required
      # Them bootstrap form builder gem seems to honor the required parameter for the input itself but not for labels
      # If required is given, we'll apply a class to the label
      required_indicator_class = options[:required] ? 'required' : 'not-required'
      label_classes = options[:label_class] || []
      label_classes = [label_classes] unless label_classes.is_a? Array
      label_classes << required_indicator_class
      options[:label_class] = label_classes
    end
    typeahead_data = {
      'typeahead-from' => suggestions_path,
      'typeahead-field' => field,
      'typeahead-model' => object&.class&.name&.underscore
    }
    if suggestion_block.present?
      typeahead_data['typeahead-template'] = view.escape_once(view.capture TypeaheadSuggestionBuilder.new(view),
                                                                           &suggestion_block)
    end
    view.render partial: 'subform_common/typeahead', locals: {
      context: self,
      form: form,
      field: field,
      readonly_options: { readonly: true, **options },
      typeahead_input_options: { data: typeahead_data, **options }
    }
  end

  def field_value(field)
    if object_present?
      form.object.send field
    else
      "<span data-typeahead-fill=\"#{field}\"></span>".html_safe
    end
  end

  def hyperlink(url_format, **options, &content_block)
    if object_present?
      href = fill_placeholders url_format
      return '' unless href.present?

      options[:href] = href
    else
      options[:data] = {'typeahead-fill-href' => url_format}
    end
    view.content_tag :a, **options, &content_block
  end

  def link_placeholder(field_name)
    "+++_+#{field_name}+_+++"
  end

  def fill_placeholders(url_format)
    values_missing = false
    url = url_format.gsub(/\+\+\+_\+([^+]+)\+_\+\+\+/) do
      value = form&.object&.attributes&.dig $1
      values_missing = true if value.nil?
      value
    end
    values_missing ? nil : url
  end

  def method_missing(symbol, *args)
    form.send symbol, *args
  end

  def parent_errors
    nil
  end

  # inject an error message into a bootstrap form builder
  def inject_error(form_group, error)
    form_group_element = Nokogiri::HTML.fragment(form_group)&.first_element_child
    return form_group unless form_group_element.present?

    error_message = content_tag(:div, error, class: 'help-block')
    form_group_element << error_message
    form_group_element['class'] = ((form_group_element['class'] || '') + ' has-error').strip
    form_group_element.to_s.html_safe
  end

  def inject_errors_for_parent(destination)
    if parent_errors.present?
      destination = inject_error(destination, view.strip_tags(parent_errors))
    end
    destination
  end

end