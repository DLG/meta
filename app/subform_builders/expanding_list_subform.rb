# frozen_string_literal: true

class ExpandingListSubform
  def self.render_subform(view, form, property, heading: property.to_s.titleize, blank_template: {},
                          selector_to_watch: nil, &inner)
    subform = self.new(view, form, property, selector_to_watch, blank_template, inner)
    view.render partial: 'expanding_list/subform', locals: { subform: subform, heading: heading }
  end

  def initialize(view, form, property, selector_to_watch, blank_template, inner)
    @view = view
    @form = form
    @property = property
    @selector_to_watch = selector_to_watch
    @blank_template = blank_template
    @inner = inner
    @items = form.object.send(property.to_sym) || []
    @items << blank_template
  end

  attr_reader :view, :form, :property, :selector_to_watch, :inner, :items, :blank_template

  def render_inner(item)
    subform.fields_for(item) do
      inner.call
    end
  end

  def object_name
    "#{form.object_name}[#{property}]"
  end

end
