# frozen_string_literal: true

class ManyToManySubform < TypeaheadAwareSubform

  # @param view [ActionView]
  # @param parent_form [FormBuilder]
  # @param join_table [Symbol]
  # @param relation [Symbol]
  # @param sorted_by: sort_field [Symbol]
  # @param heading: heading [String]
  # @yieldparam [RelatedObjectSubform]
  def self.render_subform(view, parent_form, join_table, relation, sorted_by: nil, heading:, &per_item)

      # Get our join_table (e.g, item_names) rows with the fields from the related table (e.g., names) joined up
      join_rows = parent_form.object.send join_table
      records = join_rows.joins relation
      unless sorted_by.nil?
        records = records.order(sorted_by)
      end
      subform_items = records.to_a

      # The result of join_rows.joins() will not include any uncommitted rows from a form that failed validation.
      # Here, we add them back to the end.
      subform_items += join_rows.select { |join_row| join_row.id.blank? }

      # Create an instance of a join table row (e.g, an ItemName) with an included instance of the related class (Name)
      # for Cocoon to use as a template.
      # Even if we can't create new (for example) Names from this form and ItemName doesn't except nested attributes
      # for Name, this is necessary and should work.
      build_method = "build_#{relation}".to_sym
      create_template = Proc.new { |join_row| join_row.send build_method; join_row }
      render_per_item = Proc.new { |f, sf| per_item.call self.new(view, f, sf, relation) }

      view.render partial: 'many_to_many/subform',
                  locals: { f: parent_form,
                            heading: heading,
                            join_table_relationship: join_table,
                            relation: relation,
                            subform_items: subform_items,
                            create_template: create_template,
                            render_per_item: render_per_item }
  end

  def initialize(view, join_row_form, inner_form, relation)
    @view = view
    @join_row_form = join_row_form
    @inner_form = inner_form
    @relation = relation
  end

  attr_accessor :join_row_form, :inner_form, :relation

  def join_row
    join_row_form&.object
  end

  def form
    inner_form
  end

  def object
    inner_form&.object
  end

  def object_present?
    join_row&.id.present?
  end

  def parent_errors
    join_row_form.errors_on relation
  end

end