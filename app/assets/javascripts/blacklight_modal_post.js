// Blacklight's modal helpers allow you to navigate within a modal by adding blacklight-modal="preserve" to a link.
// Blacklight 6 had functionality that let you do the same for forms, but Blacklight 7 does not.

// This patch doesn't all bring back the old BL6 functionality, but it gives us a workaround by letting us specify
// that some links should be requested as an HTTP post. Just specify blacklight-modal="preserve-post"

// This script is included in application.js by virtue of `require_tree .`. It should be imported after blacklight.


// This method is modeled on Blacklight.modal.modalAjaxLinkClick.
// Besides specifying method=POST, it also adds the CSRF token from the page's <head>.
Blacklight.modal.modalAjaxLinkClickPost = function (e) {
    e.preventDefault();
    var href = e.target.getAttribute('href');

    var options = { method: 'POST' };
    var crsfParamName = $('meta[name=csrf-param]').attr('content');
    var crsfParamValue = $('meta[name=csrf-token]').attr('content');
    if (crsfParamName && crsfParamValue) {
        var formData = new FormData();
        formData.append(crsfParamName, crsfParamValue);
        options.body = formData;
    }

    fetch(href, options)
        .then(function (response) {
            if (!response.ok) {
                throw new TypeError("Request failed");
            }
            return response.text();
        })
        .then(function (data) {
            return Blacklight.modal.receiveAjax(data);
        })
        .catch(function (error) {
            return Blacklight.modal.onFailure(error);
        });
}

// patch Blacklight.modal.setupModal to setup our new handler method
var blacklightSetupModalUnpatched = Blacklight.modal.setupModal;
Blacklight.modal.setupModal = function () {
    if (Blacklight.modal.modalHasBeenSetup) return;
    document.addEventListener('click', function (e) {
        if (e.target.matches('#blacklight-modal a[data-blacklight-modal~=preserve-post]'))
            Blacklight.modal.modalAjaxLinkClickPost(e);
    });
    blacklightSetupModalUnpatched();
    Blacklight.modal.modalHasBeenSetup = true;
};
