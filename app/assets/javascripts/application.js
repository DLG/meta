// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require popper
//= require turbolinks
//= require 'blacklight_advanced_search'
//= require twitter/typeahead
//= require blacklight/blacklight
//= require bootstrap/tooltip
//= require bootstrap/popover
//= require bootstrap/tab
//= require bootstrap
//= require chosen-jquery
//= require filterrific/filterrific
//= require cocoon
//= require_tree .


// Patch filterrific so we can include it everywhere, even on pages that don't have a #filterrific_filter
// ...which was supported pre-5.2.4
Filterrific.conditionallyInit = function () {
    if (document.querySelector('#filterrific_filter')) Filterrific.init();
}
document.removeEventListener('turbolinks:load', Filterrific.init);
document.removeEventListener('DOMContentLoaded', Filterrific.init);
document.addEventListener('turbolinks:load', Filterrific.conditionallyInit);
document.addEventListener('DOMContentLoaded', Filterrific.conditionallyInit);

// Another crutch for buggy Filterrific code. Something missed when rewriting their JS to remove jQuery?
// See https://github.com/jhund/filterrific/issues/222
// The native closest() method returns null where jQuery's would return []
// This replaces the closest() method on just the elements that filterrific observes
const unshimmedObserveField = Filterrific.observe_field;
Filterrific.observe_field = function (input_selector) {
    document.querySelectorAll(input_selector).forEach(input => {
        input.closest = function () {
            return HTMLElement.prototype.closest.apply(this, arguments) || [];
        }
    });
    unshimmedObserveField.apply(this, arguments);
};

Blacklight.onLoad(function() {

    $('select.advanced-search-facet-select').chosen({
        search_contains: true
    });
    $('select#dc-right-select, select#dc-right-select-multiple').chosen({
        allow_single_deselect: true
    });
    $('select#dcterms-type-select-multiple').chosen({
        allow_single_deselect: true
    });
    $('select#holding-institution-ids-select').chosen({
        search_contains: true
    });
    $('select#holding-institution-ids-select-multiple').chosen({
        search_contains: true
    });

    function updateFilterrificIfRelevant() {
        // Filterrific is listening to native JS change events which chosen does NOT fire
        //   when updating underlying <selects>. This function will trigger filterrific's
        //   normal form element change handler if Filterrific is present and `this` is
        //   a descendent of a filterrific form.
        // Apply this function as a change handler (using JQuery's `.change()`) for elements that
        //   are only updated via JQuery (e.g., chosen selects).
        if ($(this).closest('#filterrific_filter').length && window.Filterrific) {
            Filterrific.submitFilterForm.apply(this);
        }
    }

    $('select.collection-select').chosen().change(updateFilterrificIfRelevant);
    $('select.fancy-multiselect').chosen().change(updateFilterrificIfRelevant);
    $('select.fancy-select').chosen({
        allow_single_deselect: true
    }).change(updateFilterrificIfRelevant);

    $('a.floating-save-button').on('click', function (e) {
        e.preventDefault();
        $(document).find('form').submit();
        return false;
    });

    $('a.form-jump-link').on('click', function () {
        $($(this).attr('href')).focus();
    });

    $('a.select-element-text').on('click', function () {
        selectElementText(document.getElementById($(this).data('container-id')))
    });

    $('.document-tabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show')
    });

    var typeaheadAPIs = {};
    function getTypeaheadAPI(endpoint, field) {
        var key = endpoint + ';;' + field;
        var api = typeaheadAPIs[key];
        if (api) return api;
        return typeaheadAPIs[key] = new Bloodhound({
            datumTokenizer: function (x) { return x[field]; },
            queryTokenizer: function (x) { return x; },
            remote: {
                url: endpoint + '?q={SEARCH}',
                wildcard: '{SEARCH}'
            }
        })
    }

    function wireUpTypeahead(form) {
        $(form).find('[data-typeahead-from]:not(.typeahead)').each(function (i, el) {
            var endpoint = $(el).data('typeahead-from');
            var field = $(el).data('typeahead-field');
            var container = $(el).closest('[data-linked-relation]');
            var scope = container.data('linked-relation');
            var modelName = $(el).data('typeahead-model');;
            var dirty = false;
            var setObject = function (obj) {
                dirty = false;
                for (var key in obj) {
                    if (obj.hasOwnProperty(key)) {
                        var formElementName = scope + '[' + key + ']';
                        $('[name="' + formElementName + '"]').val(obj[key]);
                        $(container).find('[data-typeahead-fill-href]').each(function (i, elToFill) {
                            var hrefPattern = $(elToFill).data('typeahead-fill-href');
                            var missingFields = false;
                            var href = hrefPattern.replaceAll(/\+\+\+_\+([^+]+)\+_\+\+\+/g, function (match, field) {
                                var data = obj[field];
                                if ((data == null) || (data === '')) missingFields = true;
                                return (data || '').toString();
                            });
                            if (!missingFields) {
                                $(elToFill).attr('href', href);
                            } else {
                                $(elToFill).removeAttr('href');
                            }
                        });
                        $(container).find('[data-typeahead-fill]').each(function (i, elToFill) {
                           elToFill.innerText = obj[$(elToFill).data('typeahead-fill')];
                        });
                    }
                }
            }
            var clearObject = function () {
                dirty = true;
                $('[name^="' + scope + '"]').each(function (i, formEl) {
                    if (!formEl || formEl === el) return;
                    if (formEl.type === 'radio' || formEl.type === 'checkbox') {
                        formEl.checked = false;
                    } else if (formEl.type === 'button' || formEl.type === 'submit' || formEl.type === 'reset') {
                        // do nothing
                    } else {
                        formEl.value = '';
                    }
                });
                $(container).find('[data-typeahead-fill-href]').removeAttr('href');
                $(container).find('[data-typeahead-fill]').text('');
            }
            var api = getTypeaheadAPI(endpoint, field);
            var suggestionCSSClasses = 'typeahead-suggestion';
            if (modelName) suggestionCSSClasses += ' typeahead-suggestion-' + modelName;
            var suggestionTemplate = $(el).data('typeahead-template') ||
                '<span data-suggest-fill="' + field + '"></span>'
            $(el).addClass('typeahead').typeahead(null, {
                async: true,
                source: api,
                display: function (result) { return result[field]; },
                templates: {
                    suggestion: function (result) {
                        var templated = $('<div><div class="' + suggestionCSSClasses + '">' +
                                          suggestionTemplate + '</div></div>');
                        templated.find('[data-suggest-fill]').each(function (i, fillEl) {
                            var fieldValue = result[$(fillEl).data('suggest-fill')];
                            var locale = $(fillEl).data('suggest-format');
                            if (locale) {
                                if (locale === true) {
                                    fieldValue = fieldValue.toLocaleString();
                                } else {
                                    fieldValue = fieldValue.toLocaleString(locale);
                                }
                            }
                            fieldValue = fieldValue || $(fillEl).data('suggest-fallback') || '';
                            fillEl.innerHTML = fieldValue;
                        });
                        return templated.html();
                    }
                }
            }).on('typeahead:select', function (ev, result) {
                setObject(result);
            }).on('input', clearObject).on('change', function () {
                if (!dirty) return;
                var val = this.value;
                var el = this;
                api.search(val, null, function (results) {
                    var result = results && results[0];
                    if (result && (result[field] || '').toLowerCase() === (val || '').toLowerCase()) {
                        setObject(result);
                        el.value = result[field];
                    }
                });
            });
        });
    }


    var formElements = 'input, textarea, select';

    function setupFieldArray(i, arrayContainer){
        if ($(arrayContainer).closest('[data-field-template]').length > 0) return;
        var template = $(arrayContainer).find('[data-field-template]').filter(
            function(i,el) {
              return $(el).closest('[data-field-array-for]')[0]===arrayContainer;
            }
        ).remove().removeAttr('data-field-template');
        var relevantElementSelector = $(arrayContainer).data('field-array-item') || formElements;
        $(arrayContainer).on('change input', relevantElementSelector, function (e) {
            var fieldArray = $(arrayContainer).find(relevantElementSelector);
            var encounteredNonEmpty = false;
            for (var i = fieldArray.length; i > 0; i--) {
                if (fieldArray[i - 1].value) {
                    encounteredNonEmpty = true;
                    if (i === fieldArray.length) {
                        var newElement = template.clone();
                        var randomInt = Math.floor(Math.random() * 10000000 + 100);
                        $(newElement).find(formElements).each(function (i, el) {
                            if ($(el).closest('[data-field-template]').length > 0) return;
                            var name = $(el).attr('name');
                            if (name && name.indexOf('--index-placeholder--') > -1) {
                                $(el).attr('name', name.replaceAll('--index-placeholder--', randomInt));
                                $(el).attr('id', $(el).attr('id').replaceAll('--index-placeholder--', randomInt));
                            }
                        });
                        $(arrayContainer).append(newElement);
                        newElement.find('[data-field-array-for]').each(setupFieldArray);
                    }
                } else if (i < fieldArray.length) {
                    if (encounteredNonEmpty) {
                        if (e.type === 'change') {
                            if (i === 1) {
                                // We want to remove the first item, but don't want to lose the first container's styles
                                // So we move in values from the second item, and delete that
                                $(fieldArray[i-1]).val($(fieldArray[i]).val());
                                $(fieldArray[i]).closest('[data-field-wrapper]').remove();
                            } else {
                                $(fieldArray[i-1]).closest('[data-field-wrapper]').remove();
                            }
                        }
                    } else {
                        $(fieldArray[i]).closest('[data-field-wrapper]').remove();
                    }
                }
            }
        });
        $(arrayContainer).on('keypress', formElements, function (e){
            if (e.which === 13) {
                var inputs = $(arrayContainer).find(formElements);
                var index = inputs.index(this);
                if (index < inputs.length - 1) {
                    e.preventDefault();
                    e.stopPropagation();
                    inputs[index + 1].focus();
                }
            }
        });
        // disable empty elements before the form is submitted to avoid sending blanks to server
        $(arrayContainer).closest('form').on('submit', function (){
            $(arrayContainer).find(relevantElementSelector).each(function (i, el){
                if (!el.value) el.disabled = true;
            });
        });
    }



    $('[data-many-to-many-form]').each(function (i, form) {
        $(form).on('cocoon:after-insert', function () { wireUpTypeahead(form); });
    });
    $('[data-typeahead-subform]').each(function (i, form) {
        wireUpTypeahead(form);
    });
    $('[data-field-array-for]').each(setupFieldArray);

    /*
        support for file_field updates
     */
    $('.custom-file-input').change(function(e){
        var fileName = e.target.files[0].name;
        $('.custom-file-label[for=' + e.currentTarget['id'] + ']').html(fileName);
    });
});

// thx http://stackoverflow.com/a/2838358
function selectElementText(el, win) {
    win = win || window;
    var doc = win.document, sel, range;
    if (win.getSelection && doc.createRange) {
        sel = win.getSelection();
        range = doc.createRange();
        range.selectNodeContents(el);
        sel.removeAllRanges();
        sel.addRange(range);
    } else if (doc.body.createTextRange) {
        range = doc.body.createTextRange();
        range.moveToElementText(el);
        range.select();
    }
}
