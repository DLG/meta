# frozen_string_literal: true

class Serial < ApplicationRecord
  #region includes
  include PgSearch::Model
  include HasGeolocations
  include GeolocationIndexable
  include MetadataHelper
  #endregion


  #region associations
  has_many :items
  has_many :serial_geographic_locations, dependent: :destroy
  has_many_geographic_locations_through :serial_geographic_locations
  has_and_belongs_to_many :preceding_serials, class_name: :Serial, join_table: :preceding_succeeding_serials,
                          foreign_key: :succeeding_serial_id, association_foreign_key: :preceding_serial_id
  has_and_belongs_to_many :succeeding_serials, class_name: :Serial, join_table: :preceding_succeeding_serials,
                          foreign_key: :preceding_serial_id, association_foreign_key: :succeeding_serial_id
  #endregion

  #region callbacks
  after_update :reindex_child_values
  #endregion

  #region validations
  validates_presence_of :slug
  validates_presence_of :dcterms_title
  # endregion

  #region scopes
  scope :for_indexing, lambda {
    includes(items: [:collection, :repository])
  }
  scope :text_search, ->(query) { search_for query }
  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^id/ then order "serials.id #{direction}"
    when /^slug/ then order "serials.slug #{direction}"
    when /^title/ then order "serials.dcterms_title #{direction}"
    when /^item_count/ then order "item_count #{direction}" # we're assuming we have the with_item_count scope
    when /^display/ then order "displayed_item_count #{direction}" # we're assuming we have the with_item_count scope
    when /^created_at/ then order "serials.created_at #{direction}"
    when /^updated_at/ then order "serials.updated_at #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :with_identifier, lambda { |identifier_type, identifier|
    where("external_identifiers @> ?", [{ type: identifier_type, value: identifier }].to_json)
  }

  count_column_scopes_for Item, as: :item_count
  count_column_scopes_for Item.displayed, as: :displayed_item_count

  #endregion

  #region Setup for pg_search and filterrific
  pg_search_scope :search_for,
                  against: [:slug, :dcterms_title],
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'updated_at_desc' },
    available_filters: %i[sorted_by text_search]
  )
  #endregion

  #region Class methods
  def self.sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  def self.options_for_select
    order(Arel.sql 'LOWER(slug)').map { |r| [r.title_with_slug, r.id] }
  end

  def self.get_from_identifiers(identifiers_hash)
    id_types = identifiers_hash.keys
    if id_types.include? 'OCLC'
      # if we've got an OCLC #, try that first
      id_types.prepend(id_types.delete('OCLC'))
    end
    id_types.each do |type|
      identifier = identifiers_hash[type]
      results = with_identifier(type, identifier)
      return results.first if results.count == 1
    end
    nil
  end

  def self.identifier_types
    from('serials, jsonb_to_recordset(external_identifiers) j (type text, value text)')
      .pluck(Arel.sql('distinct j.type'))
  end
  #endregion

  # region public_methods

  def title_with_slug
    "#{display_title} (#{slug})"
  end

  def dcterms_identifier
    external_identifiers&.map do |identifier|
      "#{ identifier['type'] }: #{ identifier['value'] }"
    end
  end

  def call_numbers
    call_numbers = external_identifiers.select{|identifier| identifier['type'] == 'Call Number'}
    call_numbers.map{|identifier| normalize_call_number identifier['value']}
  end

  def reindex_child_values
    reindex_item_ids item_ids
  end

  # @param ids [Array<Integer>] List of Item ids
  def reindex_item_ids(ids)
    Resque.enqueue(Reindexer, 'Item', ids) if ids.any?
  end

  def safe_to_delete?
    attributes.include?('item_count') ? attributes['item_count'] == 0 : items.none?
  end

  def display_title
    dcterms_title&.first || slug
  end

  # Returns a Sunspot Hit object for the item that represents the current values in SOLR
  def solr_hit
    search = Serial.search do
      adjust_solr_params do |params|
        params[:q] = ""
      end
      with(:slug, slug)
    end

    return nil if search.total < 1

    search.hits.first
  end

  def solr_stored_hash
    hit = solr_hit

    return {} if hit.nil?

    hit.instance_variable_get(:@stored_values)
  end

  def record_id
    "Serial:#{slug}"
  end

  def dlg_url
    "#{Rails.configuration.dlg_url}/serial/#{slug}"
  end

  def thumbnail_url
    return unless items.any?

    items.last.thumbnail_url
  end
  def displayed_items
    items.filter(&:display?)
  end

  def display?
    items_size = if attributes.include?('displayed_item_count')
                   attributes['displayed_item_count']
                 else
                   displayed_items.size
                 end
    items_size > 0
  end

  def generate_collection
    return unless items.any?

    items.first.collection
  end

  def generate_holding_institutions
    return [] unless items.any?

    items.first.holding_institutions
  end


  def batch_item_xml_template
    template = %Q(
<?xml version="1.0" encoding="UTF-8"?>
<items type="array">
  <item>
   <portals type="array">
     <portal>
       <code>georgia</code>
     </portal>
   </portals>
   <serial>
     <slug>#{slug}</slug>
   </serial>
   <dpla type="boolean">true</dpla>
   <public type="boolean">true</public>
   <local type="boolean">true</local>)
    template += xml_field_template('dc_date')
    template += xml_field_template('dcterms_type') if dcterms_type.empty?
    template += xml_field_template('dc_right') if dc_right.empty?
    template + %Q(
      </item>
</items>
    )
  end

  def xml_field_template(field_name)
    %Q(
   <#{field_name} type="array">
     <#{field_name}></#{field_name}>
   </#{field_name}>)
  end
  # endregion

  #region solr_fields
  searchable do
    # Blacklight 'Required' fields #
    string(:title, as: 'title') {display_title}
    string(:class_name, stored: true) { self.class }
    string(:slug, stored: true)
    string(:record_id, stored: true)
    # set empty proxy id field so sunspot knows about it
    # value is set prior to save
    # sunspot search will not work without this, but indexing will
    # see monkeypatch @ config/initializers/sunspot_indexer_id.rb
    string(:sunspot_id, stored: true) { '' }

    string(:dcterms_title, as: 'dcterms_title_display', multiple: true)
    string(:dcterms_identifier, as: 'dcterms_identifier_display', multiple: true)
    string(:dcterms_publisher, as: 'dcterms_publisher_display', multiple: true)
    string(:dcterms_description, as: 'dcterms_description_display', multiple: true)
    string(:dcterms_subject, as: 'dcterms_subject_display', multiple: true)
    string(:dcterms_medium, as: 'dcterms_medium_display', multiple: true)
    string(:dcterms_replaces, as: 'dcterms_replaces_display', multiple: true)
    string(:dcterms_is_replaced_by, as: 'dcterms_is_replaced_by_display', multiple: true)
    string(:dcterms_creator, as: 'dcterms_creator_display', multiple: true)
    string(:dcterms_subject_fast, as: 'dcterms_subject_fast_display', multiple: true)
    string(:dc_date, as: 'dc_date_display', multiple: true)
    string(:dc_right, as: 'dc_right_display', multiple: true)
    string(:dlg_subject_personal, as: 'dlg_subject_personal_display', multiple: true)
    string(:dcterms_language, as: 'dcterms_language_display', multiple: true)
    string(:dcterms_type, as: 'dcterms_type_display', multiple: true)
    string(:dcterms_frequency, as: 'dcterms_frequency_display', multiple: true)
    string(:dcterms_spatial, as: 'dcterms_spatial_display', multiple: true) { geolocations_to_dcspatial }
    string(:external_identifiers_json, stored: true){external_identifiers.to_json}
    string(:call_numbers, stored: true, multiple: true) { call_numbers }
    string(:preceding_slugs, multiple: true, stored: true)
    string(:succeeding_slugs, multiple: true, stored: true)
    boolean(:public, stored: true) {true}
    string(:public, stored: true) { public ? 'Yes' : 'No' }
    boolean(:display) {display?}
    string(:display, stored: true) { display? ? 'Yes' : 'No' }

    string(:thumbnail, stored: true) { thumbnail_url }
    string(:portals, stored: true, multiple: true) { ['georgia'] }


    # Primary Search Fields (multivalued, indexed, stemming/tokenized)
    text :dc_date
    text :dcterms_title
    text :dcterms_creator
    text :dcterms_subject
    text :dcterms_description
    text :dcterms_publisher
    text(:dcterms_spatial){ geolocations_to_dcspatial}
    text :dcterms_identifier
    text :dlg_subject_personal
    text :dcterms_replaces
    text :dcterms_is_replaced_by
    text :dcterms_subject_fast

    # sort fields
    string(:title_sort,       as: 'title_sort') { dcterms_title.first ? dcterms_title.first.downcase.gsub(/^(an?|the)\b/, '') : '' }
    string(:creator_sort,     as: 'creator_sort') { dcterms_creator.first ? dcterms_creator.first.downcase.gsub(/^(an?|the)\b/, '') : '' }
    string(:yyyy_mm_dd_sort,  as: 'yyyy_mm_dd_sort') { IndexDates.index_yyyy_mm_dd_sort(dc_date) }
    integer(:year,            as: 'year', trie: true) { DateIndexer.new.get_sort_year(dc_date) }

    # facet fields
    string(:year_facet, as: 'year_facet', multiple: true) { IndexDates.index_years(dc_date) }
    string(:counties, as: 'counties_facet', multiple: true)
    string(:us_states, as: 'us_states_facet', multiple: true)

    # datetimes
    time(:created_at, stored: true, trie: true)
    time(:updated_at, stored: true, trie: true)

    # spatial stuff
    string(:coordinates, as: 'coordinates', multiple: true)
    string(:geojson, as: 'geojson', multiple: true)
    string(:placename, as: 'placename', multiple: true)
    string(:dpla_spatial_json, stored: true) { dpla_spatial_json }

  end
  #endregion


  def dcterms_replaces
    preceding_serials.pluck(:dcterms_title).map(&:first).compact
  end

  def preceding_slugs
    preceding_serials.pluck :slug
  end

  def dcterms_is_replaced_by
    succeeding_serials.pluck(:dcterms_title).map(&:first).compact
  end

  def succeeding_slugs
    succeeding_serials.pluck :slug
  end

  def source_xml
    # Nokogiri returns nil if source_data is nil or is not XML
    Nokogiri::XML(source_data)
  end
end
