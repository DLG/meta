class HoldingInstitutionGeographicLocation < ApplicationRecord
  belongs_to :holding_institution
  belongs_to :geographic_location
  validates_uniqueness_of :geographic_location_id, scope: :holding_institution_id
  accepts_nested_attributes_for :geographic_location
end