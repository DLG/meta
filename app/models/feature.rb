# frozen_string_literal: true

# a featured thing for the public DLG site
class Feature < ApplicationRecord
  #region includes
  include ApplicationHelper
  include Portable
  include PgSearch::Model
  #endregion

  #region ImageUploader
  mount_uploader :image, ImageUploader
  mount_uploader :large_image, ImageUploader
  #endregion

  #region validations
  validates :title, :title_link, presence: true
  validates :institution, :institution_link, presence: true
  validates :image, presence: true, unless: :primary_tab?
  validates :large_image, presence: true, if: :primary_tab?
  validates :short_description, presence: true, if: :tab?
  #endregion

  #region scopes
  scope :random, -> { order(Arel.sql('RANDOM()')) }
  scope :ordered, -> { order(primary: :desc) }
  scope :carousel, -> { ordered.where(area: 'carousel', public: true) }
  scope :tabs, -> { ordered.where(area: 'tabs', public: true).order(created_at: :desc) }
  scope :are_public, -> { where(public: true) }
  scope :are_primary, -> { where(primary: true) }
  scope :text_search, ->(query) { search_for query }

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^id/ then order "features.id #{direction}"
    when /^title/ then order "features.title #{direction}"
    when /^institution/ then order "features.institution #{direction}"
    when /^area/ then order "features.area #{direction}"
    when /^created_at/ then order "features.created_at #{direction}"
    when /^updated_at/ then order "features.updated_at #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :public_state, lambda { |active|
    case active
    when 'yes' then where(public: true)
    when 'no' then where(public: false)
    when '' then next
    else
      raise(ArgumentError, "Invalid Active option: #{active}")
    end
  }

  scope :primary_state, lambda { |active|
    case active
    when 'yes' then where(primary: true)
    when 'no' then where(primary: false)
    when '' then next
    else
      raise(ArgumentError, "Invalid Primary option: #{active}")
    end
  }

  scope :area_state, lambda { |area|
    next unless available_areas.include? area

    where(area: area)
  }

  scope :for_indexing, lambda {
  }
  #endregion

  #region solr_fields
  searchable do
    string(:class_name, stored: true) { self.class }
    string(:title, stored: true)
    string(:title_link, stored: true)
    string(:institution, stored: true)
    string(:institution_link, stored: true)
    string(:short_description, stored: true)
    string(:external_link, stored: true)
    string(:image, stored: true) {image&.solr_url}
    string(:large_image, stored: true) {large_image&.solr_url}
    string(:area, stored: true)
    boolean(:primary, stored: true)
    boolean(:public, stored: true)
    boolean(:carousel, stored: true) { carousel? }
    boolean(:primary_tab, stored: true) { primary_tab? }
    boolean(:side_tab, stored: true) { side_tab? }
    boolean(:display) { display? }
    string(:portals, stored: true, multiple: true) { portal_codes }

    # datetimes
    time(:created_at, stored: true, trie: true)
    time(:updated_at, stored: true, trie: true)

  end
  #endregion

  #region Setup for pg_search and filterrific
  pg_search_scope :search_for,
                  against: [:title],
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'updated_at_desc' },
    available_filters: %i[sorted_by text_search public_state for_portals primary_state area_state]
  )
  #endregion

  #region class methods
  def self.available_areas
    %w[carousel tabs]
  end

  def self.sort_options
    [['Recently Created', 'created_at_desc'],
     ['Recently Updated', 'updated_at_desc']]
  end

  def self.public_states
    [['Public or Not Public', ''],
     %w[Public yes],
     ['Not Public', 'no']]
  end

  def self.primary_states
    [['Primary or Not Primary', ''],
     %w[Primary yes],
     ['Not Primary', 'no']]
  end

  def self.area_states
    [%w[All all],
     %w[Carousel carousel],
     %w[Tabs tabs]]
  end
  #endregion

  #region object methods
  def to_json
    super(options.merge!(except: :id))
  end

  def tab?
    area == 'tabs'
  end

  def carousel?
    area == 'carousel'
  end

  def primary_tab?
    primary && tab?
  end

  def side_tab?
    primary == false && tab?
  end

  def display?
    public
  end
  #endregion
end