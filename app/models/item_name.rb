# frozen_string_literal: true

class ItemName < ApplicationRecord
  belongs_to :item
  belongs_to :name
end
