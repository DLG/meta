# represent a Page: an optional component of an Item
class Page < ApplicationRecord
  #region includes
  require 'httparty'
  #endregion

  #region relationships
  belongs_to :item, counter_cache: true
  #endregion

  #region validations
  validates_presence_of :media_type
  validate :allowed_media_types
  validates :file_type, presence: true
  validates :number, presence: true
  validates :number, uniqueness: { scope: :item }
  validates :number, numericality: true
  #endregion

  #region callbacks
  before_save :update_image_dimensions
  after_save :reindex_parent_item
  #endregion

  #region constants
  PAGE_PATH_LENGTH = 5
  # endregion

  mount_uploader :file, DigitalObjectUploader

  #region scopes
  scope :ordered_by_number, -> { order(Arel.sql('number::integer')) }
  scope :iiif_images, -> { where(media_type: 'image') }
  scope :iiif_videos, -> { where(media_type: 'video') }
  scope :iiif_audios, -> { where(media_type: 'audio') }
  scope :missing_height, -> { iiif_images.where('height < 1') }
  scope :missing_width, -> { iiif_images.where('width < 1') }
  scope :missing_image_dimensions, -> { missing_height.or(missing_width) }
  #endregion

  #region class_methods
  def self.media_type_codes
    %w[audio image video].freeze
  end

  def self.media_types_for_menus
    [
      %w[Audio audio],
      %w[Image image],
      %w[Video video]
    ].freeze
  end
  #endregion

  # region public methods
  def allowed_media_types
    return if Page.media_type_codes.include? media_type

    errors.add(:media_type, " '#{media_type}' is not a permitted media_type. Allowed types: #{Page.media_type_codes.join(", ")}")
  end

  def iiif_info_link
    self.class.iiif_info_link_for item.record_id, number, file_type
  end

  def iiif_file_link
    self.class.iiif_file_link_for item.record_id, number, file_type
  end

  def pdf?
    file_type.casecmp('PDF').zero?
  end

  def iiif_identifier
    return unless item

    self.class.iiif_id_for item.record_id, number, file_type
  end

  def iiif_image?
    media_type == 'image'
  end

  def iiif_title
    title.present? ? title : number
  end

  def iiif_height
    height > 0 ? height : 800
  end

  def iiif_width
    width > 0 ? width : 800
  end

  def update_image_dimensions
    return false unless iiif_image? && (width < 1 || height < 1)
    begin
      manifest = retrieve_image_manifest
      return false unless manifest

      self.width = manifest["width"] || 0
      self.height = manifest["height"] || 0
      return true
    rescue Exception => err
      notifier = NotificationService.new
      message = "Page update_image_dimension for url #{iiif_link_with_id}. Error: #{err}"
      notifier.notify(message)
    end
    false
  end

  def reindex_parent_item
    Sunspot.index item
  end

  def iiif_link_with_id
    self.class.iiif_link_for item.record_id, number, file_type
  end

  def self.iiif_id_for(record_id, page_num, file_type = 'jp2')
    parts = record_id.split '_'
    page = page_num.to_s.rjust PAGE_PATH_LENGTH, '0'
    if file_type.downcase == 'pdf'
      "dlg%2F#{parts[0]}%2F#{parts[1]}%2F#{record_id}%2F#{record_id}.#{file_type}"
    else
      "dlg%2F#{parts[0]}%2F#{parts[1]}%2F#{record_id}%2F#{record_id}-#{page}.#{file_type}"
    end
  end

  def self.iiif_link_for(*args)
    iiif_identifier = iiif_id_for *args
    return if iiif_identifier.blank?
    "#{Rails.application.credentials.iiif_base_uri.dig(Rails.env.to_sym)}#{iiif_identifier}"
  end

  def self.iiif_info_link_for(*args)
    "#{ iiif_link_for *args }/info.json"
  end

  def self.iiif_file_link_for(*args)
    "#{ iiif_link_for *args }/full/full/0/default.jpg"
  end

  def retrieve_image_manifest
    return if iiif_link_with_id.blank?
    response = HTTParty.get(iiif_link_with_id)
    response.parsed_response
  end
  #endregion
end
