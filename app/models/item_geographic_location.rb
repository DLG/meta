class ItemGeographicLocation < ApplicationRecord
  belongs_to :item
  belongs_to :geographic_location
  validates_uniqueness_of :geographic_location_id, scope: :item_id
  accepts_nested_attributes_for :geographic_location

end