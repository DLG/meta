# represent a DLG Collection
class Collection < ApplicationRecord
  #region includes
  include Slugged
  include IndexFilterable
  if FeatureFlags.enabled?(:geolocations_solr)
    include GeolocationIndexable
  else
    include GeospatialIndexable
  end
  include GeospatialIndexable
  include Portable
  include CollectionValidations
  include Provenanced
  include HasGeolocations
  include HasEducatorResources
  include PgSearch::Model
  #endregion

  #region relationships
  has_many :items, dependent: :destroy
  has_many :public_items, -> { where public: true }, class_name: 'Item'
  has_many :dpla_items, -> { where dpla: true }, class_name: 'Item'
  has_many :batch_items, dependent: :nullify
  has_many :collection_resources, -> { order(position: :asc) }
  belongs_to :repository, counter_cache: true
  has_and_belongs_to_many :subjects
  has_and_belongs_to_many :time_periods
  has_and_belongs_to_many :users
  has_many :collection_geographic_locations, dependent: :destroy
  has_many_geographic_locations_through :collection_geographic_locations
  #endregion

  #region validations
  validates_presence_of :repository
  #endregion

  # region callbacks
  before_destroy :clear_from_other_collections
  before_save :update_record_id
  after_update :record_id_change_in_solr
  after_update :reindex_children
  after_save :reindex_holding_institutions
  before_destroy :save_holding_institution_ids
  after_destroy :reindex_holding_institutions_after_destroy
  #endregion

  #region uploader
  mount_uploader :sponsor_image, ImageUploader
  #endregion

  #region scopes
  scope :for_indexing, lambda { includes(:portals, :holding_institutions, :repository, :geographic_locations,
                                  :collection_resources)
   }
  scope :updated_since, lambda { |since| where('updated_at >= ?', since) }
  scope :are_public, -> { where(public: true) }
  scope :are_displayed, -> { are_public.includes(:repository).where(repositories: {public: true}) }
  scope :alphabetized, -> { order(display_title: :asc) }
  scope :in_portal, ->(portal_type) { includes(:portals).where(portals:{code: portal_type}) }
  scope :text_search, ->(query) { search_for query }

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^id/ then order "collections.id #{direction}"
    when /^slug/ then order "collections.slug #{direction}"
    when /^title/ then order "collections.display_title #{direction}"
    when /^items_count/ then order("collections.items_count #{direction}")
    when /^collection_resources_count/ then order("collections.collection_resources_count #{direction}")
    when /^created_at/ then order "collections.created_at #{direction}"
    when /^updated_at/ then order "collections.updated_at #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :public_state, lambda { |active|
    case active
    when 'yes' then where(public: true)
    when 'no' then where(public: false)
    when '' then next
    else
      raise(ArgumentError, "Invalid Active option: #{active}")
    end
  }

  scope :for_repository, lambda { |repository_id|
    where(repository_id: repository_id)
  }

  scope :field_match, -> (field, to_match) { where("'#{to_match.downcase}' = ANY (lower(collections.#{field}::text)::text[])") }
  scope :educator_resources, -> { field_match('dcterms_medium', 'instructional materials') }
  #endregion

  # region solr_fields
  searchable do

    string :class_name, stored: true do
      self.class
    end

    string :slug, stored: true
    string :record_id, stored: true

    # set empty proxy id field so sunspot knows about it
    # value is set prior to save
    # sunspot search will not work without this, but indexing will
    # see monkeypatch @ config/initializers/sunspot_indexer_id.rb
    string :sunspot_id, stored: true do
      ''
    end

    integer :repository_id do
      repository.id
    end

    string :repository_slug do
      repository.slug
    end

    string :thumbnail, stored: true do
      # TODO: use brad's thumb links for now
      "https://dlg.galileo.usg.edu/do-th:#{repository.slug}"
    end

    boolean :public
    boolean :display do
      display?
    end
    boolean :educator_resource do
      dcterms_medium.compact.map(&:downcase).include? 'instructional materials'
    end

    string :public, stored: true do
      public ? 'Yes' : 'No'
    end

    string :display, stored: true do
      display? ? 'Yes' : 'No'
    end

    string :repository_name, stored: true, multiple: true do
      repository_titles
    end

    string :portal_names, stored: true, multiple: true do
      portal_names
    end

    string :portals, stored: true, multiple: true do
      portal_codes
    end

    string(:institution_slug, stored: true, multiple: true) do
      institution_slugs
    end

    string :display_title, stored: true, as: 'display_title_display'
    string :short_description, stored: true, as: 'short_description_display'

    string :sensitive_content, as: 'sensitive_content_display', multiple: true

    # *_display (not indexed, stored, multivalued)
    string :dcterms_provenance,             as: 'dcterms_provenance_display',             multiple: true
    string :dcterms_type,                   as: 'dcterms_type_display',                   multiple: true
    string(:dcterms_spatial, as: 'dcterms_spatial_display', multiple: true) { geolocations_to_dcspatial }
    string :dcterms_title,                  as: 'dcterms_title_display',                  multiple: true
    string :dcterms_creator,                as: 'dcterms_creator_display',                multiple: true
    string :dcterms_contributor,            as: 'dcterms_contributor_display',            multiple: true
    string :dcterms_subject,                as: 'dcterms_subject_display',                multiple: true
    string :dcterms_description,            as: 'dcterms_description_display',            multiple: true
    string :dcterms_publisher,              as: 'dcterms_publisher_display',              multiple: true
    string :edm_is_shown_at,                as: 'edm_is_shown_at_display',                multiple: true
    string :edm_is_shown_by,                as: 'edm_is_shown_by_display',                multiple: true
    string :dcterms_identifier,             as: 'dcterms_identifier_display',             multiple: true
    string :dc_date,                        as: 'dc_date_display',                        multiple: true
    string :dcterms_temporal,               as: 'dcterms_temporal_display',               multiple: true
    string :dc_format,                      as: 'dc_format_display',                      multiple: true
    string :dcterms_is_part_of,             as: 'dcterms_is_part_of_display',             multiple: true
    string :dc_right,                       as: 'dc_right_display',                       multiple: true
    string :dcterms_rights_holder,          as: 'dcterms_rights_holder_display',          multiple: true
    string :dcterms_bibliographic_citation, as: 'dcterms_bibliographic_citation_display', multiple: true
    string :dlg_local_right,                as: 'dlg_local_right_display',                multiple: true
    string :dlg_subject_personal,           as: 'dlg_subject_personal_display',           multiple: true
    string :dc_relation,                    as: 'dc_relation_display',                    multiple: true
    string :dcterms_medium,                 as: 'dcterms_medium_display',                 multiple: true
    string :dcterms_extent,                 as: 'dcterms_extent_display',                 multiple: true
    string :dcterms_language,               as: 'dcterms_language_display',               multiple: true

    string(:educator_resource_mediums, stored: true, multiple: true)

    # special collection-only fields
    string :collection_provenance_facet, multiple: true, as: 'collection_provenance_facet' do
      holding_institution_names
    end
    string :collection_type_facet, multiple: true, as: 'collection_type_facet' do
      dcterms_type
    end
    string :collection_spatial_facet, multiple: true, as: 'collection_spatial_facet' do
      geolocations_to_dcspatial
    end

    # just stored content
    string :partner_homepage_url,   as: 'partner_homepage_url_display'
    string :homepage_text,          as: 'homepage_text_display'

    # Primary Search Fields (multivalued, indexed, stemming/tokenized)
    text :dc_date
    text :dcterms_title
    text :dcterms_creator
    text :dcterms_contributor
    text :dcterms_subject
    text :dcterms_description
    text :dcterms_publisher
    text :dcterms_temporal
    text(:dcterms_spatial){geolocations_to_dcspatial}
    text :dcterms_is_part_of
    text :edm_is_shown_at
    text :edm_is_shown_by
    text :dcterms_identifier
    text :dcterms_rights_holder
    text :dlg_subject_personal
    text :dcterms_provenance

    string :title, as: 'title' do
      dcterms_title.first ? dcterms_title.first : slug
    end

    # required for Blacklight - a single valued format field
    string :format, as: 'format' do
      dc_format.first ? dc_format.first : ''
    end

    # sort fields

    string :title_sort, as: 'title_sort' do
      dcterms_title.first ? dcterms_title.first.downcase.gsub(/^(an?|the)\b/, '') : ''
    end

    string :creator_sort, as: 'creator_sort' do
      dcterms_creator.first ? dcterms_creator.first.downcase.gsub(/^(an?|the)\b/, '') : ''
    end

    string :yyyy_mm_dd_sort, as: 'yyyy_mm_dd_sort' do
      IndexDates.index_yyyy_mm_dd_sort(dc_date)
    end

    integer :year, as: 'year', trie: true do
      DateIndexer.new.get_sort_year(dc_date)
    end

    # facet fields
    string(:year_facet, as: 'year_facet', multiple: true){ IndexDates.index_years(dc_date) }
    string :counties, as: 'counties_facet', multiple: true
    string :us_states, as: 'us_states_facet', multiple: true

    # datetimes
    time :created_at, stored: true, trie: true
    time :updated_at, stored: true, trie: true

    # spatial stuff
    string :coordinates, as: 'coordinates', multiple: true
    string :geojson, as: 'geojson', multiple: true
    string :placename, as: 'placename', multiple: true

    # time periods
    string :time_periods, multiple: true, stored: true do
      time_periods.map(&:name)
    end

    # subjects
    string :subjects, multiple: true, stored: true do
      subjects.map(&:name)
    end

    string :image, stored: true do
      holding_institution_image
    end

    text(:collection_resources, stored: true) { collection_resources.to_json  }

    string(:sponsor_note, stored: true)
    string(:sponsor_image, stored: true) {  sponsor_image&.url }
  end
  #endregion

  #region Setup for pg_search and filterrific
  pg_search_scope :search_for,
                  against: [:slug, :display_title],
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'updated_at_desc' },
    available_filters: %i[sorted_by text_search public_state for_portals for_repository]
  )
  #endregion

  #region class_methods
  def self.index_query_fields
    %w[repository_id public].freeze
  end

  def self.sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  def self.public_states
    [['Public or Not Public', ''], %w[Public yes], ['Not Public', 'no']]
  end

  def self.options_for_select
    order(Arel.sql 'LOWER(display_title)').map { |r| [r.title_with_slug, r.id] }
  end
  #endregion

  #region public_methods
  def title
    display_title || 'No Title'
  end

  # allow Items to delegate collection_title
  alias_method :collection_title, :title

  def holding_institution_image
    holding_institutions.first.image&.url if holding_institutions.any?
  end

  def repository_title
    repository.title
  end

  def display?
    repository.public && public
  end

  def to_xml(options = {})
    default_options = {
      dasherize: false,
      except: %i[id repository_id created_at updated_at other_repositories
                 items_count date_range legacy_dcterms_spatial],
    }
    if options[:show_repository]
      default_options[:include] = [repository: { only: [:slug] }]
    end
    if options[:show_provenance]
      default_options[:methods] ||= []
      default_options[:methods] << :dcterms_provenance
    end
    if options[:show_spatial]
      default_options[:methods] ||= []
      default_options[:methods] << :dcterms_spatial
    end
    super(options.merge!(default_options))
  end

  def as_json(options = {})
    new_options = {
      except: [:legacy_dcterms_provenance, :legacy_dcterms_spatial],
      methods: %i[dcterms_provenance dcterms_spatial holding_institution_image]
    }
    super(options.merge!(new_options))
  end

  def other_repository_titles
    other_repos.map(&:title).uniq
  end

  def repository_titles
    (other_repository_titles << repository.title).reverse.uniq
  end

  def other_repos
    Repository.where(id: other_repositories)
  end

  def parent
    repository
  end

  def format_collection_resources_for_solr
    return [] unless collection_resources.any?

    collection_resources.map { |resource| resource.to_json }
  end

  def reindex_holding_institutions
    Sunspot.index! holding_institutions.for_indexing
  end

  def save_holding_institution_ids
    @associated_holding_institution_ids = holding_institutions.pluck(:id)
  end

  def reindex_holding_institutions_after_destroy
    Sunspot.index! HoldingInstitution.for_indexing.where(id: @associated_holding_institution_ids)
  end

  def title_public_state
    state = public ? 'Public' : 'Not Public'
    "#{display_title} (#{state})"
  end

  def title_with_slug
    "#{display_title} (#{slug})"
  end

  # Returns a Sunspot Hit object for the item that represents the current values in SOLR
  def solr_hit
    search = Collection.search do
      adjust_solr_params do |params|
        params[:q] = ""
      end
      with(:record_id, record_id)
    end

    return nil if search.total < 1

    search.hits.first
  end

  def solr_stored_hash
    hit = solr_hit

    return {} if hit.nil?

    hit.instance_variable_get(:@stored_values)
  end

  def topic_names
    subjects.pluck(:name).sort
  end

  def time_period_names
    time_periods.pluck(:name).sort
  end
  #endregion

  #region private_methods
  private

  def reindex_children
    #TODO: It looks like this logic could be simplified. SN - 9/24/20
    if saved_change_to_slug? || saved_change_to_display_title? || saved_change_to_repository_id?
      resave_children if saved_change_to_repository_id? || saved_change_to_slug?
      resaved = true
    end
    if !resaved && items.any? && saved_change_to_public?
      Resque.enqueue(Reindexer, 'Item', items.pluck(:id))
      serial_ids = items.pluck(:serial_id).uniq.compact
      Resque.enqueue(Reindexer, 'Serial', serial_ids) if serial_ids.any?
    end
    true
  end

  def resave_children
    Resque.enqueue(Resaver, items.map(&:id))
  end

  def update_record_id
    self.record_id = "#{repository.slug}_#{slug}"
  end

  def clear_from_other_collections
    is = Item.where "#{id} = ANY (other_collections)"
    is.each do |i|
      i.other_collections = i.other_collections - [id]
      i.save(validate: false)
    end
  end

  def record_id_change_in_solr
    return true unless saved_changes.include? :record_id
    previous_record_id = saved_changes[:record_id][0]
    Sunspot.remove!(OpenStruct.new(record_id: previous_record_id))
  end
  #endregion
end

