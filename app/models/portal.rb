class Portal < ApplicationRecord
  #region Associations
  has_many :portal_records
  has_many :items,          through: :portal_records,   source: :portable, source_type: 'Item'
  has_many :collections,    through: :portal_records,   source: :portable, source_type: 'Collection'
  has_many :repositories,   through: :portal_records,   source: :portable, source_type: 'Repository'
  has_many :batch_items,    through: :portal_records,   source: :portable, source_type: 'BatchItems'
  #endregion

  #region class_methods
  def self.options_for_select(use_code: false)
    order(Arel.sql('LOWER(name)')).map { |p| [p.name_with_code, (use_code ? p.code : p.id)] }
  end
  #endregion

  #region public_methods
  def name_with_code
    "#{name} (#{code})"
  end
  # endregion
end