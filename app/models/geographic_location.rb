class GeographicLocation < ApplicationRecord
  #region includes
  include PgSearch::Model
  #endregion

  #region scopes
  scope :geonames, ->{ where.not(geonames_id: nil) }
  scope :non_geonames, ->{ where(geonames_id: nil) }

  count_column_scopes_for ItemGeographicLocation, as: :item_count
  count_column_scopes_for CollectionGeographicLocation, as: :collection_count
  count_column_scopes_for SerialGeographicLocation, as: :serial_count
  count_column_scopes_for HoldingInstitutionGeographicLocation, as: :holding_institution_count
  count_column_scopes_for BatchItemGeographicLocation.joins(batch_item: :batch).where(batch: {committed_at: nil}),
                          as: :uncommitted_batch_item_count
  scope :with_all_related_counts, -> { with_item_count.with_collection_count
                                                      .with_serial_count
                                                      .with_holding_institution_count
                                                      .with_uncommitted_batch_item_count }
  #endregion

  #region associations
  has_many :item_geographic_locations, dependent: :destroy
  has_many :items, through: :item_geographic_locations
  has_many :collection_geographic_locations, dependent: :destroy
  has_many :collections, through: :collection_geographic_locations
  has_many :holding_institution_geographic_locations, dependent: :destroy
  has_many :holding_institutions, through: :holding_institution_geographic_locations
  has_many :batch_item_geographic_locations, dependent: :destroy
  has_many :batch_items, through: :batch_item_geographic_locations
  has_many :serial_geographic_locations, dependent: :destroy
  has_many :serials, through: :serial_geographic_locations
  #endregion

  #region before/after actions
  before_validation :update_code
  after_update :reindex_child_values
  #endregion

  #region validations
  validates_presence_of :code
  validates_uniqueness_of :code, unless: :has_geonames_id?, case_sensitive: false
  validates_numericality_of :geonames_id, allow_nil: true, only_integer: true
  validates_numericality_of :latitude, allow_nil: true, greater_than_or_equal_to: -90, less_than_or_equal_to: 90
  validates_numericality_of :longitude, allow_nil: true, greater_than_or_equal_to: -180, less_than_or_equal_to: 180
  #endregion

  #region Setup for pg_search and filterrific
  pg_search_scope :search_for,
                  against: [:code, :geonames_id],
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'updated_at_desc' },
    available_filters: %i[sorted_by text_search geonames_status]
  )
  #endregion

  #region scopes for filterrific
  scope :text_search, ->(query) { search_for query }
  scope :geonames_matched, -> { where.not(geonames_id: nil) }
  scope :geonames_unmatched, -> { where(geonames_id: nil) }

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^id/ then order "geographic_locations.id #{direction}"
    when /^code/ then order "geographic_locations.code #{direction}"
    when /^geonames_id/ then order "geographic_locations.geonames_id #{direction}"
    when /^longitude/ then order "geographic_locations.longitude #{direction}"
    when /^latitude/ then order "geographic_locations.latitude #{direction}"
    when /^created_at/ then order "geographic_locations.created_at #{direction}"
    when /^updated_at/ then order "geographic_locations.updated_at #{direction}"
    when /^item_count/ then order "item_count #{direction}" # we're assuming we have the with_item_count scope
    when /^collection_count/ then order "collection_count #{direction}" # same with with_collection_count scope
    when /^serial_count/ then order "serial_count #{direction}" # same with with_collection_count scope
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :geonames_status, lambda { |active|
    case active
    when 'yes' then geonames_matched
    when 'no' then geonames_unmatched
    when '' then next
    else
      raise(ArgumentError, "Invalid Active option: #{active}")
    end
  }
  #endregion

  #region Filter values for views
  def self.sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  def self.geonames_status_states
    [['All', ''], %w[Matched yes], %w[Unmatched no]]
  end
  #endregion

  def update_code
    if component_placenames.blank? && !code.blank?
      self.assign_attributes GeographicLocationLookupService.find_or_initialize(code).attributes
    end
    self.code = component_placenames.join(', ')
  end

  def has_geonames_id?
    geonames_id.present?
  end

  def coordinate_string
    return nil unless latitude.present? && longitude.present?
    "#{latitude}, #{longitude}"
  end

  # Subject to change.
  # Currently, per the emails that followed our spatial data conversations with DPLA, the format will be geonames.org/id
  # ...without a preceding http:// or https://
  def geonames_uri
    has_geonames_id? ? "geonames.org/#{geonames_id}" : nil
  end

  def dpla_hash
    {
      'names' => component_placenames,
      'coordinates' => coordinate_string,
      'uri' => geonames_uri
    }.compact
  end

  USA_NAME = 'United States'
  FIFTY_STATES = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware',
                  'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky',
                  'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi',
                  'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York',
                  'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
                  'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington',
                  'West Virginia', 'Wisconsin', 'Wyoming'].freeze

  def self.parse_location_string(str)
    latitude = longitude = nil
    # The last parenthesized clause in this regex is intended to catch additional latitudes/longitudes (which we ignore)
    # We noticed some dcterms_spatial of the form Country, State, County, City, lat1, lon1, lat2, lon2
    # ...which is presumably a bounding box. We're just taking the first pair and ignoring everything after it.
    if str =~ /^\s*(.*?),\s*(-?[\d.]+),\s*(-?[\d.]+)(,\s*-?[\d.]+)*\s*$/
      str = $1
      latitude = $2.to_f
      longitude = $3.to_f
      latitude = nil if latitude.zero?
      longitude = nil if longitude.zero?
    end
    return str, latitude, longitude
  end

  def self.extract_us_state_and_county(components)
    return nil, nil unless components.size > 1 && components[0] == USA_NAME
    state = components[1]
    return nil, nil unless FIFTY_STATES.include? state
    county = components[2]
    return state, nil unless / (County|Parish|Borough|Census Area)$/.match? county
    county_code = "#{USA_NAME}, #{state}, #{county}"
    county_record = GeographicLocation.find_by code: county_code
    return state, nil if county_record.nil?
    return state, county
  end

  def uncommitted_batch_items
    batch_items.joins(:batch).where(batches: {committed_at: nil})
  end

  def reindex_child_values
    reindex_item_ids item_ids
    reindex_collection_ids collection_ids
  end

  # @param ids [Array<Integer>] List of Item ids
  def reindex_item_ids(ids)
    Resque.enqueue(Reindexer, 'Item', ids) if ids.any?
  end

  # @param ids [Array<Integer>] List of Collection ids
  def reindex_collection_ids(ids)
    Resque.enqueue(Reindexer, 'Collection', ids) if ids.any?
  end

  # @param ids [Array<Integer>] List of Collection ids
  def reindex_serial_ids(ids)
    Resque.enqueue(Reindexer, 'Serial', ids) if ids.any?
  end

  # This is called by consolidate_into(destination) below, once per related collection
  # i.e., four times, once for each of geoloc.items, geoloc.collections, geoloc.serials, and geoloc.holding_institutions
  # @param destination [GeographicLocation] The geographic location to transfer relations to
  # @param relationship [Symbol] The relationship (e.g., :items); something the GeographicLocation class has_many of
  # @param id_field [Symbol] E.g., :item_id if relationship = :items
  def consolidate_relation(destination, relationship, id_field)
    relations = send(relationship)
    join_relations = send(relations.klass.geographic_locations_relationship)
    all_ids = relations.pluck(:id)
    if all_ids.any?
      common_ids = (all_ids & destination.send(relationship).pluck(:id))
      join_relations.where.not(id_field => common_ids).update_all(geographic_location_id: destination.id)
    end
    all_ids
  end

  # This deletes the current GeographicLocation (self) and transfers its relations to destination
  # It queues a reindex of any affected items and collections.
  # @param destination [GeographicLocation] The geographic location to transfer relations to
  def consolidate_into(destination)
    item_ids = consolidate_relation(destination, :items, :item_id)
    collection_ids = consolidate_relation(destination, :collections, :collection_id)
    serial_ids = consolidate_relation(destination, :serials, :serial_id)
    # no reindex required for holding_institutions
    consolidate_relation(destination, :holding_institutions, :holding_institution_id)
    consolidate_relation(destination, :batch_items, :batch_item_id)
    destroy
    reindex_item_ids item_ids
    reindex_collection_ids collection_ids
    reindex_serial_ids serial_ids
  end

  def has_items?
    attributes.include?('item_count') ? attributes['item_count'] > 0 : items.any?
  end

  def has_collections?
    attributes.include?('collection_count') ? attributes['collection_count'] > 0 : collections.any?
  end

  def has_serials?
    attributes.include?('serial_count') ? attributes['serial_count'] > 0 : serials.any?
  end

  def has_holding_institutions?
    attributes.include?('holding_institution_count') ?
      attributes['holding_institution_count'] > 0 :
      holding_institutions.any?
  end

  def has_uncommitted_batch_items?
    attributes.include?('uncommitted_batch_item_count') ?
      attributes['uncommitted_batch_item_count'] > 0 :
      uncommitted_batch_items.any?
  end

  def safe_to_delete?
    !(has_items? || has_collections? || has_serials? || has_holding_institutions? || has_uncommitted_batch_items?)
  end

end
