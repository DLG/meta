# represent a Repository
class Repository < ApplicationRecord
  #region includes
  include Slugged
  include Portable
  include IndexFilterable
  include PgSearch::Model
  #endregion

  #region relationships
  has_and_belongs_to_many :holding_institutions
  has_many :collections, dependent: :destroy
  has_many :items, through: :collections
  has_and_belongs_to_many :users
  #endregion

  # region callbacks
  after_update :reindex_child_values
  #endregion

  #region validations
  validates_presence_of :title
  validates_uniqueness_of :slug
  #endregion

  #region uploader
  mount_uploader :image, ImageUploader
  # endregion

  #region scopes
  scope :updated_since, lambda { |since| where('updated_at >= ?', since) }
  scope :are_public, -> { where(public: true) }

  scope :text_search, ->(query) do
    next if query.blank?
    search_for query
  end

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^id/ then order "repositories.id #{direction}"
    when /^slug/ then order "repositories.slug #{direction}"
    when /^title/ then order "repositories.title #{direction}"
    when /^public/ then order("repositories.public #{direction}")
    when /^collections_count/ then order("repositories.collections_count #{direction}")
    when /^created_at/ then order "repositories.created_at #{direction}"
    when /^updated_at/ then order "repositories.updated_at #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :public_state, lambda { |active|
    case active
    when 'yes' then where(public: true)
    when 'no' then where(public: false)
    when '' then next
    else
      raise(ArgumentError, "Invalid Active option: #{active}")
    end
  }
  # endregion

  #region Setup for pg_search and filterrific
  pg_search_scope :search_for,
                  against: [:slug, :title],
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'updated_at_desc' },
    available_filters: %i[sorted_by text_search public_state for_portals]
  )
  #endregion

  #region class_methods
  def self.index_query_fields
    %w[].freeze
  end

  def self.options_for_select
    order(Arel.sql 'LOWER(title)').map { |r| [r.title_with_slug, r.id] }
  end

  def self.public_states
    [['Public or Not Public', ''], %w[Public yes], ['Not Public', 'no']]
  end
  #endregion

  #region public_methods
  def record_id
    slug
  end

  def display?
    public?
  end

  def title_public_state
    state = public ? 'Public' : 'Not Public'
    "#{title} (#{state})"
  end

  def title_with_slug
    "#{title} (#{slug})"
  end
  # endregion

  # region private_methods
  private

  def reindex_child_values
    if saved_change_to_slug? || saved_change_to_title? || saved_change_to_public?
      queue_reindex_collections
      queue_reindex_items
    end
    true
  end

  def queue_reindex_collections
    Resque.enqueue(Reindexer, 'Collection', collections.pluck(:id)) if collections.any?
  end

  def queue_reindex_items
    if items.any?
      Resque.enqueue(Reindexer, 'Item', items.pluck(:id))
      serial_ids = items.pluck(:serial_id).uniq.compact
      Resque.enqueue(Reindexer, 'Serial', serial_ids) if serial_ids.any?
    end
  end
  # endregion
end
