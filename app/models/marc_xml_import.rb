# frozen_string_literal: true

# represent a job to import MARC XML data
class MarcXmlImport < ApplicationRecord
  #region includes
  include PgSearch::Model
  #endregion

  #region relationships
  belongs_to :user
  #endregion

  #region validations
  validates :title, presence: true
  validates :title, uniqueness: true
  validates :file, presence: true
  #endregion

  mount_uploader :file, XmlUploader

  #region Setup for pg_search and filterrific
  pg_search_scope :search_for,
                  against: %i[title description],
                  associated_against: {
                    user: %i[email]
                  },
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'finished_at_desc' },
    available_filters: %i[sorted_by text_search for_user for_status]
  )
  #endregion

  #region scopes
  scope :finished,   -> { where('finished_at IS NOT NULL') }
  scope :pending,     -> { where('finished_at IS NULL') }

  scope :text_search, ->(query) { search_for query }
  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^id/ then order "marc_xml_imports.id #{direction}"
    when /^title/ then order "marc_xml_imports.title #{direction}"
    when /^queued_at/ then order "marc_xml_imports.queued_at #{direction}"
    when /^updated_at/ then order "marc_xml_imports.updated_at #{direction}"
    when /^finished_at/ then order "marc_xml_imports.finished_at #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end

  }

  scope :for_user, -> (user_id) {
    next if user_id.blank?
    where(user_id: user_id)
  }

  scope :for_status, -> (status) {
    next if status.blank?

    case status
    when 'finished'
      finished
    when 'pending'
      pending
    else
      raise(ArgumentError, "Invalid status option: #{status}")
    end
  }

  #endregion

  #region class_methods
  def self.sort_options
    [
      ['Recently Finished', 'finished_at_desc'],
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  def self.status_states
    [%w[Finished finished], %w[Pending pending]]
  end
  #endregion

  # region public methods

  def success?
    results&.dig('status') == 'success'
  end

  def partial_failure?
    results&.dig('status') == 'partial failure'
  end

  def failed?
    results&.dig('status') == 'failed'
  end

  def result_records
    results&.dig('records')
  end

  def filename
    original_filename.presence || file_identifier
  end

  def self.options_for_select
    order('id DESC').map { |e| [e.date_title, e.id] }
  end

  def date_title
    "#{created_at.strftime("%F %T")} - #{title}"
  end

  #endregion
end
