class SerialGeographicLocation < ApplicationRecord
  belongs_to :serial
  belongs_to :geographic_location
  validates_uniqueness_of :geographic_location_id, scope: :serial_id
  accepts_nested_attributes_for :geographic_location
end