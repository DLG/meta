# frozen_string_literal: true

# represent an instance of an ingest of fulltext data
class FulltextIngest < ApplicationRecord
  #region includes
  include PgSearch::Model
  #endregion

  #region relationships
  belongs_to :user
  #endregion

  #region validations
  validates :title, presence: true
  validates :title, uniqueness: true
  validates :file, presence: true
  #endregion

  mount_uploader :file, FulltextUploader

  #region Setup for pg_search and filterrific
  pg_search_scope :search_for,
                  against: %i[title description],
                  associated_against: {
                    user: %i[email]
                  },
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'finished_at_desc' },
    available_filters: %i[sorted_by text_search for_user for_status for_undone]
  )
  #endregion

  #region scopes
  scope :finished_ingests,   -> { where('finished_at IS NOT NULL') }
  scope :pending_ingests,     -> { where('finished_at IS NULL') }
  scope :undone_ingests,   -> { where('undone_at IS NOT NULL') }
  scope :not_undone_ingests,     -> { where('undone_at IS NULL') }

  scope :text_search, ->(query) { search_for query }
  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^id/ then order "fulltext_ingests.id #{direction}"
    when /^title/ then order "fulltext_ingests.title #{direction}"
    when /^queued_at/ then order "fulltext_ingests.queued_at #{direction}"
    when /^updated_at/ then order "fulltext_ingests.updated_at #{direction}"
    when /^finished_at/ then order "fulltext_ingests.finished_at #{direction}"
    when /^undone_at/ then order "fulltext_ingests.undone_at #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end

  }

  scope :for_user, -> (user_id) {
    next if user_id.blank?
    where(user_id: user_id)
  }

  scope :for_status, -> (status) {
    next if status.blank?

    case status
    when 'finished'
      finished_ingests
    when 'pending'
      pending_ingests
    else
      raise(ArgumentError, "Invalid status option: #{status}")
    end
  }

  scope :for_undone, -> (status) {
    next if status.blank?

    case status
    when 'undone'
      undone_ingests
    when 'not_undone'
      not_undone_ingests
    else
      raise(ArgumentError, "Invalid undone option: #{status}")
    end
  }
  #endregion

  #region class_methods
  def self.sort_options
    [
      ['Recently Finished', 'finished_at_desc'],
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  def self.status_states
    [%w[Finished finished], %w[Pending pending]]
  end

  def self.undone_states
    [%w[Undone undone], ['Not Undone', 'not_undone']]
  end
  #endregion

  # region public methods
  def undo
    affected = Item.where('record_id IN (?)', modified_record_ids)
    begin
      affected.update_all(
        fulltext: nil, updated_at: Time.zone.now
      )
    rescue StandardError => e
      results['status'] = 'undo failed'
      results['message'] = e.message
      return false
    end
    affected_ids = affected.pluck :id
    Resque.enqueue(Reindexer, 'Item', affected_ids) if affected_ids.any?
    self.undone_at = Time.zone.now
    save
  end

  def success?
    results['status'] == 'success'
  end

  def succeeded
    results['files']['succeeded']
  end

  def failed?
    results && results['status'] == 'failed'
  end

  def failed
    results['files']['failed']
  end

  def partial_failure?
    results['status'] == 'partial failure'
  end

  def processed_files
    return nil unless results.key? 'files'

    results['files']
  end

  def modified_record_ids
    Item.where(id: succeeded).pluck(:record_id)
  end
  #endregion
end
