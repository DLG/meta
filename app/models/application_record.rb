# base app model
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  # Note: Scopes created with this method don't work with with ActiveRecord's .count() unless you explicitly
  #   provide a column. E.g., .count(:id)
  # That's because these scopes involve a .select('geographic_locations.*', some.expression.as('another_column')) and
  #   Postgres doesn't support queries like "SELECT COUNT(geographic_locations.*, another_column) FROM ..."
  def self.count_column_scopes_for(model, as:, **kwargs)
    with_scope = scope "with_#{as}".to_sym, -> {
      _with_counts_for model, as: as, **kwargs
    }
    scope "by_#{as}".to_sym, -> {
      send(with_scope).reorder("#{as} desc")
    }
  end

  # Example usage: Collection.with_counts_for(Item.where(...), as: 'item_count')
  scope :with_counts_for, ->(relation, kwargs = {}) {
    _with_counts_for relation, **kwargs
  }
  def self._with_counts_for(relation, as:, foreign_key: "#{name&.underscore}_id".to_sym)
    grouped = relation.select(foreign_key, relation.arel_table[:id].count.as(as.to_s))
                   .group(foreign_key).arel.as("grouped_for_#{as}")
    joins(arel_table.outer_join(grouped)
                    .on(arel_table[:id].eq grouped[foreign_key])
                    .join_sources).select(
      "#{table_name}.*",
      Arel::Nodes::NamedFunction.new(
        'coalesce',
        [grouped[as.to_sym], 0]
      ).as(as.to_s))
  end

  #A Negated select statement
  scope :select_without, lambda{|*columns|
    cols = columns.flatten.reject(&:blank?).map(&:to_s)
    next if cols.empty?

    select(self.column_names - cols)
  }

end