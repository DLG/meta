# frozen_string_literal: true

# represent an occurrence of a remediation action
class RemediationAction < ApplicationRecord
  #region includes
  include PgSearch::Model
  # endregion

  #region relationships
  belongs_to :user
  #endregion

  # region class_methods
  def self.allowed_fields # (needs to be defined before validations)
    %w[
      dcterms_publisher
      dcterms_creator
      dcterms_contributor
      dcterms_subject
      dlg_subject_personal
      dc_date
      dcterms_temporal
      dc_format
      dc_right
      dcterms_type
      dc_relation
      dcterms_medium
      dcterms_language
    ]
  end

  def self.sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  def self.status_states
    [%w[Performed performed], ['Not Performed',  'not_performed']]
  end
  # endregion

  #region validations
  validates :old_text, presence: true
  validates :new_text, presence: true
  validates :field, presence: true
  validates_inclusion_of :field, in: allowed_fields
  #endregion


  #region scopes
  scope :performed,   -> { where('performed_at IS NOT NULL') }
  scope :not_performed,     -> { where('performed_at IS NULL') }
  scope :text_search, ->(query) { where('id in (:matches)', matches: search_for(query).select(:id)) }

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^id/ then order "remediation_actions.id #{direction}"
    when /^field/ then order "remediation_actions.field #{direction}"
    when /^old_text/ then order "remediation_actions.old_text #{direction}"
    when /^new_text/ then order "remediation_actions.new_text #{direction}"
    when /^performed_at/ then order "remediation_actions.performed_at #{direction}"
    when /^created_at/ then order "remediation_actions.created_at #{direction}"
    when /^updated_at/ then order "remediation_actions.updated_at #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }
  scope :for_field, lambda { |field|
    next unless field
    where(field: field)
  }
  scope :for_user, -> (user_id) {
    next if user_id.blank?
    where(user_id: user_id)
  }
  scope :for_status, -> (status) {
    next if status.blank?

    case status
    when 'performed'
      performed
    when 'not_performed'
      not_performed
    else
      raise(ArgumentError, "Invalid status option: #{status}")
    end
  }
  # endregion

  #region Setup for pg_search and filterrific
  pg_search_scope :search_for,
                  against: [:field, :old_text, :new_text],
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'id_desc' },
    available_filters: %i[sorted_by text_search for_field for_user for_status]
  )
  #endregion

end
