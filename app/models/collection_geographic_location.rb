class CollectionGeographicLocation < ApplicationRecord
  belongs_to :collection
  belongs_to :geographic_location
  validates_uniqueness_of :geographic_location_id, scope: :collection_id

  accepts_nested_attributes_for :geographic_location
end