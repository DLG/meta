# frozen_string_literal: true

# represent an instance of an ingest of fulltext data
class PageIngest < ApplicationRecord
  #region includes
  include PgSearch::Model
  #endregion

  #region relationships
  belongs_to :user
  #endregion

  #region validations
  validates :title, presence: true
  validates :title, uniqueness: true
  #endregion

  mount_uploader :file, PageJsonUploader

  #region Setup for pg_search and filterrific
  pg_search_scope :search_for,
                  against: %i[title description],
                  associated_against: {
                    user: %i[email]
                  },
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'finished_at_desc' },
    available_filters: %i[sorted_by text_search for_user for_status]
  )
  #endregion

  #region scopes
  scope :finished_ingests,   -> { where('finished_at IS NOT NULL') }
  scope :pending_ingests,     -> { where('finished_at IS NULL') }

  scope :text_search, ->(query) { search_for query }
  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^id/ then order "page_ingests.id #{direction}"
    when /^title/ then order "page_ingests.title #{direction}"
    when /^queued_at/ then order "page_ingests.queued_at #{direction}"
    when /^updated_at/ then order "page_ingests.updated_at #{direction}"
    when /^finished_at/ then order "page_ingests.finished_at #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end

  }

  scope :for_user, -> (user_id) {
    next if user_id.blank?
    where(user_id: user_id)
  }

  scope :for_status, -> (status) {
    next if status.blank?

    case status
    when 'finished'
      finished_ingests
    when 'pending'
      pending_ingests
    else
      raise(ArgumentError, "Invalid status option: #{status}")
    end
  }
  #endregion

  #region class_methods
  def self.sort_options
    [
      ['Recently Finished', 'finished_at_desc'],
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  def self.status_states
    [%w[Finished finished], %w[Pending pending]]
  end

  def self.quick_ingest!(items, user:, title: nil)
    ingest_json = items.map { |item| {
      id: item.record_id,
      infer_pages: true
    }}.to_json

    i = 2
    unique_title = title || "Quick page ingest"
    while where(title: unique_title).any?
      unique_title = "#{title} \##{i}"
      i += 1
    end
    pi = self.create! page_json: ingest_json,
                      user: user,
                      title: unique_title,
                      queued_at: Time.zone.now
    Resque.enqueue(PageProcessor, pi.id)
    pi
  end
  #endregion

  # region public methods
  def results
    results_json
  end

  def success?
    results_json['status'] == 'success'
  end

  def succeeded
    results_json['added']
  end

  def updated
    results_json['updated']
  end

  def failed
    results_json['errors']
  end

  def failed?
    results_json['status'] == 'failed'
  end

  def partial_failure?
    results_json['status'] == 'partial failure'
  end
  #endregion
end
