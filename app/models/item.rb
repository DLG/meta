class Item < ApplicationRecord

  #region includes
  include ApplicationHelper
  include ItemType
  include IndexFilterable
  include HasEducatorResources
  include PgSearch::Model
  #endregion

  #region relationships
  belongs_to :collection, counter_cache: true
  has_one :repository, through: :collection
  has_many :batch_items
  has_many :item_geographic_locations, dependent: :destroy
  has_many_geographic_locations_through :item_geographic_locations
  has_many :pages, -> { ordered_by_number }
  has_many :item_names, dependent: :destroy
  accepts_nested_attributes_for :item_names, allow_destroy: true
  has_many :names, through: :item_names
  has_many :item_events, dependent: :destroy
  has_many :events, through: :item_events
  belongs_to :serial, optional: true
  #endregion

  # region callbacks
  before_save :set_record_id
  before_save :set_local_edm_is_shown
  after_save :reindex_serial
  after_update :record_id_change_in_solr
  after_save :check_for_event_updates
  after_save :connect_names_callback
  before_destroy :save_serial_id
  after_destroy :reindex_serial_after_destroy
  #endregion

  #region validations
  validates_uniqueness_of :slug, scope: :collection_id
  validates_presence_of :collection
  #endregion

  #region scopes
  scope :for_indexing, lambda {
                         includes(:portals, :repository, :holding_institutions, :geographic_locations, :events, :names,
                                  :serial, collection: :holding_institutions,  pages: :item)
                       }
  scope :updated_since, ->(since) { where('updated_at >= ?', since) }
  scope :are_public, -> { where(public: true) }
  scope :are_local, -> { where(local: true) }
  scope :has_pages, -> { where(id: Page.select(:item_id)) }
  scope :field_match, -> (field, to_match) { where("'#{to_match.downcase}' = ANY (lower(items.#{field}::text)::text[])") }
  scope :field_match_wildcard, -> (field, to_match) do
    subquery = Item.default_scoped.select(:id, "UNNEST(#{field}) as unnested")
    where(id: Item.default_scoped.where('unnested ILIKE ?', "#{to_match}%").from(subquery).select(:id))
  end
  scope :for_name, -> (name) do
    subquery = Item.select(:id, "UNNEST(dlg_subject_personal) as unnested")
    Item.where(id: Item.where('LOWER(unnested) SIMILAR TO ?', "#{name.downcase}( ?--%)?").from(subquery).select(:id))
  end
  scope :educator_resources, -> { field_match('dcterms_medium', 'instructional materials') }
  scope :displayed, -> { joins(collection: :repository).where(public: true, collections: {public: true, repositories: {public: true}}) }
  scope :not_displayed, -> { joins(collection: :repository).where.not(public: true, collections: {public: true, repositories: {public: true}}) }
  scope :text_search, ->(query) do
    next if query.blank?
    search_for query
  end

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^id/ then order "items.id #{direction}"
    when /^slug/ then order "items.slug #{direction}"
    when /^title/ then order "items.dcterms_title #{direction}"
    when /^public/ then order("items.public #{direction}")
    when /^valid/ then order("items.valid_item #{direction}")
    when /^pages_count/ then order("items.pages_count #{direction}")
    when /^created_at/ then order "items.created_at #{direction}"
    when /^updated_at/ then order "items.updated_at #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :public_state, lambda { |active|
    case active
    when 'yes' then where(public: true)
    when 'no' then where(public: false)
    when '' then next
    else
      raise(ArgumentError, "Invalid Active option: #{active}")
    end
  }

  scope :valid_state, lambda { |valid|
    case valid
    when 'yes' then where(valid_item: true)
    when 'no' then where(valid_item: false)
    when '' then next
    else
      raise(ArgumentError, "Invalid Valid option: #{active}")
    end
  }

  scope :display_state, lambda { |display_state|
    case display_state
    when 'yes' then displayed
    when 'no' then not_displayed
    when '' then next
    else
      raise(ArgumentError, "Invalid Valid option: #{display_state}")
    end
  }

  scope :for_collection, lambda { |*collection_ids|
    ids = collection_ids.flatten.reject(&:blank?)
    next if ids.empty?

    where(collection_id: ids)
  }

  scope :for_serials, lambda { |*serial_ids|
    ids = serial_ids.flatten.reject(&:blank?)
    next if ids.empty?
    where(serial_id: ids)
  }
  #endregion

  #region papertrail
  has_paper_trail versions: {class_name: 'ItemVersion'}, ignore: [:record_id]
  #endregion

  #region solr_fields
  searchable do
    integer(:collection_id) { collection.id }
    integer(:repository_id) { repository.id }
    integer(:serial_id) { serial&.id }
    string(:class_name, stored: true) { self.class }
    string(:local, stored: true) { local? ? '1' : '0' }
    string(:slug, stored: true)
    string(:record_id, stored: true)
    # set empty proxy id4 field so sunspot knows about it
    # value is set prior to save
    # sunspot search will not work without this, but indexing will
    # see monkeypatch @ config/initializers/sunspot_indexer_id.rb
    string(:sunspot_id, stored: true) { '' }
    string(:collection_slug, stored: true) { collection.slug }
    string(:serial_slug, stored: true) { serial&.slug }
    string(:collection_record_id, stored: true, multiple: true) { collection_record_ids }
    string(:repository_slug, stored: true) { repository.slug }
    string(:collection_titles, stored: true, multiple: true) { collection_titles }
    string(:serial_titles, stored: true, multiple: true) { serial&.dcterms_title }
    string(:call_numbers, stored: true, multiple: true) { call_numbers }
    string(:external_identifiers_json, stored: true){external_identifiers.to_json}
    string(:repository_name, stored: true, multiple: true) { repository_titles } # TODO: consider for removal
    string(:portals, stored: true, multiple: true) { portal_codes }
    string(:portal_names, stored: true, multiple: true) { portal_names }
    string(:institution_slug, stored: true, multiple: true) { institution_slugs }
    string(:event_slug, stored: true, multiple: true) { self.events.pluck(:slug) }
    string(:event_title, stored: true, multiple: true) { self.events.pluck(:title) }
    string(:name_slug, stored: true, multiple: true) { self.names.pluck(:slug) }
    string(:name_authoritative, stored: true, multiple: true) { self.names.pluck(:authoritative_name) }

    boolean(:dpla)
    boolean(:public)
    boolean(:valid_item)
    boolean(:display) { display? }
    boolean(:fulltext_present) { pages_or_item_fulltext.present? }
    boolean(:educator_resource) do
      dcterms_medium.compact.map(&:downcase).include? 'instructional materials'
    end

    string(:valid_item, stored: true) { valid_item ? 'Yes' : 'No' }
    string(:display, stored: true) { display? ? 'Yes' : 'No' }
    string(:dpla, stored: true) { dpla ? 'Yes' : 'No' }
    string(:public, stored: true) { public ? 'Yes' : 'No' }
    string(:fulltext_present, stored: true) { pages_or_item_fulltext.present? ? 'Yes' : 'No' }
    string(:educator_resource_mediums, stored: true, multiple: true)

    # *_display (not indexed, stored, multivalued)
    string :dcterms_provenance,             as: 'dcterms_provenance_display',             multiple: true
    string :dcterms_title,                  as: 'dcterms_title_display',                  multiple: true
    string :dcterms_creator,                as: 'dcterms_creator_display',                multiple: true
    string :dcterms_contributor,            as: 'dcterms_contributor_display',            multiple: true
    string :dcterms_subject,                as: 'dcterms_subject_display',                multiple: true
    string :dcterms_description,            as: 'dcterms_description_display',            multiple: true
    string :dcterms_publisher,              as: 'dcterms_publisher_display',              multiple: true
    string :edm_is_shown_at,                as: 'edm_is_shown_at_display',                multiple: true
    string :edm_is_shown_by,                as: 'edm_is_shown_by_display',                multiple: true
    string :dcterms_identifier,             as: 'dcterms_identifier_display',             multiple: true
    string :dc_date,                        as: 'dc_date_display',                        multiple: true
    string :dcterms_temporal,               as: 'dcterms_temporal_display',               multiple: true
    string(:dcterms_spatial, as: 'dcterms_spatial_display', multiple: true) {geolocations_to_dcspatial}
    string :dc_format,                      as: 'dc_format_display',                      multiple: true
    string :dcterms_is_part_of,             as: 'dcterms_is_part_of_display',             multiple: true
    string :dc_right,                       as: 'dc_right_display',                       multiple: true
    string :dcterms_rights_holder,          as: 'dcterms_rights_holder_display',          multiple: true
    string :dcterms_bibliographic_citation, as: 'dcterms_bibliographic_citation_display', multiple: true
    string :dlg_local_right,                as: 'dlg_local_right_display',                multiple: true
    string :dlg_subject_personal,           as: 'dlg_subject_personal_display',           multiple: true
    string :dc_relation,                    as: 'dc_relation_display',                    multiple: true
    string :dcterms_type,                   as: 'dcterms_type_display',                   multiple: true
    string :dcterms_medium,                 as: 'dcterms_medium_display',                 multiple: true
    string :dcterms_extent,                 as: 'dcterms_extent_display',                 multiple: true
    string :dcterms_language,               as: 'dcterms_language_display',               multiple: true

    # Primary Search Fields (multivalued, indexed, stemming/tokenized)
    text :dc_date
    text :dcterms_title
    text :dcterms_creator
    text :dcterms_contributor
    text :dcterms_subject
    text :dcterms_description
    text :dcterms_publisher
    text :dcterms_temporal
    text(:dcterms_spatial){ geolocations_to_dcspatial}
    text :dcterms_is_part_of
    text :edm_is_shown_at
    text :edm_is_shown_by
    text :dcterms_identifier
    text :dcterms_rights_holder
    text :dlg_subject_personal
    text :dcterms_provenance
    text :collection_titles
    text(:serial_titles) {serial&.dcterms_title }

    # Full Text
    text :pages_or_item_fulltext, stored: true, as: 'fulltext_texts'

    # Blacklight 'Required' fields # TODO do we use them properly in DLG?
    string(:title, as: 'title') { dcterms_title.first ? dcterms_title.first : slug }
    string(:format, as: 'format') { dc_format.first ? dc_format.first : '' }

    # sort fields
    string(:collection_sort,  as: 'collection_sort') { collection.title.downcase.gsub(/^(an?|the)\b/, '') }
    string(:title_sort,       as: 'title_sort') { dcterms_title.first ? dcterms_title.first.downcase.gsub(/^(an?|the)\b/, '') : '' }
    string(:creator_sort,     as: 'creator_sort') { dcterms_creator.first ? dcterms_creator.first.downcase.gsub(/^(an?|the)\b/, '') : '' }
    string(:yyyy_mm_dd_sort,  as: 'yyyy_mm_dd_sort') { IndexDates.index_yyyy_mm_dd_sort(dc_date) }
    integer(:year,            as: 'year', trie: true) { DateIndexer.new.get_sort_year(dc_date) }

    # facet fields
    string(:year_facet, as: 'year_facet', multiple: true) { IndexDates.index_years(dc_date) }
    string(:counties, as: 'counties_facet', multiple: true)
    string(:us_states, as: 'us_states_facet', multiple: true)

    # datetimes
    time(:created_at, stored: true, trie: true)
    time(:updated_at, stored: true, trie: true)

    # spatial stuff
    string(:coordinates, as: 'coordinates', multiple: true)
    string(:geojson, as: 'geojson', multiple: true)
    string(:placename, as: 'placename', multiple: true)
    string(:dpla_spatial_json, stored: true) { dpla_spatial_json }

    # iiif Stuff
    string(:iiif_ids, stored: true, multiple: true) { pages.map(&:iiif_identifier) }
    string(:iiif_item_types, stored: true, multiple: true) { pages.map(&:media_type) }
    string(:iiif_file_types, stored: true, multiple: true) { pages.map(&:file_type) }
    string(:iiif_titles, stored: true, multiple: true) { pages.map(&:iiif_title) }
    string(:iiif_heights, stored: true, multiple: true){ pages.map(&:iiif_height) }
    string(:iiif_widths, stored: true, multiple: true){ pages.map(&:iiif_width) }
    string(:iiif_durations, stored: true, multiple: true) { pages.map(&:duration) }
    string(:iiif_media_urls, stored: true, multiple: true) { pages.map(&:media_url) }
    string(:iiif_media_thumbnail_urls, stored: true, multiple: true) { pages.map(&:media_thumbnail_url) }
    string :iiif_manifest_url, stored: true
    string(:iiif_attribution, stored: true) { collection.holding_institutions.first&.display_name }

    string :holding_institution_image, stored: true
    # TODO: Remove year_debug when year_facet values working correctly
    string(:year_debug, stored: true, multiple: true) { IndexDates.index_years(dc_date) }

  end
  #endregion

  #region Setup for pg_search and filterrific
  pg_search_scope :search_for,
                  against: [:dcterms_title, :record_id],
                  using: {
                    tsearch: {
                      dictionary: 'simple',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'id_desc' },
    available_filters: %i[sorted_by text_search public_state valid_state display_state for_portals for_collection for_serials]
  )
  #endregion

  #region class_methods
  def self.index_query_fields
    %w[collection_id public valid_item].freeze
  end

  def self.sort_options
    [
      ['Recently Created', 'id_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  def self.public_states
    [['Any', ''], %w[Public yes], ['Not Public', 'no']]
  end

  def self.display_states
    [['Any', ''], %w[Displayed yes], ['Not Displayed', 'no']]
  end

  def self.valid_states
    [['Any', ''], %w[Valid yes], %w[Invalid no]]
  end
  #endregion

  #region public_methods
  def holding_institution_image
    collection.holding_institution_image
  end

  def pages_or_item_fulltext
    pages_fulltext || fulltext
  end

  def pages_fulltext
    txt = pages.map(&:fulltext).reject(&:blank?).join("\n")
    return nil if txt.empty?

    txt
  end

  def page_urls
    # because IIIF spec doesn't support PDF pagination,
    # provide only url for first page if the document is a PDF
    return [pages.first.iiif_info_link] if pdf?

    urls = pages.map(&:iiif_info_link).reject(&:blank?)
    return nil unless urls.any?

    urls
  end

  def pdf?
    # items is considered a PDF here if any page is a PDF
    pages.map(&:pdf?).include? true
  end

  def display?
     public && collection.public && repository.public
  end

  def facet_years
    all_years = []
    dc_date.each do |date|
      all_years << date.scan(/[0-2][0-9][0-9][0-9]/)
    end
    all_years.flatten.uniq
  end

  def collection_titles
    (other_collection_titles << collection.title).reverse.uniq
  end

  def collection_record_ids
    (other_collection_record_ids << collection.record_id).reverse.uniq
  end

  def repository_titles
    (other_repository_titles << repository.title).reverse.uniq
  end

  def to_xml(options = {})
    new_options = {
      dasherize: false,
      except: %i[collection_id record_id serial_id other_collections valid_item
                 has_thumbnail created_at updated_at fulltext
                 legacy_dcterms_provenance, legacy_dcterms_spatial
                 source_data],
      methods: [:dcterms_provenance, :dcterms_spatial],
      include: {
        other_colls: {
          skip_types: true,
          only: [:record_id]
        },
        collection: { only: :record_id },
        serial: {only: :slug },
        portals: { only: [:code] },
      }
    }
    super(options.merge!(new_options))
  end

  def as_json(options = {})
    new_options = {
      except: [:legacy_dcterms_provenance, :legacy_dcterms_spatial],
      methods: [:dcterms_provenance, :dcterms_spatial]
    }
    super(options.merge!(new_options))
  end

  def to_batch_item

    attributes = self.attributes.except(
      'id',
      'created_at',
      'updated_at',
      'valid_item',
      'pages_count'
    )

    batch_item = BatchItem.new attributes
    batch_item.item = self
    batch_item

  end

  def other_collection_titles
    other_colls.map(&:title)
  end

  def other_collection_record_ids
    other_colls.map(&:record_id)
  end

  def other_colls
    Collection.find(other_collections)
  end

  def parent
    collection
  end

  def iiif_manifest_url
    if iiif_partner_url.present? && iiif_partner_url_enabled
      iiif_partner_url
    elsif pages.any?
      "#{portal_host}/record/#{record_id}/presentation/manifest.json"
    else
      nil
    end
  end

  def connect_names_callback
    return unless saved_changes.include? :dlg_subject_personal

    connect_names
  end

  def connect_names
    names = []
    name_lookup = Name.lookup_hash

    dlg_subject_personal.each do |subject|
      # we want to drop any sub-subjects that follow double hyphens, e.g.,
      # - Abernathy, Barbara, 1941- --Political activity (note the space following 1941-)
      # - Abernathy, Ralph, 1926-1990--Homes and haunts
      name_portion = subject.sub(/ ?--.*/,"")

      # the following should always find only one name,
      # because authoritative names are unique
      name = name_lookup[name_portion.downcase]
      names << name if name
    end
    self.names = names  # could be []
  end

  def check_for_event_updates
    if portals.pluck(:code).include?("crdl")
      update_events if subjects_changed?
    else
      self.events = [] # not crdl
    end
  end

  def subjects_changed?
    checking = %w[dcterms_subject dlg_subject_personal]
    checking - saved_changes.keys != checking
  end

  def update_events
    events = []
    Event.all.each do |event|
      if item_matches_event?(dcterms_subject, event.dcterms_subject_matches)
        events << event
      elsif item_matches_event?(dlg_subject_personal, event.dlg_subject_personal_matches)
        events << event
      end
    end
    self.events = events # could be []
  end

  def item_matches_event?(item_values, event_values)
    return false if item_values.blank? || event_values.blank?
    event_values.each do |match|
      if match =~ /^(.*)[*]$/
        prefix = $1.downcase
        item_values.map { |val| return true if val&.downcase =~ /^#{prefix}.*/ }
      else
        item_values.map { |val| return true if val&.downcase == match.downcase }
      end
    end
    false
  end

  # Returns a Sunspot Hit object for the item that represents the current values in SOLR
  def solr_hit
    search = Item.search do
      adjust_solr_params do |params|
        params[:q] = ""
      end
      with(:record_id, record_id)
    end

    return nil if search.total < 1

    search.hits.first
  end

  def solr_stored_hash
    hit = solr_hit

    return {} if hit.nil?

    solr_hash = hit.instance_variable_get(:@stored_values)

    # Truncate fulltext
    return solr_hash unless solr_hash["fulltext_texts"]&.any?

    solr_hash["fulltext_texts"] = solr_hash["fulltext_texts"].map { |text| text.truncate(100) }

    solr_hash

  end

  def thumbnail_url
    "https://dlg.galileo.usg.edu/#{repository.slug}/#{collection.slug}/do-th:#{slug}"
  end

  def reindex_serial
    return unless serial && saved_change_to_public?

    Resque.enqueue(Reindexer, 'Serial', [serial.id])
  end

  def save_serial_id
    return unless serial

    @associated_serial_id = serial.id
  end

  def reindex_serial_after_destroy
    return unless @associated_serial_id

    Resque.enqueue(Reindexer, 'Serial', [@associated_serial_id])
  end

  def call_numbers
    external_call_numbers = external_identifiers.select{|identifier| identifier['type'] == 'Call Number'}
    call_numbers = external_call_numbers.map{|identifier| normalize_call_number identifier['value']}

    serial ? call_numbers + serial.call_numbers : call_numbers
  end

  def set_local_edm_is_shown
    return unless self.local

    self.edm_is_shown_at << "#{portal_host}/record/#{record_id}" if self.edm_is_shown_at.empty?
    self.edm_is_shown_by << "https://dlg.galileo.usg.edu/do:#{record_id}" if self.edm_is_shown_by.empty?

  end


  def source_xml
    # Nokogiri returns nil if source_data is nil or is not XML
    Nokogiri::XML(source_data)
  end

  #endregion

  # region private_methods
  private

  def reindex
    Sunspot.index! self
  end

  def set_record_id
    self.record_id = "#{repository.slug}_#{collection.slug}_#{slug}".downcase
  end

  def other_repository_titles
    Collection.find(other_collections).map(&:repository_title)
  end

  def record_id_change_in_solr
    return true unless saved_changes.include? :record_id
    previous_record_id = saved_changes[:record_id][0]
    Sunspot.remove!(OpenStruct.new(record_id: previous_record_id))
  end
  # endregion

end

