class Event < ApplicationRecord
  #region includes
  include ApplicationHelper
  include IndexFilterable
  include PgSearch::Model
  #endregion

  # region callbacks
  after_save :update_associated_items
  before_destroy :save_associated_item_ids # This needs to come before the dependent: :destroy on item_events relationship
  after_destroy :reindex_associated_items
  #endregion

  #region relationships
  has_many :item_events, dependent: :destroy # Creates a callback
  has_many :items, through: :item_events
  #endregion

  #region validations
  validates_presence_of :slug
  validates_presence_of :title
  validates_presence_of :dcterms_temporal
  validates_uniqueness_of :slug
  #endregion

  #region scopes
  scope :for_indexing, lambda {
  }
  scope :are_public, -> { where(public: true) }
  #endregion

  #region Setup for pg_search and filterrific
  pg_search_scope :search_for,
                  against: [:slug, :title, :description],
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'updated_at_desc' },
    available_filters: %i[sorted_by text_search public_state]
  )
  #endregion

  #region scopes for filterrific
  scope :text_search, ->(query) { search_for query }

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^id/ then order "events.id #{direction}"
    when /^slug/ then order "events.slug #{direction}"
    when /^title/ then order "events.title #{direction}"
    when /^created_at/ then order "events.created_at #{direction}"
    when /^updated_at/ then order "events.updated_at #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :public_state, lambda { |active|
    case active
    when 'yes' then where(public: true)
    when 'no' then where(public: false)
    when '' then next
    else
      raise(ArgumentError, "Invalid Active option: #{active}")
    end
  }
  #endregion

  #region Filter values for views
  def self.sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  def self.public_states
    [['Public or Not Public', ''], %w[Public yes], ['Not Public', 'no']]
  end
  #endregion

  #region solr_fields
  searchable do
    # Blacklight 'Required' fields #
    string(:title, as: 'title')

    string(:class_name, stored: true) { self.class }
    string(:slug, stored: true)
    string(:dcterms_temporal, stored: true, multiple: true)
    string(:description, stored: true)
    string(:canned_search, stored: true)
    boolean(:public, stored: true)
    boolean(:display) { display? }
    string(:portals, stored: true, multiple: true) { ['crdl'] }

    # datetimes
    time(:created_at, stored: true, trie: true)
    time(:updated_at, stored: true, trie: true)

  end
  #endregion

  # region public methods
  def display?
    public
  end

  def update_associated_items
    unless matches_changed?
      Sunspot.index! items.for_indexing
      return
    end

    new_item_match_ids = matched_item_ids
    current_item_ids = items.pluck(:id)

    #Add new matched items
    item_ids_to_add = new_item_match_ids.difference current_item_ids
    new_item_event_hashes = item_ids_to_add.map { |item_id| {item_id: item_id, event_id: self.id} }
    ItemEvent.import(new_item_event_hashes, validate: false)

    # Remove unmatched items
    item_ids_to_remove = current_item_ids.difference new_item_match_ids
    ItemEvent.where(event_id: self.id, item_id: item_ids_to_remove).delete_all

    # Reindex newly associated and unmatched items
    item_ids_to_reindex = item_ids_to_add.union(item_ids_to_remove)
    Sunspot.index! Item.where(id: item_ids_to_reindex).for_indexing
  end

  def matches_changed?
    checking = %w[dcterms_subject_matches dlg_subject_personal_matches]
    checking - saved_changes.keys != checking
  end

  def matched_item_ids
    dcterms_subject_ids = subject_matches("dcterms_subject", dcterms_subject_matches)
    dlg_subject_personal_ids = subject_matches("dlg_subject_personal", dlg_subject_personal_matches)
    dcterms_subject_ids.union(dlg_subject_personal_ids)
  end

  def subject_matches(field, matches)
    item_ids = matches.map do |match|
      if match =~ /^(.*)[*]$/
        Item.for_portals("crdl").field_match_wildcard(field, $1).pluck(:id)
      else
        Item.for_portals("crdl").field_match(field, match).pluck(:id)
      end
    end
    item_ids.flatten.compact
  end

  def save_associated_item_ids
    @associated_item_ids = items.pluck(:id)
  end

  def reindex_associated_items
    Sunspot.index! Item.where(id: @associated_item_ids).for_indexing
  end
  # endregion
end
