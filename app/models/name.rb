# frozen_string_literal: true

class Name < ApplicationRecord
  #region includes
  include ApplicationHelper
  include IndexFilterable
  include Portable
  include PgSearch::Model
  include ActionView::Helpers
  #endregion

  #region callbacks
  before_validation :update_slugs, unless: :skip_before_validation?
  after_save :update_associated_items
  before_destroy :save_associated_item_ids # Needs to come before the dependent: :destroy in the item_names relationship below
  after_destroy :reindex_associated_items
  #endregion

  #region relationships
  has_many :item_names, dependent: :destroy # This dependent: :destroy creates a callback
  has_many :items, through: :item_names
  #endregion

  #region validations
  validates_presence_of :slug
  validates_presence_of :authoritative_name
  validates_presence_of :name_type
  validates_uniqueness_of :slug
  validates_uniqueness_of :legacy_slug, allow_blank: true
  validate :slugs_unique
  validate :allowed_name_types

  #endregion

  #region scopes
  scope :for_indexing, lambda {
  }
  scope :are_public, -> { where(public: true) }
  scope :text_search, ->(query) { where('id in (:matches)', matches: search_for(query).select(:id)) }

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^id/ then order "names.id #{direction}"
    when /^slug/ then order "names.slug #{direction}"
    when /^authoritative_name/ then order "names.authoritative_name #{direction}"
    when /^created_at/ then order "names.created_at #{direction}"
    when /^updated_at/ then order "names.updated_at #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :public_state, lambda { |active|
    case active
    when 'yes' then where(public: true)
    when 'no' then where(public: false)
    when '' then next
    else
      raise(ArgumentError, "Invalid Active option: #{active}")
    end
  }

  scope :for_name_type, lambda { |name_type|
    where(name_type: name_type)
  }

  scope :in_additional_slugs, ->(slugs) { where("additional_slugs::text[]          && ARRAY[:s]", s: slugs) }
  scope :in_slug,             ->(slugs) { where("string_to_array(slug, '^')        && ARRAY[:s]", s: slugs) }
  scope :in_legacy_slug,      ->(slugs) { where("string_to_array(legacy_slug, '^') && ARRAY[:s]", s: slugs) }

  #endregion

  #region Setup for pg_search and filterrific
  pg_search_scope :search_for,
                  against: [:slug, :legacy_slug, :additional_slugs, :authoritative_name,
                            :alternate_names_public, :alternate_names_not_public, :oclc_number],
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'updated_at_desc' },
    available_filters: %i[sorted_by text_search public_state for_portals for_name_type]
  )
  #endregion

  #region class_methods
  def self.sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  def self.public_states
    [['Public or Not Public', ''], %w[Public yes], ['Not Public', 'no']]
  end

  # All name type codes
  # @return [Array]
  def self.name_type_codes
    %w[person corpent]
  end

  def self.name_types_for_menus
    [
      %w[Person person],
      ['Corporate Entity', 'corpent'],
    ]
  end

  def self.normalized(name)
    name.gsub(/[[:punct:]]/, ' ') # change punctuations to spaces
        .gsub(/[^ -~]/,      ' ') # change non-ascii to spaces
        .strip                    # strip leading/trailing spaces
        .gsub(/  */,         ' ') # collapse multiple spaces
        .gsub(/ /,           '_') # change spaces to underscores
        .downcase                 # lowercase
  end

  def self.lookup_hash
    name_hash = {}
    Name.all.each do |name|
      name_hash[name.authoritative_name.downcase] = name
    end
    name_hash
  end
  #endregion

  # region public methods
  # All view type codes
  # @return [Array]
  def allowed_name_types
    return if Name.name_type_codes.include? name_type

    errors.add(:name_type, " '#{name_type}' is not a permitted name_type. Allowed types: #{Name.name_type_codes.join(", ")}")
  end

  def display?
    public
  end

  def authoritative_name_for_sort
    authoritative_name.downcase.gsub(/[^a-z0-9 ]/, '').sub(/^(?:the |an |a )/, '')
  end

  def first_char
    first_char = authoritative_name_for_sort[0]
    if first_char.match(/^[0-9]$/)
      '0-9'
    else
      first_char.upcase
    end
  end

  def update_slugs
    return true unless authoritative_name_changed?
    return true if authoritative_name.blank?
    new_slug = Name.normalized authoritative_name
    # want to remove new_slug if it had previously been there:
    self.additional_slugs = ([slug, additional_slugs].flatten.uniq - [new_slug]).compact
    self.slug = new_slug
  end

  def skip_before_validation?(value = false)
    @skip_before_validation = @skip_before_validation ? @skip_before_validation : value
  end

  def authoritative_name_changed?
    checking = ["authoritative_name"]
    checking - changes.keys != checking
  end

  def slugs_unique
    slug_in_additional_slugs
    slug_in_legacy_slug
    additional_slugs_in_slug
    additional_slugs_in_additional_slugs
    additional_slugs_in_legacy_slug
    legacy_slug_in_slug
    legacy_slug_in_additional_slugs
    legacy_slug_in_these_additional_slugs
  end

  def slug_in_additional_slugs
    found = Name.in_additional_slugs(slug).where.not(id: id)
    return unless found.any?
    errors.add(:slug, "#{slug} not unique in #{t('activerecord.attributes.name.additional_slugs')}, e.g., see #{found.first.authoritative_name} (#{found.first.id})")
  end

  def slug_in_legacy_slug
    found = Name.where(legacy_slug: slug).where.not(id: id)
    return unless found.any?
    errors.add(:slug, "#{slug} not unique in #{t('activerecord.attributes.name.legacy_slug')}, e.g., see #{found.first.authoritative_name} (#{found.first.id})")
  end

  def additional_slugs_in_slug
    return unless Name.in_slug(additional_slugs).where.not(id: id).any?
    additional_slugs.each do |slug|
      found = Name.where(slug: slug).where.not(id: id)
      if found.any?
        errors.add(:additional_slugs, "#{slug} not unique in #{t('activerecord.attributes.name.slug')}, e.g., see: #{found.first.authoritative_name} (#{found.first.id})")
        break
      end
    end
  end

  def additional_slugs_in_additional_slugs
    return unless Name.in_additional_slugs(additional_slugs).where.not(id: id).any?
    additional_slugs.each do |slug|
      found = Name.where("additional_slugs @> ?", "{#{slug}}").where.not(id: id)
      if found.any?
        errors.add(:additional_slugs, "#{slug} not unique, e.g., see: #{found.first.authoritative_name} (#{found.first.id})")
        break
      end
    end
  end

  def additional_slugs_in_legacy_slug
    return unless Name.in_legacy_slug(additional_slugs).where.not(id: id).any?
    additional_slugs.each do |slug|
      found = Name.where(legacy_slug: slug).where.not(id: id)
      if found.any?
        errors.add(:additional_slugs, "#{slug} not unique in #{t('activerecord.attributes.name.legacy_slug')}, e.g., see: #{found.first.authoritative_name} (#{found.first.id})")
        break
      end
    end
  end

  def legacy_slug_in_slug
    found = Name.where(slug: legacy_slug).where.not(id: id)
    return unless found.any?
    errors.add(:legacy_slug, "#{legacy_slug} not unique in #{t('activerecord.attributes.name.slug')}, e.g., see #{found.first.authoritative_name} (#{found.first.id})")
  end

  def legacy_slug_in_additional_slugs
    found = Name.in_additional_slugs(legacy_slug).where.not(id: id)
    return unless found.any?
    errors.add(:legacy_slug, "#{legacy_slug} not unique in #{t('activerecord.attributes.name.additional_slugs')}, e.g., see #{found.first.authoritative_name} (#{found.first.id})")
  end

  def legacy_slug_in_these_additional_slugs
    return unless additional_slugs.include? legacy_slug
    errors.add(:additional_slugs, "contains #{t('activerecord.attributes.name.legacy_slug')}: #{legacy_slug}")
  end

  def all_slugs
    slugs = [slug]
    if self.respond_to?(:additional_slugs)
      slugs += additional_slugs
    end
    if self.respond_to?(:legacy_slug)
      slugs += [legacy_slug]
    end
    slugs.uniq
  end

  # note: these respond_to? calls can be simplified once the name migrations are in production
  def all_search_names
      [authoritative_name, alternate_names_public, alternate_names_not_public].flatten.uniq
  end

  def update_associated_items
    unless saved_changes.include? :authoritative_name
      Sunspot.index! items.for_indexing
      return
    end

    new_item_ids = matched_item_ids
    current_item_ids = items.pluck(:id)

    # Add new matched items
    item_ids_to_add = new_item_ids.difference current_item_ids
    matching_item_names = item_ids_to_add.map { |item_id| {item_id: item_id, name_id: self.id} }
    ItemName.import(matching_item_names, validate: false)

    # Remove unmatched items
    item_ids_to_remove = current_item_ids.difference new_item_ids
    ItemName.where(name_id: self.id, item_id: item_ids_to_remove).delete_all

    # Reindex newly associated and unmatched items
    item_ids_to_reindex = item_ids_to_add.union item_ids_to_remove
    Sunspot.index! Item.where(id: item_ids_to_reindex).for_indexing
  end

  def matched_item_ids
    Item.for_name(authoritative_name).pluck(:id)
  end

  def save_associated_item_ids
    @associated_item_ids = items.pluck(:id)
  end

  def reindex_associated_items
    Sunspot.index! Item.where(id: @associated_item_ids).for_indexing
  end

  # endregion

  #region solr_fields
  searchable do
    string(:class_name, stored: true) { self.class }
    # Blacklight 'Required' fields #
    string(:title, as: 'title'){ authoritative_name }

    string(:slug, stored: true)
    string(:all_slugs, stored: true, multiple: true)
    string(:authoritative_name, stored: true)
    string(:alternate_names, stored: true, multiple: true) { alternate_names_public }
    string(:related_names, stored: true, multiple: true)
    string(:bio_history, stored: true)
    string(:authoritative_name_source, stored: true, multiple: true)
    string(:name_first_char, stored: true) { first_char }
    string(:source_uri, stored: true)
    string(:oclc_number, stored: true)
    string(:name_type, stored: true)
    boolean(:public, stored: true)
    boolean(:display) { display? }
    string(:portals, stored: true, multiple: true) { portal_codes }

    # datetimes
    time(:created_at, stored: true, trie: true)
    time(:updated_at, stored: true, trie: true)

    # Primary Search Fields (multivalued, indexed, stemming/tokenized)
    text(:dcterms_title) { all_search_names }
  end
  #endregion
end
