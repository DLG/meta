# represents a Holding Institution (dcterms_provenance value)
class HoldingInstitution < ApplicationRecord
  #region includes
  include IndexFilterable
  include PgSearch::Model
  include HasGeolocations
  #endregion

  #region relationships
  has_and_belongs_to_many :repositories
  has_and_belongs_to_many :collections
  has_and_belongs_to_many :items
  has_and_belongs_to_many :batch_items
  has_many :projects
  has_many :holding_institution_geographic_locations, dependent: :destroy
  has_many_geographic_locations_through :holding_institution_geographic_locations
  #endregion


  mount_uploader :image, ImageUploader

  # region callbacks
  before_destroy :confirm_unassigned
  after_update :reindex_child_values
  #endregion

  #region validations

  validates :slug, uniqueness: true, presence: true
  validates :authorized_name, uniqueness: true, presence: true
  # validates_presence_of :institution_type # TODO: ever?
  validate :coordinates_format
  #endregion

  #region scopes
  scope :for_indexing, lambda {
    includes(:repositories, collections: :portals)
  }
  scope :are_public, -> { where(public: true) }
  scope :text_search, ->(query) { search_for query }

  scope :sorted_by, lambda { |sort_option|
    direction = /desc$/.match?(sort_option) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^id/ then order "holding_institutions.id #{direction}"
    when /^slug/ then order "holding_institutions.slug #{direction}"
    when /^authorized_name/ then order "holding_institutions.authorized_name #{direction}"
    when /^institution_type/ then order "holding_institutions.institution_type #{direction}"
    when /^public/ then order "holding_institutions.public #{direction}"
    when /^created_at/ then order "holding_institutions.created_at #{direction}"
    when /^updated_at/ then order "holding_institutions.updated_at #{direction}"
    else
      raise(ArgumentError, "Invalid sort option: #{sort_option.inspect}")
    end
  }

  scope :public_state, lambda { |active|
    case active
    when 'yes' then where(public: true)
    when 'no' then where(public: false)
    when '' then next
    else
      raise(ArgumentError, "Invalid Active option: #{active}")
    end
  }
  scope :for_institution_type, lambda { |inst_type|
    where(inst_type: inst_type)
  }
  scope :for_portals, lambda { |portal_ids|
    ids = portal_ids.reject(&:blank?)
    next if ids&.empty?

    includes(repositories: :portals).where(repositories: { portals: { id: ids } })
  }
  #endregion

  #region Setup for pg_search and filterrific
  pg_search_scope :search_for,
                  against: [:slug, :authorized_name],
                  using: {
                    tsearch: {
                      dictionary: 'english',
                      prefix: true
                    }
                  }

  filterrific(
    default_filter_params: { sorted_by: 'updated_at_desc' },
    available_filters: %i[sorted_by text_search public_state for_portals for_institution_type]
  )
  #endregion

  #region solr_fields
  searchable do
    string(:class_name, stored: true) { self.class }
    # Blacklight 'Required' fields #
    string(:title, as: 'title'){ authorized_name }
    string(:authorized_name, stored: true)
    string(:display_name, stored: true)
    string(:slug, stored: true)
    string(:short_description, stored: true)
    string(:description, stored: true)
    string(:homepage_url, stored: true)
    string(:public_contact_address, stored: true)
    string(:public_contact_email, stored: true)
    string(:public_contact_phone, stored: true)
    string(:coordinates, stored: true)
    string(:parent_institution, stored: true)
    string(:strengths, stored: true)
    string(:first_char, stored: true) { first_char }
    string(:image, stored: true) {  image&.solr_url }
    string(:public_collections, stored: true, multiple: true) {format_collections_for_solr(public_collections)}
    boolean(:public, stored: true)
    boolean(:display) { display? }
    string(:portals, stored: true, multiple: true) { portal_codes }

    # datetimes
    time(:created_at, stored: true, trie: true)
    time(:updated_at, stored: true, trie: true)

  end
  #endregion

  #region class_methods
  def self.index_query_fields
    %w[institution_type]
  end
  def self.sort_options
    [
      ['Recently Created', 'created_at_desc'],
      ['Recently Updated', 'updated_at_desc']
    ]
  end

  def self.public_states
    [['Public or Not Public', ''], %w[Public yes], ['Not Public', 'no']]
  end

  def self.institution_types
    HoldingInstitution.all.pluck(:institution_type).uniq.map{|type| [type, type]}
  end

  def self.options_for_select
    order(Arel.sql 'LOWER(authorized_name)').map { |r| [r.authorized_name, r.id] }
  end
  #endregion

  #region public_methods
  def public_collections
    collections.are_public.alphabetized
  end

  def public_collections_georgia
    collections.are_public.for_portals('georgia').alphabetized
  end

  def public_collections_other
    collections.are_public.for_portals('other').alphabetized
  end

  def public_collections_crdl
    collections.are_public.for_portals('crdl').alphabetized
  end

  def public_collections_amso
    collections.are_public.for_portals('amso').alphabetized
  end

  def format_collections_for_solr(collections)
    collection_hashes = []
    collections.map do |collection|
      collection_hashes << {
        record_id: collection.record_id,
        slug: collection.slug,
        display_title: collection.display_title,
        short_description: collection.short_description,
        portals: collection.portal_codes
      }.to_json
    end
    collection_hashes
  end

  def item_count
    items.count
  end

  def display?
    public
  end

  def portals
    repositories.collect(&:portals).flatten.uniq
  end

  def portal_names
    portals.collect(&:name)
  end

  def portal_codes
    portals.collect(&:code)
  end

  def authorized_name_for_sort
    authorized_name.downcase.gsub(/[^a-z0-9 ]/, '').sub(/^(?:the |an |a )/, '')
  end

  def first_char
    first_char = authorized_name_for_sort[0]
    if first_char.match(/^[0-9]$/)
      '0-9'
    else
      first_char.upcase
    end
  end

  def confirm_unassigned
    return true if collections.empty? && items.empty?

    raise HoldingInstitutionInUseError, "Cannot delete Holding Institution as it remains assigned to #{items.length} Items and #{collections.length} Collections"
  end

  def coordinates_format
    return true if !coordinates || coordinates.empty?

    lat, lon = coordinates.split(', ')
    begin
      lat = Float(lat)
      lon = Float(lon)
      if lat < -90 || lat > 90 || lon < -180 || lon > 180
        errors.add(:coordinates, I18n.t('activerecord.errors.messages.holding_institution.coordinates'))
        return false
      end
    rescue TypeError, ArgumentError
      errors.add(:coordinates, I18n.t('activerecord.errors.messages.holding_institution.coordinates'))
      return false
    end
  end

  def reindex_child_values
    collection_queued = false
    if saved_change_to_authorized_name? || saved_change_to_slug?
      mark_children_updated
      queue_reindex_collections
      queue_reindex_items
      collection_queued = true
    end
    queue_reindex_collections if saved_change_to_image? && !collection_queued
    true
  end

  def mark_children_updated
    items.update_all(updated_at: updated_at) if items.any?
    collections.update_all(updated_at: updated_at) if collections.any?
  end

  def queue_reindex_collections
    Resque.enqueue(Reindexer, 'Collection', collections.map(&:id)) if collections.any?
  end

  def queue_reindex_items
    Resque.enqueue(Reindexer, 'Item', items.map(&:id)) if items.any?
  end

  # Returns a Sunspot Hit object for the holding institution that represents the current values in SOLR
  def solr_hit
    search = HoldingInstitution.search do
      adjust_solr_params do |params|
        params[:q] = ""
      end
      with(:slug, slug)
    end

    return nil if search.total < 1

    search.hits.first
  end

  def solr_stored_hash
    hit = solr_hit

    return {} if hit.nil?

    hit.instance_variable_get(:@stored_values)
  end

  def dlg_url
    "#{Rails.configuration.dlg_url}/institutions/#{slug}"

  end

  def crdl_url
    "#{Rails.configuration.crdl_url}/institutions/#{slug}"
  end
#endregion
end
