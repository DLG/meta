# BatchItem model describes relations to Batch and Item records, as well as the
# BatchItem commit process
class BatchItem < ApplicationRecord
  #region includes
  include ItemType
  # endregion

  # region relationships
  belongs_to :batch, counter_cache: true, optional: true
  belongs_to :batch_import, counter_cache: true, optional: true
  belongs_to :collection
  belongs_to :serial, optional: true
  belongs_to :item, optional: true
  has_one :repository, through: :collection
  has_many :batch_item_geographic_locations, dependent: :destroy
  has_many_geographic_locations_through :batch_item_geographic_locations
  # endregion

  # region callbacks
  before_create :clean_up_metadata
  # endregion

  #region constants
  COMMIT_SCRUB_ATTRIBUTES = %w[
    id
    created_at
    updated_at
    batch_id
    batch_import_id
  ].freeze
  # endregion

  # region scopes
  scope :updated_since, ->(since) { where('updated_at >= ?', since) }
  # endregion

  # region public_methods
  def thumbnail?
    has_thumbnail
  end

  def other_collection_titles
    other_colls.map(&:title)
  end

  def other_colls
    Collection.find(other_collections.reject(&:nil?))
  end

  def commit
    attributes = self.attributes.except(*COMMIT_SCRUB_ATTRIBUTES)
    item_id = attributes.delete('item_id')
    ActiveRecord::Base.transaction do
      if item_id
        if item.nil?
          raise "Batch item has an associated item ID (#{item_id}) but no item. Was Item #{item_id} deleted?"
        end
        remove_existing_from_index(item) if batch_import.try(:match_on_id?) # de-index object since we are changing primary id
        item.update attributes
      else
        self.item = Item.new attributes
      end
      item.portals = portals
      item.holding_institutions = holding_institutions
      item.geographic_locations = geographic_locations
      item.save! if item_id
    end
    item
  end

  def next
    batch.batch_items.where('id > ?', id).order(:id).first
  end

  def previous
    batch.batch_items.where('id < ?', id).order(:id).last
  end

  def clean_up_metadata
    MetadataCleaner.new.clean self
  end
  # endregion

  # region private_methods
  private

  def remove_existing_from_index(item)
    Sunspot.remove item
  end
  # endregion
end