# behaviors for models that have a slug attribute
module Slugged
  extend ActiveSupport::Concern
  included do
    validates_presence_of :slug
    validates_format_of :slug, without: %r{[^a-z0-9-]+}, message: 'can only contain the characters a-z, 0-9, and -'
  end

  def sanitize_slug(str)
    str.downcase.gsub(/[^a-z0-9]/, '-').gsub(/-+/, '-')
  end
end
