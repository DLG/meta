module GeolocationIndexable
  extend ActiveSupport::Concern

  GEORGIA_COUNTY_REGEX = /United States, Georgia, (\w*|\w*\s\w*) County/

  def counties
    counties = []
    geographic_locations.each do |geoloc|
      if geoloc.code.match(GEORGIA_COUNTY_REGEX)
        counties << geoloc.code.match(GEORGIA_COUNTY_REGEX).captures
      end
    end
    counties.flatten
  end

  def us_states
    states = []
    geographic_locations.each do |geoloc|
      if geoloc.us_state
        states << geoloc.us_state
      end
    end
    states.uniq
  end

  def has_coordinates?
    geographic_locations.each do |gl|
      return true if gl.longitude && gl.latitude
    end
    false
  end

  def geojson
    multiple_geojson_objects = []
    coordinates(true).each_with_index do |c, i|
      multiple_geojson_objects << %({"type":"Feature","geometry":{"type":"Point","coordinates":[#{c}]},"properties":{"placename":"#{placename[i]}"}})
    end
    multiple_geojson_objects
  end

  def placename
    return ['No Location Information Available'] unless geographic_locations.any?
    geographic_locations.map(&:code)
  end

  # return first set of discovered coordinates, or a silly spot if none found
  # todo: figure out a way to not index a location for items with no coordinates
    def coordinates(alt_format = false)
    coordinates_array = []
    geographic_locations.each do |gl|
      next unless gl.longitude && gl.latitude
      coordinates = if alt_format
                      "#{gl.longitude}, #{gl.latitude}"
                    else
                      "#{gl.latitude}, #{gl.longitude}"
                    end

      coordinates_array << coordinates
    end

    if coordinates_array.empty?
      coordinates_array = if alt_format
                            ['-80.394617, 31.066399']
                          else
                            ['31.066399, -80.394617']
                          end
    end

    coordinates_array
  end

  # Converts associated GeographicLocations into dc_spatial array
  def geolocations_to_dcspatial
    geographic_locations.map do |gl|
      dcspatial = gl.code
      unless gl.latitude.blank? || gl.longitude.blank?
        dcspatial +=", #{gl.latitude}, #{gl.longitude}"
      end
      dcspatial
    end
  end

  def set_geolocations_from_dcspatial(dcterms_spatial_arr)
    from_dcspatial = dcterms_spatial_arr.map do |str|
      code, latitude, longitude = GeographicLocation.parse_location_string str
      gl = GeographicLocationLookupService.find_or_initialize code
      if gl.new_record? && gl.latitude.nil? && gl.longitude.nil?
        gl.latitude = latitude
        gl.longitude = longitude
      end
      gl
    end
    ids_from_dcspatial = from_dcspatial.pluck(:id).to_set
    related_geographic_locations.each do |rgl|
      rgl.mark_for_destruction unless ids_from_dcspatial.include? rgl.geographic_location_id
      ids_from_dcspatial.delete rgl.geographic_location_id
    end
    from_dcspatial.each do |gl|
      if gl.new_record?
        related_geographic_locations.new geographic_location: gl
      elsif ids_from_dcspatial.include? gl.id
        related_geographic_locations.new geographic_location_id: gl.id
      end
    end
  end

  def dcterms_spatial
    geolocations_to_dcspatial
  end

  def dcterms_spatial=(dcspatial)
    set_geolocations_from_dcspatial dcspatial
  end

  # Returns geographic location data in the format we came up with for DPLA
  def dpla_spatial
    geographic_locations.map &:dpla_hash
  end

  def dpla_spatial_json
    dpla_spatial.to_json
  end

end