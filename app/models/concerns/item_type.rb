# Functionality for Items and BatchItems
module ItemType
  extend ActiveSupport::Concern

  # region includes
  include Slugged
  include MetadataHelper
  include Portable
  include Provenanced
  include HasGeolocations
  include GeolocationIndexable

  # endregion

  #region included
  included do
    #region callbacks
    before_save :update_validation_cache
    # endregion

    #region validations
    before_validation :set_fields_from_serial
    validates_presence_of :collection, message: I18n.t('activerecord.errors.messages.item_type.collection')
    validates_presence_of :dcterms_title
    validate :dcterms_temporal_characters
    validate :dcterms_type_required_value
    validate :subject_value_provided
    validate :dc_date_characters
    validate :edm_is_shown_at_valid
    validate :edm_is_shown_by_valid
    validate :one_dc_right_present_and_valid
    validate :has_at_least_one_geographic_location
    # endregion

    #region scopes
    # endregion
  end
  # endregion

  # region public_methods
  def title
    dcterms_title.first ? dcterms_title.first.strip : 'No Title'
  end

  def portal_host
    # use CRDL if the item exists in CRDL and not the Georgia portal
    # otherwise use DLG
    if portal_codes.include?('crdl') && !portal_codes.include?('georgia')
      Rails.application.credentials.crdl_public_url.dig(Rails.env.to_sym)
    else
      Rails.application.credentials.dlg_public_url.dig(Rails.env.to_sym)
    end
  end

  def set_fields_from_serial
    return unless serial

    self.dcterms_publisher = serial.dcterms_publisher if self.dcterms_publisher.empty?
    self.dcterms_description = serial.dcterms_description if self.dcterms_description.empty?
    self.dcterms_subject = serial.dcterms_subject if self.dcterms_subject.empty?
    self.dcterms_medium = serial.dcterms_medium if self.dcterms_medium.empty?
    self.dcterms_contributor = serial.dcterms_contributor if self.dcterms_contributor.empty?
    self.dcterms_creator = serial.dcterms_creator if self.dcterms_creator.empty?
    self.dc_right = serial.dc_right if self.dc_right.empty?
    self.dlg_subject_personal = serial.dlg_subject_personal if self.dlg_subject_personal.empty?
    self.dcterms_language = serial.dcterms_language if self.dcterms_language.empty?
    self.dcterms_type = serial.dcterms_type if self.dcterms_type.empty?
    self.dcterms_title = generate_title_from_serial if self.dcterms_title.empty?
    self.slug = generate_slug_from_serial if self.slug.blank?

    self.collection_id = serial.generate_collection&.id if self.collection_id.blank?
    self.holding_institutions = serial.generate_holding_institutions if self.holding_institutions.empty?

    if self.geographic_locations.empty? && self.related_geographic_locations.empty?
      self.geographic_locations = serial.geographic_locations
    end
  end

  def generate_title_from_serial
    return [] unless self.serial

    ["#{self.serial.display_title} #{self.dc_date.first if self.dc_date&.any?}"]
  end

  def generate_slug_from_serial
    return '' unless self.serial
    "#{self.serial.slug}-#{sanitize_slug(self.dc_date.first) if self.dc_date&.any?}"
  end
  # endregion

  # region private_methods
  private

  def one_dc_right_present_and_valid
    if dc_right.empty?
      errors.add(:dc_right, I18n.t('activerecord.errors.messages.blank'))
      return
    end
    errors.add(:dc_right, I18n.t('activerecord.errors.messages.item_type.dc_right_not_single_valued')) if dc_right.size > 1
    errors.add(:dc_right, I18n.t('activerecord.errors.messages.dc_right_not_approved')) if (dc_right & all_rights_statements_uris).empty?
  end

  def subject_value_provided
    if dcterms_subject.empty? && dlg_subject_personal.empty?
      errors.add(:dcterms_subject, I18n.t('activerecord.errors.messages.item_type.dcterms_subject'))
    end
  end

  def dc_date_characters
    if dc_date.empty? || dc_date.any?(&:blank?)
      errors.add(:dc_date, I18n.t('activerecord.errors.messages.blank'))
      return
    end
    dc_date.each do |v|
      if v =~ %r{([^0-9\/-])}
        errors.add(:dc_date, "#{I18n.t('activerecord.errors.messages.date_invalid_character')}: #{v}")
        break
      end
    end
  end

  def dcterms_temporal_characters
    dcterms_temporal.each do |v|
      if v =~ %r{([^0-9\/-])}
        errors.add(:dcterms_temporal, "#{I18n.t('activerecord.errors.messages.temporal_invalid_character')}: #{v}")
        break
      end
    end
  end

  def dcterms_type_required_value
    if dcterms_type.empty?
      errors.add(:dcterms_type, I18n.t('activerecord.errors.messages.blank'))
      return
    end
    if (dcterms_type & dcmi_valid_types).empty?
      errors.add(:dcterms_type, I18n.t('activerecord.errors.messages.type_required_value'))
    end
  end

  def edm_is_shown_at_valid
    if edm_is_shown_at.empty? && local == false
      errors.add(:edm_is_shown_at, I18n.t('activerecord.errors.messages.item_type.edm_is_shown_at_non_local.blank'))
    end

    return if edm_is_shown_at.empty?

    edm_is_shown_at.each do |v|
      unless valid_url? v
        errors.add(:edm_is_shown_at, "#{I18n.t('activerecord.errors.messages.item_type.invalid_url_provided')}: #{v}")
        break
      end
    end
  end

  def edm_is_shown_by_valid
    return if edm_is_shown_by.empty?

    edm_is_shown_by.each do |v|
      unless valid_url? v
        errors.add(:edm_is_shown_by, "#{I18n.t('activerecord.errors.messages.item_type.edm_is_shown_by_when_local.url')}: #{v}")
        break
      end
    end
  end

  def has_at_least_one_geographic_location
    unless geographic_locations.any? || related_geographic_locations.any?
      errors.add(:geographic_locations, I18n.t('activerecord.errors.messages.item_type.geographic_locations_empty'))
    end
  end

  def valid_url?(url)
    url =~ URI::DEFAULT_PARSER.make_regexp
  end

  def update_validation_cache
    self.valid_item = valid?
    true
  end
  # endregion
end