module IndexDates
  extend ActiveSupport::Concern

  #region constants
  MAX_YEAR_RANGE    = 3000
  EXTRA_CIRCA_YEARS = 2
  TWO_DIGIT_CUTOFF  = 10

  CIRCA_YYYY      = %r{^ca\. (?<year>\d{4})$}  # ca. 1915, etc
  LAST_DITCH_YYYY = %r{\b(?<year>\d{3,4})\b}   # 1978--03, 1972 [c1856, etc

  # only one year is captured
  SINGLE_YEAR_REGX = [
    %r{^ \d{1,2}   [-/] \d{1,2}   [-/] (?<year>\d{4}) $}x,  # MM_DD_YYYY 10-1-1999, 10/12/1977, etc
    %r{^ \d{1,2}   [-/] (?<year>\d{4})                $}x,  # MM_YYYY    03-1957, etc
    %r{^ \d{1,2}   [-/] \d{1,2}   [-/] (?<year>\d{2}) $}x,  # MM_DD_YY   2-6-97 (1997), 3-18-01 (2001), etc
    %r{^ (?<year>\d{4}) \??                           $}x,  # YYYY       1995, 2014, etc
    %r{^ (?<year>\d{4})   [-/] 0?\d{1,2} [-/] \d{1,2} $}x,  # YYYY_MM_DD 1999-02-05, 2005-07-19, 1993/01/06, etc
    %r{^ (?<year>\d{4})   [-/] \d{1,2}                $}x,  # YYYY_MM    1999/12, 2000-01, etc
    %r{^ \d{1,2} - [A-Z][a-z]{2} - (?<year>\d{1,2})   $}x,  # DD_MON_YY  24-Oct-58 (1958), 5-May-59, 26-Nov-60, etc
  ]

  # two years are captured
  RANGE_YEARS_REGX = [
    %r{^ (?<year1>\d{3,4}) \??                  [-/] (?<year2>\d{3,4}) \??                    $}x,  # YYYY_YYYY YYY_YYY  1300/1399 (100 years), 1636/1640, etc
    %r{^ (?<year1>\d{4}) [-/] \d{1,2}              / (?<year2>\d{4}) [-/] \d{1,2}              $}x,  # YYYY_MM_YYYY_MM 1823-01/1832-03, 2007/11/2003/12, etc
    %r{^ (?<year1>\d{4}) [-/] \d{1,2}              / (?<year2>\d{4})                           $}x,  # YYYY_MM_YYYY    1854-04/1856, 1982-09/1983, etc
    %r{^ (?<year1>\d{4})                           / (?<year2>\d{4}) [-/] \d{1,2}              $}x,  # YYYY_YYYY_MM    1863/1864-04, 1980/1982-12, etc
    %r{^ (?<year1>\d{4}) [-/] \d{1,2} [-/] \d{1,2} / (?<year2>\d{4}) [-/] \d{1,2} [-/] \d{1,2} $}x,  # YYYY_MM_DD_YYYY_MM_DD 1786-02-12/1788-08-24, etc
    %r{^ (?<year1>\d{4}) [-/] \d{1,2}              / (?<year2>\d{4}) [-/] \d{1,2} [-/] \d{1,2} $}x,  # YYYY_MM_YYYY_MM_DD    1981-01/1980-02-24, etc
    %r{^ (?<year1>\d{4}) [-/] \d{1,2} [-/] \d{1,2} / (?<year2>\d{4}) [-/] \d{1,2}              $}x,  # YYYY_MM_DD_YYYY_MM    1861-05-01/1873-09, etc
    %r{^ (?<year1>\d{4})                           / (?<year2>\d{4}) [-/] \d{1,2} [-/] \d{1,2} $}x,  # YYYY_YYYY_MM_DD 1976/1981-02-22, etc
    %r{^ (?<year1>\d{4}) [-/] \d{1,2} [-/] \d{1,2} / (?<year2>\d{4})                           $}x,  # YYYY_MM_DD_YYYY 1970-06-27/2000, etc
    %r{^(?<year1>\d{4}); (?:\d{4}; )*(?<year2>\d{4});?$},  # YYYY_YYYY_YYYY "1900; 1901; 1902; 1903; 1904;", etc
  ]

  # match one year and maybe a month and day
  YMD_REGX = [
    %r{^ (?<year>\d{4}) \??                                                  $}x,  # YYYY       1995, 2014, etc
    %r{^ (?<year>\d{4})       [-/] 0?(?<month>\d{1,2}) [-/] (?<day>\d{1,2})  $}x,  # YYYY_MM_DD 1999-02-05, 2005-07-19, 1993/01/06, etc
    %r{^ (?<year>\d{4})       [-/]   (?<month>\d{1,2})                       $}x,  # YYYY_MM    1999/12, 2000-01, etc
    %r{^ (?<year>\d{4}) \??   [-/]                     \d{4} \??             $}x,  # YYYY_YYYY  1300/1399 (100 years), 1636/1640, etc
    %r{^ (?<year>\d{4})       [-/]   (?<month>\d{1,2})                      / \d{4} [-/] \d{1,2}              $}x,  # YYYY_MM_YYYY_MM 1823-01/1832-03, 2007/11/2003/12, etc
    %r{^ (?<year>\d{4})       [-/]   (?<month>\d{1,2})                      / \d{4}                           $}x,  # YYYY_MM_YYYY    1854-04/1856, 1982-09/1983, etc
    %r{^ (?<year>\d{4})                                                     / \d{4} [-/] \d{1,2}              $}x,  # YYYY_YYYY_MM    1863/1864-04, 1980/1982-12, etc
    %r{^ (?<year>\d{4})       [-/]   (?<month>\d{1,2}) [-/] (?<day>\d{1,2}) / \d{4} [-/] \d{1,2} [-/] \d{1,2} $}x,  # YYYY_MM_DD_YYYY_MM_DD 1786-02-12/1788-08-24, etc
    %r{^ (?<year>\d{4})       [-/]   (?<month>\d{1,2})                      / \d{4} [-/] \d{1,2} [-/] \d{1,2} $}x,  # YYYY_MM_YYYY_MM_DD    1981-01/1980-02-24, etc
    %r{^ (?<year>\d{4})       [-/]   (?<month>\d{1,2}) [-/] (?<day>\d{1,2}) / \d{4} [-/] \d{1,2}              $}x,  # YYYY_MM_DD_YYYY_MM    1861-05-01/1873-09, etc
    %r{^ (?<year>\d{4})                                                     / \d{4} [-/] \d{1,2} [-/] \d{1,2} $}x,  # YYYY_YYYY_MM_DD  1976/1981-02-22, etc
    %r{^ (?<year>\d{4})       [-/]   (?<month>\d{1,2}) [-/] (?<day>\d{1,2}) / \d{4}                           $}x,  # YYYY_MM_DD_YYYY  1970-06-27/2000, etc
    %r{^(?<year>\d{4}); (?:\d{4}; )*\d{4};?$},  # YYYY_YYYY_YYYY "1900; 1901; 1902; 1903; 1904;", etc
  ]

  MDY_REGX = [
    %r{^ (?<month>\d{1,2}) [-/] (?<day>\d{1,2}) [-/] (?<year>\d{4}) $}x,  # MM_DD_YYYY 10-1-1999, 10/12/1977, etc
    %r{^ (?<month>\d{1,2}) [-/] (?<day>\d{1,2}) [-/] (?<year>\d{2}) $}x,  # MM_DD_YY   2-6-97 (1997), 3-18-01 (2001), etc
  ]

  MM_YYYY   = %r{^ (?<month>\d{1,2}) [-/] (?<year>\d{4}) $}x                         # 03-1957, etc
  DD_MON_YY = %r{^ (?<day>\d{1,2}) - (?<month>[A-Z][a-z]{2}) - (?<year>\d{1,2}) $}x  # 01-Jan-00
  #endregion

  def self.index_years(dc_date)
    years = []
    dc_date.compact.each do |date|
      years << (single_year_match(date) || range_years_match(date) || circa_match(date) || last_ditch_match(date))
    end

    years.flatten.compact.uniq.map do |year|
      year.rjust(4, '0')
    end
  end

  def self.single_year_match(date)
    SINGLE_YEAR_REGX.each do |regx|
      if (m = date.match(regx))
        return year_expanded(m[:year])
      end
    end
    nil
  end

  def self.range_years_match(date)
    RANGE_YEARS_REGX.each do |regx|
      if (m = date.match(regx))
        year1 = m[:year1].to_i(10)
        year2 = m[:year2].to_i(10)
        year1, year2 = year2, year1 if year1 > year2
        year2 = check_max_range( year1, year2 )
        return (year1.to_s .. year2.to_s).to_a
      end
    end
    nil
  end

  def self.check_max_range( year1, year2 )
    if year2 - year1 < MAX_YEAR_RANGE
      year2
    elsif year1 + MAX_YEAR_RANGE < current_year_plus_10_years
      year1 + MAX_YEAR_RANGE
    else
      current_year_plus_10_years
    end
  end

  def self.current_year_plus_10_years
    (Date.today + 10.years).strftime("%Y").to_i(10)
  end

  def self.circa_match(date)
    if (m = date.match(CIRCA_YYYY))
      year = m[:year].to_i(10)
      return ((year - EXTRA_CIRCA_YEARS).to_s .. (year + EXTRA_CIRCA_YEARS).to_s).to_a
    end
    nil
  end

  def self.last_ditch_match(date)
    if (m = date.match(LAST_DITCH_YYYY))
      return m[:year]
    end
    nil
  end

  def self.index_yyyy_mm_dd_sort(dc_date)
    dc_date.compact.each do |date|
      date = (year_month_day_match(date) || month_day_year_match(date) || month_year_match(date) || day_mon_year_match(date) || last_ditch_ymd_match(date))
      return date if date.present?
    end
    nil
  end

  def self.year_month_day_match(date)
    YMD_REGX.each do |regx|
      if (m = date.match(regx))
        month = m.names.include?('month') ? m[:month] : '01'
        day   = m.names.include?('day')   ? m[:day]   : '01'
        return full_date_year_month_day(year_expanded(m[:year]), month, day)
      end
    end
    nil
  end

  def self.month_day_year_match(date)
    MDY_REGX.each do |regx|
      if (m = date.match(regx))
        return full_date_year_month_day(year_expanded(m[:year]), m[:month], m[:day])
      end
    end
    nil
  end

  def self.month_year_match(date)
    if (m = date.match(MM_YYYY))
      return full_date_year_month(m[:year], m[:month])
    end
    nil
  end

  def self.day_mon_year_match(date)
    months = {Jan: '01', Feb: '02', Mar: '03', Apr: '04', May: '05', Jun: '06', Jul: '07', Aug: '08', Sep: '09', Oct: '10', Nov: '11', Dec: '12'}
    if (m = date.match(DD_MON_YY))
      month = months[m[:month].to_sym]
      return full_date_year_month_day(year_expanded(m[:year]), month, m[:day])
    end
    nil
  end

  def self.last_ditch_ymd_match(date)
    if (m = date.match(LAST_DITCH_YYYY))
      year = m[:year]
      return full_date_year(year)
    end
    nil
  end

  def self.year_expanded(year)
    return year if year.length == 4
    y = year.to_i(10)
    if year.length == 2
      if y < TWO_DIGIT_CUTOFF
        y += 2000
      else
        y += 1900
      end
    end
    y.to_s
  end

  def self.full_date_year_month_day(year, month, day)
    sprintf("%04d-%02d-%02d", year.to_i(10), month.to_i(10), day.to_i(10))
  end

  def self.full_date_year_month(year, month)
    sprintf("%04d-%02d-%02d", year.to_i(10), month.to_i(10), 1)
  end

  def self.full_date_year(year)
    sprintf("%04d-%02d-%02d", year.to_i(10), 1, 1)
  end

  # @param [String] First ISO 8601b date string (could be a single date of any precision or a range)
  # @param [String] Second ISO 8601b date string (could be a single date of any precision or a range)
  # @return [Boolean] whether the dates overlap (ranges overlap, range includes single date, or single dates are same)
  # This does not support the same variety of formats as the methods above.
  # '/' must be used to separate range components and '-' must be used to separate year, month, and day components
  def self.dc_dates_overlap?(dc_date1, dc_date2)
    parse_strict_range(dc_date1).overlaps? parse_strict_range(dc_date2)
  rescue Date::Error
    false
  end

  def self.parse_strict_range(dc_date)
    range_start, range_end = dc_date&.split('/', -1)
    if range_end.nil?
      # not a range
      range_end = range_start
    end
    first_possible_date_for(range_start)..last_possible_date_for(range_end)
  rescue Date::Error
    puts "Invalid dc_date #{dc_date}"
    raise
  end

  def self.ymd_components_for(single_date_string)
    if single_date_string =~ /(\d{4})(-(\d{1,2})(-(\d{1,2}))?)?/
      [$1, $3, $5].compact.map &:to_i
    else
      []
    end
  end

  def self.first_possible_date_for(single_date_string)
    components = ymd_components_for single_date_string
    return -Float::INFINITY if components.empty?
    Date.new *components
  end

  def self.last_possible_date_for(single_date_string)
    components = ymd_components_for single_date_string
    return Float::INFINITY if components.empty?
    Date.new(*components) + {
      1 => 1.year,
      2 => 1.month,
      3 => 1.day
    }[components.size] - 1.day
  end

end
