module HasGeolocations
  extend ActiveSupport::Concern

  module ClassMethods
    attr_accessor :geographic_locations_relationship
    def has_many_geographic_locations_through(relation)
      has_many :geographic_locations, ->{ order :code }, through: relation
      accepts_nested_attributes_for relation, allow_destroy: true
      scope :includes_geographic_locations_join_table, -> { includes(relation) }
      self.geographic_locations_relationship = relation
    end
  end

  def self.included(base)
    base.extend ClassMethods
  end

  def related_geographic_locations
    self.send self.class.geographic_locations_relationship
  end

end
