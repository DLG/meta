# behaviors for models that can have educator_resource_mediums listed in their dcterms_medium
module HasEducatorResources
  extend ActiveSupport::Concern

  # region public_methods
  def educator_medium_types
    ['annotated bibliographies', 'bibliographies', 'quizzes', 'worksheets',
     'timelines (chronologies)', 'study guides', 'teaching guides', 'slide shows', 'lesson plans', 'learning modules',
     'online exhibitions'
    ]
  end

  def educator_resource_mediums
    return [] if dcterms_medium.empty?
    dcterms_medium.select do | medium |
      next if medium.blank?
      educator_medium_types.include?(medium.downcase)
    end
  end
  # endregion
end
