class BatchItemGeographicLocation < ApplicationRecord
  belongs_to :batch_item
  belongs_to :geographic_location
  validates_uniqueness_of :geographic_location_id, scope: :batch_item_id

  accepts_nested_attributes_for :geographic_location
end