class SearchBuilder < Blacklight::SearchBuilder
  include Blacklight::Solr::SearchBuilderBehavior

  include BlacklightAdvancedSearch::AdvancedSearchBuilder
  self.default_processor_chain += %i[
    add_advanced_parse_q_to_solr
    add_advanced_search_to_solr
    show_only_desired_classes
]

  def show_only_desired_classes(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << 'class_name_ss:(Item OR Collection OR Serial)'
  end
end
