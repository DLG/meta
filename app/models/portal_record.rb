class PortalRecord < ApplicationRecord
  belongs_to :portal
  belongs_to :portable, polymorphic: true,  optional: true

  # region callbacks
  after_save :item_update_events
  #endregion

  #region public_methods
  def item_update_events
    Item.find(saved_changes[:portable_id][1]).update_events if is_crdl_item?
  end

  def is_crdl_item?
    saved_changes && saved_changes[:portable_type][1] == "Item" && Portal.find(saved_changes[:portal_id][1]).code == "crdl"
  end
  #endregion
end
