# CollectionResource is intended for display in the public site as
# supplemental information for a collection
class CollectionResource < ApplicationRecord
  belongs_to :collection, counter_cache: true

  default_scope { order position: :asc }

  validates_presence_of :slug, unless: :use_external_link
  validates_presence_of :title
  validates_presence_of :external_link, if: :use_external_link, message: 'cannot be blank if use external link is true'
  validates_presence_of :content, unless: :use_external_link
  validates_presence_of :collection_id

  after_save :reindex_parent_collection
  before_destroy :save_collection_id
  after_destroy :reindex_collection_after_destroy

  def as_json(options = {})
    new_options = {
      only: %i[slug title position use_external_link external_link content]
    }
    super(options.merge!(new_options))
  end

  def reindex_parent_collection
    Sunspot.index! collection
  end

  def save_collection_id
    @associated_collection_id = collection.id
  end

  def reindex_collection_after_destroy
    Sunspot.index! Collection.for_indexing.where(id: @associated_collection_id)
  end
end
