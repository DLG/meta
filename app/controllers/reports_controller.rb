# frozen_string_literal: true

require 'csv'

# Reports related actions
class ReportsController < ApplicationController
  authorize_resource class: false

  def show
    csv_report, filename = case params[:id]
                           when 'all_rows_for'
                             params[:table_name] == 'Name' ? ReportService.all_names : ReportService.all_rows_for(params[:table_name])
                           when 'holding_insts_contacts'
                             ReportService.holding_insts_contacts
                           when 'holding_insts_subject_counts'
                             ReportService.holding_insts_subject_counts
                           when 'holding_inst_subjects'
                             ReportService.holding_inst_subjects(params[:holding_inst_id])
                           when 'repositories_collections'
                             ReportService.repositories_collections(params[:report_type], params[:start_date].to_date, params[:end_date].to_date)
                           when 'iiif_partner_urls'
                             ReportService.iiif_partner_urls
                           when 'collections_items'
                             ReportService.collections_items(params[:report_type], params[:start_date].to_date, params[:end_date].to_date)
                           when 'fulltext_items'
                             ReportService.fulltext_items
                           when 'item_links'
                             ReportService.item_links(params[:report_type])
                           when 'collections_item_count'
                             ReportService.collections_item_count(params[:report_type], params[:start_date].to_date, params[:end_date].to_date)
                           when 'collections_urls_portals'
                             ReportService.collections_urls_portals
                           when 'items_with_pages'
                             ReportService.items_with_pages
                           when 'items_without_holding_insts'
                             ReportService.items_without_holding_insts
                           when 'holding_insts'
                             ReportService.holding_insts(request.url)
                           when 'holding_insts_collection_item_counts'
                             ReportService.holding_insts_collection_item_counts
                           when 'crdl_names'
                             ReportService.crdl_names
                           when 'created_monographs'
                             ReportService.created_monographs(marc_xml_import_id: params[:marc_xml_import_id])
                           when 'collections_new'
                             ReportService.collections_new( params[:start_date].to_date, params[:end_date].to_date)
                           else
                             raise ActiveRecord::RecordNotFound
                           end

    respond_to do |format|
      format.csv do
        cookies[:downloadStarted] = {
          value: 1,
          expires: 20.seconds.from_now,
          path: '/',
          secure: false,
          httponly: false
        }
        send_data csv_report, filename: "#{filename}-#{Date.today}.csv"
      end
    end
  end

  def index
    @holding_institutions = HoldingInstitution.all.order(:authorized_name)
    @marc_xml_imports = MarcXmlImport.all
  end

end
