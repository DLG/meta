# actions for CRUD on Holding Institutions
class HoldingInstitutionsController < ApplicationController
  load_and_authorize_resource
  include HoldingInstitutionsHelper
  include ErrorHandling
  include Sorting

  MULTIVALUED_TEXT_FIELDS = %w[oai_urls analytics_emails].freeze

  before_action :set_data, only: %i[index new create edit update]

  rescue_from HoldingInstitutionInUseError do |exception|
    redirect_to({ action: 'index' }, alert: exception.message)
  end

  # GET /holding_institutions
  def index
    @filterrific = initialize_filterrific(
      HoldingInstitution,
      params[:filterrific],
      select_options: {
        sorted_by: HoldingInstitution.sort_options,
        portals: Portal.options_for_select,
        public_states: HoldingInstitution.public_states,
        institution_types: HoldingInstitution.institution_types
      }
    )

    @holding_institutions = @filterrific.find.page(params[:page])
                                        .per(per_page_setting).includes(:projects, repositories: :portals)

    respond_to do |format|
      format.xml { send_data @holding_institutions.to_xml }
      format.html { render :index }
      format.json
      format.js
    end

  end

  # GET /holding_institutions/1
  def show; end

  # GET /holding_institutions/new
  def new
    @holding_institution = HoldingInstitution.new
  end

  # GET /holding_institutions/1/edit
  def edit; end

  # POST /holding_institutions
  def create
    @holding_institution = HoldingInstitution.new(holding_institution_params)
    if @holding_institution.save
      redirect_to holding_institution_path(@holding_institution), notice: I18n.t('meta.defaults.messages.success.created', entity: 'Holding Institution')
    else
      render :new
    end
  end

  # PATCH/PUT /holding_institutions/1
  def update
    if @holding_institution.update(holding_institution_params)
      redirect_to holding_institution_path(@holding_institution), notice: I18n.t('meta.defaults.messages.success.updated', entity: 'Holding Institution')
    else
      render :edit
    end
  end

  # DELETE /holding_institutions/1
  def destroy
    @holding_institution.destroy
    redirect_to holding_institutions_url, notice: I18n.t('meta.defaults.messages.success.destroyed', entity: 'Holding Institution')
  end

  def solr
  end

  private

  def holding_institution_params
    prepare_params(
      params
        .require(:holding_institution)
        .permit(:display_name, :short_description, :description, :public,
                :homepage_url, :coordinates, :strengths, :authorized_name,
                :contact_information, :galileo_member, :institution_type,
                :contact_name, :contact_email, :harvest_strategy, :oai_urls,
                :ignored_collections, :analytics_emails, :subgranting,
                :grant_partnerships, :image, :remove_image, :image_cache,
                :notes, :parent_institution, :slug, :public_contact_address,
                :public_contact_email, :public_contact_phone,
                :last_harvested_at, :training, :site_visits, :consultations,
                :impact_stories, :newspaper_partnerships,
                :committee_participation, :other, :wikidata_id, :filterrific,
                repository_ids: [],
                holding_institution_geographic_locations_attributes: edit_geographic_locations_params)
    )
  end

  def set_data
    @data = {}
    @data[:institution_types] = HOLDING_INSTITUTION_TYPES.dup.unshift('')
    @data[:repositories] = Repository.all.order(:title)
    @data[:portals] = Portal.all.order(:name)
  end

  def prepare_params(params)
    params.each do |f, v|
      if v.is_a? Array
        params[f] = v.reject(&:empty?)
        next
      end
      params[f] = v.gsub("\r\n", "\n").strip.split("\n") if MULTIVALUED_TEXT_FIELDS.include? f
      if f.ends_with? '_geographic_locations_attributes'
        params[f] = prepare_geographic_locations v
      end
    end
  end
end

