class GeographicLocationsController < ApplicationController

  include Sorting
  include Filterable
  include TypeaheadSuggestions
  include AssociatedRecords

  before_action :set_geographic_location, only: [:show, :edit, :update, :destroy, :items, :collections, :serials,
                                                 :consolidate, :review_consolidation, :perform_consolidation]

  # GET /geographic_locations
  # GET /geographic_locations.json
  def index
    @filterrific = initialize_filterrific(
      GeographicLocation.with_all_related_counts,
      params[:filterrific],
      select_options: {
        geonames_status_states: GeographicLocation.geonames_status_states,
        sorted_by: GeographicLocation.sort_options
      }
    )

    @geographic_locations = @filterrific.find.page(params[:page])
                          .per(per_page_setting)

    respond_to do |format|
      format.xml { send_data @geographic_locations.to_xml }
      format.html
      format.js
    end
  end

  # GET /geographic_locations/1
  # GET /geographic_locations/1.json
  def show
  end

  # GET /geographic_locations/new
  def new
    @geographic_location = GeographicLocation.new
  end

  # GET /geographic_locations/1/edit
  def edit
  end

  # POST /geographic_locations
  # POST /geographic_locations.json
  def create
    @geographic_location = GeographicLocation.new(geographic_location_params)
    @geographic_location.updated_by = @geographic_location.created_by = current_user.email

    respond_to do |format|
      if @geographic_location.save
        format.html { redirect_to @geographic_location, notice: 'Geographic location was successfully created.' }
        format.json { render :show, status: :created, location: @geographic_location }
      else
        format.html { render :new }
        format.json { render json: @geographic_location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /geographic_locations/1
  # PATCH/PUT /geographic_locations/1.json
  def update
    respond_to do |format|
      @geographic_location.assign_attributes geographic_location_params
      @geographic_location.updated_by = current_user.email
      if @geographic_location.save
        format.html { redirect_to @geographic_location, notice: 'Geographic location was successfully updated.' }
        format.json { render :show, status: :ok, location: @geographic_location }
      else
        format.html { render :edit }
        format.json { render json: @geographic_location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /geographic_locations/1
  # DELETE /geographic_locations/1.json
  def destroy
    @geographic_location.destroy
    respond_to do |format|
      format.html { redirect_to geographic_locations_url, notice: 'Geographic location was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  typeahead_suggestions :suggest,
                        ->(term) { GeographicLocation.text_search(term).by_item_count },
                        match_attr: :code,
                        exact_match_finder: (lambda do |term|
                          result = GeographicLocationLookupService.find_existing_match term
                          return nil if result.nil?

                          item_count = result.items&.count
                          result.attributes.merge({item_count: item_count})
                        end)

  # GET /geographic_locations/1/items
  def items
    handle_item_index @geographic_location.items
  end

  # GET /geographic_locations/1/collections
  def collections
    handle_collection_index @geographic_location.collections
  end

  def serials
    handle_serial_index @geographic_location.serials
  end

  # GET /geographic_locations/1/consolidate
  def consolidate
    @destination = GeographicLocation.new
  end

  # POST /geographic_locations/1/consolidate/review
  def review_consolidation
    @destination = existing_geoloc_from_params
    if @destination.nil?
      redirect_to consolidate_geographic_location_path(@geographic_location),
                  notice: 'Destination geographic location must exist.'
      return false
    end
    if @destination.id == @geographic_location.id
      redirect_to consolidate_geographic_location_path(@geographic_location),
                  notice: 'Destination geographic location cannot be the same as source geographic location!'
      return false
    end
    true
  end

  # POST /geographic_locations/1/consolidate/perform
  def perform_consolidation
    if review_consolidation
      @items = @geographic_location.items.order(:record_id).to_a
      @collections = @geographic_location.collections.order(:slug).to_a
      @serials = @geographic_location.serials.order(:slug).to_a
      @holding_institutions = @geographic_location.holding_institutions.order(:slug).to_a
      @geographic_location.consolidate_into @destination
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_geographic_location
    @geographic_location = GeographicLocation.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def geographic_location_params
    prepare_params(params.require(:geographic_location)
                         .permit(:code, :component_placenames, :geonames_id,
                                 :us_state, :us_county, :latitude, :longitude, :geoname_verified))
  end

  def suggest_params
    params.permit(:q)
  end

  def prepare_params(params)
    params[:component_placenames] = params[:component_placenames].strip.split(/\s*\n\s*/)
    params
  end

  # @return [GeographicLocation]
  def existing_geoloc_from_params
    filtered_params = params.require(:geographic_location).permit(:id, :code)
    if filtered_params[:id].present?
      GeographicLocation.find(filtered_params[:id])
    else
      # if the form was submitted while the typeahead was lagging, the id might not be present
      GeographicLocationLookupService.find_existing_match filtered_params[:code]
    end
  end
end
