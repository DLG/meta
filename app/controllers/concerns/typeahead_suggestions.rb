# frozen_string_literal: true

module TypeaheadSuggestions
  extend ActiveSupport::Concern
  TYPEAHEAD_SUGGESTION_COUNT = 5

  module ClassMethods
    def typeahead_suggestions(action_name, search_func, match_attr: nil, exact_match_finder: nil)
      define_method action_name do
        search_term = params.permit(:q)[:q]&.strip
        if search_term.blank?
          results = []
        else
          results = search_func.call(search_term)
          if results.is_a? ActiveRecord::Relation
            results = results.limit(TYPEAHEAD_SUGGESTION_COUNT)
          end
          results = results.to_a

          # make sure any exact match is displayed at top
          if results.any?
            exact_match = nil
            # If we know what field to look at, we'll try to boost the exact match from lower in our existing results
            if match_attr.present?
              exact_match = results.find {|item| item.send(match_attr).downcase == search_term.downcase}
            end
            # If we couldn't boost it from the existing search results, we'll do another search to check for exact match
            if exact_match.nil? && exact_match_finder.present?
              exact_match = exact_match_finder.call(search_term)
            end

            unless exact_match.nil?
              results.delete exact_match
              results.insert 0, exact_match
              results = results[0, TYPEAHEAD_SUGGESTION_COUNT]
            end
          end
        end
        render json: results.to_json
      end
    end
  end

  def self.included(base)
    base.extend ClassMethods
  end

end
