module AssociatedRecords
  extend ActiveSupport::Concern

  included do
    helper_method :render_item_list, :render_collection_list, :render_serial_list
  end

  def handle_item_index(scoped_items = Item)
    @filterrific = initialize_filterrific(
      scoped_items,
      params[:filterrific],
      select_options: {
        sorted_by: Item.sort_options,
        portals: Portal.options_for_select(use_code: true),
        collections: Collection.options_for_select,
        serials: Serial.options_for_select,
        display_states: Item.display_states,
        public_states: Item.public_states,
        valid_states: Item.valid_states
      }
    )

    @items = if current_user.super? || current_user.viewer?
               @filterrific.find.includes(:portals, :collection)
             else
               @filterrific.find.where(id: user_collection_ids).includes(:portals, :collection)
             end

    respond_to do |format|
      format.xml { send_data @items.to_xml }
      format.tsv { render 'items/item' }
      format.html {
        @pagy, @items = pagy(@items, items: per_page_setting)
      }
      format.json {
        @pagy, @items = pagy(@items, items: per_page_setting)
      }
      format.js {
        @pagy, @items = pagy(@items, items: per_page_setting)
        render 'shared/filterrific/index'
      }
    end
  end

  def handle_collection_index(scoped_collections = Collection)
    @filterrific = initialize_filterrific(
      scoped_collections,
      params[:filterrific],
      select_options: {
        sorted_by: Collection.sort_options,
        portals: Portal.options_for_select(use_code: true),
        repositories: Repository.options_for_select,
        public_states: Collection.public_states
      }
    )

    @collections = if current_user.super? || current_user.viewer?
                     @filterrific.find.includes(:portals, :repository)
                   else
                     @filterrific.find.where(id: user_collection_ids).includes(:portals, :repository)
                   end

    respond_to do |format|
      format.xml { send_data @collections.to_xml }
      format.html {
        @pagy, @collections = pagy(@collections, items: per_page_setting)
      }
      format.json {
        @pagy, @collections = pagy(@collections, items: per_page_setting)
      }
      format.js {
        @pagy, @collections = pagy(@collections, items: per_page_setting)
        render 'shared/filterrific/index'
      }
    end
  end

  def handle_serial_index(scoped_serials = Serial)
    @filterrific = initialize_filterrific(
      scoped_serials.with_item_count.with_displayed_item_count,
      params[:filterrific],
      select_options: {
        sorted_by: Serial.sort_options
      }
    )

    @serials = @filterrific.find

    respond_to do |format|
      format.xml { send_data @serials.to_xml }
      format.html {
        @pagy, @serials = pagy(@serials, items: per_page_setting)
      }
      format.js {
        @pagy, @serials = pagy(@serials, items: per_page_setting)
        render 'shared/filterrific/index'
      }
    end
  end

  def render_item_list(show_filters: true)
    rendered = if show_filters
                 render_to_string partial: 'items/sort_and_filter', locals: { filterrific: @filterrific }
               else
                 ''.html_safe
               end
    rendered += render_to_string partial: 'shared/filterrific/list', locals: {
      pagy: @pagy,
      filterrific: @filterrific
    }
    rendered
  end

  def render_collection_list(show_filters: true)
    rendered = if show_filters
                 render_to_string partial: 'collections/sort_and_filter', locals: { filterrific: @filterrific }
               else
                 ''.html_safe
               end
    rendered += render_to_string partial: 'shared/filterrific/list', locals: {
      pagy: @pagy,
      filterrific: @filterrific
    }
    rendered
  end

  def render_serial_list(show_filters: true)
    rendered = if show_filters
                 render_to_string partial: 'serials/sort_and_filter', locals: { filterrific: @filterrific }
               else
                 ''.html_safe
               end
    rendered += render_to_string partial: 'shared/filterrific/list', locals: {
      pagy: @pagy,
      filterrific: @filterrific
    }
    rendered
  end

end