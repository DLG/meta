class EventsController < RecordController

  load_and_authorize_resource

  include Blacklight::SearchContext
  #include Blacklight::SearchHelper
  include ErrorHandling
  include MetadataHelper

  before_action :set_event, only: [:show, :edit, :update, :destroy]

  def index
    @filterrific = initialize_filterrific(
      Event,
      params[:filterrific],
      select_options: {
        sorted_by: Event.sort_options,
        public_states: Event.public_states
      }
    )

    @events = @filterrific.find.page(params[:page])
                                      .per(per_page_setting)

    respond_to do |format|
      format.xml { send_data @events.to_xml }
      format.html
      format.js
    end
  end

  def show
  end

  def new
    @event = Event.new
  end

  def edit
  end

  def create
    @event = Event.new(event_params)

    if @event.save
      redirect_to event_path(@event), notice: I18n.t('meta.defaults.messages.success.created', entity: 'Item')
    else
      render :new, alert: I18n.t('meta.defaults.messages.errors.not_created', entity: 'Item')
    end
  end

  def update
    if @event.update(event_params)
      redirect_to event_path(@event), notice: I18n.t('meta.defaults.messages.success.updated', entity: 'Item')
    else
      render :edit, alert: I18n.t('meta.defaults.messages.errors.not_updated', entity: 'Item')
    end
  end

  def destroy
    @event.destroy
    redirect_to events_path, notice: I18n.t('meta.defaults.messages.success.destroyed', entity: 'Item')
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    def set_data
      @data = {}
      @data[:portals] = Portal.all.order(:name)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      prepare_params(
        params.require(:event).permit(:title, :date, :description, :canned_search, :public, :dcterms_temporal,
                                      :dcterms_subject_matches, :dlg_subject_personal_matches, :slug )
      )
    end
end
