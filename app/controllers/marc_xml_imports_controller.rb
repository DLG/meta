# frozen_string_literal: true

# controller for MARC XML import actions
class MarcXmlImportsController < ApplicationController
  load_and_authorize_resource
  include ErrorHandling
  include Sorting

  # show all imports
  def index
    @filterrific = initialize_filterrific(
      MarcXmlImport,
      params[:filterrific],
      select_options: {
        sorted_by: MarcXmlImport.sort_options,
        users: current_user.users_for_menus,
        statuses: MarcXmlImport.status_states
      }
    )

    @marc_xml_imports = @filterrific.find.page(params[:page]).per(per_page_setting).includes(:user)

    respond_to do |format|
      format.html
      format.js
    end
  end

  # show form to create a new import
  def new
    @marc_xml_import = MarcXmlImport.new
  end

  # create an import and queue job
  def create
    import_attributes = marc_xml_import_params.to_h
    import_attributes[:original_filename] = marc_xml_import_params[:file]&.original_filename
    @marc_xml_import = MarcXmlImport.new import_attributes
    @marc_xml_import.user = current_user
    @marc_xml_import.queued_at = Time.now
    if @marc_xml_import.save
      Resque.enqueue(MarcXmlImportWorker, @marc_xml_import.id)
      redirect_to(
        marc_xml_import_path(@marc_xml_import),
        notice: I18n.t('meta.marc_xml_imports.messages.success.queued')
      )
    else
      render :new, alert: I18n.t('meta.defaults.messages.errors.not_created',
                                 entity: 'MARC XML Import')
    end
  end

  def show; end

  def destroy
    redirect_to marc_xml_import_path(@marc_xml_import), notice: "Sorry, you can't delete this."
  end

  def destroy_records
    Serial.all.destroy_all
    Item.where.not(serial_id: nil).update_all(serial_id: nil)
    Item.where.not(source_data: '').destroy_all

    respond_to do |format|
      format.html { redirect_to marc_xml_imports_path, notice: "All XML imported records successfully destroyed." }
    end
  end

  private

  def marc_xml_import_params
    params.require(:marc_xml_import).permit(:title, :description, :file,
                                            :file_cache, :user_id)
  end

end
