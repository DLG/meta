# frozen_string_literal: true

class ToolsController < ApplicationController
  authorize_resource class: false

  def index; end

  def csv_import_tool
    @model_types = [%w[Name Name], %w[Event Event]]
    render 'tools/csv_imports/index'
  end

  def csv_import
    @model_type = params[:model_type]
    @csv_importer = CsvImporter.new @model_type, params[:csv_file].path
    @csv_importer.import
  rescue StandardError => e
    @error = e
  ensure
    render 'tools/csv_imports/results'
  end

  def csv_template
    klass_name = params[:model_type]

    klass_template, _file_name = if klass_name == "Name"
                 ReportService.all_names 2
               else
                 ReportService.all_rows_for klass_name, 2
               end
    respond_to do |format|
      format.csv do
        send_data klass_template, filename: "#{klass_name.underscore}_template_#{Date.today}.csv"
      end
    end
  end
end
