class CollectionsController < RecordController

  load_and_authorize_resource

  include Blacklight::SearchContext
  #include Blacklight::SearchHelper
  include ErrorHandling
  include MetadataHelper
  include Sorting
  include Filterable
  include AssociatedRecords

  before_action :set_data, only: %i[index new create edit update]

  def index
    handle_collection_index
  end

  def show; end

  def new
    @collection = Collection.new
  end

  def create

    @collection = Collection.new collection_params

    respond_to do |format|
      if @collection.save
        format.html { redirect_to collection_path(@collection), notice: I18n.t('meta.defaults.messages.success.created', entity: 'Collection') }
      else
        format.html { render :new }
      end
    end
  end

  def edit
    # TODO: setup_next_and_previous_documents
  end

  def update
    if @collection.update collection_params
      redirect_to collection_path(@collection), notice: I18n.t('meta.defaults.messages.success.updated', entity: ('Collection'))
    else
      render :edit, alert: I18n.t('meta.defaults.messages.errors.not_updated', entity: 'Collection')
    end
  rescue PortalError => e
    @collection.errors.add :portals, e.message
    render :edit, alert: I18n.t('meta.defaults.messages.errors.not_updated', entity: 'Repository')
  end

  def destroy
    @collection.destroy
    redirect_to collections_path, notice: 'Collection destroyed.'
  end

  def reindex_items
    Resque.enqueue(Reindexer, 'Item', @collection.items.map(&:id))
    redirect_to collection_path(@collection), notice: 'Reindex job queued. Check Background Jobs for progress.'
  end

  def solr
  end

  private

  def set_data
    @data = {}
    @data[:subjects] = Subject.all.order(:name)
    @data[:time_periods] = TimePeriod.all.order(:name)
    @data[:repositories] = Repository.all.order(:title)
    @data[:holding_institutions] = HoldingInstitution.all.order(:authorized_name)
    @data[:portals] = portals_for_form
  end

  def portals_for_form
    if @collection && @collection.persisted?
      @collection.repository.portals
    else
      Portal.all.order(:name)
    end
  end

  def collection_params
    prepare_params(
      params.require(:collection).permit(
        :repository_id,
        :slug,
        :public,
        :display_title,
        :short_description,
        :date_range,
        :dc_relation,
        :dc_format,
        :dc_date,
        :dcterms_is_part_of,
        :dcterms_contributor,
        :dcterms_creator,
        :dcterms_description,
        :dcterms_extent,
        :dcterms_medium,
        :dcterms_identifier,
        :dcterms_language,
        :dcterms_spatial,
        :dcterms_publisher,
        :dcterms_rights_holder,
        :dcterms_subject,
        :dcterms_temporal,
        :dcterms_title,
        :edm_is_shown_at,
        :edm_is_shown_by,
        :dcterms_license,
        :dlg_local_right,
        :dcterms_bibliographic_citation,
        :dlg_subject_personal,
        :partner_homepage_url,
        :homepage_text,
        :sponsor_note,
        :sponsor_image,
        :sponsor_image_cache,
        :remove_sponsor_image,
        :sensitive_content,
        holding_institution_ids: [],
        dc_right: [],
        dcterms_type: [],
        subject_ids: [],
        time_period_ids: [],
        other_repositories: [],
        portal_ids: [],
        collection_geographic_locations_attributes: edit_geographic_locations_params
      )
    )

  end

  def start_new_search_session?
    action_name == 'index'
  end
end

