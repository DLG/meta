class ApplicationController < ActionController::Base
  helper Openseadragon::OpenseadragonHelper
  include Blacklight::Controller
  include Pagy::Backend

  layout :determine_layout if respond_to? :layout

  append_view_path Rails.root.join('app/subform_builders/views')

  before_action :set_paper_trail_whodunnit
  before_action :authenticate_user!
  before_action :prepare_exception_notifier

  # https://github.com/heartcombo/devise#controller-filters-and-helpers
  protect_from_forgery prepend: true

  helper_method :per_page_setting

  def user_for_paper_trail
    current_user ? current_user.id : nil
  end

  protected

  def prepare_geographic_locations record_geographic_locations_attributes
    record_geographic_locations_attributes.each do |_k, attrs|
      if attrs[:id].blank?
        attrs.delete :id
        geoloc_attrs = attrs[:geographic_location_attributes]
        unless geoloc_attrs[:id].present?
          geoloc_attrs = GeographicLocationLookupService.find_or_initialize(geoloc_attrs['code']).attributes
        end
        if geoloc_attrs['id'].present?
          attrs[:geographic_location_id] = geoloc_attrs['id']
          attrs.delete :geographic_location_attributes
        else
          geoloc_attrs[:created_by] = geoloc_attrs[:updated_by] = current_user.email
          attrs[:geographic_location_attributes] = geoloc_attrs
        end
      end
    end
  end

  def prepare_simple_many_to_many(attributes, relation)
    attributes.each do |_k, attrs|
      # If our model (which is a join table like item_names) accepts attributes for its inner model (e.g., names),
      # then those attributes will be in attrs['name_attributes'].
      # But if (sticking with our example) item_names, does not accept attributes for names, those same parameters will
      # be in attrs['name'].
      inner_attrs_key = attrs.include?("#{relation}_attributes") ? "#{relation}_attributes" : relation
      if attrs[:id].blank?
        attrs.delete :id
        inner_attrs = attrs[inner_attrs_key]
        next if inner_attrs.nil?
        if inner_attrs['id'].present?
          attrs["#{relation}_id".to_sym] = inner_attrs['id']
        end
      end
      attrs.delete inner_attrs_key
    end
  end

  def edit_geographic_locations_params
    [:id, :_destroy, geographic_location_attributes: [:id, :geonames_id, :code]]
  end

  def per_page_cookie
    cookies["per_page.#{controller_name}.#{action_name}"].presence&.to_i
  end

  def per_page_cookie=(value)
    cookies.permanent["per_page.#{controller_name}.#{action_name}"] = value
  end

  def per_page_setting
    explicitly_set = params[:per_page].presence&.to_i
    if explicitly_set
      self.per_page_cookie = explicitly_set
    else
      per_page_cookie || Pagy::DEFAULT[:items]
    end
  end

  private

  def prepare_exception_notifier
    request.env['exception_notifier.exception_data'] = {
      current_user: current_user ? current_user.email : 'none'
    }
  end

end
