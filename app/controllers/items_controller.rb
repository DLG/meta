# Handles CRUD for Items
# TODO: Factor out de facto index method for deleted items (ItemVersion)
class ItemsController < RecordController

  load_and_authorize_resource

  include Blacklight::SearchContext
  #include Blacklight::SearchHelper
  include ErrorHandling
  include MetadataHelper
  include Sorting
  include Filterable
  include AssociatedRecords

  def index
    handle_item_index
  end

  def show; end

  def new
    @item = Item.new
  end

  def new_from_serial
    @item = Item.new(serial_id: params[:serial_id])
    @item.set_fields_from_serial
    @item.collection_id = @item.serial.generate_collection&.id
    @item.holding_institutions = @item.serial.generate_holding_institutions

    render :new
  end

  def create
    @item = Item.new item_params
    if @item.save
      redirect_to item_path(@item), notice: I18n.t('meta.defaults.messages.success.created', entity: 'Item')
    else
      render :new, alert: I18n.t('meta.defaults.messages.errors.not_created', entity: 'Item')
    end
  end

  def copy
    @item = Item.new(@item.attributes.except('id').merge geographic_locations: @item.geographic_locations,
                                                         holding_institutions: @item.holding_institutions,
                                                         portals: @item.portals)
    render :edit
  end

  def edit
    # TODO: setup_next_and_previous_documents
  end

  def update
    if @item.update item_params
      redirect_to item_path(@item), notice: I18n.t('meta.defaults.messages.success.updated', entity: 'Item')
    else
      render :edit, alert: I18n.t('meta.defaults.messages.errors.not_updated', entity: 'Item')
    end
  end

  def destroy
    @item.destroy
    redirect_to items_path, notice: I18n.t('meta.defaults.messages.success.destroyed', entity: 'Item')
  end

  def multiple_destroy
    Item.destroy(multiple_action_params[:entities].split(','))
    Sunspot.commit
    respond_to do |format|
      format.json { render json: {}, status: :ok }
    end
  end

  def xml
    ids = if params[:entities].is_a? String
            params[:entities].split(',')
          else
            params[:entities]
          end
    @items = Item.where id: ids
    respond_to do |format|
      format.xml { render xml: @items }
    end
  end

  def fulltext; end

  def deleted

    set_filter_options [:user]

    @item_versions = ItemVersion
                       .index_query(params)
                       .where(item_type: 'Item', event: 'destroy')
                       .order(sort_column('item_versions') + ' ' + sort_direction)
                       .page(params[:page])
                       .per(per_page_setting)
  end

  def solr
  end

  private

  def item_params
    prepare_params(
      params.require(:item).permit(
        :collection_id,
        :serial_id,
        :slug,
        :dpla,
        :public,
        :local,
        :date_range,
        :dc_relation,
        :dc_format,
        :dc_date,
        :dc_right,
        :dcterms_is_part_of,
        :dcterms_contributor,
        :dcterms_creator,
        :dcterms_description,
        :dcterms_extent,
        :dcterms_medium,
        :dcterms_identifier,
        :dcterms_language,
        :dcterms_spatial,
        :dcterms_publisher,
        :dcterms_rights_holder,
        :dcterms_subject,
        :dcterms_temporal,
        :dcterms_title,
        :edm_is_shown_at,
        :edm_is_shown_by,
        :dcterms_bibliographic_citation,
        :dlg_local_right,
        :dlg_subject_personal,
        :fulltext,
        :iiif_partner_url,
        :iiif_partner_url_enabled,
        holding_institution_ids: [],
        dcterms_type: [],
        other_collections: [],
        portal_ids: [],
        item_geographic_locations_attributes: edit_geographic_locations_params,
        item_names_attributes: {},
        event_ids: [],
        external_identifiers: {}
      )
    )
  end

  def multiple_action_params
    params.permit(:entities, :format)
  end

  def start_new_search_session?
    action_name == 'index'
  end

end
