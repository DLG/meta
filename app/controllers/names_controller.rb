class NamesController < RecordController

  load_and_authorize_resource

  include Blacklight::SearchContext
  #include Blacklight::SearchHelper
  include ErrorHandling
  include MetadataHelper
  include Sorting
  include Filterable
  include TypeaheadSuggestions

  before_action :set_name, only: [:show, :edit, :update, :destroy]
  before_action :set_data, only: %i[index new create edit update]

  typeahead_suggestions :suggest,
                        -> (term) { Name.text_search(term) },
                        exact_match_finder: -> (term) { Name.where(authoritative_name: term).first }

  def index
    @filterrific = initialize_filterrific(
      Name,
      params[:filterrific],
      select_options: {
        sorted_by: Name.sort_options,
        portals: Portal.options_for_select(use_code: true),
        public_states: Name.public_states,
        name_types: Name.name_types_for_menus
      }
    )

    @names = @filterrific.find.includes(:portals).page(params[:page])
                         .per(per_page_setting)

    respond_to do |format|
      format.xml { send_data @names.to_xml }
      format.html
      format.js
    end
  end

  def show
  end

  def new
    @name = Name.new
  end

  def edit
  end


  def create
    @name = Name.new(name_params)

    if @name.save
      redirect_to name_path(@name), notice: I18n.t('meta.defaults.messages.success.created', entity: 'Name')
    else
      render :new, alert: I18n.t('meta.defaults.messages.errors.not_created', entity: 'Name')
    end
  end

  def update
    if @name.update(name_params)
      redirect_to name_path(@name), notice: I18n.t('meta.defaults.messages.success.updated', entity: 'Name')
    else
      render :edit, alert: I18n.t('meta.defaults.messages.errors.not_updated', entity: 'Name')
    end
  end

  def destroy
    @name.destroy
    redirect_to names_path, notice: I18n.t('meta.defaults.messages.success.destroyed', entity: 'Name')
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_name
      @name = Name.find(params[:id])
    end

    def set_data
      @data = {}
      @data[:portals] = Portal.all.order(:name)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def name_params
      prepare_params(
        params.require(:name).permit(
          :slug, :authoritative_name, :name_type, :alternate_names_public, :alternate_names_not_public,
          :related_names, :bio_history, :authoritative_name_source,
          :source_uri, :public, :filterrific, :oclc_number, :additional_slugs, :legacy_slug, portal_ids:[]
        )
      )
    end
end
