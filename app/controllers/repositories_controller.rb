# Controller action for Repository functionality, duh
class RepositoriesController < ApplicationController
  load_and_authorize_resource
  include ErrorHandling
  include Sorting
  include Filterable
  before_action :set_data, only: %i[index new create edit update]

  def index
    @filterrific = initialize_filterrific(
      Repository,
      params[:filterrific],
      select_options: {
        portals: Portal.options_for_select(use_code: true),
        public_states: Repository.public_states
      }
    )

    @pagy, @repositories = if current_user.super? || current_user.viewer?
                           pagy(@filterrific.find.includes(:portals), items: per_page_setting)
                         else
                           pagy(@filterrific.find.where(id: current_user.repository_ids).includes(:portals), items: per_page_setting)
                         end

    respond_to do |format|
      format.html
      format.js {
        render 'shared/filterrific/index'
      }
    end
  end

  def show; end

  def new
    @repository = Repository.new
  end

  def create
    @repository = Repository.new repository_params
    if @repository.save
      redirect_to repository_path(@repository), notice: I18n.t('meta.defaults.messages.success.created', entity: 'Repository')
    else
      render :new, alert: I18n.t('meta.defaults.messages.errors.not_created', entity: 'Repository')
    end
  end

  def edit; end

  def update
    if @repository.update(repository_params)
      redirect_to repository_path(@repository), notice: I18n.t('meta.defaults.messages.success.updated', entity: 'Repository')
    else
      render :edit, alert: I18n.t('meta.defaults.messages.errors.not_updated', entity: 'Repository')
    end
  rescue PortalError => e
    @repository.errors.add :portals, e.message
    render :edit, alert: I18n.t('meta.defaults.messages.errors.not_updated', entity: 'Repository')
  end

  def destroy
    @repository.destroy
    redirect_to repositories_path, notice: I18n.t('meta.defaults.messages.success.destroyed', entity: 'Repository')
  end

  private

  def set_data
    @data = {}
    @data[:portals] = Portal.all.order(:name)
  end

  def repository_params
    params
      .require(:repository)
      .permit(:slug, :title, :notes, :public, portal_ids: [])
  end

end
