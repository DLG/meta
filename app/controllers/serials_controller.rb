class SerialsController < RecordController
  load_and_authorize_resource
  include Sorting
  include Filterable
  include AssociatedRecords

  # GET /serials or /serials.json
  def index
    handle_serial_index
  end

  # GET /serials/1 or /serials/1.json
  def show
  end

  # GET /serials/new
  def new
    @serial = Serial.new
  end

  # GET /serials/1/edit
  def edit
  end

  # POST /serials or /serials.json
  def create
    @serial = Serial.new(serial_params)

    respond_to do |format|
      if @serial.save
        format.html { redirect_to serial_url(@serial), notice: "Serial was successfully created." }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /serials/1 or /serials/1.json
  def update
    respond_to do |format|
      if @serial.update(serial_params)
        format.html { redirect_to serial_url(@serial), notice: "Serial was successfully updated." }
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /serials/1 or /serials/1.json
  def destroy
    @serial.destroy

    respond_to do |format|
      format.html { redirect_to serials_url, notice: "Serial was successfully destroyed." }
    end
  end

  def items
    handle_item_index @serial.items
  end

  def solr
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_serial
      @serial = Serial.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def serial_params
      prepare_params(params.require(:serial)
                           .permit(:slug, :dcterms_title, :dcterms_description, :dcterms_publisher,
                                   :dcterms_description, :dcterms_subject, :dcterms_medium, :dcterms_replaces,
                                   :dcterms_is_replaced_by, :dcterms_contributor, :dcterms_creator,
                                   :dcterms_subject_fast, :dc_date, :dc_right, :dlg_subject_personal,
                                   :dcterms_language, :dcterms_frequency, dcterms_type: [],
                                   external_identifiers: {},
                                   serial_geographic_locations_attributes: edit_geographic_locations_params))
    end

  def xml_import_params
    prepare_params(params.permit(:xml_file, :_method, :authenticity_token, :button))
  end
end
