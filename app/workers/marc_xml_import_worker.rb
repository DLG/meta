# job to process marc xml into serials and items
class MarcXmlImportWorker
  @queue = :xml

  def self.perform(marc_xml_import_id)
    @marc_xml_import = MarcXmlImport.find(marc_xml_import_id)

    importer = MarcXmlImporter.new @marc_xml_import.file.current_path
    begin
      importer.start
      status = 'success'
      if importer.records_failed > 0
        status = (importer.records_processed - importer.records_failed > 0) ? 'partial failure' : 'failed'
      end
      results = {
        'status' => status,
        'records' => importer.results,
        'records_processed' => importer.records_processed,
        'records_failed' => importer.records_failed,
        'serials_created' => importer.serials_created,
        'serials_updated' => importer.serials_updated,
        'skipped' => importer.skipped
      }
    rescue StandardError => e
      results = {
        'status' => 'failed',
        'message' => e.message
      }
    end

    @marc_xml_import.results = results

    @marc_xml_import.finished_at = Time.now
    @marc_xml_import.save
  end

end