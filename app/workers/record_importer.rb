# job to process source records into batch_items
include XmlImportHelper
class RecordImporter
  MAX_BATCH_SIZE = 8_000
  @queue = :xml
  # @logger = Logger.new('./log/xml_import.log')
  @slack = Slack::Notifier.new Rails.application.credentials.slack_worker_webhook.dig(Rails.env.to_sym)

  def self.batch_too_large?
    item_nodes = @batch_import.xml.match(/<item>/)
    return unless item_nodes.present?

    item_nodes_count = item_nodes.length
    batch_size = @batch_import.batch.batch_items_count
    new_size = item_nodes_count + batch_size
    if new_size > MAX_BATCH_SIZE
      raise(JobTooBigError, "This XML Ingest would result in a Batch over the limit of #{MAX_BATCH_SIZE} batch items.")
    end
  end

  def self.perform(batch_import_id)
    t1 = Time.now
    @batch_import = BatchImport.find(batch_import_id)

    @added = 0
    @failed = 0
    @batch = @batch_import.batch
    @validate = @batch_import.validations?
    @added = []
    @updated = []
    @failed = []

    @records = []

    import_type = @batch_import.format

    case import_type
    when 'file', 'text'
      xml_import(@batch_import)
    when 'search query'
      search_query_import(@batch_import)
    else
      total_failure 'No format specified'
    end

    @batch_import.results = {
      added: @added,
      updated: @updated,
      failed: @failed
    }

    @batch_import.completed_at = Time.now

    save_batch_import
    t2 = Time.now
    notify "BatchImport `#{batch_import_id}` complete in `#{t2 - t1}` seconds. `#{@added.length}` new. `#{@updated.length}` updated and `#{@failed.length}` errors."

  end

  def self.xml_import(batch_import)
    count = 0
    begin
      batch_too_large?
      Nokogiri::XML::Reader(batch_import.xml).each do |node|
        next unless node.name == 'item' && node.node_type == Nokogiri::XML::Reader::TYPE_ELEMENT

        count += 1
        record = Hash.from_xml(node.outer_xml)
        unless record
          add_failed(count, 'Item node could not be converted to hash.')
          next
        end
        unless record.key? 'item'
          add_failed(count, 'No Item node could be extracted from XML')
          next
        end
        unless record['item'].is_a?(Hash)
          add_failed(count, "Item could not be extracted from XML: #{node.outer_xml} ")
          next
        end
        # MK 3/12/2019
        # Moved this outside fo the XML Reader to hopefully save some memory
        # create_or_update_record count, batch_item['item']
        @records << record['item']
      end
    rescue Nokogiri::XML::SyntaxError => e
      total_failure "Fundamental XML parsing error: #{e.message}"
    rescue JobTooBigError => e
      total_failure e.message
    rescue StandardError => e
      total_failure "Problem extracting data in batch_item #{count}: #{e.message}"
    end

    if @records.any?
      @records.each_with_index do |record, i|
        upsert_batch_item i + 1, record
      end
    else
      total_failure 'No records could be extracted from the XML'
    end
  end

  def self.search_query_import(batch_import)
    batch_import.item_ids.each_with_index do |id, index|
      begin
        i = Item.find id
        batch_item = i.to_batch_item
        batch_item.batch_import = batch_import
        batch_item.batch = batch_import.batch
        batch_item.portals = i.portals
        batch_item.geographic_locations = i.geographic_locations
        batch_item.holding_institutions = i.holding_institutions
        batch_item.save(validate: false)
        add_updated batch_item
      rescue ActiveRecord::RecordNotFound
        add_failed(index, "Record with ID #{id} could not be found to add to Batch.",)
      rescue StandardError => e
        add_failed(index, "Item #{i.record_id} could not be added to Batch: #{e}")
      end
    end
  end

  def self.upsert_batch_item(num, record_data)
    record_data = XmlImportHelper.prepare_item_hash(record_data)

    batch_item, action  = create_batch_item(num, record_data)

    return unless batch_item

    begin
      if batch_item.save!(validate: @validate)
        if action == :update
          add_updated(batch_item)
        else
          add_added(batch_item)
        end
      else
        add_failed(num, batch_item.errors, safe_record_slug)
      end
    rescue ActiveRecord::RecordInvalid => e
      message = "XML contains invalid record #{record_data['slug']}. Validation message: #{e}"
      add_failed(num, message, safe_record_slug)
    rescue StandardError => e
      add_failed(num, "Could not save record #{record_data['slug']}. Error: #{e}", safe_record_slug)
    end
  end

  def self.find_collection(num, record_data)
    collection_data = record_data['collection']

    unless collection_data.is_a?(Hash) # Will also catch the nil case
      add_failed(num, "No collection node could be extracted for record #{num}. Data provided: #{collection_data}")
      return
    end

    unless collection_data['slug'] || collection_data['record_id']
      add_failed num, "Collection slug for record ##{num} could not be extracted from XML."
      return
    end
    unless collection = Collection.find_by_slug(collection_data['slug']) || Collection.find_by_record_id(collection_data['record_id'])
      add_failed num, "Collection for record #{record_data['slug']} could not be found using record id: #{collection_data['record_id']}." if collection_data['record_id']
      add_failed num, "Collection for record #{record_data['slug']} could not be found using slug: #{collection_data['slug']}." if collection_data['slug']
      return
    end
    collection
  end

  def self.safe_record_slug
    @batch_item.try(:slug) ? @batch_item.slug : 'Slug could not be determined'
  end

  def self.create_batch_item(num, record_data)
    serial = get_serial(num, record_data)
    collection = if record_data['collection']
                   find_collection(num, record_data)
                 else
                   serial&.generate_collection
                 end
    return unless collection

    existing_item = if @batch_import.match_on_id?
                      Item.find record_data['id']
                    else
                      # look for existing item based on unique attributes
                      Item.find_by(slug: record_data['slug'], collection: collection)
                    end

    if existing_item.nil? && @batch_import.match_on_id?
      add_failed num, "Item with database ID #{record_data['id']} not found." unless existing_item
      return
    end

    batch_item, action = if existing_item
                           [existing_item.to_batch_item, :update]
                         else
                           [BatchItem.new, :add]
                         end
    batch_item.assign_attributes filtered_params(record_data)
    batch_item.serial = serial
    batch_item.set_fields_from_serial

    batch_item.collection = collection
    batch_item.holding_institutions = if record_data['dcterms_provenance']
                                        get_holding_institutions(record_data)
                                      else
                                        serial&.generate_holding_institutions
                                      end
    batch_item.portals = get_portals(record_data)
    batch_item.other_collections = get_other_collections(record_data)
    batch_item.batch = @batch
    batch_item.batch_import = @batch_import

    [batch_item, action]
  rescue StandardError => e
    add_failed num, "Generating BatchItem failed for batch_item #{record_data['slug']}. Error: #{e} \n #{e.backtrace}"
    return
  end

  # reject any values the db is not prepared for...
  def self.filtered_params(record_data)
    excluded_columns = %w[id portals other_colls dcterms_provenance] #These are handled in separate methods
    allowed_columns = BatchItem.column_names - excluded_columns + ['dcterms_spatial']
    prepared_data = {}
    record_data.each do |column_name, column_value|
      next unless allowed_columns.include?(column_name)

      prepared_data[column_name] = if column_value.is_a? Array
                           column_value.map do |val|
                              if val.is_a? String
                                val.gsub('\\n', "\n").gsub('\\t', "\t").strip
                              else
                                val
                              end
                           end
                         else
                           column_value
                         end
    end
    prepared_data
  end

  def self.add_failed(num, message, slug = nil)
    @failed << {
      number: num,
      slug: slug,
      message: message
    }
  end

  def self.add_added(batch_item)
    @added << {
      batch_item_id: batch_item.id,
      slug: batch_item.slug
    }
  end

  def self.add_updated(batch_item)
    @updated << {
      batch_item_id: batch_item.id,
      item_id: batch_item.item.id,
      slug: batch_item.item.slug
    }
  end

  def self.total_failure(msg)
    notify "Batch Import `#{@batch_import.id}` failed: #{msg}"
    add_failed 0, msg
    # @batch_import.results = {
    #   added: @added,
    #   updated: @updated,
    #   failed: [{ number: 0, message: msg }]
    # }
    # save_batch_import
  end

  def self.save_batch_import
    raise JobFailedError("BatchImport could not be updated: #{@batch_import.errors.inspect}") unless @batch_import.save
  end

  def self.get_portals(record_data)
    return [] unless record_data['portals']
    portals_info = record_data['portals']
    portals_info.map { |portal| Portal.find_by_code portal['code'] }
  rescue StandardError => e
    raise StandardError, "Portal values could not be set. #{record_data} #{e}"
  end

  def self.get_other_collections(record_data)
    return [] unless record_data['other_colls']

    record_data['other_colls'].map do |other_coll|
      Collection.find_by_record_id(other_coll['record_id']).id if other_coll['record_id']
    end
  rescue StandardError => e
    raise StandardError, 'Other Collections values could not be set.'
  end

  def self.get_holding_institutions(record_data)
    return [] unless record_data['dcterms_provenance']

    record_data['dcterms_provenance'].map do |holding_institution_name|
      holding_institution = HoldingInstitution.find_by_authorized_name holding_institution_name
      raise(StandardError, "Could not find Holding Institution for '#{holding_institution_name}'") unless holding_institution

      holding_institution
    end
  rescue StandardError => e
    raise StandardError, "Problem setting Holding Institution: #{e}."
  end

  def self.get_serial(num, record_data)
    return unless record_data['serial']

    serial_data = record_data['serial']

    unless serial_data.is_a?(Hash) # Will also catch the nil case
      raise StandardError, "Serial node could be extracted for batch_item #{num}. Data provided: #{serial_data}"
    end

    if serial_data['slug'].blank?
      raise StandardError, "Serial slug for batch_item ##{num} could not be extracted from XML."
    end

    serial = Serial.find_by_slug(serial_data['slug'])

    if serial.nil?
      message = "Serial for batch_item #{record_data['slug']} could not be found using slug: #{serial_data['slug']}."
      raise StandardError, message
    end

    serial
  end

  def self.notify(msg)
    @slack.ping msg unless Rails.env.development? || Rails.env.test?
  end

end