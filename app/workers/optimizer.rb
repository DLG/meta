# handles optimization of the solr index
class Optimizer
  @queue = :optimize
  @slack = Slack::Notifier.new Rails.application.credentials.slack_worker_webhook.dig(Rails.env.to_sym)

  def self.perform
    t1 = Time.now
    @slack.ping 'Solr optimize starting'
    Sunspot.optimize
    elapsed = Time.now - t1
    @slack.ping("Solr optimize completed in #{elapsed} seconds")
  end

end