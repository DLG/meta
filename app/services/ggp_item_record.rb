# frozen_string_literal: true

class GgpItemRecord < GgpXmlRecord
  def slug
    slug_from_856 || slug_from_call_number
  end

  def slug_from_856
    slugs_in_urls = electronic_location_access.map do |url|
      url =~ /(?:dlg_ggpd_|id:|key:|id%3[aA]|key%3[aAdD])([\w-]+)/ && $1
    end
    slugs_in_urls.compact.first
  end

  def slug_from_call_number
    call_number = external_identifiers.filter { |x| x[:type] == 'Call Number' }.first
    return nil unless call_number
    GgpQueryable.slug_from_call_number call_number[:value].sub(/\/$/, '')
  end

  def portal_ids
    [1]
  end

  def edm_is_shown_at
    ["#{Rails.configuration.dlg_url}/record/dlg_ggpd_#{slug}"]
  end

  def edm_is_shown_by
    ["#{Rails.configuration.dlg_url}/do:dlg_ggpd_#{slug}"]
  end

  def dcterms_type
    %w[Text]
  end

  def dc_right
    [I18n.t(:uri, scope: [:meta, :rights, :noc_us])]
  end

  def dcterms_subject
    subjects = super

    subjects << 'Georgia'
  end

  def dcterms_medium
    mediums = super
    mediums << 'Monograph'
  end

  def public
    true
  end

  def to_hash(remove_methods: [], add_methods: [] )
    remove = %i(slug_from_856 slug_from_call_number)
    super(remove_methods: (remove_methods + remove), add_methods: add_methods )
  end
end
