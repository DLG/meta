# frozen_string_literal: true

require 'csv'

# Class to import CSV data
class CsvImporter
  SUPPORTED_MODELS = [Name, Event].freeze

  attr_reader :csv_hash, :model_type, :created_models, :updated_models

  # initialization for CsvImporter.new
  #
  # @param model_type [String] `Name` or `Event`
  # @error model_type must be in SUPPORTED_MODELS
  # @param csv_source [String] `csv string` or `csv file path`
  # @param is_string [Boolean] if true then `csv string` else `csv file path`
  # @return [void]
  def initialize(model_type, csv_source, is_string = false)
    @is_string = is_string
    @csv_hash = CsvImporter.extract_csv_hash csv_source, true
    @model_type = model_type.constantize
    raise ArgumentError, "#{model_type} not supported" unless SUPPORTED_MODELS.include? @model_type

    @created_models = []
    @updated_models = []
  end

  # Returns the union of field names in csv file with the column names on the active record model.
  #
  # @return [Array<Symbol>]
  def updated_fields
    csv_headers = @csv_hash.first.keys.map(&:to_sym)
    field_names = csv_headers & @model_type.column_names.map(&:to_sym)
    if @model_type == Name
      field_names << :portal_names if csv_headers.include? :portals
    end
    field_names
  end

  # Starts the CSV import
  #
  # @return [void]
  def import
    @csv_hash.each do |row|
      row = prepare_fields(row)

      target = find_target_model(row)
      if target
        target.update! row
        @updated_models << target
      else
        target = @model_type.create! row.except(:id)
        @created_models << target
      end
    end
  end

  # Parses csv_source with correct encoding and removes headers
  #
  # @param csv_source [String]
  # @param symbol_headers [Boolean]
  # @return [Array<Hash{Symbol, String => String}>] Array of row hashes
  def self.extract_csv_hash(csv_source, symbol_headers = false)
    csv_rows, csv_headers = parse_csv(csv_source)
    rows = []
    csv_rows.each do |row|
      hash = {}
      row.each_with_index do |val, index|
        if symbol_headers
          hash[csv_headers[index].to_sym] = val
        else
          hash[csv_headers[index]] = val
        end

      end
      rows << hash
    end
    rows
  end

  # Parses csv_source with correct encoding
  #
  # @param csv_source [String]
  # @error header cells must not be blank
  # @return [Array<Array<String>>, Array<String>] Array of csv rows and Array of headers
  def self.parse_csv(csv_source)
    if @is_string
      array = CSV.parse(csv_source)  # csv string
    else
      array = CSV.read(csv_source)   # csv file path
    end
    headers = array.shift
    if headers.any?(&:blank?)
      raise Exceptions::CsvImporterError.new, "All Header values must have at least one character. Headers: #{headers}"
    end
    headers.each do |header|
      header.downcase!
    end

    rows = array.map { |row| row[0..(headers.length - 1)] }
    [rows, headers]
  end

  private

  # find model to update--by id if Name, by slug if Event
  # TODO: do we want to fall back to by slug for Name if no :id in headers?
  #
  # @param hash [Hash{Symbol, String => String}] csv row hash
  # @return [void]
  def find_target_model(hash)
    if @model_type == Name
      if hash[:id]
        @model_type.find hash[:id]
      else
        nil
      end
    else
      @model_type.find_by_slug hash[:slug]
    end
  end

  # ensure that each field conforms to the model attributes
  #
  # @param hash [Hash{Symbol, String => String}] csv row hash
  # @return [Hash{Symbol, String => String}] prepared csv row hash
  def prepare_fields(hash)
    if @model_type == Name
      prepare_name_fields(hash)
    elsif @model_type == Event
      prepare_event_fields(hash)
    else
      hash
    end
  end

  # returns an attribute hash that can be used to create/update a Name
  #
  # @param (see #prepare_fields)
  # @return (see #prepare_fields)
  def prepare_name_fields(hash)
    return hash unless @model_type == Name

    filtered_hash = {}

    if hash.has_key? :portals
      filtered_hash[:portal_ids] = prepare_portal_ids hash[:portals]
    end

    if hash.has_key? :public
      filtered_hash[:public] = parse_boolean_field('public', hash[:public])
    end

    [:id, :slug].each do |field|
      if hash.has_key? field
        filtered_hash[field] = hash[field].dup
      end
    end

    [:additional_slugs,
     :alternate_names_public,
     :alternate_names_not_public,
     :related_names,
     :authoritative_name_source].each do |field|
      if hash.has_key? field
        filtered_hash[field] = parse_array_field(hash[field])
      end
    end

    [:name_type,
     :authoritative_name,
     :legacy_slug,
     :bio_history,
     :source_uri,
     :oclc_number].each do |field|
      if hash.has_key? field
        filtered_hash[field] = parse_string_field(hash[field])
      end
    end

    filtered_hash
  end

  # convert portal codes (e.g., `crdl, georgia`) to portal ids
  #
  # @param portal_codes [String]
  # @return [Array<Integer>]
  def prepare_portal_ids(portal_codes)
    return [] if portal_codes.blank?

    Portal.where(code: portal_codes.split(/[,\s]+/)).map(&:id)

  end

  # returns an attribute hash that can be used to create/update an Event
  #
  # @param (see #prepare_fields)
  # @return (see #prepare_fields)
  def prepare_event_fields(hash)
    return hash unless @model_type == Event

    filtered_hash = {}

    if hash.has_key? :public
      filtered_hash[:public] = parse_boolean_field('public', hash[:public])
    end

    [:dcterms_temporal,
     :dcterms_subject_matches,
     :dlg_subject_personal_matches].each do |field|
      if hash.has_key? field
        filtered_hash[field] = parse_array_field(hash[field])
      end
    end

    [:slug,
     :title,
     :description].each do |field|
      if hash.has_key? field
        filtered_hash[field] = parse_string_field(hash[field])
      end
    end

    filtered_hash
  end

  # returns array even if field is blank/nil
  #
  # @param array_string [String, nil] serialized array
  # @return [Array<String>]
  def parse_array_field(array_string)
    return [] if array_string.blank?
    JSON.parse(array_string.strip)
  end

  # returns string even if field is blank/nil
  #
  # @param string [String, nil] serialized array
  # @return [String]
  def parse_string_field(string)
    return "" if string.blank?
    string.strip
  end

  # returns string that ActiveRecord should interpret as boolean
  #
  # @param field [String] field name for error message
  # @param bool [String] string that represents true/false
  # @error bool cannot be blank/nil
  # @return [String]
  def parse_boolean_field(field, bool)
    raise Exceptions::CsvImporterError.new, "boolean field #{field} can't be blank: should be 'true' or 'false'" if bool.blank?
    bool
  end

end
