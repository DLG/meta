class GgpItemMatchingService

  def initialize(serials: Serial.all, notifier: nil, previous_856_lookup: nil)
    @serials = Serial.where(id: serials.select(:id))
    @serial_titles = {}
    @serial_slugs = {}
    @serial_call_numbers = {}
    @serial_ultimate_queries = {}
    @ggp_collection = Collection.find_by_slug 'ggpd'
    @ggp_queryable = GgpQueryable.new
    @ultimate_url_matches = Hash.new { |h, k| h[k] = Set.new }
    @call_number_matches = Hash.new { |h, k| h[k] = Set.new }
    @previous_856_lookup = previous_856_lookup
    @notifier = notifier
  end

  attr_reader :ggp_queryable, :ggp_collection, :serials, :previous_856_lookup

  def match_serials_to_items
    find_all_possible_matches
    commit_best_matches
  end

  def find_all_possible_matches
    serials.find_each do |serial|
      log_info "Finding matches for #{serial.slug}"
      record = GgpXmlRecord.new(serial.source_xml)
      ultimate_url_matches_for(serial, record).each do |item_id|
        next unless %w[y i].include? ggp_queryable.get_item(item_id)['rt']
        @ultimate_url_matches[item_id] << serial.id
      end
      record.gadocs_call_numbers.each do |call_num|
        items_for_call_number(call_num).each do |item_id|
          next unless %w[y i].include? ggp_queryable.get_item(item_id)['rt']
          @call_number_matches[item_id] << serial.id
        end
      end
      @serial_slugs[serial.id] = serial.slug
      @serial_titles[serial.id] = serial.dcterms_title
      @serial_call_numbers[serial.id] = record.gadocs_call_numbers
    end
  end

  def commit_best_matches
    csv_headers = [
      'Item Id',
      'Item Slug',
      'Best Serial Match',
      'Potential Matches',
      'Item Call Number',
      'Ultimate Query',
      'Serial Call Number(s)',
      'Call Number Match?',
      'Ultimate Query Match?',
      'Date Match?',
      'Suppl Status Match?',
      'Title Match Quality',
      'Is Best Title Match'
    ]
    CSV.open("#{Rails.root}/tmp/ggp_matching_report-#{Date.today}.csv",
                     'wb',
                     headers: csv_headers) do |csv|
      csv << csv_headers
      all_matched_items.each do |item_id|
        item_slug = ggp_queryable.get_item(item_id)['id']

        ultimate_url_matches = @ultimate_url_matches[item_id]
        call_number_matches = @call_number_matches[item_id]
        serial_ids = ultimate_url_matches | call_number_matches

        # rank serial ids in ascending order of match quality
        ranked = serial_ids.sort_by do |serial_id|
          [
            ultimate_url_matches.include?(serial_id) ? 1 : 0,
            dates_possibly_compatible?(item_id, serial_id) ? 1 : 0,
            same_suppl_status?(item_id, serial_id) ? 1 : 0,
            best_title_match_quality(item_id, serial_id),
            call_number_matches.include?(serial_id) ? 1 : 0
          ]
        end

        matching_id = ranked.last
        save_match item_id, matching_id

        best_title_match = ranked.sort_by{|s| best_title_match_quality(item_id, s)}.last
        call_numbers = @serial_call_numbers[matching_id]
        ultimate_query = @serial_ultimate_queries[matching_id]
        item_call_number = ggp_queryable.get_item(item_id)['ca']

        # call_number_match may be true, false, nil (if no call numbers), or 'punctuation/spacing difference'
        call_number_match = nil
        call_number_match = call_number_matches.include?(matching_id) if call_numbers.any?
        if call_number_match == false
          without_spaces_and_punc = reduce_spaces_and_punctuation(item_call_number)
          call_numbers.each do |call_num|
            if without_spaces_and_punc.starts_with? reduce_spaces_and_punctuation(normalized_call_number(call_num))
              call_number_match = 'punctuation/spacing difference'
            end
          end
        end

        csv << {
                 'Item Id' => item_id,
                 'Item Slug' => item_slug,
                 'Best Serial Match' => @serial_slugs[matching_id],
                 'Potential Matches' => serial_ids.size,
                 'Item Call Number' => item_call_number,
                 'Ultimate Query' => ultimate_query,
                 'Serial Call Number(s)' => call_numbers.join(' | '),
                 'Call Number Match?' => call_number_match,
                 'Ultimate Query Match?' => (ultimate_url_matches.include?(matching_id) if ultimate_query),
                 'Date Match?' => dates_possibly_compatible?(item_id, matching_id),
                 'Suppl Status Match?' => same_suppl_status?(item_id, matching_id),
                 'Title Match Quality' => best_title_match_quality(item_id, matching_id),
                 'Is Best Title Match' => matching_id == best_title_match
               }
      end
    end

    unmatched_item_count = ggp_queryable.query('rt: y|i').count - all_matched_items.size
    notify "#{unmatched_item_count} annuals/issues not matched to any serial"
  end


  def all_matched_items
    @ultimate_url_matches.keys | @call_number_matches.keys
  end

  def items_for_call_number(call_num)
    normalized_call_num = normalized_call_number call_num
    return [] if normalized_call_num == 'ga'
    matching_items = ggp_queryable.items.filter do |item|
      item['ca'].starts_with?(normalized_call_num)
    end
    matching_items.map {|item| item[:id]}
  end

  def normalized_call_number(call_number)
    "ga #{call_number.downcase}".sub(/\bsuppl\b\.?/, '').sub(/\/$/, '').strip
  end

  def reduce_spaces_and_punctuation(call_number)
    call_number.gsub(/\W+/,' ').strip
  end

  def same_suppl_status?(item_id, serial_id)
    item_is_supplement = ggp_queryable.get_item(item_id)['ca'].match? /\bsuppl\b/
    serial_is_supplement = @serial_call_numbers[serial_id].any? do |call_num|
      call_num.downcase.match? /\bsuppl\b/
    end
    item_is_supplement == serial_is_supplement
  end

  def serial_title_stem(title)
    return '' if title.blank?
    title.split('...').first.strip.sub /\.$/, ''
  end

  def prefix_match_quality(prefix, whole)
    if whole.starts_with? prefix
      case_penalty = 0
    elsif whole.downcase.starts_with? prefix.downcase
      case_penalty = 1
    else
      return nil
    end
    rest = whole.downcase[prefix.length..]
    if rest.starts_with? /\s*\[/
      # the gold standard is "Serial title [date]"
      20 - case_penalty
    elsif rest == ''
      # The item title exactly matched the serial title
      20 - case_penalty
    elsif rest.starts_with? /\s*[^\w\s]/
      # e.g., "Serial title; volume" or "Serial title (date)"
      18 - case_penalty
    elsif rest.starts_with? /\s+\d/
      # e.g., "Serial title 1991"
      16 - case_penalty
    elsif rest.starts_with? /\s/
      14 - case_penalty
    else
      # Weird
      12 - case_penalty
    end
  end

  def common_prefix_word_count(title1, title2)
    title1 = title1.downcase
    title2 = title2.downcase
    common_prefix = title1[0...(
      (0..[title1.length, title2.length].min).find { |i| title1[i] != title2[i] }
    )]
    common_prefix.split(/\W+/).length
  end

  def best_title_match_quality(item_id, serial_id)
    item_title = item_title_cache[item_id]
    serial_titles = @serial_titles[serial_id]
    serial_titles.map { |serial_title| title_match_quality serial_title, item_title }.max
  end

  def title_match_quality(serial_title, item_title)
    return 0 if serial_title.blank? || item_title.blank?

    score = prefix_match_quality serial_title_stem(serial_title), item_title
    return score if score.present?

    before_colon, after_colon = serial_title.split(': ', 2)
    if after_colon.present?
      alternate_title_indicator = before_colon.strip.downcase.ends_with? 'title', 'called', 'named', 'also', 'as'
      score = prefix_match_quality serial_title_stem(after_colon), item_title
      return score - (alternate_title_indicator ? 0 : 2) if score.present?
    end
    # lowest possible score from above section is a 9 (12 - 1 - 2)

    serial_words = serial_title.scan(/\w+/).map(&:downcase).to_set
    item_words = serial_title.scan(/\w+/).map(&:downcase).to_set
    matched_word_ratio = (serial_words & item_words).size.to_f / serial_words.size

    return 7 if item_title.include? serial_title
    return 8 if item_title.downcase.include? serial_title.downcase

    matched_word_ratio
  end

  def save_match(item_id, serial_id)
    return if serial_id.nil?
    Item.where(id: item_id).update_all(serial_id: serial_id)
  end

  def item_title_cache
    @item_title_cache ||= Item.where(id: all_matched_items)
                              .pluck(:id, :dcterms_title)
                              .to_h
                              .transform_values { |titles| titles.first }
  end

  def item_date_cache
    @item_date_cache ||= Item.where(id: all_matched_items).pluck(:id, :dc_date).to_h
  end

  def serial_date_cache
    @serial_date_cache ||=  serials.pluck(:id, :dc_date).to_h
  end

  def dates_possibly_compatible?(item_id, serial_id)
    serial_date_cache[serial_id].each do |serial_date|
      item_date_cache[item_id].each do |item_date|
        return true if IndexDates.dc_dates_overlap? serial_date, item_date
      end
    end
    false
  end

  def self.ggp_query_for_url(url, previous_856_lookup: nil)
    uri = URI.parse(url.strip)
    uri.host = uri.host.sub(/\.$/, '')
    if uri.host == 'dlg.galileo.usg.edu'
      if uri.path.match(/^\/ggp\/query:(.*)$/)
        CGI.unescape($1)
      elsif uri.path.match(/^\/ggp\/(\w+:(.*))$/)
        CGI.unescape($1)
      elsif uri.path == '/cgi/ggpd'
        CGI::parse(uri.query || '')['query']&.first
      else
        raise "Unknown GGP URL pattern: #{url}"
      end
    elsif uri.host == 'www.galileo.usg.edu' || uri.host == 'galileo.usg.edu'
      if uri.path == '/express'
        link = CGI::parse(uri.query || '')['link']&.first
        if link =~ /^ggpd&(.*)$/
          inner_params = CGI::parse($1)
          if inner_params.include? 'query'
            return inner_params['query'].first
          else
            (inner_params.without('parms', 'action').map do |key, val|
              return "#{key}: #{val.first}"
            end).join(' ')
          end
        end
        raise "Unknown GALILEO URL pattern"
      end
    elsif uri.host == 'dlg.usg.edu' && uri.path =~ /^\/serial\/(\w+)/ && previous_856_lookup
      previous_856 = previous_856_lookup[$1]
      return nil unless previous_856
      ggp_query_for_url(previous_856)
    else
      # Don't throw an error for non-GGP/GALILEO sites. We know there's nothing we can do
      nil
    end
  end


  def ultimate_url_matches_for(serial, record = GgpXmlRecord.new(serial.source_xml))
    all_856s = record.repeatable_datafield('856', subfields: ['u'])
    return [] unless all_856s.any?
    all_856s.flat_map { |field_856| ultimate_url_matches_from_856(serial, field_856) }
  end

  def ultimate_url_matches_from_856(serial, field_856)
    begin
      query = self.class.ggp_query_for_url field_856, previous_856_lookup: previous_856_lookup
      return [] unless query
    rescue
      log_warning "Couldn't attach items to #{serial.slug}. Couldn't find query in URL '#{field_856}'"
      return []
    end

    begin
      # extract slug and call number queries from query
      results = ggp_queryable.query(query)
      @serial_ultimate_queries[serial.id] = query
      results
    rescue
      log_warning "Couldn't attach items to #{serial.slug}. Couldn't understand query '#{query}'"
      []
    end
  end

  def notify(message)
    if @notifier
      @notifier.notify(message)
    else
      puts message
    end
  end

  def log_info(message)
    puts message # this could be changed to `notify message` for extra-verbose Slack output
  end

  def log_warning(message)
    notify "Warning: #{message}"
  end

end