# frozen_string_literal: true

class SolrService
  SOLR_TABLES = %w[Item Collection HoldingInstitution Name Event Feature Serial].freeze
  REINDEX_BATCH_SIZE = ENV['REINDEX_BATCH_SIZE']&.to_i || 10_000
  REINDEX_MAX_FULLTEXT_SIZE = ENV['REINDEX_MAX_FULLTEXT_SIZE']&.to_i || 5_000_000

  def self.reindex_all(batch_size: REINDEX_BATCH_SIZE, notifier: NotificationService.new)
    notifier.notify "Reindexing all objects."
    start = Time.zone.now
    SOLR_TABLES.each do |table_name|
      reindex(table_name, batch_size: batch_size, notifier: notifier)
    end
    notifier.notify("Solr reindex_all finished in `#{((Time.zone.now - start)/60.0).round(1)}` minutes")
  end

  def self.reindex(table_name, batch_size: REINDEX_BATCH_SIZE, notifier: NotificationService.new, starting_id: 0)
    batch_size = batch_size > 0 ? batch_size : REINDEX_BATCH_SIZE
    starting_id = starting_id >= 0 ? starting_id : 0

    notifier.notify "Solr reindex #{table_name} initialized with batch_size of #{batch_size} starting at id: #{starting_id}."
    raise ArgumentError, "Table #{table_name} not supported" unless SOLR_TABLES.include? table_name&.classify

    table = table_name.classify.constantize

    total_committed = 0
    start = Time.zone.now
    rows = if Rails.env == 'development' && table == Item
             table.where('id >= ?', starting_id).has_pages.limit(10_000)
           else
             table.where('id >= ?', starting_id)
           end

    if table == Item
      smart_reindex_items rows, max_item_count: batch_size
    else
      rows.for_indexing.order(:id).find_in_batches(batch_size: batch_size) do |batch|
        batch_start = Time.zone.now
        Sunspot.index!(batch)
        batch_end = (Time.zone.now - batch_start).round(1)
        total_committed += batch.size
        percent_done = (total_committed.to_f/rows.all.size * 100.0).round(2)
        total_elapsed_minutes = ((Time.zone.now - start)/60.0).round(1)
        notifier.notify "#{table_name} reindex: #{total_committed}/#{rows.all.size} rows (#{percent_done}%). last id: #{batch.last.id}. batch Solr time: #{batch_end} seconds. total time: #{total_elapsed_minutes} minutes."
      end
    end
    notifier.notify("Solr reindex #{table_name} finished in `#{((Time.zone.now - start)/60.0).round(1)}` minutes")
  end

  def self.delete_all(notifier = NotificationService.new)
    notifier.notify 'Solr delete_all all initialized'
    start = Time.zone.now
    SOLR_TABLES.each do |table_name|
      delete_table table_name, notifier
    end
    notifier.notify("Solr delete all finished in `#{(Time.zone.now - start).round(1)}` seconds")
  end

  def self.delete_table(table_name, notifier = NotificationService.new)
    notifier.notify "Solr delete_table #{table_name} initialized"
    start = Time.zone.now

    raise ArgumentError, "Table #{table} not supported" unless SOLR_TABLES.include? table_name&.classify

    table = table_name.classify.constantize

    Sunspot.remove_all! table
    notifier.notify("Solr delete_table #{table_name} finished in `#{(Time.zone.now - start).round(1)}` seconds")
  end

  def self.smart_reindex_items(relation = Item.all, max_fulltext_size: REINDEX_MAX_FULLTEXT_SIZE,
                               max_item_count: REINDEX_BATCH_SIZE, notifier: NotificationService.new)
    relation = relation.reorder(:id)
    item_sizes = relation.pluck(:id, 'pg_column_size(fulltext)')
    item_count = item_sizes.size

    batch_fulltext_size = 0
    batch_item_count = 0
    items_indexed = 0
    first_id = nil
    last_id = nil

    start = Time.zone.now

    item_sizes.each do |item_id, fulltext_size|
      batch_item_count += 1
      batch_fulltext_size += fulltext_size if fulltext_size.present?
      if last_id && (batch_item_count > max_item_count || batch_fulltext_size > max_fulltext_size)
        # batch would be too large if we included the current item
        # index a batch of everything up to (but not including) the current item
        batch_item_count -= 1

        batch_start = Time.zone.now
        Sunspot.index! relation.where(id: first_id..last_id).for_indexing
        batch_time = (Time.zone.now - batch_start).round(1)

        percent_done = ((items_indexed + batch_item_count).to_f/item_count * 100.0).round(2)
        total_elapsed_minutes = ((Time.zone.now - start)/60.0).round(1)
        notifier.notify "Item reindex: #{items_indexed + batch_item_count}/#{item_count} rows (#{percent_done}%). last id: #{last_id}. batch time: #{batch_time} seconds. total time: #{total_elapsed_minutes} minutes."

        items_indexed += batch_item_count
        first_id = nil
        batch_fulltext_size = fulltext_size || 0
        batch_item_count = 1
      end
      first_id ||= item_id
      last_id = item_id
    end

    # final batch
    batch_start = Time.zone.now
    Sunspot.index! relation.where(id: first_id..).for_indexing
    batch_time = (Time.zone.now - batch_start).round(1)

    percent_done = ((items_indexed + batch_item_count).to_f/item_count * 100.0).round(2)
    total_elapsed_minutes = ((Time.zone.now - start)/60.0).round(1)
    notifier.notify "Item reindex: #{items_indexed + batch_item_count}/#{item_count} rows (#{percent_done}%). last id: #{last_id}. batch time: #{batch_time} seconds. total time: #{total_elapsed_minutes} minutes."
    notifier.notify("Smart reindex of items finished in #{total_elapsed_minutes} minutes.")
  end
end