# frozen_string_literal: true

class AudioVideoProbeService

  AV_FILE_EXTENSIONS = %w[mp3 mp4 mpg mpeg m4p m4a m4v wav wmv mov]

  def self.path_for(item_or_record_id)
    if item_or_record_id.is_a? Item
      repo_slug = item_or_record_id.repository.slug
      collection_slug = item_or_record_id.collection.slug
      record_id = item_or_record_id.record_id
    elsif item_or_record_id.is_a? String
      record_id = item_or_record_id
      raise ArgumentError unless record_id =~ /^([A-Za-z0-9-]+)_([A-Za-z0-9-]+)_[A-Za-z0-9-]+$/
      repo_slug = $1
      collection_slug = $2
    else
      raise ArgumentError
    end
    Pathname(Rails.configuration.av_files_root).join(repo_slug, collection_slug, record_id).to_s
  end

  def self.files_for_item(item_or_record_id)
    Dir.glob("#{path_for item_or_record_id}.*").filter do |path|
      ext = File.extname(path).sub /^\./, ''
      AV_FILE_EXTENSIONS.include? ext
    end
  end

  def self.page_attributes_for_item(item_or_record_id)
    number = 0
    files_for_item(item_or_record_id).map do |f|
      number += 1
      relative_path = Pathname.new(f).relative_path_from(Rails.configuration.av_files_root)
      dlg_url = URI.join(Rails.configuration.dlg_url,
                         Pathname.new(Rails.configuration.public_av_files_root).join(relative_path).to_s)
      {
        'number' => number,
        'media_url' => dlg_url,
        **probe_file(f)
      }
    end

  end

  def self.probe_file(file_path)
    std_out, std_err, status = Open3.capture3(
      {'AV_FILE' => file_path},
      'ffprobe -v error -show_entries stream=codec_type,width,height,duration -of json "$AV_FILE"'
    )
    raise "ffprobe failed: #{std_err}" unless status == 0

    result = JSON.parse(std_out)
    streams = result['streams'] || []
    video_streams = streams.filter { |x| x['codec_type'] == 'video' }
    audio_streams = streams.filter { |x| x['codec_type'] == 'audio' }
    if video_streams.any?
      media_type = 'video'
    elsif audio_streams.any?
      media_type = 'audio'
    else
      raise "No AV streams found"
    end
    duration = streams.map { |x| x['duration']&.to_f }.compact.max
    width = video_streams.map { |x| x['width']&.to_i }.compact.max
    height = video_streams.map { |x| x['height']&.to_i }.compact.max

    file_type = File.extname(file_path).sub /^\./, ''

    {
      'media_type' => media_type,
      'file_type' => file_type,
      'duration' => duration,
      'width' => width || 0,
      'height' => height || 0
    }

  rescue JSON::ParserError
    raise "ffprobe failed"
  end

end
