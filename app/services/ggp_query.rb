# frozen_string_literal: true

class GgpQuery
  def initialize(query_text, items_scope = nil, query_cache: {})
    @parsed_query = GgpQueryParser.parse query_text
    @models = items_scope
    @cached_queries = query_cache
  end

  attr_reader :parsed_query

  def models
    raise "You didn't supply a scope and are using fields that rely on ActiveRecord" unless @models
    @models
  end

  def ids_for_sql_query(filtered_models)
    @cached_queries[filtered_models.to_sql] ||= filtered_models.pluck(:id).to_set
  end

  def test_sql_query(filtered_models, id)
    ids_for_sql_query(filtered_models).include? id
  end

  def test_against(item_hash)
    test_query_component parsed_query, against: item_hash
  end

  def test_query_component(component, against:)
    item_hash = against
    if component.is_a? GgpQueryParser::Term
      test_term component, against: item_hash
    elsif component.is_a? GgpQueryParser::WildcardTerm
      test_wildcard component, against: item_hash
    elsif component.is_a?(GgpQueryParser::Quoted) && component.bound_phrase?
      test_bound_phrase component, against: item_hash
    elsif component.is_a? GgpQueryParser::RangeGroup
      test_range_group component, against: item_hash
    elsif component.op == :AND
      component.members.all? { |m| test_query_component m, against: item_hash }
    elsif component.op == :OR
      component.members.any? { |m| test_query_component m, against: item_hash }
    elsif component.op == :NOT
      component.members.all? { |m| !test_query_component m, against: item_hash }
    elsif component.try(:members)&.size == 1
      test_query_component component.members.first, against: item_hash
    else
      raise "Unknown component/operation. Don't know what to do."
    end
  end

  def test_term(term, against:)
    item_hash = against
    if item_hash.include? term.field
      value = item_hash[term.field]
      if value.is_a? String
        value.match? Regexp.new("(^|\\s)#{ term.as_regex_stem }(\\s|$)")
      elsif value.is_a? Array
        # For year field
        value.any? {|v| v.to_s == term.term }
      end
    elsif term.field == 'ti'
      test_title "%#{ term.for_sql_wildcard }%", item_hash[:id]
    elsif term.field == 'kw'
      test_keyword term.term, item_hash[:id]
    else
      raise "Don't know how to test term for #{term.field}"
    end
  end

  def test_wildcard(wildcard, against:)
    item_hash = against
    if item_hash.include? wildcard.field
      item_hash[wildcard.field].match? Regexp.new("(^|\\s)#{ wildcard.as_regex_stem }(\\s|$)")
    elsif wildcard.field == 'ti'
      test_title "%#{ wildcard.for_sql_wildcard }%", item_hash[:id]
    elsif wildcard.field == 'kw'
      bad_approximation_of_keyword = (wildcard.parts.filter do |x|
        x != GgpQueryParser::WildcardTerm::WILDCARD_PLACEHOLDER
      end).join ' '
      test_keyword bad_approximation_of_keyword, item_hash[:id]
    else
      raise "Don't know how to test wildcard for #{wildcard.field}"
    end
  end

  def test_range_group(range_group, against:)
    item_hash = against
    if item_hash.include? range_group.field
      values = item_hash[range_group.field]
      values = [values] unless values.is_a? Array
      range_group.items.any? do |range_group_item|
        values.any? do |value|
          if range_group_item.is_a? Range
            range_group_item.member? value
          else
            range_group_item == value
          end
        end
      end
    else
      raise NotImplementedError
    end
  end

  def test_bound_phrase(phrase, against:)
    item_hash = against
    field = phrase.underlying_field
    if item_hash.include? field
      regex = Regexp.new("^#{ phrase.members.map(&:as_regex_stem).join ' ' }$")
      item_hash[field].match? regex
    elsif field == 'ti'
      test_title phrase.for_sql_wildcard, item_hash[:id]
    else
      raise "Don't know how to test bound phrase for #{field}"
    end
  end

  # Implementations of the fields that have to go to SQL queries
  def test_title(sql_wildcard, id)
    test_sql_query models.where('dcterms_title[1] ILIKE ?', sql_wildcard), id
  end

  def test_keyword(keyword, id)
    test_sql_query models.text_search(keyword), id
  end

end
