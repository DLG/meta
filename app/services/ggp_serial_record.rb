# frozen_string_literal: true

class GgpSerialRecord < GgpXmlRecord
  def slug
    oclc_number || control_field('001')
  end

  def dcterms_replaces
    repeatable_datafield('780')
  end

  def dcterms_is_replaced_by
    repeatable_datafield('785')
  end

  def dcterms_frequency
    frequency = []
    frequency += repeatable_datafield('310')
    frequency += repeatable_datafield('321')
    frequency.compact
  end

  def dcterms_subject_fast
    tags = (600..699).to_a
    subjects_fast = []
    tags.each do |tag|
      fields = data_field(tag, ind2: 7).to_a.filter{|field| field.css('subfield[code=2]').text.downcase == 'fast'}
      fields.each do |field|
        subjects_fast << field.css('subfield').map(&:text).join('--')
      end
    end
    subjects_fast
  end

  def dcterms_subject
    subjects = super

    subjects << 'Georgia Government Documents--Serial'
  end
end
