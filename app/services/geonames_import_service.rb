require 'zip'

class GeonamesImportService
  SUPPORTED_TABLES  = [Item, Collection, BatchItem].freeze
  SOLR_TABLES  = [Item, Collection].freeze
  REMOTE_PATH_GEONAMES_USA_DUMP = 'http://download.geonames.org/export/dump/US.zip'
  PATH_WITHIN_ARCHIVE = 'US.txt'
  # See column spec at http://download.geonames.org/export/dump/readme.txt
  COLUMNS = ['geonameid',
             'name',
             'asciiname',
             'alternatenames',
             'latitude',
             'longitude',
             'feature class',
             'feature code',
             'country code',
             'cc2',
             'admin1 code',
             'admin2 code',
             'admin3 code',
             'admin4 code',
             'population',
             'elevation',
             'dem',
             'timezone',
             'modification date'].freeze
  GEONAMES_IMPORT_BATCH_SIZE = 1000

  def initialize(process_name, notifier = NotificationService.new)
    @process_name = process_name
    @states = {}
    @counties = {}
    @other_places = []
    @independent_entities = []
    @whitelisted_geonames_ids = [].to_set
    @batch = []
    @added = 0
    @updated = 0
    @invalid = 0
    @notifier = notifier
  end

  def import_usa_geonames_archive(path_to_archive)
    read_geonames_dump path_to_archive, PATH_WITHIN_ARCHIVE
    notify 'Done reading geonames archive.'
    notify 'Deleting old records...'
    GeographicLocation.delete_all
    notify 'Done. Exporting new records...'
    all_geographies_as_attrs do |attrs|
      add_to_batch GeographicLocation.new attrs
    end
    push_batch
  end

  def self.import_usa(local_path)
    GeonamesImportService.new('USA Geonames Initial Import').import_usa_geonames_archive local_path
  end

  def update_from_usa_geonames_archive(path_to_archive, replace_names: false)
    @replace_names = replace_names
    @whitelisted_geonames_ids = GeographicLocation.geonames.pluck(:geonames_id).to_set
    notify "#{@whitelisted_geonames_ids.size} distinct Geonames IDs currently in database"
    notify 'Reading geonames archive...'
    read_geonames_dump path_to_archive, PATH_WITHIN_ARCHIVE
    notify 'Done reading geonames archive.'
    i = 0
    all_geographies_as_attrs do |attrs|
      update_or_add_from_attributes attrs
      i += 1
      if i % 5_000 == 0
        notify "Progress: Processed #{i} geographies. #{@added} added. #{@updated} updated. #{@invalid} invalid."
      end
    end
    notify "Done: Processed #{i} geographies. #{@added} added. #{@updated} updated. #{@invalid} invalid."
  end

  def update_or_add_from_attributes(attributes)
    db_record = nil
    geonames_id = attributes[:geonames_id]
    db_record = GeographicLocation.find_by geonames_id: geonames_id
    if db_record.nil?
      db_record = GeographicLocation.new attributes
      db_record.created_by = db_record.updated_by = @process_name
      db_record.save!
      @added += 1
    else
      unless @replace_names
        attributes = attributes.select {|key, _| [:latitude, :longitude, :geoname_verified].include? key}
      end
      db_record.assign_attributes attributes
      if db_record.changed?
        db_record.updated_by = @process_name
        db_record.save!
        @updated += 1
      end
    end
  rescue StandardError => e
    if db_record.nil?
      notify "Failed on item with attributes: #{attributes.to_s}"
      raise e
    end
    row_identifier = db_record.id.nil? ? 'New record' : "ID: #{db_record.id}"
    notify "#{e.message} (#{row_identifier}; #{db_record.component_placenames.join(', ')}; Geonames ID: #{db_record.geonames_id})"
    @invalid += 1
  end

  def self.update_usa(local_path)
    GeonamesImportService.new('USA Geonames Update').update_from_usa_geonames_archive local_path
  end

  def self.update_usa_overwrite_names(local_path)
    GeonamesImportService
      .new('USA Geonames Update - Overwrite Names')
      .update_from_usa_geonames_archive local_path, replace_names: true
  end

  def self.retrieve_usa(local_path = nil, notifier = NotificationService.new)
    notifier.notify 'Downloading US archive from geonames.org...'
    if local_path.nil?
      local_path = File.join(temporary_folder, 'US.zip')
      ensure_exists temporary_folder
    end
    download_file(REMOTE_PATH_GEONAMES_USA_DUMP, local_path)
    notifier.notify 'Done downloading geonames archive. Beginning import...'
    local_path
  end

  def self.delete_all_geographic_locations
    ItemGeographicLocation.delete_all
    CollectionGeographicLocation.delete_all
    GeographicLocation.delete_all
    ActiveRecord::Base.connection.reset_pk_sequence!('item_geographic_locations')
    ActiveRecord::Base.connection.reset_pk_sequence!('collection_geographic_locations')
    ActiveRecord::Base.connection.reset_pk_sequence!('geographic_locations')
  end

  REMEDIATE_BATCH_SIZE = 10_000

  def self.supports_table?(model_or_relation)
    model = if model_or_relation.is_a? ActiveRecord::Relation
              model_or_relation.klass
            else
              model_or_relation
            end
    SUPPORTED_TABLES.include? model
  end

  def self.reindex_table?(model_or_relation)
    model = if model_or_relation.is_a? ActiveRecord::Relation
              model_or_relation.klass
            else
              model_or_relation
            end
    SOLR_TABLES.include? model
  end

  # Conversion service from old dc_coverage_spatial to new geographic_location
  # @param [ApplicationRecord|ActiveRecord::Relation] model
  # @param [NotificationService] notifier
  def self.remediate_geographic_info(model, notifier = NotificationService.new)
    raise ArgumentError, "#{model} not supported" unless supports_table? model

    rows_count = 0
    process_name = 'Remediation from legacy_dcterms_spatial'

    model.includes_geographic_locations_join_table.all
         .find_in_batches(batch_size: REMEDIATE_BATCH_SIZE).each do |batch|
      batch.each do |row|
        next if row.legacy_dcterms_spatial.blank?
        row.legacy_dcterms_spatial.each do | location_string |
          location_string, latitude, longitude = GeographicLocation.parse_location_string location_string
          next if location_string.blank?
          components = location_string.split /, */
          # normalize the separator, just in case
          location_string = components.join ', '
          matching_locations = GeographicLocation.where code: location_string
          if matching_locations.count.zero?
            state, county = GeographicLocation.extract_us_state_and_county components
            geographic_location = GeographicLocation.create!(
              component_placenames: components,
              latitude: latitude,
              longitude: longitude,
              us_state: state,
              us_county: county,
              created_by: process_name,
              updated_by: process_name
            )
          else
            geographic_location = matching_locations.first
            if matching_locations.count > 1
              notifier.notify("Encountered ambiguous location: #{location_string}. " +
                                "Matches #{matching_locations.count} existing locations. Assigning to ID: #{geographic_location.id}")
            end
          end
          unless row.geographic_locations.include? geographic_location
            row.related_geographic_locations.create!(geographic_location_id: geographic_location.id)
          end
        rescue
          notifier.notify("Failed on location string: #{location_string}")
          raise
        end
      rescue
        location_strings = row.legacy_dcterms_spatial&.join("\n")
        notifier.notify("Failed on #{model.name} id #{row.id}.\nCoverage spatial:\n#{location_strings}")
        raise
      end
      rows_count += batch.size
      notifier.notify("Processed #{rows_count} rows")
    end
  end

  INCREMENTAL_REMEDIATE_BATCH_SIZE = 1000

  # Conversion service from old dc_coverage_spatial to new geographic_location
  # Unlike the above method, this one deletes old geographic_locations on the objects before remediation
  # ...and reindexes them after remediation.
  # @param [ApplicationRecord|ActiveRecord::Relation] filtered_list: the records to remediate
  # @param [NotificationService] notifier
  def self.incremental_remediatate_geographic_info(filtered_list, notifier = NotificationService.new)
    should_reindex = reindex_table? filtered_list
    num_processed = 0
    total = filtered_list.count
    filtered_list.in_batches(of: INCREMENTAL_REMEDIATE_BATCH_SIZE).each do |batch|
      notifier.notify("Processing records #{num_processed} — #{num_processed += batch.size} / #{total}...")
      batch.each { |record| record.related_geographic_locations.delete_all }
      GeonamesImportService.remediate_geographic_info batch, notifier
      Sunspot.index batch if should_reindex
    end
    Sunspot.commit if should_reindex
  end

  private

  def handle_other_place_in_georgia(record)
    code = record['feature code']
    if code.start_with?('PPL')
      @other_places << record
    end
  end

  def handle_other_place_non_georgia(record)
    code = record['feature code']
    if code.start_with?('PPL') && code != 'PPLX' && record['population'] != '0'
      @other_places << record
    end
  end

  def state_geographic_location(record)
    attrs = geoloc_attributes_from(record, [GeographicLocation::USA_NAME])
    attrs[:us_state] = record['name']
    attrs
  end

  def county_geographic_location(record)
    state = get_state(record)
    attrs = geoloc_attributes_from(record, [GeographicLocation::USA_NAME, state])
    attrs[:us_state] = state
    attrs[:us_county] = record['name']
    attrs
  end

  def other_geographic_location(record)
    state, county = get_state_and_county record
    ancestors = [GeographicLocation::USA_NAME]
    ancestors << state unless state.nil?
    ancestors << county unless county.nil?
    attrs = geoloc_attributes_from(record, ancestors)
    attrs[:us_state] = state unless state.nil?
    attrs[:us_county] = county unless county.nil?
    attrs
  end

  def all_geographies_as_attrs
    @independent_entities.each { |record| yield geoloc_attributes_from record }
    @states.values.each { |record| yield state_geographic_location record }
    @counties.values.each { |record| yield county_geographic_location record }
    @other_places.each { |record| yield other_geographic_location record }
  end

  def notify(message)
    @notifier.notify(message) unless @notifier.nil?
  end

  def read_geonames_dump(path_to_archive, path_within_archive)
    Zip::File.open(path_to_archive) do |zip_file|
      zip_file.get_input_stream(path_within_archive).each do |line|
        record = (COLUMNS.zip line.split("\t")).to_h
        record['name'] = String.new record['name'], encoding: 'UTF-8'
        code = record['feature code']
        state_abbr = record['admin1 code']
        county_fips = record['admin2 code']
        county_key = [state_abbr, county_fips]
        geonames_id = record['geonameid'].to_i
        if code == 'ADM1'
          @states[state_abbr] = record
        elsif code == 'ADM2'
          @counties[county_key] = record
        elsif code.start_with?('PCL')
          @independent_entities << record
        elsif geonames_id > 0 && @whitelisted_geonames_ids.include?(geonames_id)
          @other_places << record
        elsif state_abbr == 'GA'
          handle_other_place_in_georgia record
        else
          handle_other_place_non_georgia record
        end
      end
    end
  end

  def geographic_location_from(record, qualified_name=record['name'])
    GeographicLocation.new(
      geonames_id: record['geonameid'],
      code: qualified_name,
      latitude: record['latitude'],
      longitude: record['longitude']
    )
  end

  def geoloc_attributes_from(record, ancestors=[])
    component_placenames = ancestors + [record['name']]
    {
      geonames_id: record['geonameid'],
      component_placenames: component_placenames,
      latitude: record['latitude'],
      longitude: record['longitude'],
      geoname_verified: true
    }
  end

  def map_to_state(record)
    qualified_name = record['name']
    state_abbr = record['admin1 code']
    state = @states[state_abbr]
    state_name = state&.dig 'name'
    qualified_name = "United States, #{state_name}, #{qualified_name}" if state_name
    return qualified_name
  end

  def get_state(record)
    state_abbr = record['admin1 code']
    state = @states[state_abbr]
    state&.dig 'name'
  end

  def get_state_and_county(record)
    state_abbr = record['admin1 code']
    county_fips = record['admin2 code']
    county_key = [state_abbr, county_fips]
    county = @counties[county_key]
    return get_state(record), nil if county.nil?
    state = get_state(county)
    return state, county['name']
  end

  def add_to_batch(geographic_location)
    geographic_location.created_by = geographic_location.updated_by = @process_name
    @batch << geographic_location
    push_batch if @batch.size >= GEONAMES_IMPORT_BATCH_SIZE
  end

  def push_batch
    GeographicLocation.import @batch
    @batch = []
  end

  def self.download_file(remote_path, local_path)
    File.open(local_path, 'wb') do |file|
      response = HTTParty.get(remote_path, stream_body: true, follow_redirects: true) do |chunk|
        file.write(chunk)
      end
      raise "Unable to download #{remote_path}" unless response.success?
    end
  end

  def self.temporary_folder
    File.join(Rails.root, 'tmp', 'geonames_import')
  end

  def self.ensure_exists(path)
    FileUtils.mkdir_p(path)
  end

end
