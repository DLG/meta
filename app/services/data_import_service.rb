# frozen_string_literal: true

require 'net/ftp'
require 'aws-sdk-s3'
require 'open3'


# Class to import Production Data
class DataImportService
  ALLOWED_ENVIRONMENTS = %w[development dev staging experimental].freeze
  LOCAL_IMPORT_PATH = File.join(Rails.root, 'tmp', 'data_import').freeze
  REMOTE_BASE_PATH = 'meta_prod'.freeze
  SQL_DUMP_FILE_NAME = 'current_db.sql.gz'.freeze
  UPLOAD_DUMP_FILE_NAME = 'meta-uploads-20220414.tar.gz'.freeze
  UPLOADS_PATH = File.join(Rails.root, 'public', 'uploads').freeze

  # Starts the whole data import process
  # @return [Array<Hash<Symbol, String>>] Rows in csv file
  def self.start(notifier: NotificationService.new)
    unless ALLOWED_ENVIRONMENTS.include? Rails.env
      message = "Not allowed to run data importer in #{Rails.env}"
      raise Exceptions::DataImportError.new, message
    end

    #Load SQL Data
    sql_gz_file = retrieve_sql_dump(notifier: notifier)
    drop_all_tables notifier: notifier
    load_sql gz_file_path: sql_gz_file,  notifier: notifier

    #Load uploads data
    uploads_gz_file = retrieve_uploads_dump(notifier: notifier)
    clear_uploads_folder notifier: notifier
    load_uploads gz_file_path: uploads_gz_file,  notifier: notifier

    #Migrate
    notifier.notify "Running DB migration"
    start = Time.zone.now
    ActiveRecord::Base.connection.migration_context.migrate
    notifier.notify("DB migration finished in `#{((Time.zone.now - start)/60.0).round(1)}` minutes")

  end

  def self.retrieve_sql_dump(notifier: NotificationService.new)
    retrieve(SQL_DUMP_FILE_NAME, remote_path: REMOTE_BASE_PATH, notifier: notifier)
  end

  def self.retrieve_uploads_dump( notifier: NotificationService.new)
    retrieve(UPLOAD_DUMP_FILE_NAME, notifier: notifier)
  end

  # Retrieves the sql data dump from the ftp
  # @return [String] retrieved encrypted file path
  def self.retrieve(file_name, remote_path: nil, notifier: NotificationService.new)
    initialize_path LOCAL_IMPORT_PATH, file_name
    local_file_path = File.join(LOCAL_IMPORT_PATH, file_name)

    notifier.notify "Retrieving #{file_name}"
    start = Time.zone.now
      ftp_settings = {
      debug_mode: !Rails.env.production?,
      port: 990,
      ssl: { verify_mode: OpenSSL::SSL::VERIFY_NONE,
             min_version: OpenSSL::SSL::TLS1_2_VERSION,
             max_version: OpenSSL::SSL::TLS1_2_VERSION },
      username: Rails.application.credentials.galftp_username,
      password: Rails.application.credentials.galftp_password
    }
    Net::FTP.open('galftp.galib.uga.edu', ftp_settings) do |ftp|
      if remote_path
        ftp.chdir(remote_path)
      end
      ftp.getbinaryfile(file_name, local_file_path.to_s, 1024)
    end
    notifier.notify("Retrieving #{file_name} finished in `#{((Time.zone.now - start)/60.0).round(1)}` minutes")

    local_file_path
  end

  def self.load_sql(gz_file_path: File.join(LOCAL_IMPORT_PATH, SQL_DUMP_FILE_NAME), notifier: NotificationService.new)
    notifier.notify "Loading SQL dump file"
    start = Time.zone.now
    unless ALLOWED_ENVIRONMENTS.include? Rails.env
      message = "Not allowed to run load_sql in #{Rails.env}"
      raise Exceptions::DataImportError.new, message
    end

    db = ActiveRecord::Base.connection_db_config.configuration_hash

    postgres_uri = "postgres://#{db[:username]}:#{db[:password]}@#{db[:host]}:#{db[:port]}/#{db[:database]}"
    postgres_uri += '?sslmode=require' unless Rails.env == 'development'

    # The sed command is just looking for stuff of the format "ALTER __ OWNER TO __;" and removing it.
    # The tables, etc will belong to the user the script is logged in as (username in the postgres_uri)
    # These SQL dumps are pretty consistent, but we made a provision for padding at the beginning or end of the line just in case.
    # The pattern is case-sensitive however (the SQL keywords need to be in caps).
    command = "gunzip -c #{gz_file_path} | sed '/^ *ALTER[^;]*OWNER TO[^;]*; *$/d' | psql '#{postgres_uri}'"

    _stdout_str, error_str, status = Open3.capture3({}, command)
    if status.success?
      notifier.notify("Loading SQL dump file succeeded. Finished in `#{((Time.zone.now - start)/60.0).round(1)}` minutes")
      return
    end

    message = "Unable to load sql. #{error_str}"
    raise Exceptions::DataImportError.new, message
  end

  def self.drop_all_tables(notifier: NotificationService.new)
    notifier.notify "Dropping all SQL tables"
    start = Time.zone.now

    unless ALLOWED_ENVIRONMENTS.include? Rails.env
      message = "Not allowed to run drop_all_tables in #{Rails.env}"
      raise Exceptions::DataImportError.new, message
    end
    # (SN - 09/29/21) I have omitted "if exists" from the drop command because we should have the current list. If we
    # start running into issues, we may want to add it.
    conn = ActiveRecord::Base.connection
    conn.tables.each { |t| conn.execute("DROP TABLE #{t} CASCADE") }
    notifier.notify("Drop all SQL tables finished in `#{((Time.zone.now - start)/60.0).round(1)}` minutes")
  end

  # Load the uploads tar.gz file into the rails uploads folder.
  def self.load_uploads(gz_file_path: File.join(LOCAL_IMPORT_PATH, UPLOAD_DUMP_FILE_NAME), notifier: NotificationService.new)
    notifier.notify "Loading uploads dump file"
    start = Time.zone.now
    unless ALLOWED_ENVIRONMENTS.include? Rails.env
      message = "Not allowed to run load_uploads in #{Rails.env}"
      raise Exceptions::DataImportError.new, message
    end

    Dir.mkdir(UPLOADS_PATH) unless Dir.exist?(UPLOADS_PATH)

    # --strip-components=1 removes the root folder of the tar file, usually "meta_uploads/"
    command = "tar -xf #{gz_file_path} --strip-components=1 -C #{UPLOADS_PATH}"

    _stdout_str, error_str, status = Open3.capture3({}, command)
    if status.success?
      notifier.notify("Loading uploads dump file succeeded. Finished in `#{(Time.zone.now - start).round(1)}` seconds")
      return
    end

    message = "Unable to load uploads. #{error_str}"
    raise Exceptions::DataImportError.new, message
  end

  def self.clear_uploads_folder( notifier: NotificationService.new)
    notifier.notify "Clearing Uploads Folder"
    start = Time.zone.now
    unless ALLOWED_ENVIRONMENTS.include? Rails.env
      message = "Not allowed to run clear_uploads_folder in #{Rails.env}"
      raise Exceptions::DataImportError.new, message
    end

    FileUtils.rm_rf(Dir["#{UPLOADS_PATH}/*"])
    notifier.notify("Clearing Uploads Folder succeeded. Finished in `#{(Time.zone.now - start).round(1)}` seconds")
  end

  # Empties path if it exists, otherwise creates path
  # @param [String] path Path to be initialized
  # @param [String] file_name to be removed
  def self.initialize_path(path, file_name)
    unless File.exist?(path)
      FileUtils.mkdir_p(path)
      return
    end
    file_path = File.join(LOCAL_IMPORT_PATH, file_name)
    FileUtils.rm(Dir[file_path]) if File.exist?(file_path)
  end
end
