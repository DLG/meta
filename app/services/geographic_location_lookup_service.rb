# frozen_string_literal: true

class GeographicLocationLookupService

  # @param code [String]
  # @return [GeographicLocation]
  def self.find_existing_match(code)
    # 99.9% of the time, this should only return zero or one items, but the sort is there for those few cases
    # where two hamlets of the same name exist in the same county in the same state.
    # For those, we go with the item that already has the most items. If it's a tie, go with the lower id number.
    candidates = GeographicLocation.where(GeographicLocation.arel_table[:code].lower.eq(normalize_code(code)))
                                   .sort do |a, b|
                                      comparison = b.items.count <=> a.items.count
                                      comparison = a.id <=> b.id if comparison.zero?
                                      comparison
                                    end
    candidates.first
  end

  # Create a new GeographicLocation from an array of component placenames.
  # Populates latitude, longitude, us_state, and us_county from Geonames if an appropriate result is found using the API
  # ...but the record is created whether a match is found with the API or not.
  # @param components [Array<String>]
  # @return [GeographicLocation] the newly created location
  def self.initialize_from_components(components)
    geographic_location = GeographicLocation.new(component_placenames: components)
    begin
      match = GeonamesApiService.get_best_result components
      if match.present?
        geographic_location.geonames_id = match.geonames_id
        geographic_location.latitude = match.latitude
        geographic_location.longitude = match.longitude
        geographic_location.us_state = match.us_state
        geographic_location.us_county = match.us_county
      end
      geographic_location.geoname_verified = true
    rescue # api error
      geographic_location.geoname_verified = false
    end
    geographic_location
  end

  # Create a new GeographicLocation from a string code.
  # Populates latitude, longitude, us_state, and us_county from Geonames if an appropriate result is found using the API
  # ...but the record is created whether a match is found with the API or not.
  # @param code [String]
  # @return [GeographicLocation] the newly created location
  def self.initialize_from_code(code)
    components = split_code code
    initialize_from_components(components)
  end

  def self.split_code(code)
    code.split(/,\s*/)
  end

  def self.normalize_code(code)
    split_code(code).join(', ').downcase
  end

  # Looks for an existing GeographicLocation that matches code, returns it if found, creates a new one otherwise.
  # If a new one is created, try to populate metadata with values from the Geonames API, but create the record even
  # if the Geonames API returns no results.
  # @param code [String]
  # @return [GeographicLocation]
  def self.find_or_initialize(code)
    code = code.strip
    geographic_location = find_existing_match code
    return geographic_location unless geographic_location.nil?
    initialize_from_code(code)
  end

  def self.find_or_create(code)
    geographic_location = find_or_initialize(code)
    geographic_location.save! if geographic_location.new_record?
  end

end