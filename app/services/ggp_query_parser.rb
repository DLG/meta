# frozen_string_literal: true
require 'parslet'
require 'parslet/convenience'

class GgpQueryParser < Parslet::Parser

  FIELD_ALIASES = {
    au: %w[author],
    _au: %w[authors],
    ca: %w[call_number is], # "is" (issue) is its own field in Ultimate, but is also usually included in call number
    _ca: %w[call_numbers],
    id: %w[key],
    kw: %w[keywords],
    rt: %w[record_type_code],
    ti: %w[title],
    _ti: %w[titles],
    year: %w[y4 yr date dates _yr]
  }.freeze
  BOUND_PHRASE_FIELDS = {
    '_ca' => 'ca',
    '_ti' => 'ti',
    '_au' => 'au'
  }.freeze
  RANGE_FIELDS = %w[year].freeze
  FIELDS_BY_ALIAS = (FIELD_ALIASES.map do |canon, aliases|
    canon = canon.to_s
    [[canon, canon]] + aliases.map { |a| [a, canon] }
  end).flatten(1).to_h.freeze
  OPERATORS = {
    'and' => :AND,
    'all' => :AND,
    '&' => :AND,
    'or' => :OR,
    'any' => :OR,
    '|' => :OR,
    'not' => :NOT,
    '!' => :NOT
  }.freeze

  class UnknownFieldError < StandardError; end

  rule(:whitespace) { match('[\s]').repeat(1) }
  rule(:whitespace?) { whitespace.maybe }
  rule(:wildcard) { str('*').as(:wildcard) }
  rule(:boolean_operator_word) { (str('and') | str('or') | str('not')).as(:op) }
  rule(:prefix_operation) { (str('and') | str('or') | str('any') | str('all')).as(:op) }
  rule(:not_boolean_word_lookahead) do
    (match('and\b') | match('or\b') | match('not\b')).absent?
  end
  rule(:boolean_operator_symbol) { match('[&!|]').as(:op) }
  rule(:boolean_joiner) do
    (whitespace >> boolean_operator_word >> whitespace) |
      (whitespace? >> boolean_operator_symbol >> whitespace?)
  end

  def wildcard_version_of(term)
    (
      (((term >> wildcard.present?) | (wildcard >> term.present?)).repeat(1) >> (wildcard | term)) | wildcard
    ).as(:wildcard_term)
  end
  def terms_string(unwildcarded, wildcarded, as:)
    (whitespace? >> ((unwildcarded >> wildcard.absent?) | wildcarded)).repeat(1).as(as)
  end

  rule(:backslash_escaped) { str('\\') >> match('.') }

  # Double-quoted terms
  rule(:double_quoted_term) { (match('[^\s"*\\\\]') | backslash_escaped).repeat(1).as(:term) }
  rule(:double_quoted_wildcard_term) { wildcard_version_of double_quoted_term }
  rule(:double_quoted_terms) { terms_string double_quoted_term, double_quoted_wildcard_term, as: :terms}
  rule(:double_quoted) do
    (str('"').as(:quote_start) >> double_quoted_terms >> whitespace? >> str('"').as(:quote_end)).as(:quoted_terms)
  end

  # Single-quoted terms (behave just like double-quoted terms)
  rule(:single_quoted_term) { (match("[^\s'*\\\\]") | backslash_escaped).repeat(1).as(:term) }
  rule(:single_quoted_wildcard_term) { wildcard_version_of single_quoted_term }
  rule(:single_quoted_terms) { terms_string single_quoted_term, single_quoted_wildcard_term, as: :terms}
  rule(:single_quoted) do
    (str("'").as(:quote_start) >> single_quoted_terms >> whitespace? >> str("'").as(:quote_end)).as(:quoted_terms)
  end

  # Unquoted terms
  rule(:unescaped_term) do
    not_boolean_word_lookahead >>
      (match(%Q{[^\s"'&|!()*:=\\\\]}) | backslash_escaped).repeat(1).as(:term) >>
      str(':').absent?
  end
  rule(:unescaped_wildcard) { wildcard_version_of unescaped_term }
  rule(:unescaped_term_or_wildcard) { (unescaped_term >> wildcard.absent?) | unescaped_wildcard }

  # Ranges (we're just using this for year for now, so we'll only support integers)
  rule(:integer) { match('[0-9]').repeat(1) }
  rule(:range) do
    ((integer.as(:lower) >> str('-') >> integer.as(:upper).maybe) | (str('-') >> integer.as(:upper))).as(:range)
  end
  rule(:standalone_number) { integer.as(:standalone_number) }
  rule(:possible_range_group) do
    ((range >> (str(',') >> (range | standalone_number)).repeat) |
      (standalone_number >> (str(',') >> (range | standalone_number)).repeat(1))).as(:range_group) >> wildcard.absent?
  end

  # Any terms
  rule(:term ) { possible_range_group | unescaped_term_or_wildcard | single_quoted | double_quoted }

  # Field queries like `ca: "blah blah blah"`
  rule(:field_name) { match('\w').repeat(1).as(:field) }
  rule(:field_expression_argument) do
    (parenthetical_expression | expression).as(:group)
  end
  rule(:field_expression) do
    (field_name >> str(':') >> whitespace? >> field_expression_argument).as(:field_query)
  end

  # or=(...), any=(...), and=(...), all=(...) syntax
  rule(:prefix_expression) do
    (
      prefix_operation >> str('=') >> whitespace? >>
        str('(') >> whitespace? >> expression.as(:inner) >> whitespace? >> str(')')
    ).as(:prefix_expression)
  end

  # Compound expressions using parentheses and booleans
  rule(:non_boolean_expression) do
    field_expression | parenthetical_expression | prefix_expression | term
  end
  rule(:implicit_group) do
    (non_boolean_expression >> (whitespace? >> non_boolean_expression).repeat).as(:group)
  end
  rule(:expression) do
    boolean_expression | implicit_group
  end
  rule(:parenthetical_expression) do
    str('(') >> whitespace? >> expression >> whitespace? >> str(')')
  end
  rule(:boolean_expression) do
    infix_expression(implicit_group, [boolean_joiner, 1, :left]).as(:infix)
  end

  root :expression

  class Term < Struct.new(:term, :field)
    def to_s
      "[#{ field }: #{ term }]"
    end

    def as_regex_stem
      Regexp.escape term
    end

    def for_sql_wildcard
      term.gsub(/[\\%]/) { |m| "\\#{m}" }
    end
  end

  class WildcardTerm < Struct.new(:parts, :field)
    WILDCARD_PLACEHOLDER = :*
    def to_s
      "[#{ field }: #{ parts.map { |x| x == WILDCARD_PLACEHOLDER ? '**' : x }.join }]"
    end

    def as_regex_stem
      parts.map { |x| x == WILDCARD_PLACEHOLDER ? '.*' : Regexp.escape(x) }.join
    end

    def for_sql_wildcard
      parts.map { |x| x == WILDCARD_PLACEHOLDER ? '%' : x.gsub(/[\\%]/) { |m| "\\#{m}" } }.join
    end
  end

  class Group < Struct.new(:members, :field, :op)
    def initialize(*args)
      super
      if field
        normalized_field = FIELDS_BY_ALIAS[field]
        raise UnknownFieldError.new("Unknown field: #{field}") unless normalized_field.present?
        self.field = normalized_field # This will also call propogate_field_to_members
      end
      collapse!
      if field.nil? && members.any? { |m| m.try :retry_collapse? }
        @retry_collapse = true
      end
      @needs_default_op = (op.nil? && members.size > 1) || members.any? { |m| m.try :needs_default_op? }
    end

    def collapse!
      self.members = flattened_members
    end

    def collapsible?
      true
    end

    def retry_collapse?
      @retry_collapse
    end

    def flattened_members
      members.map do |member|

        if member.try :collapsible?
          if member.is_a? Quoted
            member = member.group
          end
          # First we flatten any descendents that are groups with 0 or 1 items
          if member.members.none?
            next []
          elsif member.members.size == 1 && member.op != :NOT
            member = member.members.first
          end
        end

        # Next we flatten any groups that are doing the same operation as us
        if member.try :collapsible?
          if member.op == op
            member.members
          else
            member
          end
        else
          member
        end
      end.flatten
    end

    def field=(value)
      super
      propagate_field_to_members!
      if retry_collapse?
        collapse!
        @retry_collapse = false
      end
    end

    def propagate_field_to_members!
      members.each do |m|
        m.field = field if m.field.nil?
      end
      self
    end

    def needs_default_op?
      @needs_default_op
    end

    def hand_down_op(op)
      return unless needs_default_op?
      self.op = op if self.op.nil?
      members.each do |m|
        m.hand_down_op(op) if m.respond_to? :hand_down_op
      end
      collapse!
      @needs_default_op = false
    end

    def propagate_op_to_members!
      hand_down_op(op)
      self
    end

    # Since the normal flatten process only happens when nodes are added to a parent,
    #   it never happens for the very top of the tree.
    # Thus, we might have a root note that's just a group with one member which itself is a group
    #   If that's the case, just use the child as the new root.
    def root_group
      if self.members.size == 1 && self.members.first.is_a?(Group)
        return self.members.first
      end
      self
    end

    def to_s
      "#{op}(#{ members.map(&:to_s).join ', ' })"
    end
  end

  class Quoted < Struct.new(:phrase, :group)
    delegate :field, :field=, :op, :members, to: :group

    def to_s
      "[#{field}: '#{ phrase }']"
    end

    def self.from(start_token:, stop_token:, members:)
      group = Group.new(members, nil, :AND)
      raw_string = start_token.position.as_json['string']
      phrase = raw_string[(start_token.position.charpos + 1)..(stop_token.position.charpos - 1)]
      Quoted.new phrase, group
    end

    def bound_phrase?
      BOUND_PHRASE_FIELDS.include? field
    end

    def underlying_field
      BOUND_PHRASE_FIELDS[field]
    end

    def collapsible?
      field.present? && !bound_phrase?
    end

    def retry_collapse?
      field.nil?
    end

    def for_sql_wildcard
      members.map(&:for_sql_wildcard).join ' '
    end

  end

  class RangeGroup < Struct.new(:items, :field)
    def as_term
      # Used if this turns out to be a non-range field and is treated as a normal term
      Term.new(items.map{ |x| x.is_a?(Range) ? "#{x.begin}-#{x.end}" : x.to_s }.join(','), field)
    end

    # If this is a non-range field, pretend to be a group with a single term
    def members
      [as_term]
    end
    def op; end

    def range_queryable_field?
      RANGE_FIELDS.include? field
    end

    def collapsible?
      field.present? && !range_queryable_field?
    end

    def retry_collapse?
      field.nil?
    end

    def to_s
      "[#{field} IN (#{ items.map(&:to_s).join(', ') })]"
    end
  end

  class GgpParseTransformer < Parslet::Transform

    rule(term: simple(:term)) do
      # Unescape any backslashes
      Term.new(term.to_s.gsub(/\\(.)/, '\1'))
    end
    rule(wildcard: simple(:wc)) { WildcardTerm::WILDCARD_PLACEHOLDER }
    rule(wildcard: simple(:wc), term: simple(:term)) { [WildcardTerm::WILDCARD_PLACEHOLDER, ] }
    rule(wildcard_term: sequence(:parts)) { WildcardTerm.new(parts.map { |p| p.is_a?(Term) ? p.term : p }) }
    rule(wildcard_term: simple(:part)) { WildcardTerm.new([part]) }
    rule(standalone_number: simple(:num)) { num.to_i }
    rule(range: {lower: simple(:lower), upper: simple(:upper)}) { lower.to_i..upper.to_i }
    rule(range: {lower: simple(:lower)}) { lower.to_i..(Float::INFINITY) }
    rule(range: {upper: simple(:upper)}) { (-Float::INFINITY)..upper.to_i }
    rule(range_group: sequence(:items)) { RangeGroup.new(items) }
    rule(range_group: simple(:item)) { RangeGroup.new([item]) }
    rule(group: sequence(:members)) { Group.new(members) }
    rule(group: simple(:member)) { Group.new([member]) }
    rule(field_query: { field: simple(:field),
                        group: sequence(:members) }) { Group.new(members, field.to_s) }
    rule(field_query: { field: simple(:field),
                        group: simple(:member) }) { Group.new([member], field.to_s) }
    rule(quoted_terms: { terms: sequence(:members),
                         quote_start: simple(:open),
                         quote_end: simple(:close)}) do
      Quoted.from start_token: open, stop_token: close, members: members
    end
    rule(quoted_terms: { terms: simple(:member),
                         quote_start: simple(:open),
                         quote_end: simple(:close)}) do
      Quoted.from start_token: open, stop_token: close, members: [member]
    end
    rule(infix: simple(:group)) { group }
    rule(l: simple(:left), r: simple(:right), o: { op: simple(:op) }) do
      operator = OPERATORS[op.to_s]
      if operator == :NOT
        Group.new([left, Group.new([right], nil, :NOT)], nil, :AND)
      else
        Group.new([left, right], nil, operator)
      end
    end
    rule(prefix_expression: { inner: sequence(:members), op: simple(:op) }) do
      Group.new(members, nil, OPERATORS[op.to_s]).propagate_op_to_members!
    end
    rule(prefix_expression: { inner: simple(:member), op: simple(:op) }) do
      Group.new([member], nil, OPERATORS[op.to_s]).propagate_op_to_members!
    end
  end

  def self.parse(query)
    if query.count("'") == 1 && !query.include?("\\'")
      # special handling to treat an unmatched single quote as an apostrophe
      query = query.sub("'") { "\\'" }
    end
    query = "kw: and=(#{query.strip.downcase})"
    tree = new.parse(query)
    GgpParseTransformer.new.apply(tree).root_group
  end
end
