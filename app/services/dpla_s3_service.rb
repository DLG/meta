# frozen_string_literal: true

require 'digest/md5'

# Facilitates upload to the DPLA's AWS S3 bucket for the DLG
class DplaS3Service

  # Upload size limit is 15 MB to avoid multipart uploads that throw off MD5 checksum
  # Going to be conservative and assume that's metric (1KB = 1000 (not 1024) bytes)
  # Furthermore, going to subtract 4KB in case there's a header or such that counts toward the limit
  FILE_SIZE_LIMIT = 15_000_000 - 4_000

  # @param [String] date_string
  def initialize(date_string, dry_run: false)
    configure_aws
    @bucket = Rails.application.credentials.dpla_s3_dlg_bucket
    @destination_folder = date_string
    @working_folder = Rails.root.join 'tmp', "dpla_feed_#{date_string}"
    Dir.mkdir(@working_folder) unless Dir.exist?(@working_folder)
    @notifier = NotificationService.new
    @dry_run = dry_run

    @file_count = 0
    @current_file = nil
    @current_file_size = 0

    @outcomes = []
  end

  attr_reader :outcomes, :file_count

  def write_doc(doc)
    serialized = doc.to_json + "\n"
    unless room_for? serialized
      if serialized.bytesize > FILE_SIZE_LIMIT
        # This single record is larger than the limit all by itself!
        @notifier.notify "Skipping document #{doc[:id]} with excessive size (#{serialized.bytesize} bytes)."
        return
      end
      new_file!
    end
    @current_file_size += @current_file.write serialized
  end

  def upload_last_file
    return unless @current_file.present?
    @current_file.close
    name = File.join @destination_folder, current_file_name
    file_md5 = Digest::MD5.file(@current_file.path).base64digest

    if @dry_run
      @notifier.notify "Would upload #{@current_file.path} to #{@bucket}/#{name}"
      return
    end

    upload_success = @s3.bucket(@bucket)
                    .object(name)
                    .upload_file(@current_file.path, content_md5: file_md5)
    unless upload_success
      @notifier.notify "DPLA S3 Service: upload of `#{name}` to `#{@bucket}` failed!"
    end
    outcomes << {
      run: file_count,
      file: @current_file.path,
      uploaded: upload_success
    }
  end

  private

  def room_for?(string)
    return false unless @current_file
    @current_file_size + string.bytesize <= FILE_SIZE_LIMIT
  end

  def current_file_name
    "set_#{@file_count}.jsonl"
  end

  def new_file!
    upload_last_file
    @file_count += 1
    @current_file = File.open File.join(@working_folder, current_file_name), 'w'
    @current_file_size = 0
  end

  def configure_aws
    return if @dry_run
    s3_client = Aws::S3::Client.new(
      region: 'us-east-1',
      access_key_id: Rails.application.credentials.dpla_s3_access_key_id,
      secret_access_key: Rails.application.credentials.dpla_s3_secret_access_key
    )
    @s3 = Aws::S3::Resource.new(client: s3_client)
  end
end