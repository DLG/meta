# frozen_string_literal: true

class FeatureFlags
  def self.enabled?(feature_name)
    case feature_name.to_sym
    when :geolocations_solr
      true
    when :edit_geolocations_directly
      true
    when :edit_names_directly
      false
    when :serials
      true
    when :quick_page_ingest
      true
    else
      false
    end
  end
end
