# frozen_string_literal: true

# Service to prepare reports
class ReportService

  ALL_ROWS_SUPPORTED_TABLES = {
    'Event' => 'Events',
    'Name' => 'Names',
    'GeographicLocation' => 'Locations'
  }.freeze

  # All Rows for a given ActiveRecord Klass
  # @param [String] klass_name
  def self.all_rows_for(klass_name, number_of_random_rows = 0)
    unless ALL_ROWS_SUPPORTED_TABLES.include? klass_name
      raise "Forbidden table"
      return
    end
    klass = Object.const_get klass_name
    excluded_cols = %w[id legacy_data canned_search]
    cols = klass.attribute_names - excluded_cols
    rows = if number_of_random_rows > 0
             klass.order(Arel.sql('RANDOM()')).limit(number_of_random_rows)
           else
             klass.all
           end
    results = rows.pluck(*cols)
    filename = klass_name.downcase
    [to_csv(cols, results), filename]
  end

  # @param [ActiveRecord::Relation] activerecord_relation
  # @param [Array<String>] columns
  # @param [String] filename
  def self.generalized_report(activerecord_relation, columns, filename: 'generalized_report')
    cols = columns
    results = activerecord_relation.map do |row|
      columns.map{|column| row.send(column)}
    end

    [to_csv(cols, results), filename]
  end

  def self.all_names(number_of_random_rows = 0)
    cols = %w[id slug authoritative_name name_type portals
              additional_slugs legacy_slug public
              alternate_names_public alternate_names_not_public
              related_names bio_history authoritative_name_source
              source_uri oclc_number created_at updated_at]

    rows = if number_of_random_rows > 0
             Name.includes(:portals).order(Arel.sql('RANDOM()')).limit(number_of_random_rows)
           else
             Name.includes(:portals).all
           end

    results = rows.map do |name|
      [name.id, name.slug, name.authoritative_name, name.name_type, name.portals.pluck(:code)&.join(", "),
       name.additional_slugs, name.legacy_slug, name.public,
       name.alternate_names_public, name.alternate_names_not_public,
       name.related_names, name.bio_history, name.authoritative_name_source,
       name.source_uri, name.oclc_number, name.created_at, name.updated_at]
    end
    filename = 'names'
    [to_csv(cols, results, true), filename]
  end

  def self.holding_insts_contacts
    cols = %w[slug authorized_name contact_name contact_email analytics_emails]
    results = HoldingInstitution.all.map do |inst|
      [inst.slug, inst.authorized_name, inst.contact_name, inst.contact_email, inst.analytics_emails]
    end
    filename = 'holding_insts_contacts'
    [to_csv(cols, results), filename]
  end

  def self.holding_insts_subject_counts
    cols = %w[holding_institution_id authorized_name subject count]
    results = Item.select("holding_institutions_items.holding_institution_id, holding_institutions.authorized_name, unnest(items.dcterms_subject) AS subject, COUNT(*) AS subject_count")
                  .joins(:holding_institutions)
                  .group("holding_institutions_items.holding_institution_id, holding_institutions.authorized_name, subject")
                  .order("holding_institutions.authorized_name ASC, subject_count DESC")

    results = results.map do |result|
      [result.holding_institution_id, result.authorized_name, result.subject, result.subject_count]
    end
    filename = 'holding_insts_subject_counts'
    [to_csv(cols, results), filename]
  end

  def self.holding_inst_subjects(holding_inst_id)
    cols = %w[subject]
    results = results = Item.select("holding_institutions_items.holding_institution_id, holding_institutions.authorized_name, unnest(items.dcterms_subject) AS subject")
                            .joins(:holding_institutions)
                            .where("holding_institutions_items.holding_institution_id" => holding_inst_id)
                            .order("holding_institutions.authorized_name ASC, subject ASC")
    results = results.map do |result|
      [result.subject]
    end
    filename = 'holding_inst_subjects'
    [to_csv(cols, results), filename]
  end

  def self.repositories_collections(report_type, start_date, end_date)
    cols = %w[repository_title repository_slug repository_created_at collection_slug collection_title collection_created_at collection_updated_at]
    collections = if report_type == 'new'
                    Collection.all.includes(:repository)
                              .where(created_at: start_date.beginning_of_day..end_date.end_of_day)
                  else
                    Collection.where(created_at: Date.new(0)...start_date.beginning_of_day,
                                     updated_at: start_date.beginning_of_day..end_date.end_of_day)
                              .includes(:repository)
                  end
    results = collections.map do |collection|
      [collection.repository.title, collection.repository.slug, collection.repository.created_at, collection.slug, collection.display_title, collection.created_at, collection.updated_at]
    end
    filename = "repositories_#{report_type}_collections"
    [to_csv(cols, results), filename]
  end

  def self.iiif_partner_urls
    cols = %w[record_id iiif_partner_url repository_slug collection_slug item_created_at item_updated_at]

    results =  Item.includes(:collection, :repository).where.not(iiif_partner_url: [nil,""]).map do |item|
      [item.record_id, item.iiif_partner_url, item.repository.slug, item.collection.slug, item.created_at, item.updated_at]
    end
    filename = "iiif_partner_url"
    [to_csv(cols, results), filename]
  end

  def self.collections_items(report_type, start_date, end_date)
    cols = %w[collection_record_id collection_slug collection_title collection_created_at item_slug item_title item_record_id item_created_at item_updated_at]
    items = if report_type == 'new'
                    Item.all.includes(:collection)
                        .where(created_at: start_date.beginning_of_day..end_date.end_of_day)
                  else
                    Item.where(created_at: Date.new(0)...start_date.beginning_of_day,
                               updated_at: start_date.beginning_of_day..end_date.end_of_day)
                            .includes(:collection)
                  end
    results = items.map do |item|
      [item.collection.record_id, item.collection.slug, item.collection.display_title, item.collection.created_at, item.slug, item.dcterms_title.first, item.record_id, item.created_at, item.updated_at]
    end
    filename = "collections_#{report_type}_items"
    [to_csv(cols, results), filename]
  end

  def self.fulltext_items

    cols = %w[item_slug item_title item_record_id portals collection_slug collection_title item_provenance item_created_at item_updated_at]
    results =  Item.left_joins(:portals, :collection, :holding_institutions)
                   .where.not(fulltext: [nil,""])
                   .group('items.id')
                   .pluck(Arel.sql('items.slug, items.dcterms_title, items.record_id, array_agg(portals.code), array_agg(collections.slug), array_agg(collections.display_title), array_agg(holding_institutions.authorized_name), items.created_at, items.updated_at'))
    results = results.map do |result|
      result[1] = result[1].first #items.dcterms_title
      result[3] = result[3].uniq.sort.join(", ")  #portals.code
      result[4] = result[4].first #collections.slug
      result[5] = result[5].first #collections.display_title
      result[6] = result[6].first #holding_institutions.authorized_name
      result
    end
    filename = "fulltext_items"
    [to_csv(cols, results), filename]
  end

  def self.item_links(report_type)
    cols = %w[item_slug edm_is_shown_at edm_is_shown_by item_title item_record_id repository_slug collection_slug item_created_at item_updated_at]
    is_local = false
    if report_type == 'local'
      is_local = true
    end
    repository_slugs = Repository.pluck(:id, :slug).to_h
    collections = Collection.pluck(:id, :slug, :repository_id).map do |id, slug, repo_id|
      [id, {slug: slug, repo_slug: repository_slugs[repo_id]}]
    end.to_h
    results = Item.where(local: is_local)
                  .pluck('collection_id', 'slug', 'edm_is_shown_at', 'edm_is_shown_by', 'dcterms_title',
                         'record_id', 'created_at', 'updated_at')
                  .map do |collection_id, slug, edm_is_shown_at, edm_is_shown_by, dcterms_title, record_id,
                           created_at, updated_at |
      title = dcterms_title.first ? dcterms_title.first.strip : 'No Title'
      collection = collections[collection_id]
      [
        slug,
        edm_is_shown_at&.first,
        edm_is_shown_by&.first,
        title,
        record_id,
        collection&.dig(:repo_slug),
        collection&.dig(:slug),
        created_at,
        updated_at
      ]
    end
    filename = "#{report_type}_item_links"
    [to_csv(cols, results), filename]
  end

  def self.collections_item_count(report_type, start_date, end_date)
    cols = ['collection_record_id', 'collection_slug', 'collection_title', 'portals',
            'collection_created_at', "#{report_type}_item_count"]
    relevant_items = if report_type == 'new'
                       Item.where created_at: start_date.beginning_of_day..end_date.end_of_day
                     else
                       Item.where created_at: Date.new(0)...start_date.beginning_of_day,
                                  updated_at: start_date.beginning_of_day..end_date.end_of_day
                     end
    results = Collection.all
                        .includes(:portals)
                        .with_counts_for(relevant_items, as: 'relevant_item_count')
                        .map do |collection|
      [
        collection.record_id,
        collection.slug,
        collection.display_title,
        collection.portals.pluck(:code)&.sort&.join(", "),
        collection.created_at,
        collection.relevant_item_count
      ]
    end
    filename = "collections_#{report_type}_item_count"
    [to_csv(cols, results), filename]
  end

  def self.collections_urls_portals
    cols = %w[collection_slug collection_display_title portal_amso portal_crdl portal_dlg portal_other holding_institution edm_is_shown_at collection_created_at collection_updated_at]
    results = Collection.includes(:portals, :holding_institutions).map { |collection|
      portals = collection.portals.pluck(:code)
      portal_amso = portals.include?("amso") ? "amso" : ''
      portal_crdl = portals.include?("crdl") ? "crdl" : ''
      portal_dlg = portals.include?("georgia") ? "georgia" : ''
      portal_other = portals.include?("other") ? "other" : ''
      [collection.slug, collection.display_title, portal_amso, portal_crdl, portal_dlg, portal_other, collection.holding_institution&.authorized_name,
     collection.edm_is_shown_at.first, collection.created_at, collection.updated_at]
    }
    filename = "collections_urls_portals"
    [to_csv(cols, results), filename]
  end

  def self.collections_new(start_date, end_date)
    collections = Collection.includes(:portals, :holding_institutions, :time_periods, :subjects).where(created_at: start_date.beginning_of_day..end_date.end_of_day)

    cols = %w[collection_record_id public portals display_title short_description holding_institution spatial date subject]
    all_topics = Subject.all.pluck(:name).sort
    all_times = TimePeriod.all.order(:start).pluck(:name)
    cols = cols + all_topics + all_times

    results = collections.map do |collection|
      record = [collection.record_id, collection.public?, collection.portals.pluck(:code).sort.join('|'), collection.display_title,
       collection.short_description, collection.holding_institutions.pluck(:authorized_name).sort.join('|'),
       collection.geographic_locations.pluck(:code).sort.join('|'),collection.created_at, collection.dcterms_subject.sort.join('|')]
      topic_results = all_topics.map{|topic| collection.topic_names.include?(topic) ? topic : ''}
      time_results = all_times.map{|time| collection.time_period_names.include?(time) ? time : ''}
      record + topic_results + time_results
    end

    filename = 'collections_new'
    [to_csv(cols, results), filename]
  end

  def self.items_with_pages
    cols = %w[item_slug item_title item_record_id item_pages_count portals collection_slug collection_title item_provenance item_created_at item_updated_at]
    results =  Item.includes(:collection,:portals, :holding_institutions, :pages).where(pages_count: 1..Float::INFINITY).map do |item|
      [item.slug, item.title, item.record_id, item.pages_count, item.portals.pluck(:code)&.join(", "), item.collection.slug, item.collection.display_title, item&.dcterms_provenance.first, item.created_at, item.updated_at]
    end
    filename = "items_with_pages"
    [to_csv(cols, results), filename]
  end

  def self.items_without_holding_insts
    cols = %w[item_slug item_title item_record_id item_created_at item_updated_at]
    results = Item.left_joins(:holding_institutions).where(holding_institutions: {id: nil}).map do |item|
      [item.slug, item.title, item.record_id, item.created_at, item.updated_at]
    end
    filename = "items_without_holding_insts"
    [to_csv(cols, results), filename]
  end

  def self.holding_insts_collection_item_counts
    cols = %w[portal_code inst_name inst_slug collection_name collection_record_id public_item_count]
    results = []
    HoldingInstitution.includes(collections: :portals, repositories: :portals).are_public.each do |inst|
      inst.portal_codes.each do |inst_portal_code|
        collections = inst.collections.for_portals(inst_portal_code).includes(:portals).are_displayed
        if collections.any?
          collections.each do |collection|
            item_count = collection.items.for_portals(inst_portal_code).are_public.count
            results << [inst_portal_code, inst.authorized_name, inst.slug, collection.display_title, collection.record_id, item_count]
          end
        else
          results << [inst_portal_code, inst.authorized_name, inst.slug, '', '', '' ]
        end
      end
    end
    filename = "holding_insts_collection_item_counts"
    [to_csv(cols, results), filename]
  end

  def self.holding_insts(host)
    cols = %w[slug authorized_name image_available image_path coordinates state counties portal_amso portal_crdl portal_dlg portal_other]
    results = HoldingInstitution.includes(repositories: :portals).map do |inst|
      portals = inst.repositories.map {|repo| repo.portals.map &:code}.flatten.uniq
      portal_amso = portals.include?("amso") ? "amso" : ''
      portal_crdl = portals.include?("crdl") ? "crdl" : ''
      portal_dlg = portals.include?("georgia") ? "georgia" : ''
      portal_other = portals.include?("other") ? "other" : ''
      counties = inst.geographic_locations.pluck(:us_county).filter{|x| x.present?}.uniq.join(', ')
      states = inst.geographic_locations.pluck(:us_state).filter{|x| x.present?}.uniq.join(', ')
      image_url = if inst.image.url
                    URI.join(host,inst.image.url)
                  else
                    ''
                  end
      [inst.slug, inst.authorized_name, image_url.present?, image_url, inst.coordinates, states, counties, portal_amso, portal_crdl, portal_dlg, portal_other]
    end
    filename = 'holding_insts'
    [to_csv(cols, results), filename]
  end

  def self.crdl_names
    cols = %w[dlg_subject_personal item_count name_slug name_item_count]

    item_personal_array = Item.for_portals('crdl').where.not(dlg_subject_personal: "{}").pluck('dlg_subject_personal').map(&:uniq).flatten.compact
    subject_personal_counts_pairs = item_personal_array.group_by(&:itself).transform_values(&:count).to_a
    name_lookup = Name.lookup_hash
    results = subject_personal_counts_pairs.each do |count_pair|
      found_name = name_lookup[count_pair.first.sub(/ ?--.*/,"").downcase]

      count_pair.append(found_name&.slug)
      count_pair.append(found_name&.items&.count)
      end

    filename = 'crdl_names'
    [to_csv(cols, results), filename]
  end

  def self.created_monographs(marc_xml_import_id: MarcXmlImport.last.id)
    import = MarcXmlImport.find marc_xml_import_id
    items = Item.where(id: import.result_records.filter{|rec| rec['type']=='Item' && rec['created']}.map{|rec|rec['id']})
    item_structs = items.map{|item| record = GgpXmlRecord.new(item.source_xml); OpenStruct.new(slug: item.slug, title: item.dcterms_title.first, gadocs_call_numbers: record.gadocs_call_numbers, electronic_location_access: record.electronic_location_access ,mms_id: record.mms_id)}
    ReportService.generalized_report(item_structs, %w(slug title gadocs_call_numbers electronic_location_access mms_id), filename: "created_monographs" )
  end

  def self.to_csv(cols, results, force_quotes = false)
    CSV.generate(force_quotes: force_quotes) do |csv|
      csv << cols unless cols.blank?
      results.each do |result|
        csv << result
      end
    end
  end
end
