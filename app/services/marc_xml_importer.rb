# frozen_string_literal: true
class MarcXmlImporter

  attr_reader :file_path, :results, :records_processed, :serials_created, :serials_updated,
              :records_failed, :skipped
  def initialize(xml_file_path)
    @file_path = xml_file_path

    @results = []
    @result_rows_by_serial_id = {}
    @records_processed = 0
    @serials_created = 0
    @serials_updated = 0
    @records_failed = 0
    @skipped = 0

    @ggp_collection = Collection.find_by_slug 'ggpd'
    @ggp_holding_institution = HoldingInstitution.find_by_slug 'gyca'
  end

  def start
    Nokogiri::XML::Reader(File.open(@file_path)).each do |node|
      if node.name == 'record' && node.node_type == Nokogiri::XML::Reader::TYPE_ELEMENT
        @records_processed += 1
        begin
          record = GgpXmlRecord.new(node)
          if record.serial?
            upsert_serial GgpSerialRecord.new(node)
          else #monographs
            upsert_item GgpItemRecord.new(node)
          end
        rescue ActiveRecord::RecordInvalid => e
          log_record_failure e.message, source_data: record.source_data
          next
        rescue StandardError => e
          log_record_failure e.message
          next
        end
      end
    end
    serial_ids = @results.select{|result| result[:type] == 'serial'}.pluck(:id)
    attach_related_serials(Serial.where id: serial_ids)
  end

  def attach_related_serials(scope = Serial.all)
    scope.find_each do |serial|
      record = GgpXmlRecord.new serial.source_xml
      serial.preceding_serials |= record.preceding_identifiers.map { |h| Serial.get_from_identifiers h }.compact
      serial.succeeding_serials = record.succeeding_identifiers.map { |h| Serial.get_from_identifiers h }.compact
    end
  end

  def ggp_queryable
    @ggp_queryable ||= GgpQueryable.new
  end

  # @param [GgpQuery] query
  # @return [ActiveRecord::Relation<Item>]
  def ggp_items_from_query(query)
    # extract slug and call number queries from query
    Item.where id: ggp_queryable.query(query)
  end

  # @param [GgpSerialRecord] record
  def upsert_serial(record)
    if serial = Serial.find_by_slug(record.slug)
      serial.update! record.to_hash
    else
      serial = Serial.create!(record.to_hash)
    end
    log_record_success serial

    serial
  end

  # @param [GgpItemRecord] record
  def upsert_item(record)
    item = if record.slug_from_856
             Item.find_by_slug record.slug_from_856
           elsif record.slug_from_call_number
             alt_slug = record.slug_from_call_number.sub(/^(.)-/, '\1-ga-b')
             Item.where('slug LIKE ? OR slug LIKE ?', "#{record.slug_from_call_number}%", "#{alt_slug}%").first
           end

    if item
      dcterms_spatial = item.dcterms_spatial + record.dcterms_spatial
      item.update! external_identifiers: record.external_identifiers, dcterms_spatial: dcterms_spatial
    else
      item_hash = record.to_hash()
      item_hash[:collection_id] = @ggp_collection.id
      item_hash[:holding_institution_ids] = [@ggp_holding_institution.id]
      item = Item.create!(item_hash)
    end
    log_record_success item

    item
  end



  def monograph_item_by_856(record)
    field_856 = record.repeatable_datafield('856', subfields: ['u']).first
    return nil unless  field_856
    begin
      query = GgpItemMatchingService.ggp_query_for_url field_856
    rescue
      return nil
    end
    begin
      # extract slug and call number queries from query
      items = ggp_items_from_query(query)
    rescue
      return nil
    end
    if items.count == 1
      items.first
    else
      nil
    end
  end

  def log_record_success(record)
    result = {
      success: true,
      type: record.class.name,
      slug: record.slug,
      id: record.id
    }

    if record.id_previously_changed?
      result[:created] = true
      result[:message] = "#{record.class.name} created",
      @serials_created += 1
    else
      result[:updated] = true
      result[:message] = "#{record.class.name} updated",
      @serials_updated += 1
    end
    @results << result
    @result_rows_by_serial_id[record.id] = result
    @results
  end

  def log_record_failure(message, options={})
    @results << {
      message: message,
      fail: true,
      **options
    }
    @records_failed += 1
  end

  def log_record_skipped(message, options={})
    @results << {
      message: message,
      **options
    }
    @skipped += 1
  end

  def log_warning(message, options={})
    @results << {
      message: "Warning: #{message}",
      fail: true,
      **options
    }
  end

end
