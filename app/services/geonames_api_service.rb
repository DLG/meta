# frozen_string_literal: true

class GeonamesApiService
  BASE_URL = 'http://api.geonames.org'

  class GeonamesApiError < StandardError; end
  class GeonamesCreditsExhausted < GeonamesApiError; end

  class GeonamesApiResult
    TYPICAL_COMPONENT_MATCHES = %w[countryName adminName1 adminName2 adminName3, adminName4]

    # @param geonames_hash [Hash]
    def initialize(geonames_hash)
      @geonames_hash = geonames_hash
    end

    # @return [Hash]
    def original
      @geonames_hash
    end

    # @return [GeographicLocation]
    def state_geoloc
      GeographicLocation.find_by geonames_id: original['adminId1']
    end

    # @return [GeographicLocation]
    def county_geoloc
      GeographicLocation.find_by geonames_id: original['adminId2']
    end

    # @return [String]
    def us_county
      county_geoloc&.us_county
    end

    # @return [String]
    def us_state
      state_geoloc&.us_state
    end

    # @return [Integer]
    def geonames_id
      @geonames_hash['geonameId']
    end

    # @return [String]
    def latitude
      @geonames_hash['lat']
    end

    # @return [String]
    def longitude
      @geonames_hash['lng']
    end

    # @return [Array<String>]
    def alternate_names
      if @geonames_hash.include?('alternateNames')
        @geonames_hash['alternateNames'].map do |alt_name|
          normalize(alt_name['name'])
        end
      else
        []
      end
    end

    # @return [Array<String>]
    def names
      names = alternate_names
      %w[name, asciiName, toponymName].each do |key|
        name = @geonames_hash[key]
        names << normalize(name) unless name.blank?
      end
      names
    end

    # @return [String]
    def normalize(name)
      name.strip.downcase
    end

    # @return [String]
    def toponym_name
      @geonames_hash['toponymName']
    end

    def inspect
      "<Geoname ##{geonames_id}: #{@geonames_hash['countryName']}, #{@geonames_hash['adminName1']}, #{@geonames_hash['adminName2']}, #{toponym_name}>"
    end

    # @param components [Array<String>]
    # @return [Numeric]
    def score_against(components)
      components = components.map do |comp|
        normalize(comp)
      end
      name = components.pop
      score = names.include?(name) ? 5 : 0
      score += 5 if normalize(toponym_name) == name

      components.each_with_index do |component, i|
        key = TYPICAL_COMPONENT_MATCHES[i]
        next if key.nil?
        corresponding_name = @geonames_hash[key]
        next if corresponding_name.nil?
        corresponding_name = normalize corresponding_name
        if corresponding_name == component
          score += 2
        elsif component.include?(corresponding_name) || corresponding_name.include?(component)
          score += 1
        else
          score -= 1
        end
      end
      score
    end
  end

  # @return [String]
  def self.geonames_api_key
    Rails.application.credentials.geonames_api_key
  end

  # Runs a geonames.org query using our credentials
  # Raises GeonamesCreditsExhausted if appropriate and GeonamesApiError in the event of other weirdness
  # @param endpoint [String]
  # @param query [Hash]
  # @return [Hash] parsed JSON result
  def self.do_geonames_query(endpoint, query)
    query['username'] = geonames_api_key
    response = HTTParty.get "#{BASE_URL}/#{endpoint}", query: query
    if response.success?
      result = response.parsed_response
      if result.present?
        if result['status'].present?
          status_code = result.dig 'status', 'value'
          if [18, 19, 20].include? status_code
            # http://www.geonames.org/export/webservice-exception.html
            message = {
              18 => 'daily limit of credits exceeded',
              19 => 'hourly limit of credits exceeded',
              20 => 'weekly limit of credits exceeded'
            }[status_code]
            raise GeonamesCreditsExhausted message
          else
            raise GeonamesApiError result.dig('status', 'message')
          end
        end
        return result
      end
    end
    raise GeonamesApiError
  end

  # @param components [Array<String>]
  # @return [Array<GeonamesApiResult>]
  def self.search_on_components(components)
    results = do_geonames_query('searchJSON', {
      'q': components.join(', '),
      'lang': 'en',
      'style': 'full',
      'maxRows': 10,
    })&.dig 'geonames'
    raise GeonamesApiError if results.nil?
    i = 0
    results.map { |r| GeonamesApiResult.new r }
  end

  # Runs a text search on Geonames.org and re-sorts so using the GeonamesApiResult.score_against method
  # Ties are won by the result that came first in the original results as returned by Geonames.org
  # @param components [Array<String>]
  # @return [Array<GeonamesApiResult>]
  def self.ranked_search_on_components(components)
    i = 0
    results = search_on_components(components).map do |r|
      i += 1
      { result: r, api_rank: i}
    end
    results = results.sort do |a, b|
      comparison = b[:result].score_against(components) <=> a[:result].score_against(components)
      comparison = a[:api_rank] <=> b[:api_rank] if comparison.zero?
      comparison
    end
    results.pluck :result
  end

  # @param components [Array<String>]
  # @return [<GeonamesApiResult>]
  def self.get_best_result(components)
    ranked_search_on_components(components).first
  end

end