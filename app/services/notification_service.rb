# frozen_string_literal: true

# facilitate the sending of (slack) notifications
class NotificationService

  NOTIFICATION_ENVIRONMENTS = %w[production staging development dev experimental].freeze

  def initialize(**options)
    @slack = Slack::Notifier.new Rails.application.credentials.slack_worker_webhook.dig(Rails.env.to_sym)
    @allow_slack_failure = options[:allow_slack_failure] == true
  end

  # send a basic notification
  def notify(message)
    hostname = if Rails.env == "development"
                 " #{Socket.gethostname}"
               else
                 ''
               end
    if NOTIFICATION_ENVIRONMENTS.include? Rails.env
      puts message
      begin
        @slack.ping("`#{Rails.env}#{hostname}`: #{message}")
      rescue Slack::Notifier::APIError
        raise unless @allow_slack_failure
      end
    end
  end
end