class MarcXmlRecord
  attr_reader :document

  def initialize(node)
    @document = if node.respond_to? :outer_xml
                  Nokogiri::XML(node.outer_xml)
                else
                  node
                end
  end

  def leader
    @document.at('/record/leader').text
  end

  def serial?
    leader[7] == 's'
  end

  def control_field(tag)
    @document.css("record controlfield[tag='#{tag}']").text
  end

  def data_field(tag, ind1: nil, ind2: nil )
    selector = "record datafield[tag='#{tag}']"

    if ind1
      selector += "[ind1='#{ind1}']"
    end

    if ind2
      selector += "[ind2='#{ind2}']"
    end

    @document.css(selector)
  end

  def title_statement
    nonrepeatable_datafield('245')
  end

  def varying_form_of_titles
    repeatable_datafield('246')
  end

  # Take a string like "(OCoLC) ocm123456" and return ["OCLC", "123456"]
  def extract_identifier_and_type(text)
    # match strings of the form "(identifier_type) identifier_value"
    if /^\(([^)]+)\) *(.+)$/ =~ text
      type = $1.strip
      value = $2.strip

      # formatting rules for specific identifier types go here
      if type == 'OCoLC' && /^[a-zA-Z]{0,3}(\d+)$/ =~ value
        type = 'OCLC'
        value = $1
      elsif type == 'EXLNZ-01GALI_NETWORK'
        type = 'NZ MMS ID'
      end

    else
      type = 'Other'
      value = text
    end
    [type, value]
  end

  def system_control_numbers
    (repeatable_datafield('035', subfields: %w(a) ).map do |subfield_value|
      next if subfield_value.blank?
      type, value = extract_identifier_and_type subfield_value
      { type: type, value: value}
    end).compact
  end


  def preceding_identifiers
    (data_field('780').map do |df|
      df.css('subfield[code=w]')
        .map { |sf| extract_identifier_and_type sf }
        .filter { |type, _id| type != 'Other' }
        .to_h
    end).filter &:present?
  end

  def succeeding_identifiers
    (data_field('785').map do |df|
      df.css('subfield[code=w]')
        .map { |sf| extract_identifier_and_type sf }
        .filter { |type, _id| type != 'Other' }
        .to_h
    end).filter &:present?
  end

  def oclc_number
    slug = system_control_numbers.filter{ |x| x[:type] == 'OCLC' }.first&.dig :value
    return slug if slug.present?

    control_field('001').sub(/^[^0-9]+/, '') if control_field('003') == 'OCoLC'
  end

  def electronic_location_access
    repeatable_datafield('856', subfields: %w(u)).reject(&:blank?)
  end

  # @return [Array<String>]
  def repeatable_datafield(tag, ind1: nil, ind2: nil, subfields: nil, separator: ' ' )
    selector = 'subfield'

    if subfields
      subfields = [subfields] unless subfields.is_a? Array
      selector = subfields.map {|subfield| "subfield[code='#{subfield}']"}.join(', ')
    end

    data_field(tag, ind1: ind1, ind2: ind2).map do |field|
      field.css(selector).map(&:text).join(separator)
    end
  end

  # @return [String]
  def nonrepeatable_datafield(tag, separator = ' ' )
    data_field(tag).css("subfield").map(&:text).join(separator).presence
  end

  def to_hash(remove_methods: [], add_methods: [] )
    remove = %i(document serial? leader data_field control_field extract_identifier_and_type repeatable_datafield
                title_statement system_control_numbers varying_form_of_titles nonrepeatable_datafield
                preceding_identifiers succeeding_identifiers to_hash oclc_number electronic_location_access)
    methods = self.public_methods(true) - Object.public_methods(true)  - remove - remove_methods + add_methods

    hash = { }
    methods.each{|method| hash[method] = self.send(method)}
    hash
  end
end
