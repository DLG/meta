# frozen_string_literal: true

class GgpQueryable
  def self.ggp_collection
    Collection.find_by_slug('ggpd')
  end

  def initialize(scope = self.class.ggp_collection.items)
    @scope = scope
    @by_item_id = {}
    @items = create_item_hashes
    @query_cache = {}
  end

  attr_reader :scope, :items

  def create_item_hashes
    @scope.pluck(:id, :slug, :dc_date).map do |id, slug, dc_date|
      h = {
        id: id,
        'id' => slug,
        'ca' => call_number_from_slug(slug),
        'rt' => slug[0],
        'year' => years_from_dc_date(dc_date)
      }
      @by_item_id[id] = h
      h
    end
  end

  def get_item(id)
    @by_item_id[id]
  end

  def query(query_text)
    q = GgpQuery.new query_text, @scope, query_cache: @query_cache
    items.filter{ |item| q.test_against item }.pluck :id
  end

  def call_number_from_slug(slug)
    slug.downcase.sub(/^[isy]-/, '')
                 .gsub('-s', '/')
                 .gsub('-p', '.')
                 .gsub('-m', ',')
                 .gsub('-l', '+')
                 .gsub('-o', '(')
                 .gsub('-c', ')')
                 .gsub('-b', ' ')
                 .gsub('-a', '&')
                 .gsub('-0', '#')
                 .gsub('-h', '-')
                 .gsub(/ +/, ' ')
  end

  # If type is not provided, this will default to 's' ("single"/monograph)
  def self.slug_from_call_number(call_number, type: 's')
    encoded = call_number.downcase.gsub('-', '-h')
                         .gsub('#', '-0')
                         .gsub('&', '-a')
                         .gsub('&', '-a')
                         .gsub(' ', '-b')
                         .gsub(')', '-c')
                         .gsub('(', '-o')
                         .gsub('+', '-l')
                         .gsub(',', '-m')
                         .gsub('.', '-p')
                         .gsub('/', '-s')
    "#{type}-#{encoded}"
  end

  def years_from_dc_date(dc_date)
    IndexDates.index_years(dc_date).map(&:to_i)
  end

  def inspect
    "<#{self.class.name} (#{items.size} items)>"
  end

end
