class GgpXmlRecord < MarcXmlRecord
  # @return [Array<String>]

  def slug; end

  def external_identifiers
    ids = [
      {type: "OCLC", value: oclc_number},
      {type: "IZ MMS ID", value: iz_mms_id},
      {type: "NZ MMS ID", value: nz_mms_id}
    ].filter {|x| x[:value]}
    ids += gadocs_call_numbers.map { |v| { type: "Call Number", value: v } }
    ids += lc_call_numbers.map { |v| { type: "Call Number", value: v } }
    ids += system_control_numbers
    ids.uniq
  end

  def dcterms_title
    titles = []
    titles << nonrepeatable_datafield('130')
    titles << nonrepeatable_datafield('245')
    titles += repeatable_datafield('246')
    titles += repeatable_datafield('830')
    titles.compact
  end

  def dcterms_publisher
    publishers = []
    publishers += repeatable_datafield('260', subfields: %w(a b) )
    publishers += repeatable_datafield('264')
    publishers.compact
  end

  def dcterms_description
    tags = [362,(500..599).to_a].flatten.without(590, 597, 530)
    descriptions = []
    tags.each {|tag| descriptions += repeatable_datafield(tag.to_s) }

    descriptions.compact
  end

  def dcterms_subject
    subjects = []
    subjects += repeatable_datafield('610', separator: '--')
    subjects += repeatable_datafield('611', separator: '--')
    subjects += repeatable_datafield('650', separator: '--')
    subjects += repeatable_datafield('651', separator: '--')
    subjects.compact
    subjects.empty? ? ['Georgia'] : subjects
  end

  def dcterms_spatial
    spatial = repeatable_datafield('651', ind2: "7", subfields: %w(a z), separator: ', ')
    dcterms_spatial = spatial.map do |place|
      new_place = place.tr('.', '')
      first_place = new_place.split(',').first.strip.titleize
      if GeographicLocation::FIFTY_STATES.include? first_place
        new_place = "United States, #{new_place}"
      end
      new_place
    end
    dcterms_spatial.any? ? dcterms_spatial : ['United States, Georgia']
  end

  def dcterms_medium
    ['state government records']
  end

  def dcterms_contributor
    repeatable_datafield('710')
  end

  def dcterms_creator
    creators = []
    creators << nonrepeatable_datafield('100')
    creators << nonrepeatable_datafield('110')
    creators << nonrepeatable_datafield('111')
    creators.compact
  end

  def dc_date
    gen_info  = control_field('008')
    date_type = gen_info[6].downcase

    if date_type == 'e'
      detailed_date = "#{gen_info[7..10]}-#{gen_info[11..12]}-#{gen_info[13..14]}"
      return [detailed_date]
    end

    begin_date = gen_info[7..10].gsub('u', '0').strip
    end_date = gen_info[11..14].gsub('u', '9').strip
    return ['/'] if (begin_date.blank? && end_date.blank?) ||
                 (begin_date == '0000' && end_date == '9999')

    dc_date = if begin_date == end_date
                begin_date
              else
                "#{begin_date}/#{end_date}"
    end

    [dc_date]
  end

  def dcterms_language
    [control_field('008')[35..37]]
  end

  def dlg_subject_personal
    repeatable_datafield('600')
  end

  def source_data
    @document.to_s
  end

  def gadocs_call_numbers
    repeatable_datafield('086', subfields: 'a')
  end

  def to_hash(remove_methods: [], add_methods: [] )
    remove = %i(dcterms_replaces dcterms_is_replaced_by gadocs_call_numbers lc_call_numbers mms_id nz_mms_id iz_mms_id)
    super(remove_methods: (remove_methods + remove), add_methods: add_methods )
  end

  def lc_call_numbers
    repeatable_datafield('050', subfields: 'a')
  end

  def mms_id
    return nil if control_field('003').present?
    id = control_field('001')
    return nil unless /^[0-9]+(59|31)$/.match? id
    id
  end

  def nz_mms_id
    mms_id&.ends_with?('31') ? mms_id : nil
  end

  def iz_mms_id
    mms_id&.ends_with?('59') ? mms_id : nil
  end

end