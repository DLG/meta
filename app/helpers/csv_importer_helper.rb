module CsvImporterHelper

  def importer_results_field(value)
    return '' if value.blank?

    value.kind_of?(Array) ? array_html_list(value) : value
  end
end
