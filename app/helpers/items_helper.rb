module ItemsHelper
  def show_item_iiif_viewer?
    @item.pages.any? && show_iiif_viewer?
  end

  def legacy_thumbnail_tag(item) # todo this duplicates functionality in BlacklightHelper
    url = "https://dlg.galileo.usg.edu/#{item.repository.slug}/#{item.collection.slug}/do-th:#{item.slug}"
    image_tag url, class: 'img-thumbnail'
  end

  def item_validation_status(item)
    if item.valid_item
      content_tag(:span, nil, class: 'glyphicon glyphicon-ok', aria: { hidden: true } )
    else
      content_tag(:span, nil, class: 'glyphicon glyphicon-remove', aria: { hidden: true } )
      # todo very hard on page load times when most items are invalid...using staging vm server
      item.validate
      content_tag(:span, nil, class: 'glyphicon glyphicon-remove validation-errors', aria: { hidden: true }, data: { content: errors_html(item.errors), toggle: 'popover' } ) +
        content_tag(:sup, item.errors.count)
    end
  end

  def pages_count(rec)
    rec.pages_count ? rec.pages_count : '0'
  end

  def portal_badges(rec)
    badges = rec.portals.map do |p|
      if p.code == 'georgia'
        link_to(content_tag(:span, p.code, class: 'badge badge-primary'), rec.dlg_url)
      elsif p.code == 'crdl'
        link_to(content_tag(:span, p.code, class: 'badge badge-primary'), rec.crdl_url)
      else
        content_tag(:span, p.code, class: 'badge badge-primary')
      end
    end
    badges.join('<br />').html_safe
  end

  def version_author_name(version)
    return 'Batch Action' unless version.terminator

    user = User.get_version_author(version)
    user ? user.email : I18n.t('meta.defaults.labels.deleted_user', id: version.terminator)
  end

  def diff(version1, version2)
    changes = Diffy::Diff.new(version1, version2,
                              include_plus_and_minus_in_html: true,
                              include_diff_info: true
    )
    changes.to_s.present? ? changes.to_s(:html).html_safe : false
  end

  def geographic_locations_list(item)

    geographic_location_links = item.geographic_locations.map do |gl|
      link_to gl.code, geographic_location_path(gl)
    end
    geographic_location_links.join('<br>').html_safe
  end

  def serial_logo_html
    "<img class=\"serial-logo\" src=\"#{asset_path 'serial.svg'}\" alt=\"#{t('activerecord.attributes.item.serial_logo_hover')}\" title=\"#{t('activerecord.attributes.item.serial_logo_hover')}\">".html_safe
  end

  private

  def errors_html(errors)
    html = ''
    errors.each do |field, message|
      html += "<p>#{field} #{message}</p>"
    end
    html
  end

end