require 'uri'

# global app helpers
module ApplicationHelper
  include Pagy::Frontend

  INDEX_TRUNCATION_VALUE = 2500

  def score_display(options)
    "<span class='badge bg-primary text-light'>#{options[:value].first}</span>".html_safe
  end

  def boolean_facet_labels(value)
    %w[true 1].include?(value) ? 'Yes' : 'No'
  end

  # handle linking in catalog results
  def linkify(options = {})
    url = options[:value].first
    link_to url, url
  end

  def regex_linkify(options = {})
    values = options[:value]
    values.map! do |v|
      URI.extract(v).each do |uri|
        v = v.sub(uri, link_to(uri, uri))
      end
      v
    end
    values.join('<br>').html_safe
  end

  # truncate when displaying search results
  def truncate_index(options = {})
    truncate(
      options[:value].join(I18n.t('support.array.words_connector')),
      length: INDEX_TRUNCATION_VALUE,
      omission: I18n.t('meta.search.index.truncated_field'),
      escape: false
    )
  end

  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = is_current_sort_column?(column) ? "current #{sort_direction}" : nil
    direction = is_current_sort_column?(column) && sort_direction == 'asc' ? 'desc' : 'asc'
    link_to title, params.permit(:id, :per_page, :page, :repository_id, :public, :valid_item, :user_id, :status, :controller,:action,:item_id,:direction,:sort,:batch_id,:format,:utf8,:commit,:collection_id,portal_id:[]).to_h.merge(sort: column, direction: direction), class: css_class
  end

  # This returns true if column matches Sorting.sort_column OR if it's qualified w/ table name and matches the
  # sort parameter in the query string.
  # This is because Sorting.sort_column seems to get confused with these qualified column names.
  def is_current_sort_column?(column)
    (column == sort_column) || (column.include?('.') && column == params[:sort])
  end

  # set menu items as 'active'
  def admin_active_class(key)
    request.original_url.include?('/' + key) ? 'active' : ''
  end

  def meta_textarea_label(klass, term, serial_icon: false)
    label = ''
    label += label_tag "#{term}", t("activerecord.attributes.#{klass}.#{term}")
    label += "&nbsp;<em class='text-muted'>#{term}</em>"
    label += "&nbsp;#{serial_logo_html}" if serial_icon
    label.html_safe
  end

  def meta_textarea(object, term, serial_icon: false)
    klass = object.class.name.demodulize.underscore
    html = '<div class="form-group">'
    html += meta_textarea_label(klass, term, serial_icon: serial_icon)
    html += text_area_tag"#{klass}[#{term}]", object.method(term).call.join("\n"), { rows: '5', class: 'form-control', id: term }
    html += '</div>'
    html.html_safe
  end

  def controlled_vocabulary_select(entity, field, values, multiple, serial_icon: false)
    id = "#{field.to_s.tr('_', '-')}-select#{'-multiple' if multiple}"
    klass = entity.class.name.demodulize.underscore
    html = '<div class="form-group">'
    html += meta_textarea_label klass, field, serial_icon: serial_icon
    html += select(
      klass, field, values,
      { include_blank: true },
      { class: 'form-control', id: id, multiple: multiple }
    )
    html += '</div>'
    html.html_safe
  end

  def boolean_check_icon(value)
    value = false if value == 'false'
    value ? content_tag(:span, nil, class: 'glyphicon glyphicon-ok') : ''
  end

  def display_date(value)
    value ? l(value, format: :h) : ''
  end

  def per_page_values
    [20, 100, 500, 1000]
  end

  def per_page_selected(pp)
    per_page_setting == pp ? 'btn-primary' : 'btn-default'
  end

  def link_to_edit_previous_document(previous_document)
    if previous_document
      klass, id = previous_document['sunspot_id_ss'].split(' ')
      link_opts = session_tracking_params(previous_document, search_session['counter'].to_i - 1).merge(class: 'previous', rel: 'prev')
      link_to url_for(controller: klass.downcase.pluralize, action: 'edit', id: id), link_opts do
        content_tag :span, 'Previous Result', class: 'previous'
      end
    end

  end

  def link_to_edit_next_document(next_document)
    if next_document
      klass, id = next_document['sunspot_id_ss'].split(' ')
      link_opts = session_tracking_params(next_document, search_session['counter'].to_i + 1).merge(class: 'next', rel: 'next')
      link_to url_for(controller: klass.downcase.pluralize, action: 'edit', id: id), link_opts do
        content_tag :span, 'Next Result', class: 'next'
      end
    end
  end

  def pretty_json(json_string)
    return '' if json_string.blank?

    json = JSON.parse(json_string)
    tag.pre(JSON.pretty_generate(json))
  end

  def array_html_list(single_dimension_array, linkify: false)
    return '' unless  single_dimension_array.kind_of?(Array) && single_dimension_array.any?
    items = ""
    single_dimension_array.each do |elem|
      elem = find_urls_and_linkify(elem) if linkify
      items << tag.li(elem)
    end
    tag.ul(items, escape: false)
  end

  #For use w/ filterriffic
  def dlg_sorting_link(filterrific, column_sym, opts = {})
    options = opts.merge(url_for_attrs: request.parameters.except(:controller, :action, :page))
    filterrific_sorting_link(filterrific, column_sym, options)
  end

  def entities_links_array(entities, display_field)
    return [] unless entities&.any?
    entities.map do |entity|
      link_to entity.send(display_field), entity
    end
  end

  def edit_record_path(document)
    class_name, id = document['sunspot_id_ss'].split(' ')

    send("edit_#{class_name.underscore}_path", id)
  end

  def show_record_path(document)
    class_name, id = document['sunspot_id_ss'].split(' ')

    send("#{class_name.underscore}_path", id)
  end

  # shows a right-justified div with Pagy-generated info
  # @param [Pagy] pagy
  # @return [String]
  def displayed_items(pagy)
    tag.div(class: 'float-right ') do
      pagy_info(pagy).html_safe
    end
  end

  def filterrific_model_class
    model = @filterrific.model_class
    if model.is_a? ActiveRecord::Relation
      return model.klass
    end
    model
  end

  def user_label_for(object)
    if object.try :user
      object.user.email
    else
      if object.try :user_id
        I18n.t('meta.defaults.labels.deleted_user', id: object.user_id)
      else
        I18n.t('meta.defaults.labels.blank_user')
      end
    end
  end
end
