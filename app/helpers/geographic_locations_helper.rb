module GeographicLocationsHelper

  GEONAMES_EXTERNAL_LINK_PREFIX = 'https://www.geonames.org/'

  def geonames_url(geonames_id)
    "#{GEONAMES_EXTERNAL_LINK_PREFIX}#{geonames_id}/"
  end

  def geonames_external_link(geonames_id, text = geonames_id)
    return nil if geonames_id.blank?

    link_to raw("#{text} <i class=\"glyphicon glyphicon-new-window\"></i>"),
            geonames_url(geonames_id),
            target: 'geonames'
  end

  def geonames_external_link_or_status(geographic_location)
    return geonames_external_link(geographic_location.geonames_id) if geographic_location.geonames_id.present?
    geographic_location.geoname_verified ? nil : '<i>Pending check</i>'.html_safe
  end

  # @param form [FormBuilder]
  def render_geographic_location_edit_form(form, serial_icon: false)
    heading = t('activerecord.attributes.geographic_location.geographic_locations')
    heading += "&nbsp;#{serial_logo_html}" if serial_icon

    many_to_many_subform_of form,
                            per_related: :geographic_location,
                            via: form.object.class.geographic_locations_relationship,
                            sorted_by: :code,
                            heading: heading.html_safe do |subform|
      concat(render partial: 'shared/geographic_location_subform_inner', locals: { subform: subform })
    end
  end

  # create a link to a location name facet value
  def link_to_placename_field field_value, field, displayvalue = nil, link_options = {}
    if params[:f] && params[:f][field] && params[:f][field].include?(field_value)
      new_params = params
    else
      new_params = search_state.filter(field).add(field_value).params
    end
    new_params[:view] = default_document_index_view_type
    link_to(displayvalue.presence || field_value,
            search_catalog_path(new_params.except(:id, :spatial_search_type, :coordinates)),
            link_options)
  end


end
