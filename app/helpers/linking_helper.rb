# frozen_string_literal: true

# Common helper methods for HTML link/button generating logic
module LinkingHelper
  # Find all urls and linkify for a blacklight document
  def linkify_urls(options = {})
    new_values= options[:value].map {|str| find_urls_and_linkify(str)}
    new_values.join('<br>').html_safe
  end

  # Find all urls and linkify for a blacklight document for the first value
  def linkify_urls_single(options = {})
    str = options[:value].first
    find_urls_and_linkify(str).html_safe
  end

  # Find all urls in a string and replace with anchor tags
  def find_urls_and_linkify(str)
    return str if str.blank?
    urls = URI.extract(strip_links(str), %w[http https]) #look for urls and disregard existing anchor tags
    url_map = urls.to_h {|url| [url, link_to(url, url, target: '_blank')]}
    url_map.each {|k, v| str.sub!(k, v)}

    sanitize(str, tags: %w(a), attributes: %w(href target)).html_safe
  end
end