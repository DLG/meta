# frozen_string_literal: true

module SubformHelper

  # @param form [FormBuilder]
  # @param per_related: relation [Symbol]
  # @param via: join_table [Symbol]
  # @param sorted_by: sort_field [Symbol]
  # @param heading: heading [String]
  # @yieldparam [RelatedObjectSubform]
  def many_to_many_subform_of(form, per_related:, via:, sorted_by: nil, heading:, &per_item)
    ManyToManySubform.render_subform(self, form, via, per_related,
                                     sorted_by: sorted_by, heading: heading, &per_item)
  end

  def typeahead_subform(form, &per_item)
    TypeaheadAwareSubform.render_subform(self, form, &per_item)
  end

  def expanding_list_subform(form, property, **options, &per_item)
    ExpandingListSubform.render_subform(self, form, property, **options, &per_item)
  end

  def render_external_identifiers_edit_form(form)
    render partial: 'shared/external_identifiers_subform', locals: { form: form }
  end

end
