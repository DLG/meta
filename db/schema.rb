# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2025_01_15_142121) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "hstore"
  enable_extension "plpgsql"

  create_table "batch_imports", id: :serial, force: :cascade do |t|
    t.string "format", null: false
    t.integer "user_id", null: false
    t.integer "batch_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.json "results", default: {}
    t.boolean "validations"
    t.integer "batch_items_count", default: 0, null: false
    t.datetime "completed_at", precision: nil
    t.integer "item_ids", default: [], null: false, array: true
    t.string "xml"
    t.string "file_name"
    t.boolean "match_on_id", default: false, null: false
  end

  create_table "batch_item_geographic_locations", force: :cascade do |t|
    t.bigint "batch_item_id"
    t.bigint "geographic_location_id"
    t.index ["batch_item_id"], name: "index_batch_item_geographic_locations_on_batch_item_id"
    t.index ["geographic_location_id"], name: "index_batch_item_geographic_locations_on_geographic_location_id"
  end

  create_table "batch_items", id: :serial, force: :cascade do |t|
    t.boolean "dpla", default: false, null: false
    t.boolean "public", default: false, null: false
    t.text "dc_format", default: [], null: false, array: true
    t.text "dc_right", default: [], null: false, array: true
    t.text "dc_date", default: [], null: false, array: true
    t.text "dc_relation", default: [], null: false, array: true
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "batch_id", null: false
    t.integer "collection_id"
    t.string "slug", null: false
    t.text "dcterms_is_part_of", default: [], null: false, array: true
    t.text "dcterms_contributor", default: [], null: false, array: true
    t.text "dcterms_creator", default: [], null: false, array: true
    t.text "dcterms_description", default: [], null: false, array: true
    t.text "dcterms_extent", default: [], null: false, array: true
    t.text "dcterms_medium", default: [], null: false, array: true
    t.text "dcterms_identifier", default: [], null: false, array: true
    t.text "dcterms_language", default: [], null: false, array: true
    t.text "legacy_dcterms_spatial", default: [], null: false, array: true
    t.text "dcterms_publisher", default: [], null: false, array: true
    t.text "dcterms_rights_holder", default: [], null: false, array: true
    t.text "dcterms_subject", default: [], null: false, array: true
    t.text "dcterms_temporal", default: [], null: false, array: true
    t.text "dcterms_title", default: [], null: false, array: true
    t.text "dcterms_type", default: [], null: false, array: true
    t.text "edm_is_shown_at", default: [], null: false, array: true
    t.text "legacy_dcterms_provenance", default: [], null: false, array: true
    t.integer "item_id"
    t.integer "other_collections", default: [], array: true
    t.text "dlg_local_right", default: [], null: false, array: true
    t.boolean "valid_item", default: false, null: false
    t.boolean "has_thumbnail", default: false, null: false
    t.text "dcterms_bibliographic_citation", default: [], null: false, array: true
    t.integer "batch_import_id"
    t.boolean "local", default: false, null: false
    t.text "dlg_subject_personal", default: [], null: false, array: true
    t.string "record_id", default: "", null: false
    t.text "edm_is_shown_by", default: [], null: false, array: true
    t.text "fulltext"
    t.string "iiif_partner_url"
    t.boolean "iiif_partner_url_enabled", default: true, null: false
    t.bigint "serial_id"
    t.string "source_data", default: ""
    t.jsonb "external_identifiers", default: [], null: false
    t.index ["batch_id"], name: "index_batch_items_on_batch_id"
    t.index ["batch_import_id"], name: "index_batch_items_on_batch_import_id"
    t.index ["external_identifiers"], name: "index_batch_items_on_external_identifiers", using: :gin
    t.index ["item_id"], name: "index_batch_items_on_item_id"
    t.index ["other_collections"], name: "index_batch_items_on_other_collections"
    t.index ["serial_id"], name: "index_batch_items_on_serial_id"
    t.index ["slug"], name: "index_batch_items_on_slug"
    t.index ["valid_item"], name: "index_batch_items_on_valid_item"
  end

  create_table "batch_items_holding_institutions", id: false, force: :cascade do |t|
    t.integer "holding_institution_id", null: false
    t.integer "batch_item_id", null: false
  end

  create_table "batches", id: :serial, force: :cascade do |t|
    t.integer "contained_count", default: 0, null: false
    t.string "name", null: false
    t.text "notes"
    t.datetime "committed_at", precision: nil
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "user_id"
    t.json "commit_results", default: {}, null: false
    t.integer "batch_items_count", default: 0, null: false
    t.datetime "queued_for_commit_at", precision: nil
    t.string "job_message"
    t.index ["user_id"], name: "index_batches_on_user_id"
  end

  create_table "bookmarks", id: :serial, force: :cascade do |t|
    t.integer "user_id", null: false
    t.string "user_type"
    t.string "document_id"
    t.string "title"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "document_type"
    t.index ["user_id"], name: "index_bookmarks_on_user_id"
  end

  create_table "collection_geographic_locations", force: :cascade do |t|
    t.bigint "collection_id"
    t.bigint "geographic_location_id"
    t.index ["collection_id"], name: "index_collection_geographic_locations_on_collection_id"
    t.index ["geographic_location_id"], name: "index_collection_geographic_locations_on_geographic_location_id"
  end

  create_table "collection_resources", id: :serial, force: :cascade do |t|
    t.string "slug"
    t.integer "position"
    t.string "title"
    t.text "content"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "collection_id"
    t.boolean "use_external_link", default: false, null: false
    t.string "external_link", default: "", null: false
  end

  create_table "collections", id: :serial, force: :cascade do |t|
    t.integer "repository_id"
    t.text "display_title", null: false
    t.text "short_description"
    t.text "dc_format", default: [], null: false, array: true
    t.text "dc_right", default: [], null: false, array: true
    t.text "dc_date", default: [], null: false, array: true
    t.daterange "date_range"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.text "dc_relation", default: [], null: false, array: true
    t.string "other_repositories", default: [], array: true
    t.boolean "public", default: false, null: false
    t.string "slug", null: false
    t.text "dcterms_is_part_of", default: [], null: false, array: true
    t.text "dcterms_contributor", default: [], null: false, array: true
    t.text "dcterms_creator", default: [], null: false, array: true
    t.text "dcterms_description", default: [], null: false, array: true
    t.text "dcterms_extent", default: [], null: false, array: true
    t.text "dcterms_medium", default: [], null: false, array: true
    t.text "dcterms_identifier", default: [], null: false, array: true
    t.text "dcterms_language", default: [], null: false, array: true
    t.text "legacy_dcterms_spatial", default: [], null: false, array: true
    t.text "dcterms_publisher", default: [], null: false, array: true
    t.text "dcterms_rights_holder", default: [], null: false, array: true
    t.text "dcterms_subject", default: [], null: false, array: true
    t.text "dcterms_temporal", default: [], null: false, array: true
    t.text "dcterms_title", default: [], null: false, array: true
    t.text "dcterms_type", default: [], null: false, array: true
    t.text "edm_is_shown_at", default: [], null: false, array: true
    t.text "legacy_dcterms_provenance", default: [], null: false, array: true
    t.text "dcterms_license", default: [], null: false, array: true
    t.integer "items_count", default: 0, null: false
    t.text "dcterms_bibliographic_citation", default: [], null: false, array: true
    t.text "dlg_local_right", default: [], null: false, array: true
    t.text "dlg_subject_personal", default: [], null: false, array: true
    t.string "record_id", default: "", null: false
    t.text "edm_is_shown_by", default: [], null: false, array: true
    t.string "partner_homepage_url"
    t.text "homepage_text"
    t.integer "collection_resources_count", default: 0, null: false
    t.string "sponsor_note"
    t.string "sponsor_image"
    t.text "sensitive_content", default: [], null: false, array: true
    t.index ["repository_id"], name: "index_collections_on_repository_id"
    t.index ["slug"], name: "index_collections_on_slug"
  end

  create_table "collections_holding_institutions", id: false, force: :cascade do |t|
    t.integer "holding_institution_id", null: false
    t.integer "collection_id", null: false
    t.index ["collection_id"], name: "idx_coll_hi_on_coll_id"
    t.index ["holding_institution_id"], name: "idx_coll_hi_on_hi_id"
  end

  create_table "collections_projects", id: false, force: :cascade do |t|
    t.integer "project_id", null: false
    t.integer "collection_id", null: false
  end

  create_table "collections_subjects", id: false, force: :cascade do |t|
    t.integer "collection_id", null: false
    t.integer "subject_id", null: false
    t.index ["collection_id", "subject_id"], name: "index_collections_subjects_on_collection_id_and_subject_id"
    t.index ["subject_id", "collection_id"], name: "index_collections_subjects_on_subject_id_and_collection_id"
  end

  create_table "collections_time_periods", id: false, force: :cascade do |t|
    t.integer "time_period_id", null: false
    t.integer "collection_id", null: false
    t.index ["collection_id", "time_period_id"], name: "idx_col_time_per_on_coll_and_time_per"
    t.index ["time_period_id", "collection_id"], name: "idx_col_time_per_on_time_per_and_coll"
  end

  create_table "collections_users", id: false, force: :cascade do |t|
    t.integer "collection_id", null: false
    t.integer "user_id", null: false
    t.index ["collection_id", "user_id"], name: "index_collections_users_on_collection_id_and_user_id"
    t.index ["user_id", "collection_id"], name: "index_collections_users_on_user_id_and_collection_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "slug", null: false
    t.string "title", default: "", null: false
    t.string "dcterms_temporal", default: [], null: false, array: true
    t.string "description", default: "", null: false
    t.string "canned_search", default: "", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.boolean "public", default: true, null: false
    t.json "legacy_data", default: {}, null: false
    t.string "dcterms_subject_matches", default: [], array: true
    t.string "dlg_subject_personal_matches", default: [], array: true
    t.index ["slug"], name: "index_events_on_slug", unique: true
  end

  create_table "features", id: :serial, force: :cascade do |t|
    t.string "title"
    t.string "title_link"
    t.string "institution"
    t.string "institution_link"
    t.string "short_description"
    t.string "external_link"
    t.boolean "primary"
    t.string "image"
    t.string "area"
    t.string "large_image"
    t.boolean "public", default: false
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
  end

  create_table "fulltext_ingests", id: :serial, force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.string "file"
    t.json "results", default: {}
    t.integer "user_id"
    t.datetime "queued_at", precision: nil
    t.datetime "finished_at", precision: nil
    t.datetime "undone_at", precision: nil
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
  end

  create_table "geographic_locations", force: :cascade do |t|
    t.string "code"
    t.integer "geonames_id"
    t.float "latitude"
    t.float "longitude"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "component_placenames", default: [], array: true
    t.string "us_state"
    t.string "us_county"
    t.boolean "geoname_verified", default: false
    t.string "created_by"
    t.string "updated_by"
    t.index "((to_tsvector('english'::regconfig, COALESCE((code)::text, ''::text)) || to_tsvector('english'::regconfig, COALESCE((geonames_id)::text, ''::text))))", name: "idx_geographic_locations_ft", using: :gin
  end

  create_table "holding_institution_geographic_locations", force: :cascade do |t|
    t.bigint "holding_institution_id"
    t.bigint "geographic_location_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["geographic_location_id"], name: "idx_geo_loc_on_holding_inst_geo_loc"
    t.index ["holding_institution_id"], name: "idx_holding_inst_on_holding_inst_geo_loc"
  end

  create_table "holding_institutions", id: :serial, force: :cascade do |t|
    t.string "authorized_name"
    t.text "short_description"
    t.text "description"
    t.string "image"
    t.string "homepage_url"
    t.string "coordinates"
    t.text "strengths"
    t.text "public_contact_address"
    t.string "institution_type"
    t.boolean "galileo_member"
    t.string "contact_name"
    t.string "contact_email"
    t.string "harvest_strategy"
    t.string "oai_urls", default: [], array: true
    t.text "ignored_collections"
    t.datetime "last_harvested_at", precision: nil
    t.text "analytics_emails", default: [], array: true
    t.text "subgranting"
    t.text "grant_partnerships"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.string "parent_institution"
    t.text "notes"
    t.string "display_name"
    t.string "slug"
    t.boolean "public", default: true
    t.string "public_contact_email"
    t.string "public_contact_phone"
    t.text "training"
    t.text "site_visits"
    t.text "consultations"
    t.text "impact_stories"
    t.text "newspaper_partnerships"
    t.text "committee_participation"
    t.text "other"
    t.string "wikidata_id"
    t.index ["authorized_name"], name: "index_holding_institutions_on_authorized_name"
  end

  create_table "holding_institutions_items", id: false, force: :cascade do |t|
    t.integer "holding_institution_id", null: false
    t.integer "item_id", null: false
    t.index ["holding_institution_id"], name: "idx_item_hi_on_hi_id"
    t.index ["item_id"], name: "idx_item_hi_on_item_id"
  end

  create_table "holding_institutions_repositories", id: false, force: :cascade do |t|
    t.integer "holding_institution_id", null: false
    t.integer "repository_id", null: false
  end

  create_table "item_events", force: :cascade do |t|
    t.bigint "item_id"
    t.bigint "event_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["event_id"], name: "index_item_events_on_event_id"
    t.index ["item_id"], name: "index_item_events_on_item_id"
  end

  create_table "item_geographic_locations", force: :cascade do |t|
    t.bigint "item_id"
    t.bigint "geographic_location_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["geographic_location_id"], name: "index_item_geographic_locations_on_geographic_location_id"
    t.index ["item_id"], name: "index_item_geographic_locations_on_item_id"
  end

  create_table "item_names", force: :cascade do |t|
    t.bigint "item_id"
    t.bigint "name_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["item_id"], name: "index_item_names_on_item_id"
    t.index ["name_id"], name: "index_item_names_on_name_id"
  end

  create_table "item_versions", id: :serial, force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.datetime "created_at", precision: nil
    t.json "object"
    t.index ["item_type", "item_id"], name: "index_item_versions_on_item_type_and_item_id"
  end

  create_table "items", id: :serial, force: :cascade do |t|
    t.integer "collection_id"
    t.boolean "dpla", default: false, null: false
    t.boolean "public", default: false, null: false
    t.text "dc_format", default: [], null: false, array: true
    t.text "dc_right", default: [], null: false, array: true
    t.text "dc_date", default: [], null: false, array: true
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.text "dc_relation", default: [], null: false, array: true
    t.string "slug", null: false
    t.text "dcterms_is_part_of", default: [], null: false, array: true
    t.text "dcterms_contributor", default: [], null: false, array: true
    t.text "dcterms_creator", default: [], null: false, array: true
    t.text "dcterms_description", default: [], null: false, array: true
    t.text "dcterms_extent", default: [], null: false, array: true
    t.text "dcterms_medium", default: [], null: false, array: true
    t.text "dcterms_identifier", default: [], null: false, array: true
    t.text "dcterms_language", default: [], null: false, array: true
    t.text "legacy_dcterms_spatial", default: [], null: false, array: true
    t.text "dcterms_publisher", default: [], null: false, array: true
    t.text "dcterms_rights_holder", default: [], null: false, array: true
    t.text "dcterms_subject", default: [], null: false, array: true
    t.text "dcterms_temporal", default: [], null: false, array: true
    t.text "dcterms_title", default: [], null: false, array: true
    t.text "dcterms_type", default: [], null: false, array: true
    t.text "edm_is_shown_at", default: [], null: false, array: true
    t.text "legacy_dcterms_provenance", default: [], null: false, array: true
    t.integer "other_collections", default: [], array: true
    t.text "dlg_local_right", default: [], null: false, array: true
    t.boolean "valid_item", default: false, null: false
    t.boolean "has_thumbnail", default: false, null: false
    t.text "dcterms_bibliographic_citation", default: [], null: false, array: true
    t.boolean "local", default: false, null: false
    t.text "dlg_subject_personal", default: [], null: false, array: true
    t.string "record_id", default: "", null: false
    t.text "edm_is_shown_by", default: [], null: false, array: true
    t.text "fulltext"
    t.integer "pages_count", default: 0, null: false
    t.string "iiif_partner_url"
    t.boolean "iiif_partner_url_enabled", default: true, null: false
    t.bigint "serial_id"
    t.string "source_data", default: ""
    t.jsonb "external_identifiers", default: [], null: false
    t.index ["collection_id"], name: "index_items_on_collection_id"
    t.index ["dpla"], name: "index_items_on_dpla"
    t.index ["external_identifiers"], name: "index_items_on_external_identifiers", using: :gin
    t.index ["other_collections"], name: "index_items_on_other_collections"
    t.index ["public"], name: "index_items_on_public"
    t.index ["serial_id"], name: "index_items_on_serial_id"
    t.index ["slug"], name: "index_items_on_slug"
    t.index ["valid_item"], name: "index_items_on_valid_item"
  end

  create_table "marc_xml_imports", force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.string "file"
    t.json "results", default: {}
    t.bigint "user_id"
    t.datetime "queued_at", precision: nil
    t.datetime "finished_at", precision: nil
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "original_filename", default: ""
    t.index ["user_id"], name: "index_marc_xml_imports_on_user_id"
  end

  create_table "names", force: :cascade do |t|
    t.string "slug", null: false
    t.string "authoritative_name", default: "", null: false
    t.string "related_names", default: [], null: false, array: true
    t.string "bio_history", default: "", null: false
    t.string "authoritative_name_source", default: [], null: false, array: true
    t.string "source_uri", default: "", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.boolean "public", default: false, null: false
    t.string "oclc_number", default: "", null: false
    t.json "legacy_data", default: {}, null: false
    t.string "name_type", default: "", null: false
    t.string "legacy_slug", default: "", null: false
    t.string "additional_slugs", default: [], null: false, array: true
    t.string "alternate_names_public", default: [], null: false, array: true
    t.string "alternate_names_not_public", default: [], null: false, array: true
    t.index ["additional_slugs"], name: "index_names_on_additional_slugs", using: :gin
    t.index ["slug"], name: "index_names_on_slug", unique: true
  end

  create_table "page_ingests", id: :serial, force: :cascade do |t|
    t.integer "user_id"
    t.string "title"
    t.string "description"
    t.string "file"
    t.json "results_json", default: {}
    t.json "page_json", default: {}
    t.datetime "queued_at", precision: nil
    t.datetime "finished_at", precision: nil
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
  end

  create_table "pages", id: :serial, force: :cascade do |t|
    t.integer "item_id"
    t.text "fulltext"
    t.string "title"
    t.string "number"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "file_type"
    t.string "file"
    t.integer "width", default: 0, null: false
    t.integer "height", default: 0, null: false
    t.float "duration", default: 0.0, null: false
    t.string "media_url", default: "", null: false
    t.string "media_type", default: "image", null: false
    t.string "media_thumbnail_url"
    t.index ["item_id", "number"], name: "index_pages_on_item_id_and_number", unique: true
  end

  create_table "portal_records", id: :serial, force: :cascade do |t|
    t.integer "portal_id"
    t.integer "portable_id"
    t.string "portable_type"
    t.index ["portable_type", "portable_id"], name: "index_portal_records_on_portable_type_and_portable_id"
    t.index ["portal_id"], name: "index_portal_records_on_portal_id"
  end

  create_table "portals", id: :serial, force: :cascade do |t|
    t.string "code"
    t.text "name"
  end

  create_table "preceding_succeeding_serials", force: :cascade do |t|
    t.integer "preceding_serial_id", null: false
    t.integer "succeeding_serial_id", null: false
    t.index ["preceding_serial_id", "succeeding_serial_id"], name: "index_preceding_succeeding_serials_unique", unique: true
    t.index ["preceding_serial_id"], name: "index_preceding_succeeding_serials_on_preceding_serial_id"
    t.index ["succeeding_serial_id"], name: "index_preceding_succeeding_serials_on_succeeding_serial_id"
  end

  create_table "projects", id: :serial, force: :cascade do |t|
    t.string "title"
    t.string "fiscal_year"
    t.string "hosting"
    t.float "storage_used"
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
    t.integer "holding_institution_id"
    t.string "funding_sources", default: [], null: false, array: true
    t.string "notes"
  end

  create_table "remediation_actions", id: :serial, force: :cascade do |t|
    t.string "field"
    t.string "old_text"
    t.string "new_text"
    t.integer "user_id"
    t.integer "items", default: [], array: true
    t.datetime "performed_at", precision: nil
    t.datetime "created_at", precision: nil
    t.datetime "updated_at", precision: nil
  end

  create_table "repositories", id: :serial, force: :cascade do |t|
    t.string "slug", null: false
    t.boolean "public", default: false, null: false
    t.string "title", null: false
    t.text "notes"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "collections_count", default: 0, null: false
    t.string "image"
    t.index ["slug"], name: "index_repositories_on_slug", unique: true
  end

  create_table "repositories_users", id: false, force: :cascade do |t|
    t.integer "repository_id", null: false
    t.integer "user_id", null: false
    t.index ["repository_id", "user_id"], name: "index_repositories_users_on_repository_id_and_user_id"
    t.index ["user_id", "repository_id"], name: "index_repositories_users_on_user_id_and_repository_id"
  end

  create_table "searches", id: :serial, force: :cascade do |t|
    t.text "query_params"
    t.integer "user_id"
    t.string "user_type"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.index ["user_id"], name: "index_searches_on_user_id"
  end

  create_table "serial_geographic_locations", force: :cascade do |t|
    t.bigint "serial_id"
    t.bigint "geographic_location_id"
    t.index ["geographic_location_id"], name: "index_serial_geographic_locations_on_geographic_location_id"
    t.index ["serial_id"], name: "index_serial_geographic_locations_on_serial_id"
  end

  create_table "serials", force: :cascade do |t|
    t.string "slug", null: false
    t.string "dcterms_title", default: [], null: false, array: true
    t.string "dcterms_publisher", default: [], null: false, array: true
    t.string "dcterms_description", default: [], null: false, array: true
    t.string "dcterms_subject", default: [], null: false, array: true
    t.string "dcterms_medium", default: [], null: false, array: true
    t.string "dcterms_contributor", default: [], null: false, array: true
    t.string "dcterms_creator", default: [], null: false, array: true
    t.string "dcterms_subject_fast", default: [], null: false, array: true
    t.string "dc_date", default: [], null: false, array: true
    t.string "dc_right", default: [], null: false, array: true
    t.string "dlg_subject_personal", default: [], null: false, array: true
    t.string "dcterms_language", default: [], null: false, array: true
    t.string "dcterms_type", default: [], null: false, array: true
    t.string "dcterms_frequency", default: [], null: false, array: true
    t.jsonb "external_identifiers", default: [], null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "dcterms_spatial", default: [], null: false, array: true
    t.string "source_data", default: "", null: false
    t.index ["external_identifiers"], name: "index_serials_on_external_identifiers", using: :gin
    t.index ["slug"], name: "index_serials_on_slug", unique: true
  end

  create_table "subjects", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
  end

  create_table "time_periods", id: :serial, force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.datetime "span", precision: nil
    t.integer "start"
    t.integer "finish"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.datetime "remember_created_at", precision: nil
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at", precision: nil
    t.datetime "last_sign_in_at", precision: nil
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.boolean "guest", default: false
    t.integer "creator_id"
    t.string "invitation_token"
    t.datetime "invitation_created_at", precision: nil
    t.datetime "invitation_sent_at", precision: nil
    t.datetime "invitation_accepted_at", precision: nil
    t.integer "invitation_limit"
    t.integer "invited_by_id"
    t.string "invited_by_type"
    t.integer "invitations_count", default: 0
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at", precision: nil
    t.boolean "is_super", default: false, null: false
    t.boolean "is_coordinator", default: false, null: false
    t.boolean "is_committer", default: false, null: false
    t.boolean "is_uploader", default: false, null: false
    t.boolean "is_viewer", default: false, null: false
    t.boolean "is_pm", default: false, null: false
    t.boolean "is_fulltext_ingester", default: false, null: false
    t.boolean "is_page_ingester", default: false, null: false
    t.index ["creator_id"], name: "index_users_on_creator_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invitations_count"], name: "index_users_on_invitations_count"
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "batch_items", "batches"
  add_foreign_key "batch_items", "collections"
  add_foreign_key "collections", "repositories"
  add_foreign_key "items", "collections"
end
