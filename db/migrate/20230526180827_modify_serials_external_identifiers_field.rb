class ModifySerialsExternalIdentifiersField < ActiveRecord::Migration[6.1]
  def up
    change_column :serials, :external_identifiers, :jsonb
    add_index :serials, :external_identifiers, using: :gin
  end

  def down
    remove_index :serials, :external_identifiers
    change_column :serials, :external_identifiers, :json
  end
end
