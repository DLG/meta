class AddOriginalFilenameToMarcXmlImports < ActiveRecord::Migration[6.1]
  def change
    add_column :marc_xml_imports, :original_filename, :string, default: ''
  end
end
