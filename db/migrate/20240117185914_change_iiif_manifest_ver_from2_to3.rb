class ChangeIiifManifestVerFrom2To3 < ActiveRecord::Migration[6.1]
  def up
    items = Item.where('iiif_partner_url ILIKE ?', "%https://iiif.archive.org/iiif/2%")
    items.update_all("iiif_partner_url = REPLACE(iiif_partner_url, 'https://iiif.archive.org/iiif/2', 'https://iiif.archive.org/iiif/3')")
    Sunspot.index! items
  end

  def down
    items = Item.where('iiif_partner_url ILIKE ?', "%https://iiif.archive.org/iiif/3%")
    items.update_all("iiif_partner_url = REPLACE(iiif_partner_url, 'https://iiif.archive.org/iiif/3', 'https://iiif.archive.org/iiif/2')")
    Sunspot.index! items
  end
end
