class AddItemToBatchItem < ActiveRecord::Migration[4.2]
  def change
    change_table :batch_items do |t|
      t.references :item, index: true
    end
  end
end
