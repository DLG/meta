class AddVideoToPages < ActiveRecord::Migration[5.2]
  def up
    add_column :pages, :duration, :float, null: false, default: 0.0
    add_column :pages, :media_url, :string, null: false, default: ''
    add_column :pages, :media_type, :string, null: false, default: 'image'

    Page.reset_column_information

    Page.all.update_all(media_type: 'image')
  end

  def down
    remove_column :pages, :duration, :float, null: false, default: 0.0
    remove_column :pages, :media_url, :string, null: false, default: ''
    remove_column :pages, :media_type, :string, null: false, default: 'image'
  end
end
