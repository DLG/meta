class ChangeItemOtherCollections < ActiveRecord::Migration[4.2]
  def change
    remove_column :items, :other_collections
    add_column :items, :other_collections, :string, array: true, default: []
  end
end
