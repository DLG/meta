class AddOclcNumberToCrdl < ActiveRecord::Migration[5.2]
  def change
    add_column :names, :oclc_number, :string, nil:false, default: ''
    add_column :corporate_entities, :oclc_number, :string, nil:false, default: ''
  end
end
