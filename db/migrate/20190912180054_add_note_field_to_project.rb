# frozen_string_literal: true

# add note field to project
class AddNoteFieldToProject < ActiveRecord::Migration[4.2]
  def change
    add_column :projects, :notes, :string
  end
end
