class AddMediaThumbnailUrlToPages < ActiveRecord::Migration[5.2]
  def change
    add_column :pages, :media_thumbnail_url, :string
  end
end
