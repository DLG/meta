class AddMarcXmlImport < ActiveRecord::Migration[6.1]
  def change
    create_table :marc_xml_imports do |t|
      t.string :title
      t.string :description
      t.string :file
      t.json :results, default: {}
      t.references :user
      t.datetime :queued_at
      t.datetime :finished_at
      t.timestamps
    end
  end
end
