class ChangeSerials < ActiveRecord::Migration[6.1]
  def change
    add_column :serials, :dcterms_spatial, :string, array:true, null:false, default:[]
    rename_column :serials, :dcterms_identifiers, :dcterms_identifier
  end
end
