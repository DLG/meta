
# Plan:
# 1. put current `slug` field value into `legacy_slug` field
# 2. put authoritative_name-generated slug into `slug` field
# 3. put ONLY additional slugs (not legacy_slug and not authoritative_name-generated slug) into `additional_slugs` field

# Note:
# The `normalized` method creates a string that only contains [_a-z0-9]+
# The tiebreaker logic on the other hand includes a hyphen '-'
# By using hyphen, we hope to avoid collisions with slugs for names that end in [0-9]

class AddLegacySlugChangeAllSlugsToAdditionalSlugs < ActiveRecord::Migration[5.2]

  def up
    remove_column :names, :all_slugs

    add_column    :names, :legacy_slug,      :string,              null: false, default: ''
    add_column    :names, :additional_slugs, :string, array: true, null: false, default: []
    add_index     :names, :additional_slugs, using: :gin

    Name.all.each do |name|
      name.legacy_slug = name.slug  # 1. above

      new_slug = normalized(name.authoritative_name)
      @seen ||= Hash.new( 0 )

      # add tiebreaker to slug value, if needed
      if needs_tiebreaker?(name.id)
        new_slug += "-1"
      else
        suffix = @seen[new_slug]
        @seen[new_slug] += 1
        new_slug += "-#{suffix}" if suffix > 0
      end

      name.slug = new_slug  # 2. above
      name.save
    end

    # report @seen-initiated tiebreakers out of curiosity (expect none)
    @seen.each_key do |key|
      puts key if key =~ /-[0-9]+$/
    end

    # match some currently used slugs from crdl (ultimate) production
    name = Name.find(19445)
    name.additional_slugs = ['castle_doris_jean_d_1997']  # 3. above
    name.save

    name = Name.find(24547)
    name.additional_slugs = ['mett_frederick_p_1910_1972']
    name.save

    name = Name.find(25190)
    name.additional_slugs = ['o_neal_john_1940']
    name.save
  end

  def down

    # return legacy_slug value to slug field
    Name.all.each do |name|
      name.slug = name.legacy_slug
      name.save
    end

    remove_column :names, :legacy_slug
    remove_column :names, :additional_slugs

    # empty column, so rollback and re-migrate will work
    add_column :names, :all_slugs, :string, array: true, null: false, default: []
  end

  def normalized(name)
    name.gsub(/[[:punct:]]/, ' ') # change punctuations to spaces
        .gsub(/[^ -~]/,      ' ') # change non-ascii to spaces
        .strip                    # strip leading/trailing spaces
        .gsub(/  */,         ' ') # collapse multiple spaces
        .gsub(/ /,           '_') # change spaces to underscores
        .downcase                 # lowercase
  end

  # see 20220215160739_add_other_slugs_to_name.rb for why these ids where chosen
  def needs_tiebreaker?(id)
    [17873,19426,19644,19654,19691,
     19968,20148,20518,20674,21003,
     21033,21900,22406,23510,24765,
     24969,25293,26548,27852,28166,
     28243].include? id
  end
end
