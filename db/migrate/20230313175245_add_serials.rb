class AddSerials < ActiveRecord::Migration[6.1]
  def change
    create_table :serials do |t|
      t.string :slug, null: false, uniqueness:true
      t.index   :slug, unique: true
      t.string :dcterms_title, array:true, null:false, default:[]
      t.string :dcterms_identifiers, array:true, null:false, default:[]
      t.string :dcterms_publisher, array:true, null:false, default:[]
      t.string :dcterms_description, array:true, null:false, default:[]
      t.string :dcterms_subject, array:true, null:false, default:[]
      t.string :dcterms_medium, array:true, null:false, default:[]
      t.string :dcterms_replaces, array:true, null:false, default:[]
      t.string :dcterms_is_replaced_by, array:true, null:false, default:[]
      t.string :dcterms_contributor, array:true, null:false, default:[]
      t.string :dcterms_creator, array:true, null:false, default:[]
      t.string :dcterms_subject_fast, array:true, null:false, default:[]
      t.string :dc_date, array:true, null:false, default:[]
      t.string :dc_right, array:true, null:false, default:[]
      t.string :dlg_subject_personal, array:true, null:false, default:[]
      t.string :dcterms_language, array:true, null:false, default:[]
      t.string :dcterms_type, array:true, null:false, default:[]
      t.string :dcterms_frequency, array:true, null:false, default:[]
      t.json :external_identifiers

      t.timestamps
    end

    change_table :items do |t|
      t.belongs_to  :serial,    null: true
    end

    change_table :batch_items do |t|
      t.belongs_to  :serial,    null: true
    end

    create_table :serial_geographic_locations do |t|
      t.belongs_to :serial, index: true
      t.belongs_to :geographic_location, index: true
    end
  end
end
