class RemoveNotNullConstraintOnBatchItem < ActiveRecord::Migration[4.2]
  def change
    change_column_null :batch_items, :collection_id, true
  end
end
