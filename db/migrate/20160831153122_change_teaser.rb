class ChangeTeaser < ActiveRecord::Migration[4.2]
  def change

    change_table :collections do |t|
      t.remove :teaser
    end

    change_table :repositories do |t|
      t.remove :teaser
      t.boolean :teaser
    end

  end
end
