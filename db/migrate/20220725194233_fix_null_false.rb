class FixNullFalse < ActiveRecord::Migration[5.2]

  def change
    Name.where(oclc_number: nil).update_all( "oclc_number = ''" )

    change_column_null(:names, :oclc_number, false)
    change_column_null(:names, :legacy_data, false)
    change_column_null(:events, :legacy_data, false)
  end
end
