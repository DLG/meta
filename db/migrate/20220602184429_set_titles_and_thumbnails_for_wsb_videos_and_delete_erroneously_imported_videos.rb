MEDIA_URLS_TO_DELETE = %w[https://dlg.usg.edu/news/mp4/wsbn19869.mp4 https://dlg.usg.edu/news/mp4/wsbn29157.mp4
                          https://dlg.usg.edu/news/mp4/wsbn29253.mp4 https://dlg.usg.edu/news/mp4/wsbn42598.mp4
                          https://dlg.usg.edu/news/mp4/wsbn43176.mp4 https://dlg.usg.edu/news/mp4/wsbn45232.mp4
                          https://dlg.usg.edu/news/mp4/wsbn46073.mp4 https://dlg.usg.edu/news/mp4/wsbn49730.mp4
                          https://dlg.usg.edu/news/mp4/wsbn56944.mp4 https://dlg.usg.edu/news/mp4/wsbn57077.mp4
                          https://dlg.usg.edu/news/mp4/wsbn60448.mp4]

class SetTitlesAndThumbnailsForWsbVideosAndDeleteErroneouslyImportedVideos < ActiveRecord::Migration[5.2]
  def up
    wsb = Collection.find_by_record_id 'ugabma_wsbn'
    wsb_items = wsb.items.for_indexing
    wsb_items.each do |item|
      page_number = 0
      item.pages.to_a.each do |page|
        if MEDIA_URLS_TO_DELETE.include? page.media_url
          item.pages.destroy page
          next
        end
        page_number += 1
        page.number = page_number
        if /wsbn\d+-([a-zA-Z0-9]+)(-broll)?\.mp4$/ =~ page.media_url
          segment = $1
          is_b_roll = $2.present?
          page.title = "Segment #{segment}#{' B-Roll' if is_b_roll}"
        elsif item.pages.count == 1 # this should always be true for the else case, but just making sure
          page.title = 'Segment 1'
        end
        if /(wsbn[a-zA-Z0-9\-]+)\.mp4$/ =~ page.media_url # this should match any WSB video, but, again, just making sure
          thumb_url = "https://dlg.usg.edu/thumbnails/ugabma/wsbn/ugabma_wsbn_#{$1}.jpg"
          thumb_response = HTTParty.get(thumb_url)
          thumb_exists = thumb_response.code == 200 && thumb_response.content_type&.start_with?('image/')
          page.media_thumbnail_url = thumb_url if thumb_exists
        end
        page.save!
      end
    end
    Sunspot.index! wsb_items
  end

  def down
    wsb = Collection.find_by_record_id 'ugabma_wsbn'
    wsb_items = wsb.items.for_indexing
    wsb_items.each do |item|
      item.pages.each do |page|
        page.title = nil
        page.media_thumbnail_url = nil
        page.save!
      end
    end
    Sunspot.index! wsb_items
  end
end
