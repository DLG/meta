# add timestamps to Feature model
class AddTimestampsToFeature < ActiveRecord::Migration[4.2]
  def change
    change_table :features, &:timestamps
  end
end
