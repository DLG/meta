class AddDplaToRepository < ActiveRecord::Migration[4.2]
  def change
    change_table :repositories do |t|
      t.boolean :dpla, null: false, default: false, index: true
    end
  end
end
