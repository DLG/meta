class ModifyArrayLowercaseFunction < ActiveRecord::Migration[5.2]
  def up
    execute <<~SQL
      CREATE OR REPLACE FUNCTION public.array_lowercase(text[])
       RETURNS text[]
       LANGUAGE plpgsql
       IMMUTABLE STRICT
      AS $function$
      DECLARE
      arrStrings ALIAS FOR $1;
      retVal text[];
      BEGIN
        FOR I IN COALESCE(array_lower(arrStrings, 1), 0)..COALESCE(array_upper(arrStrings, 1), 0) LOOP
        retVal[I] := lower(arrStrings[I]);
        END LOOP;
        RETURN retVal;
        END;
        $function$
    SQL
  end
  def down
    # This is the version of this function as it existed in our database as of 2022-01-25. Its origin is unknown though.
    execute <<~SQL
      CREATE OR REPLACE FUNCTION public.array_lowercase(text[]) RETURNS text[]
        LANGUAGE plpgsql STABLE STRICT
        AS $_$
        DECLARE
            arrStrings ALIAS FOR $1;
            retVal text[];
        BEGIN
            FOR I IN array_lower(arrStrings, 1)..array_upper(arrStrings, 1) LOOP
                    retVal[I] := lower(arrStrings[I]);
                END LOOP;
            RETURN retVal;
        END;
        $_$;
    SQL
  end
end
