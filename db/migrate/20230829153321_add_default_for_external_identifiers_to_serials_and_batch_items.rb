class AddDefaultForExternalIdentifiersToSerialsAndBatchItems < ActiveRecord::Migration[6.1]
  def change
    # serials
    change_column_default :serials, :external_identifiers, from: nil, to: []
    reversible do |direction|
      direction.up { Serial.where(external_identifiers: nil).update_all(external_identifiers: []) }
    end
    change_column_null :serials, :external_identifiers, false

    # batch items
    change_column_default :batch_items, :external_identifiers, from: nil, to: []
    reversible do |direction|
      direction.up { BatchItem.where(external_identifiers: nil).update_all(external_identifiers: []) }
    end
    change_column_null :batch_items, :external_identifiers, false
  end
end
