class AddUpdatedCountToBatchImport < ActiveRecord::Migration[4.2]
  def change
    change_table :batch_imports do |t|
      t.integer :updated
    end
  end
end
