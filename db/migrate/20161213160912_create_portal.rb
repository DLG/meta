class CreatePortal < ActiveRecord::Migration[4.2]
  def change
    create_table :portals do |t|
      t.string :code
      t.text :name
    end
  end
end
