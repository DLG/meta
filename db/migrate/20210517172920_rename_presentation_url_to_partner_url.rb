class RenamePresentationUrlToPartnerUrl < ActiveRecord::Migration[5.2]
  def change
    rename_column :items, :iiif_presentation_url, :iiif_partner_url
    rename_column :items, :iiif_presentation_url_enabled, :iiif_partner_url_enabled
    rename_column :batch_items, :iiif_presentation_url, :iiif_partner_url
    rename_column :batch_items, :iiif_presentation_url_enabled, :iiif_partner_url_enabled
  end
end
