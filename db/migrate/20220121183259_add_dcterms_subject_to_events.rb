class AddDctermsSubjectToEvents < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :dcterms_subject_matches,      :string, array: true, default: []
    add_column :events, :dlg_subject_personal_matches, :string, array: true, default: []

    # populate subject fields from canned_search
    Event.all.each do |event|
      cs = event.canned_search

      # _su: "Freedom ride*"
      if cs =~ /^_su: "([^"]+)"$/
        event.dcterms_subject_matches      << $1

      # _sp: "King, Martin Luther, Jr., 1929-1968--Assassination"
      elsif cs =~ /^_sp: "([^"]+)"$/
        event.dlg_subject_personal_matches << $1

      # _su: "University of Alabama" or "College integration--Alabama--Tuscaloosa"
      elsif cs =~ /^_su: "([^"]+)" or "([^"]+)"$/
        event.dcterms_subject_matches      << $1
        event.dcterms_subject_matches      << $2

      # _sp: "Till, Emmett, 1941-1955--Assassination" or "Till, Emmett, 1941-1955--Death and burial"
      elsif cs =~ /^_sp: "([^"]+)" or "([^"]+)"$/
        event.dlg_subject_personal_matches << $1
        event.dlg_subject_personal_matches << $2

      # _su: "Brown versus Board of Education of Topeka*" or _sp: "Brown, Oliver, 1918*"
      elsif cs =~ /^_su: "([^"]+)" or _sp: "([^"]+)"$/
        event.dcterms_subject_matches << $1
        event.dlg_subject_personal_matches << $2

      # _sp: "King, Martin Luther, Jr., 1929-1968" or "King, Martin Luther, Jr., 1929-1968--Awards" or _su: "Nobel Prizes"
      elsif cs =~ /^_sp: "([^"]+)" or "([^"]+)" or _su: "([^"]+)"$/
        event.dlg_subject_personal_matches << $1
        event.dlg_subject_personal_matches << $2
        event.dcterms_subject_matches      << $3
      end

      event.save
    end
  end
end
