class RemoveSerialDcTermsIdentifier < ActiveRecord::Migration[6.1]
  def change
    remove_column :serials, :dcterms_identifier, :string, default: [], null: false, array: true
  end
end
