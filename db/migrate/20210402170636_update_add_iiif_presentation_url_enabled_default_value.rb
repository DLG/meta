class UpdateAddIiifPresentationUrlEnabledDefaultValue < ActiveRecord::Migration[5.2]
  def up
    change_column :items, :iiif_presentation_url_enabled, :boolean, default: true, null: false
    change_column :batch_items, :iiif_presentation_url_enabled, :boolean, default: true, null: false
    Item.update_all iiif_presentation_url_enabled: true
    BatchItem.update_all iiif_presentation_url_enabled: true
  end

  def down
    change_column :items, :iiif_presentation_url_enabled, :boolean, default: false , null: false
    change_column :batch_items, :iiif_presentation_url_enabled, :boolean, default: false, null: false
    Item.update_all iiif_presentation_url_enabled: false
    BatchItem.update_all iiif_presentation_url_enabled: false
  end
end
