class CreateAlternateNames < ActiveRecord::Migration[5.2]
  def change
    create_table :alternate_names do |t|
      t.belongs_to :name, index: true
      t.string :alt_name, null: false, default: ''
      t.boolean :public, null: false, default: false
    end
  end
end
