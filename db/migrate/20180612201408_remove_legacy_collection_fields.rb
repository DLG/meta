# frozen_string_literal: true

# remove some old legacy fields from Collection
class RemoveLegacyCollectionFields < ActiveRecord::Migration[4.2]
  def change
    change_table :collections do |t|
      t.remove :in_georgia
      t.remove :color
    end
  end
end
