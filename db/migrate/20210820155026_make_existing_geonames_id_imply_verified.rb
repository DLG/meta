class MakeExistingGeonamesIdImplyVerified < ActiveRecord::Migration[5.2]
  def change
    GeographicLocation.where.not(geonames_id: nil).update_all geoname_verified: true
  end
end
