class BatchitemAddHasThumbnail < ActiveRecord::Migration[4.2]
  def change
    add_column :batch_items, :has_thumbnail, :boolean, default: false, null: false
  end
end
