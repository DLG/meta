class ChangePublicOnCrdlTables < ActiveRecord::Migration[5.2]
  def change
    remove_column :names, :public, :crdl_public
    remove_column :corporate_entities, :public, :crdl_public

    add_column  :names, :public, :boolean, null: false, default: false
    add_column  :corporate_entities, :public, :boolean, null: false, default: false
    add_column  :events, :public, :boolean, null: false, default: true
  end
end
