class CreateItemVersion < ActiveRecord::Migration[4.2]
  def change
    rename_table :versions, :item_versions
  end
end
