class AddDimensionsToPages < ActiveRecord::Migration[5.2]
  def change
    add_column :pages, :width, :integer, null: false, default: 0
    add_column :pages, :height, :integer, null: false, default: 0
  end
end
