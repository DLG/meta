class AddIiifManifestUrlToItem < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :iiif_presentation_url, :string
    add_column :batch_items, :iiif_presentation_url, :string
  end
end
