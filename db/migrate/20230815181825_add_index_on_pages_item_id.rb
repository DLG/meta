class AddIndexOnPagesItemId < ActiveRecord::Migration[6.1]
  def change
    add_index :pages, [:item_id, :number], unique: true
  end
end
