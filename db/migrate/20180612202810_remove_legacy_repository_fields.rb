# frozen_string_literal: true

# remove some old legacy fields from Repo
class RemoveLegacyRepositoryFields < ActiveRecord::Migration[4.2]
  def change
    change_table :repositories do |t|
      t.remove :color
      t.remove :in_georgia
    end
  end
end
