class UpdateExistingFeaturesToGeorgiaPortal < ActiveRecord::Migration[5.2]
  def change
    ga_portal = Portal.where(code: 'georgia')
    Feature.all.each { |feature| feature.portals = ga_portal }
  end
end
