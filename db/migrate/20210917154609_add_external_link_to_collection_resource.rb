class AddExternalLinkToCollectionResource < ActiveRecord::Migration[5.2]
  def change
    add_column :collection_resources, :use_external_link, :boolean, null: false, default: false
    add_column :collection_resources, :external_link, :string, null: false, default: ''
  end
end
