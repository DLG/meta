class RenameBatchImportFields < ActiveRecord::Migration[4.2]
  def change
    change_table :batch_imports do |t|
      t.rename :errors, :failed
      t.rename :type, :format
    end
  end
end
