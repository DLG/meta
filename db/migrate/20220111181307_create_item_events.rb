class CreateItemEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :item_events do |t|
      t.references :item, index: true
      t.references :event, index: true

      t.timestamps
    end
  end
end
