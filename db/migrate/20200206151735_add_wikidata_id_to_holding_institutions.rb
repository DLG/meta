class AddWikidataIdToHoldingInstitutions < ActiveRecord::Migration[4.2]
  def change
    add_column :holding_institutions, :wikidata_id, :string
  end
end
