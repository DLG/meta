# add integer field to Item to cache number of associated pages
class AddPageCountToItem < ActiveRecord::Migration[4.2]
  def change
    add_column :items, :pages_count, :integer
  end
end
