class AddPrecedingSucceedingTitlesJoinTable < ActiveRecord::Migration[6.1]
  def change
    create_table :preceding_succeeding_serials do |t|
      t.integer :preceding_serial_id, null: false, index: true
      t.integer :succeeding_serial_id, null: false, index: true
    end
    add_index :preceding_succeeding_serials, [:preceding_serial_id, :succeeding_serial_id], unique: true,
              name: 'index_preceding_succeeding_serials_unique'
  end
end
