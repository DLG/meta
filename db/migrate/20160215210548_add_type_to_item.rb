class AddTypeToItem < ActiveRecord::Migration[4.2]
  def change

    change_table :items do |t|

      t.string :type

    end

  end
end
