class CreateItemNames < ActiveRecord::Migration[5.2]
  def change
    create_table :item_names do |t|
      t.references :item, index: true
      t.references :name, index: true

      t.timestamps
    end
  end
end
