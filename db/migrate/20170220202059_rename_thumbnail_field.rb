class RenameThumbnailField < ActiveRecord::Migration[4.2]
  def change

    change_table :repositories do |t|
      t.rename :thumbnail_url, :thumbnail_path
    end

  end
end
