class AddIiifPresentationUrlEnabledToItem < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :iiif_presentation_url_enabled, :boolean, default: false
    add_column :batch_items, :iiif_presentation_url_enabled, :boolean, default: false
  end
end
