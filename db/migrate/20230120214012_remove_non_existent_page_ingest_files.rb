class RemoveNonExistentPageIngestFiles < ActiveRecord::Migration[6.0]
  def up
    PageIngest.find_each do |pi|
      unless pi.file.path.present? && File.exists?(pi.file.path)
        pi.file = nil
        pi.save
      end
    end
  end

  def down
    PageIngest.where(file: nil).update_all file: 'data.json'
  end
end
