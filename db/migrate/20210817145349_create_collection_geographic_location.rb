class CreateCollectionGeographicLocation < ActiveRecord::Migration[5.2]
  def change
    create_table :collection_geographic_locations do |t|
      t.belongs_to :collection, index: true
      t.belongs_to :geographic_location, index: true
    end
  end
end
