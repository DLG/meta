class ChangeVersionFieldToJson < ActiveRecord::Migration[4.2]
  def change
    change_table :item_versions do |t|
      t.remove :object
      t.json :object
    end
  end
end
