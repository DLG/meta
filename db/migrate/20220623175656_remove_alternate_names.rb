class RemoveAlternateNames < ActiveRecord::Migration[5.2]
  def change
    drop_table :alternate_names
  end
end
