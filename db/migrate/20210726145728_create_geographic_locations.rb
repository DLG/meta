class CreateGeographicLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :geographic_locations do |t|
      t.string :code
      t.integer :geonames_id
      t.float :latitude
      t.float :longitude

      t.timestamps
    end
  end
end
