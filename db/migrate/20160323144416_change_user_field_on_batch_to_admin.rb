class ChangeUserFieldOnBatchToAdmin < ActiveRecord::Migration[4.2]
  def change
    change_table :batches do |t|
      t.remove :user_id
      t.references :admin
      t.index :admin_id
    end
  end
end
