class AddAlternateNameArrays < ActiveRecord::Migration[5.2]

  # UP ---------------------------------------------------------------
  def up
    add_column :names, :alternate_names_public,     :string, array: true, null: false, default: []
    add_column :names, :alternate_names_not_public, :string, array: true, null: false, default: []

    Name.reset_column_information

    AlternateName.includes(:name).all.each do |an|
      if an.public
        an.name.alternate_names_public     << an.alt_name
      else
        an.name.alternate_names_not_public << an.alt_name
      end
      an.name.save(:validate => false)
    end
  end

  # DOWN -------------------------------------------------------------
  def down
    remove_column :names, :alternate_names_public
    remove_column :names, :alternate_names_not_public
  end

end
