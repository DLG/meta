class AddGeographicLocationsToBatchItems < ActiveRecord::Migration[5.2]
  def change
    create_table :batch_item_geographic_locations do |t|
      t.belongs_to :batch_item, index: true
      t.belongs_to :geographic_location, index: true
    end
  end
end
