class RemoveSerialDcTermsReplacesAndReplacedBy < ActiveRecord::Migration[6.1]
  def change
    remove_column :serials, :dcterms_replaces, array:true, null:false, default:[]
    remove_column :serials, :dcterms_is_replaced_by, array:true, null:false, default:[]
  end
end
