class RemoveCountFieldsFromBatchImport < ActiveRecord::Migration[4.2]
  def change
    change_table :batch_imports do |t|
      t.remove :added
      t.remove :updated
      t.remove :failed
    end
  end
end
