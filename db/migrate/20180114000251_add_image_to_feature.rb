class AddImageToFeature < ActiveRecord::Migration[4.2]
  def change
    add_column :features, :image, :string
  end
end
