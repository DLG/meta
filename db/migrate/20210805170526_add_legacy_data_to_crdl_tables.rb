class AddLegacyDataToCrdlTables < ActiveRecord::Migration[5.2]
  def change
    add_column :names, :legacy_data, :json, nil: false, default: {}
    add_column :corporate_entities, :legacy_data, :json, nil: false, default: {}
    add_column :events, :legacy_data, :json, nil: false, default: {}
  end
end
