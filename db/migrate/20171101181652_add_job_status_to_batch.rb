class AddJobStatusToBatch < ActiveRecord::Migration[4.2]
  def change
    add_column :batches, :job_message, :string
  end
end
