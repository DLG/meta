class AddSensitiveDataToCollections < ActiveRecord::Migration[5.2]
  def change
    change_table :collections do |t|
      t.text    :sensitive_content, array: true, null: false, default: []
    end
  end
end
