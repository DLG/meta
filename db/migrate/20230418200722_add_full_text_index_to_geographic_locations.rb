class AddFullTextIndexToGeographicLocations < ActiveRecord::Migration[6.1]
  def up
    execute <<SQL
      CREATE INDEX idx_geographic_locations_ft
          ON geographic_locations USING GIN ((to_tsvector('english',
                                                          COALESCE("geographic_locations"."code"::text, '')) ||
                                              to_tsvector('english',
                                                          COALESCE("geographic_locations"."geonames_id"::text, ''))));
SQL
  end

  def down
    execute 'DROP INDEX IF EXISTS idx_geographic_locations_ft'
  end
end
