# support file uploads to Page

class AddFileToPage < ActiveRecord::Migration[4.2]
  def change
    change_table :pages do |t|
      t.string :file
    end
  end
end
