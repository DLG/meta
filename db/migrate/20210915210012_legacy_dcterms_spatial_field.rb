class LegacyDctermsSpatialField < ActiveRecord::Migration[5.2]
  def change
    rename_column :items, :dcterms_spatial, :legacy_dcterms_spatial
    rename_column :batch_items, :dcterms_spatial, :legacy_dcterms_spatial
    rename_column :collections, :dcterms_spatial, :legacy_dcterms_spatial
  end
end
