class AddExternalIdentifiersToItem < ActiveRecord::Migration[6.1]
  def up
    add_column :items, :source_data, :string,  default: ''
    add_column :items, :external_identifiers, :jsonb, null:false, default: []
    add_index :items, :external_identifiers, using: :gin

    add_column :batch_items, :source_data, :string, default: ''
    add_column :batch_items, :external_identifiers, :jsonb
    add_index :batch_items, :external_identifiers, using: :gin
  end

  def down
    remove_column :items, :source_data, :string, default: ''
    remove_index :items, :external_identifiers
    remove_column :items, :external_identifiers, :jsonb, null:false, default: []

    remove_column :batch_items, :source_data, :string, default: ''
    remove_index :batch_items, :external_identifiers
    remove_column :batch_items, :external_identifiers, :jsonb, null:false, default: {}
  end
end
