class DropDcIdentifier < ActiveRecord::Migration[4.2]
  def change
    change_table :items do |t|
      t.remove :dc_identifier
    end
    change_table :batch_items do |t|
      t.remove :dc_identifier
    end
    change_table :collections do |t|
      t.remove :dc_identifier
    end
  end
end
