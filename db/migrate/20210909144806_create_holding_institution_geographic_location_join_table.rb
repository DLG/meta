class CreateHoldingInstitutionGeographicLocationJoinTable < ActiveRecord::Migration[5.2]
  def change
    create_table :holding_institution_geographic_locations do |t|
      t.belongs_to :holding_institution, index: { name:  'idx_holding_inst_on_holding_inst_geo_loc'}
      t.belongs_to :geographic_location, index: { name:  'idx_geo_loc_on_holding_inst_geo_loc'}
      t.timestamps
    end
  end
end
