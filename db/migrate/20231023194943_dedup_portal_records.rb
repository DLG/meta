class DedupPortalRecords < ActiveRecord::Migration[6.1]
  def up
    # Removed orphaned PortalRecords
    PortalRecord.where(portable_type: 'BatchItem').joins(Arel.sql("left join batch_items on batch_items.id = portable_id")).where(batch_items: {id: nil}).delete_all

    # Dedup portal_records
    execute <<-SQL
      delete from portal_records
      where id in (
        select x.id
        from portal_records x
        inner join portal_records y
          on
            x.portable_type = y.portable_type and
            x.portable_id = y.portable_id and
            x.portal_id = y.portal_id and
            x.id > y.id
        )
    SQL

  end

  def down
  end
end
