class AddViewerRoleToUser < ActiveRecord::Migration[4.2]
  def change
    add_column :users, :is_viewer, :boolean, null: false, default: false
  end
end
