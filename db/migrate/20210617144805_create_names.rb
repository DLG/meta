class CreateNames < ActiveRecord::Migration[5.2]
  def change
    create_table :names do |t|
      t.string :slug, null: false, uniqueness:true
      t.index   :slug, unique: true

      t.string :authoritative_name, null:false, default:''
      t.string :alternate_names, array:true, null:false, default:[]
      t.string :related_names, array:true, null:false, default:[]
      t.string :bio_history, null:false, default:''
      t.string :authoritative_name_source, array:true, null:false, default:[]
      t.string :source_uri, null:false, default:''
      t.string :public, null:false, default:''

      t.timestamps
    end
  end
end
