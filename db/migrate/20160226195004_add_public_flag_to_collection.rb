class AddPublicFlagToCollection < ActiveRecord::Migration[4.2]
  def change

    change_table :collections do |t|
      t.boolean :public,        null: false, default: false, index: true
    end
  end
end
