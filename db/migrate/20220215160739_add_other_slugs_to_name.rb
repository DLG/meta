class AddOtherSlugsToName < ActiveRecord::Migration[5.2]

  def up
    add_column :names, :all_slugs, :string, array: true, null: false, default: []
    add_index :names, :all_slugs, using: :gin

    Name.all.each do |name|
      @seen ||= Hash.new( 0 )
      new_slug = normalized(name.authoritative_name)
      if needs_tiebreaker?(name.id)
        new_slug += "-1"
      else
        suffix = @seen[new_slug]
        @seen[new_slug] += 1
        new_slug += "-#{suffix}" if suffix > 0
      end
      name.all_slugs = [new_slug, name.slug].uniq
      name.save
    end
    return

    # report @seen-initiated tiebreakers out of curiosity
    @seen.each_key do |key|
      puts key if key =~ /-[0-9]+$/
    end

    # match some legacy slugs from crdl (ultimate) production
    name = Name.find(19445)
    name.all_slugs = [name.all_slugs, 'castle_doris_jean_d_1997'].uniq
    name.save

    name = Name.find(24547)
    name.all_slugs = [name.all_slugs, 'mett_frederick_p_1910_1972'].uniq
    name.save

    name = Name.find(25190)
    name.all_slugs = [name.all_slugs, 'o_neal_john_1940'].uniq
    name.save
  end

  def down
    remove_column :names, :all_slugs
  end

  def normalized(name)
    name.gsub(/[[:punct:]]/, ' ') # change punctuations to spaces
        .gsub(/[^ -~]/,      ' ') # change non-ascii to spaces
        .strip                    # strip leading/trailing spaces
        .gsub(/  */,         ' ') # collapse multiple spaces
        .gsub(/ /,           '_') # change spaces to underscores
        .downcase                 # lowercase
  end

  # see below __END__ for explanation of these id choices
  def needs_tiebreaker?(id)
    [17873,19426,19644,19654,19691,
     19968,20148,20518,20674,21003,
     21033,21900,22406,23510,24765,
     24969,25293,26548,27852,28166,
     28243].include? id
  end
end

__END__
Research:
  - (looking to see which duplicate names show in old production crdl)
  - "(wait)" means neither showed when I checked, but one might show soon

Allen, J. R. (James( R.), 1930-1973
(wait)
https://dlgadmin.galileo.usg.edu/names/17872
https://dlgadmin.galileo.usg.edu/names/17873

Carver, George Washington, 1864?-1943
Biography:
Contemporary Black Biography, Volume 4. Gale Research, 1993. "George Washington Carver devoted his life to research projects ...
https://dlgadmin.galileo.usg.edu/names/19425 <===
https://dlgadmin.galileo.usg.edu/names/19426

Clark, Ramsey, 1927-
Biography:
Wikipedia 26 Oct. 2012: "William Ramsey Clark (born December 18, 1927) is an American lawyer, activist and former public official...
https://dlgadmin.galileo.usg.edu/names/19643 <===
https://dlgadmin.galileo.usg.edu/names/19644

Clark, Velisa
(wait)
https://dlgadmin.galileo.usg.edu/names/19653
https://dlgadmin.galileo.usg.edu/names/19654

Clayton, Xernona
Biography:
Atlanta-based civil rights leader and broadcasting executive. Prior to coming to Atlanta to work as an event organizer for the Southern Christian Leadership Conference (SCLC) in 1965, Clayton conducted undercover employment discrimination investigations for the Chicago Urban League.
https://dlgadmin.galileo.usg.edu/names/19692 <===
https://dlgadmin.galileo.usg.edu/names/19691

Cotton, Dorothy F., 1930-2018
Biography:
Dorothy Lee Foreman was born in Goldsboro, North Carolina. After her mother's death when she was only three years old, ... Cotton passed away on June 10, 2018. From: http://www.dorothycotton.com/html/honorary_degree.html
https://dlgadmin.galileo.usg.edu/names/19969 <===
https://dlgadmin.galileo.usg.edu/names/19968

Dabney, Virginius, 1901-1995
Biography:
Journalist, editor of the Richmond Times-Dispatch (1936–1969), writer, historian, and recipient of the Pulitzer Prize for editorial writing.
https://dlgadmin.galileo.usg.edu/names/20149 <===
https://dlgadmin.galileo.usg.edu/names/20148

Douglass, Frederick, 1818-1895
Biography:
http://memory.loc.gov/ammem/doughtml/doughome [accessed 2 November, 2012]. Nineteenth-century African-American abolitionist ...
https://dlgadmin.galileo.usg.edu/names/20519 <===
https://dlgadmin.galileo.usg.edu/names/20518

East, Percy Dale
(wait)
https://dlgadmin.galileo.usg.edu/names/20673
https://dlgadmin.galileo.usg.edu/names/20674

Fisher, Dorothy Canfield, 1879-1958
Biography:
Wikipedia 26 Oct. 2012: "Educational reformer, social activist, and best-selling American author ...
https://dlgadmin.galileo.usg.edu/names/21002 <===
https://dlgadmin.galileo.usg.edu/names/21003

Flory, Ishmael
(wait)
https://dlgadmin.galileo.usg.edu/names/21034
https://dlgadmin.galileo.usg.edu/names/21033

Harden, William Preston
NOT CRDL (not public)
https://dlgadmin.galileo.usg.edu/names/21899
https://dlgadmin.galileo.usg.edu/names/21900

Holton, A. Linwood (Abner Linwood), 1923-
Biography:
Virginia Union University professor, pastor of Moore Street Baptist church in Richmond, and a civil rights activist. He co-founded the Richmond chapter of the Urban League and wrote newspaper columns for the Associated Negro Press. He worked with the Virginia Interracial Commission and the Southern Regional Council.
https://dlgadmin.galileo.usg.edu/names/22406
https://dlgadmin.galileo.usg.edu/names/22407 <===

Lawrence, John
Biography:
African American high school senior at Lincoln High School, Milwaukee, Wisconsin, in 1967.
https://dlgadmin.galileo.usg.edu/names/23510
https://dlgadmin.galileo.usg.edu/names/23509 <===

Moore, Frank
Biography:
Frank Moore was investigated for his involvement in civil rights demonstrations and a boycott in Edwards, Mississippi by the Mississippi Sovereignty Commission.
https://dlgadmin.galileo.usg.edu/names/24766 <===
https://dlgadmin.galileo.usg.edu/names/24765

Murrow, Edward R.
Biography:
http://www.museum.tv/eotvsection.php?entrycode=murrowedwar [accessed 31 Oct. 2012] "(Egbert Roscoe Murrow). Born in ...
https://dlgadmin.galileo.usg.edu/names/24968 <===
https://dlgadmin.galileo.usg.edu/names/24969

Oxnam, G. Bromley (Garfield Bromley), 1891-1963
(wait)
https://dlgadmin.galileo.usg.edu/names/25292 <===
https://dlgadmin.galileo.usg.edu/names/25293 (not public)

Seaver, Ted
Biography:
White social worker who came to Jackson, Mississippi as a civil rights activist in the summers of 1964 and 1965; in Jackson, he served as coordinator of the Community Development Agency.
https://dlgadmin.galileo.usg.edu/names/26549 <===
https://dlgadmin.galileo.usg.edu/names/26548

Walker, Elven
Biography:
African American physician who was the plaintiff in a 1963 lawsuit that desegregated Emory University and Fulton County Medical and Dental Society.
https://dlgadmin.galileo.usg.edu/names/27852
https://dlgadmin.galileo.usg.edu/names/27853 <===

White, Walter Francis, 1893-1955
Biography:
Wikipedia, 31 Oct. 2012: "Civil rights activist ...
https://dlgadmin.galileo.usg.edu/names/28167 <===
https://dlgadmin.galileo.usg.edu/names/28166

Williams, Edgar
Biography:
Member and one-time president of the Portland, Oregon NAACP.
https://dlgadmin.galileo.usg.edu/names/28242 <===
https://dlgadmin.galileo.usg.edu/names/28243

