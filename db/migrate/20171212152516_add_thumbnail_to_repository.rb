class AddThumbnailToRepository < ActiveRecord::Migration[4.2]
  def change
    remove_column :repositories, :thumbnail_path
    add_column :repositories, :thumbnail, :string
  end
end
