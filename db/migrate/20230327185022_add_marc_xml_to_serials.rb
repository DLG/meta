class AddMarcXmlToSerials < ActiveRecord::Migration[6.1]
  def change
    add_column :serials, :source_data, :string,  null:false, default: ''
  end
end
