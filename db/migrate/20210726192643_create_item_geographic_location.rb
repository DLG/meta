class CreateItemGeographicLocation < ActiveRecord::Migration[5.2]
  def change
    create_table :item_geographic_locations do |t|
      t.belongs_to :item, index: true
      t.belongs_to :geographic_location, index: true
      t.timestamps
    end
  end
end
