class AddCreatorToUser < ActiveRecord::Migration[4.2]
  def change
    change_table :users do |t|
      t.references :creator, index: true
    end
  end
end
