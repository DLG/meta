class AddBatchToItem < ActiveRecord::Migration[4.2]
  def change

    change_table :items do |t|

      t.references :batch, null: true, index: true, foreign_key: true

    end

  end
end
