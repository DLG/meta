class AddPolymorphicPortalRelations < ActiveRecord::Migration[4.2]
  def change

    create_table :portal_records do |t|

      t.references :portal, index: true
      t.references :portable, polymorphic: true, index: true

    end

  end
end
