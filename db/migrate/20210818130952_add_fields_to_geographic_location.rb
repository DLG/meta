class AddFieldsToGeographicLocation < ActiveRecord::Migration[5.2]
  def change
    add_column :geographic_locations, :component_placenames, :string, array: true, default: []
    add_column :geographic_locations, :us_state, :string
    add_column :geographic_locations, :us_county, :string
    add_column :geographic_locations, :geoname_verified, :boolean, default: false
    add_column :geographic_locations, :created_by, :string
    add_column :geographic_locations, :updated_by, :string
  end
end
