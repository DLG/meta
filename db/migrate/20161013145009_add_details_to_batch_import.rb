class AddDetailsToBatchImport < ActiveRecord::Migration[4.2]
  def change
    change_table :batch_imports do |t|
      t.json :results, default: {}
      t.boolean :validations
    end
  end
end
