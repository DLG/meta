class AddNameTypeToNames < ActiveRecord::Migration[5.2]
  def up
    add_column :names, :name_type, :string, null: false, default: ''

    Name.update_all(name_type: 'person')

    drop_table :corporate_entities
  end

  def down
    create_table :corporate_entities do |t|
      t.string :slug, null: false, uniqueness:true
      t.index   :slug, unique: true

      t.string :authoritative_name, null:false, default:''
      t.string :alternate_names, array:true, null:false, default:[]
      t.string :related_names, array:true, null:false, default:[]
      t.string :bio_history, null:false, default:''
      t.string :authoritative_name_source, array:true, null:false, default:[]
      t.string :source_uri, null:false, default:''
      t.string :public, null:false, default:''
      t.string :oclc_number, nil:false, default: ''
      t.json   :legacy_data, nil: false, default: {}

      t.timestamps
    end

    remove_column :names, :name_type
  end
end
