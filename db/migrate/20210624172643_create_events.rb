class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :slug, null: false, uniqueness:true
      t.index   :slug, unique: true

      t.string :title, null:false, default:''
      t.string :dcterms_temporal, array:true, null:false, default:[]
      t.string :description, null:false, default:''
      t.string :canned_search, null:false, default:''

      t.timestamps
    end
  end
end
