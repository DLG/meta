class AddResultsToBatch < ActiveRecord::Migration[4.2]
  def change
    change_table :batches do |t|
      t.column :commit_results, :json, default: {}, null: false
    end
  end
end
