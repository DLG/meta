# adds file type string field to Page model
class AddFileTypeToPage < ActiveRecord::Migration[4.2]
  def change
    add_column :pages, :file_type, :string
  end
end
