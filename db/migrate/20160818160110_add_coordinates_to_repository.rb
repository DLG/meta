class AddCoordinatesToRepository < ActiveRecord::Migration[4.2]
  def change
    change_table :repositories do |t|
      t.string :coordinates
    end
  end
end
