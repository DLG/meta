class UpdateCounterCachesDefaultValues < ActiveRecord::Migration[5.0]
  def up
    BatchImport.where(batch_items_count: nil).update_all(batch_items_count: 0)
    Batch.where(contained_count: nil).update_all(contained_count: 0)
    Batch.where(batch_items_count: nil).update_all(batch_items_count: 0)
    Collection.where(items_count: nil).update_all(items_count: 0)
    Collection.where(collection_resources_count: nil).update_all(collection_resources_count: 0)
    Item.where(pages_count: nil).update_all(pages_count: 0)
    Repository.where(collections_count: nil).update_all(collections_count: 0)

    change_column :batch_imports, :batch_items_count, :integer, default: 0, null: false
    change_column :batches, :contained_count, :integer, default: 0, null: false
    change_column :batches, :batch_items_count, :integer, default: 0, null: false
    change_column :collections, :items_count, :integer, default: 0, null: false
    change_column :collections, :collection_resources_count, :integer, default: 0, null: false
    change_column :items, :pages_count, :integer, default: 0, null: false
    change_column :repositories, :collections_count, :integer, default: 0, null: false
  end
  def down
    # Can't reverse setting a default value
  end
end
