class RemoveAlternateNamesArrayFromName < ActiveRecord::Migration[5.2]
  def change
    remove_column :names, :alternate_names, array:true, null:false, default:[]
  end
end
