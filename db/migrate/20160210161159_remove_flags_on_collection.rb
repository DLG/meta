class RemoveFlagsOnCollection < ActiveRecord::Migration[4.2]
  def change

    change_table :collections do |t|

      t.remove :dpla
      t.remove :public

    end

  end
end
