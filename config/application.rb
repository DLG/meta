require_relative "boot"

require "rails/all"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Meta
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.0

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    config.time_zone = 'Eastern Time (US & Canada)'

    # config.eager_load_paths << Rails.root.join("extras")

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    config.i18n.load_path += Dir[Rails.root.join('locales', '*.{rb,yml}').to_s]
    config.i18n.default_locale = :en

    # Do not swallow errors in after_commit/after_rollback callbacks.
    #config.active_record.raise_in_transactional_callbacks = true #scp commented out due to deprecation

    # configure queue adapter
    # Removed in favor of Resque
    #config.active_job.queue_adapter = :delayed_job

    config.autoloader = :zeitwerk

    #autoload files in lib diretory
    config.autoload_paths << Rails.root.join('lib')

    config.generators do |g|
      g.test_framework      :rspec, fixture: true
      g.fixture_replacement :fabrication
    end

    # where to find av files on the dlg admin server
    config.av_files_root = '/dlg_av'

    # where the above directory is accessible on the public site
    # e.g., at https://dlg.usg.edu<public_av_files_root>
    config.public_av_files_root = '/dlg_av'
  end
end
