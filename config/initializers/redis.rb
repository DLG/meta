$redis = if Rails.env.test?
           MockRedis.new
         else
           Redis.new(host: Rails.application.credentials.redis_host.dig(Rails.env.to_sym),
                     port: Rails.application.credentials.redis_port.dig(Rails.env.to_sym))
         end
