# frozen_string_literal: true

# overrides for default blacklight maps helpers
Rails.application.config.to_prepare do
  BlacklightAdvancedSearch::QueryParser.module_eval do
    def filters
      unless @filters
        @filters = {}
        return @filters unless @params[:f_inclusive]&.respond_to?(:each_pair)

        @params[:f_inclusive].each_pair do |field, value_array|
          @filters[field] ||= value_array.dup if value_array
        end
      end
      @filters
    end
  end

  # Blacklight Advanced Search doesn't allow us to override defType on a field-by-field basis
  # This makes it honor a "force_deftype" parameter, which you can set in a search field's solr_local_parameters
  # ("force_deftype" is already the name of an option used internally in the gem)
  ParsingNesting::Tree::Node.module_eval do
    alias_method :build_nested_query_original, :build_nested_query
    def build_nested_query(embeddables, solr_params = {}, options = {})
      if solr_params[:force_deftype].present?
        options[:force_deftype] = solr_params[:force_deftype]
      end
      if solr_params[:wildcard] == true
        options[:force_deftype] = 'lucene'
        embeddables.each do |e|
          next unless e.is_a? ParsingNesting::Tree::Term
          # This gsub is based on RSolr.solr_escape but omits * from the match regex
          #   and adds an extra backslash to the substitution
          e.value = e.value.to_s.gsub(/([+\-&|!\(\)\{\}\[\]\^"~\?:\\\/])/, '\\\\\\\\\1')
        end
      end
      build_nested_query_original(embeddables, solr_params.without(:force_deftype, :wildcard), options)
    end
  end

  ParsingNesting::Tree::OrList.module_eval do
    alias_method :to_query_original, :to_query
    def to_query(local_params)
      # The original method does some fanciness to see if an OR list could just be consolidated into
      # something like {!mm=1} A B instead of A OR B
      # If it can't, it calls to_multi_queries to construct the A OR B syntax.
      # We always want that A OR B syntax if we've turned on our wildcard flag,
      # since the other option assumes the dismax query parser.
      if local_params[:wildcard]
        to_multi_queries(local_params)
      else
        to_query_original(local_params)
      end
    end
  end

  BlacklightAdvancedSearch::RenderConstraintsOverride.module_eval do
    def render_constraints_query(my_params = params)
      if advanced_query.nil? || advanced_query.keyword_queries.empty?
        super(my_params)
      else
        content = []
        advanced_query.keyword_queries.each_pair do |field, query|
          label = blacklight_config.search_fields[field][:label]
          content << render_constraint_element(
            label, query,
            remove: search_action_path(remove_advanced_keyword_query(field, my_params).except(:controller, :action))
          )
        end
        if advanced_query.keyword_op == 'OR' &&
          advanced_query.keyword_queries.length > 1
          content.unshift content_tag(:span, 'Any of:', class: 'operator')
          content_tag :span, class: 'inclusive_or appliedFilter well' do
            safe_join(content.flatten, "\n")
          end
        else
          safe_join(content.flatten, "\n")
        end
      end
    end

    def render_constraints_filters(my_params = params)
      f_params = controller.search_state_class.new({ f: my_params[:f] }, blacklight_config, self)
      content = super(f_params)
      advanced_query&.filters&.each_pair do |field, value_list|
        label = facet_field_label(field)
        new_value_list = value_list.map do |val|
          facet_display_value(field, val)
        end
        content << render_constraint_element(
          label,
          safe_join(Array(new_value_list), " <strong class='text-muted constraint-connector'>OR</strong> ".html_safe),
          remove: search_action_path(remove_advanced_filter_group(field, my_params).except(:controller, :action))
        )
      end
      content
    end
  end
end
