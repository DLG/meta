Resque.redis = $redis

# Runs resque jobs right away in the same process. Allows you to run jobs without workers.
# Used for debugging but should be commented out before being merged.
Resque.inline = Rails.env.development? # || Rails.env.test?