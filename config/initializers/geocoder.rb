Geocoder.configure(
  timeout: 3,
  opencagedata: {
    api_key: Rails.application.credentials.opencage_api_key.dig(Rails.env.to_sym),
  },
  nominatim: {
    http_headers: {
      'User-Agent' => "Digital Library of Georgia (#{Rails.application.credentials.nominatim_contact})"
    }
  },
  use_https: true,
  cache: @redis,
  cache_prefix: 'geocoder:'
)
