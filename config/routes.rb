require 'resque/server'

Rails.application.routes.draw do
  resources :serials do
    member do
      get 'items'
      get 'solr'
    end
  end
  resources :geographic_locations do
    member do
      get 'items'
      get 'collections'
      get 'serials'
      get 'consolidate'
      post 'consolidate/review', to: 'geographic_locations#review_consolidation'
      post 'consolidate/perform', to: 'geographic_locations#perform_consolidation'
    end
    collection do
      get 'suggest', to: 'geographic_locations#suggest'
    end
  end
  resources :events
  resources :names do
    collection do
      get 'suggest', to: 'names#suggest'
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # API-ish endpoints
  constraints(format: :json) do
    get 'oai_support/dump',       to: 'oai_support#dump',      as: 'oai_support_dump'
    get 'oai_support/deleted',    to: 'oai_support#deleted',   as: 'oai_support_deleted'
    get 'oai_support/metadata',   to: 'oai_support#metadata',  as: 'oai_support_metadata'
    get 'api/info',               to: 'api#info',              as: 'api_info'
    get 'api/hi_info',            to: 'api#hi_info',           as: 'api_hi_info'
    get 'api/tab_features',       to: 'api#tab_features',      as: 'api_tab_features'
    get 'api/carousel_features',  to: 'api#carousel_features', as: 'api_carousel_features'
  end

  # API v2
  namespace :api do
    namespace :v2 do
      constraints(format: :json) do
        get 'ok', to: 'base#ok'
        resources :items, only: %i[index show]
        resources :collections, only: %i[index show]
        resources :holding_institutions, only: %i[index show]
        resources :features, only: :index
        get 'collections/:collection/resource/:slug', to: 'collection_resources#show'
      end
    end
  end

  concern :searchable, Blacklight::Routes::Searchable.new
  concern :exportable, Blacklight::Routes::Exportable.new

  devise_for :users, path: 'auth', controllers: {
    invitations: 'invitations'
  }

  devise_scope :user do
    get 'auth/invitations', to: 'invitations#index'
  end

  resource :profile, only: %i[show edit], controller: 'profile' do
    collection do
      patch 'update', as: 'update'
    end
  end

  resources :repositories, :users, :subjects, :time_periods,
            :features, :projects

  resources :holding_institutions do
    member do
      get 'solr'
    end
  end

  resources :remediation_actions, except: %i[edit update] do
    member do
      post 'perform', to: 'remediation_actions#perform'
      get 'review', to: 'remediation_actions#review'
    end
  end

  resources :item_versions, only: [] do
    member do
      patch :restore
    end
  end

  resources :collections do
    resources :collection_resources
    member do
      post 'reindex_items'
      get 'solr'
    end
  end

  resources :items do
    resources :pages do
      delete :index, on: :collection, action: :destroy_all, as: :destroy_all
    end
    resources :item_versions, only: [] do
      member do
        get :diff, to: 'item_versions#diff'
        patch :rollback, to: 'item_versions#rollback'
      end
    end
    collection do
      delete 'multiple_destroy', constraints: { format: :json }
      post 'xml'
      get 'deleted'
    end
    member do
      get 'copy'
      get 'fulltext'
      get 'solr'
    end
  end
  get '/items/new_from_serial/:serial_id', to: 'items#new_from_serial', as: 'new_item_from_serial'


  resources :batches do
    member do
      post 'recreate', to: 'batches#recreate'
      post 'commit', to: 'batches#commit'
      get 'commit_form', to: 'batches#commit_form'
      get 'import', to: 'batches#import'
      get 'results', to: 'batches#results'
      post 'quick_page_ingest'
    end
    collection do
      get 'select', to: 'batches#select'
    end

    resources :batch_items do
      collection do
        post 'import', to: 'batch_items#import', constraints: { format: :json }
      end
    end

    resources :batch_imports, except: %i[edit update] do
      collection do
        get 'help'
      end
    end
  end

  resources :fulltext_ingests, except: %i[edit update]
  resources :marc_xml_imports, except: %i[edit update] do
    collection do
      delete 'destroy_records'
    end
  end
  resources :page_ingests, except: %i[edit update] do
    member do
      get 'source.json', to: 'page_ingests#source_json', as: 'source_json', defaults: { format: :json }
    end
  end

  get '/reports/:id', to: 'reports#show', as: 'report'
  get '/reports', to: 'reports#index', as: 'reports'

  resource :catalog, only: [:index], controller: 'catalog', constraints: { id: /.*/ } do
    concerns :searchable
    collection do
      get 'facets', to: 'catalog#facets'
    end
    get 'facet_values/:facet_field', to: 'catalog#all_facet_values', as: 'facet_values_csv', constraints: { format: :csv }
  end

  # Constraints used to include format: false, which Rails 6 doesn't like. Deleted for now
  resources :solr_documents, only: [:show], path: '/record', controller: 'catalog', constraints: { id: /.*/ } do
    concerns :exportable
  end

  resources :bookmarks do
    concerns :exportable

    collection do
      delete 'clear'
    end
  end

  resources :tools, only: %i[index] do
    collection do
      get :csv_import, to: 'tools#csv_import_tool'
      put :csv_import, to: 'tools#csv_import'
      get :csv_template
    end
  end

  mount Blacklight::Engine => '/'
  mount BlacklightAdvancedSearch::Engine => '/'

  # Resque Admin
  resque_constraint = lambda do |request|
    request.env['warden'].authenticate!(scope: :user)
  end
  constraints resque_constraint do
    mount Resque::Server.new => '/resque'
  end

  # Base Paths
  authenticated do
    root to: 'advanced#index', as: :authenticated_root
  end

  root to: redirect('auth/sign_in')

end
