crumb :root do
  link 'Home', root_path
end

# SEARCH

crumb :basic_search do
  link 'Basic Search'
end

crumb :advanced_search do
  link 'Advanced Search'
end

crumb :map_search do
  link 'Map Search'
end

crumb :facets_browse do
  link 'Facet Browse', facets_catalog_path
end

crumb :facet_browse do |facet|
  link facet
  parent :facets_browse
end

# PROFILE

crumb :profile do
  link 'Profile'
end

crumb :change_password do
  link 'Change Password'
end

# REPOSITORY

crumb :repositories do
  link 'Repositories', repositories_path
end

crumb :repository do |repository|
  if repository.persisted?
    link repository.title
  else
    link 'New'
  end
  parent :repositories
end

# COLLECTION

crumb :collections do
  link 'Collections', collections_path
end

crumb :collection do |collection|
  if collection.persisted?
    link collection.display_title
  else
    link 'New'
  end
  parent :collections
end

crumb :solr_collection do |collection|
  link collection.record_id, collection
  link 'Solr Collection', solr_collection_path(collection)
  parent :collections
end

# COLLECTION RESOURCES

crumb :collection_resources do |collection|
  link collection.display_title, collection
  link 'Collection Resources', collection_collection_resources_path(collection)
  parent :collections
end

crumb :collection_resource do |collection_resource|
  link collection_resource.collection.display_title,
       collection_resource.collection
  parent :collections
  link 'Collection Resources', collection_collection_resources_path(collection_resource.collection)
  if collection_resource.id
    link collection_resource.title
  else
    link 'New'
  end
end

# ITEM

crumb :items do
  link 'Items', items_path
end

crumb :deleted_items do
  link 'Deleted'
  parent :items
end

crumb :item do |item|
  if item.persisted?
    link(item.title) if item.title # todo item can not have a title...
  else
    link 'New'
  end
  parent :items
end

crumb :solr_item do |item|
  link item.record_id, item
  link 'Solr Item', solr_item_path(item)
  parent :items
end

# USER

crumb :users do
  link 'Users', users_path
end

crumb :user do |user|
  if user.persisted?
    link user.email
  else
    link 'New'
  end
  parent :users
end

# INVITATION

crumb :invitations do
  link 'Invitations', auth_invitations_path
  parent :users
end

crumb :invitation do |user|
  if user.persisted?
    link user.email
  else
    link 'New'
  end
  parent :invitations
end

# BATCH

crumb :batches do
  link 'Batches', batches_path
end

crumb :batch do |batch|
  if batch.persisted?
    link batch.name if batch.name
  else
    link 'New'
  end
  parent :batches, batch
end

crumb :batch_results do |batch|
  link batch.name, batch_path(batch) if batch.name
  link 'Results'
  parent :batches, batch
end

# BATCH IMPORT

crumb :batch_imports do |batch|
  link batch.name, batch
  link 'Batch Imports', batch_batch_imports_path(batch)
  parent :batches
end

crumb :batch_import_info do |batch_import|
  link batch_import.batch.name, batch_import.batch
  link 'Batch Imports', batch_batch_imports_path(batch_import.batch)
  link batch_import.id, batch_batch_import_path(batch_import.batch, batch_import)
  link 'Info'
  parent :batches
end

crumb :batch_import_xml do |batch_import|
  link batch_import.batch.name, batch_import.batch
  link 'Batch Imports', batch_batch_imports_path(batch_import.batch)
  link batch_import.id, batch_batch_import_path(batch_import.batch, batch_import)
  link 'XML'
  parent :batches
end

crumb :batch_import do |batch_import|
  link batch_import.batch.name, batch_import.batch
  link 'Batch Imports', batch_batch_imports_path(batch_import.batch, batch_import)
  link 'New'
  parent :batches
end

# BATCH ITEM

crumb :batch_items do |batch|
  link batch.name, batch
  link 'Batch Items', batch_batch_items_path(batch)
  parent :batches
end

crumb :batch_item do |batch_item|
  link batch_item.batch.name, batch_item.batch
  parent :batches
  if batch_item.id
    link batch_item.title
  else
    link 'New'
  end
end

# FEATURE

crumb :features do
  link 'Feature', features_path
end

crumb :feature do |feature|
  if feature.persisted?
    link feature.title
  else
    link 'New'
  end
  parent :features
end

# SUBJECT

crumb :subjects do
  link 'Subjects', subjects_path
end

crumb :subject do |subject|
  if subject.persisted?
    link subject.name
  else
    link 'New'
  end
  parent :subjects
end

# TIME PERIOD

crumb :time_periods do
  link 'Time Periods', time_periods_path
end

crumb :time_period do |time_period|
  if time_period.persisted?
    link time_period.name
  else
    link 'New'
  end
  parent :time_periods
end

# FULLTEXT INGESTS

crumb :fulltext_ingests do
  link 'Full Text Ingests', fulltext_ingests_path
end

crumb :fulltext_ingest do |fulltext_ingest|
  if fulltext_ingest.persisted?
    link fulltext_ingest.title
  else
    link 'New'
  end
  parent :fulltext_ingests
end

# PAGE INGESTS

crumb :page_ingests do
  link 'Page Ingests', page_ingests_path
end

crumb :page_ingest do |page_ingest|
  if page_ingest.persisted?
    link page_ingest.title
  else
    link 'New'
  end
  parent :page_ingests
end

# MARC XML Imports
crumb :marc_xml_imports do
  link 'MARC XML Imports', marc_xml_imports_path
  parent :tools
end

crumb :marc_xml_import do |marc_xml_import|
  if marc_xml_import.persisted?
    link marc_xml_import.title
  else
    link 'New'
  end
  parent :marc_xml_imports
end

# REMEDIATION ACTIONS

crumb :remediation_actions do
  link 'Remediation Actions', remediation_actions_path
end

crumb :remediation_action do |remediation_action|
  if remediation_action.persisted?
    link remediation_action.id
  else
    link 'New'
  end
  parent :remediation_actions
end

# PROJECTS

crumb :projects do
  link 'Projects', projects_path
end

crumb :project do |project|
  if project.persisted?
    link project.title
  else
    link 'New'
  end
  parent :projects
end

# HOLDING INSTITUTIONS

crumb :holding_institutions do
  link 'Holding Institutions', holding_institutions_path
end

crumb :holding_institution do |holding_institution|
  if holding_institution.persisted?
    link holding_institution.authorized_name
  else
    link 'New'
  end
  parent :holding_institutions
end

crumb :solr_holding_institution do |holding_institution|
  link holding_institution.authorized_name, holding_institution
  link 'Solr Holding Institution', solr_holding_institution_path(holding_institution)
  parent :holding_institutions
end

# PAGE

crumb :pages do |item|
  link item.record_id, item
  link 'Pages', item_pages_path(item)
  parent :items
end

crumb :page do |page|
  link 'Items', items_path
  link page.item.record_id, page.item
  link 'Pages', item_pages_path(page.item)
  if page.id
    link page.number
  else
    link 'New'
  end
end

# Names

crumb :names do
  link 'Names', names_path
end

crumb :name do |name|
  if name.persisted?
    link name.authoritative_name
  else
    link 'New'
  end
  parent :names
end

# Events

crumb :events do
  link 'Events', events_path
end

crumb :event do |event|
  if event.persisted?
    link event.title
  else
    link 'New'
  end
  parent :events
end

# Geographic Locations
crumb :geographic_locations do
  link 'Geographic Locations', geographic_locations_path
end

crumb :geographic_location do |geographic_location|
  if geographic_location.persisted?
    link geographic_location.code, geographic_location if geographic_location.code
  else
    if geographic_location.code.present? && geographic_location.id
      # Just deleted
      link "<strike>".html_safe + geographic_location.code + "</strike>".html_safe
    else
      link 'New'
    end
  end
  parent :geographic_locations, geographic_location
end

crumb :geographic_location_items do |geographic_location|
  link 'Items'
  parent :geographic_location, geographic_location
end

crumb :geographic_location_collections do |geographic_location|
  link 'Collections'
  parent :geographic_location, geographic_location
end

crumb :geographic_location_serials do |geographic_location|
  link 'Serials'
  parent :geographic_location, geographic_location
end


# Serials
crumb :serials do
  link 'Serials', serials_path
end

crumb :solr_serial do |serial|
  link serial.slug, serial
  link 'Solr Serial', solr_serial_path(serial)
  parent :serials
end

crumb :serial_items do |serial|
  link 'Items'
  parent :serials, serial
end

crumb :serial do |serial|
  if serial.persisted?
    link serial.display_title, serial if serial.display_title
  else
    if serial.display_title.present? && serial.id
      # Just deleted
      link "<strike>".html_safe + serial.display_title + "</strike>".html_safe
    else
      link 'New'
    end
  end
  parent :serials, serial
end

# Reports

crumb :reports do
  link 'Reports', reports_path
end

# Tools

crumb :tools do
  link 'Tools', tools_path
end

crumb :csv_importer do
  link 'CSV Importer', csv_import_tools_path
  parent :tools
end