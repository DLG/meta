# DLG Meta

A [Blacklight](https://github.com/projectblacklight/blacklight)-based administration app for the [Digital Library of Georgia](http://dlg.galileo.usg.edu/). Currently a work in progress!

See [this project on GitHub](https://github.com/GIL-GALILEO/meta)

### Developer Information

#### Requirements
+ Docker & Docker-Compose
+ PostgreSQL
+ Node.js

#### Setup
1. Clone this repo
2. Install ruby 3.2.2, create a gemset, and install bundler in that gemset
3. In Rubymine, go to File/Settings/Languages & Frameworks/Ruby SDK and Gems, select that ruby version, and run `bundle install` to install gem dependencies 
4. Add master.key to the config/ directory (need to get from one of the developers)

### Using Docker
1. Install Docker ([instructions available here](https://docs.docker.com/engine/install/ubuntu/)) and ensure you can run `docker ps` without sudo by adding yourself to the docker group (`sudo usermod -aG docker your_username`).
2. Install Docker Compose ([Instructions here](https://docs.docker.com/compose/install/), be sure to get a relatively recent version.  Package repos might be a few versions behind.
3. From the code root directory (e.g. ~/RubymineProjects/meta) run `./provision/setup_dev_env.sh` to prepare the Docker environment
4. `bundle exec rake db:setup` to create databases
5. Optionally run `bundle exec rake import_production_data` to load some data.  This can take a long time (15-30 mins).
6. `bundle exec rails s -p 3002` to start development web server
7. Go to [http://localhost:3002](localhost:3002)
8. Sign in with the test user (email: super@uga.edu, password: password) unless you imported production data.  If you did import production data then use a production credential.
9. Search for `georgia` to make sure the seed data was loaded correctly

### A note on Solr config development

The provision script at `provision/setup_dev_env.sh` supports a parameter of `SOLRCONFIG_BRANCH` which defaults to master.  You can provide this variable to build the solr container with a specific branch of the meta solr config repository.  See the repo at https://gitlab.galileo.usg.edu/infra/solr-configs/meta for a list of available branches or to create a new branch for development.  

To load a particular branch, start from a clean solr environment and then prefix the provision command with the appropriate branch:

+ `docker-compose down && docker volume rm meta_solr_data`
+ `SOLRCONFIG_BRANCH=my-branch-name ./provision/setup_dev_env.sh`

#### Docker Commands Cheatsheet
+ List containers(running and stopped) - `docker-compose ps`
+ Remove running containers - `docker-compose down`
+ Stop running containers - `docker-compose stop` (I think this is a thing)
+ Stop a single running container - `docker-compose stop solr`
+ Start containers - `docker-compose up -d`
+ Start a single container - `docker compose start solr`
+ View container logs - `docker-compose logs`
+ View logs for specific container - `docker-compose logs postgres`
+ Get a terminal in a running container - `docker-compose exec postgres /bin/bash`
+ Restart all containers - `docker-compose restart`
+ Restart a single container - `docker-compose restart solr`
+ Rebuild containers - `docker-compose build`

#### Test Suite
1. `rspec` to run all specs


### License
© 2024 Digital Library of Georgia
