# Changelog
All notable changes to the DLG Admin (META) project will be documented in this file.
The format is based on Keep a Changelog.

## [20250124] (Sprint B25)
### Changed
- Rails 7 (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/748)

## [20250115] (Sprint A25)
### Changed
- fix iiif_manifest_url logic (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/651)

## [20241018] (Sprint U24)
### Fixed
- NoMethodError when DPLA job runs in a month with no DPLA load (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/743)

## [20240712] (Sprint N24)
### Changed
- Fix extension_whitelist deprication warning (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/640)
- Update DPLA rake task to include logic to choose the correct Monday to run (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/735)
- Update Rails to 6.1.7.8 (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/640/diffs?commit_id%253Dfe79c0f9103b0461969238b59256502fe4301486)

## [20240209] (Sprint C24)
### Changed
- Quick page ingest for AV items (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/723)
- Setup directory for AV files in development (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/621)

## [20240126] (Sprint B24)
### Changed
- New report: New Collections (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/724)
- Update New Collection Report (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/618)
- Update time period in New Collection Report (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/619)


## [20231121] (Mid-Sprint X23)
### Added
- "Smart" reindex job that automatically adjusts batch size (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/716)

### Changed
- Add a way to edit external identifiers field in GUI (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/702)
- Add a way to add external identifiers via the batch import (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/713)
- Turn on serials in production (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/594)
- Add external identifiers to Solr (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/596)
- Generalized Report (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/597)

### Fixed
- Serial/GeographicLocation bug (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/591)
- Batch import fails if items have an empty collection field (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/593)
- Assets issue (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/595)

### Staging
- Postgres 14 upgrade (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/718)


## [20231020] (Sprint U23)
### Changed
- Modify item batch import to handle serials (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/701)
- Add Item Slug from Serial (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/711)
- Update generate_slug_from_serial to clean dc_date (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/582)
- Create Item from Serial (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/585)
- Add ultimate call numbers to ggp items (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/700)
- Normalize call numbers indexed in Solr (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/587)
- serial batch item template (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/586)

## [20231006] (Sprint T23)
### Changed
- Serials import: use date to assign items that match multiple serials to the correct one (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/685)

## [20230922] (Sprint S23)
### Changed
- Report: Subjects per holding institution (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/703)
- Show overlay and spinner while reports run (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/704)
- Fix issue with holding inst subjects report (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/706)
- Create another holding inst subject report that shows all subjects for one inst (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/707)
- Add repository name to Repositories' Collections report (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/705)
- Linkify URLs (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/577)

## [20230825] (Sprint Q23)
### Changed
- Tweak Solr search field weights and other search behaviors (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/566)
- Add advanced search fields for metadata and DO links that allow the use of * wildcards (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/698)
- Add an index on pages.item_id (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/697)

### Staging
- Index serial call numbers into Solr (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/694)
- Monograph integration into the call number search (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/695)


## [20230714] (Sprint N23)
### Staging
- Fix serial import (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/555, https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/556)

## [20230630] (Sprint M23)
### Changed
- Update Rails to 6.1.7.4 (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/552/diffs?commit_id=bc0875e2ba8c7cdd23b2fe4d266a8ed6dde677ae)

### Staging
- Serials - Display only when at least one visible item (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/548)
- Serials - Geographic locations (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/672)
- Serials Import - Georgia county parsing (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/691)
- Serials - link preceding and succeeding titles (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/690)

## [20230602] (Sprint K23)
### Changed
- Add portal to "Collections' Item counts" report (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/647)
- Quick page ingest (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/676, https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/549)
- Support Serials in Record Search (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/687)
- Remove n+1 query from collections_item_count report by generalizing `count_column_scopes_for` (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/537)

### Staging
- Add record_id to serials (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/538)
- Serial linking (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/539)
- Serial images (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/688)
- Improve serial display in Blacklight (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/542, https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/543)
- Import arbitrary identifiers for serials (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/673)
- Add "Display" column to serials index page (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/547)

## [20230519] (Sprint J23)
### Changed
- Speed up batches index page and save per-view per-page settings in cookie (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/684)
- Serials import - dc_date conversion (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/674)

## [20230505] (Sprint I23)
### Staging
- Add Serials to Solr (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/680, https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/529)
- Match items to serials for more GGP URL patterns (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/513)
- Serial updates (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/527)
- Index items when attached to serials (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/528)


## [20230407] (Sprint G23)
### Changed
- Serials - MARC record importer (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/667)
- Fix GGP slug conversion (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/506)
- Add source_data to Serials (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/508)
- Normalize font sizes (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/658)
- Serial import resque job w/ results page (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/669)
- Serial import - update records (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/670)
- Change pagination default to 1000 (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/515)
- Fix serial Sorting (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/516)
- 20230407 sn turn off deprecation warnings (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/517)


## [20230324] (Sprint F23)

### Staging
-  CRUD pages for Serials (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/666)

### Changed
- Reorder manage menu (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/665)

## [20230224] (Sprint D23)
### Changed
- Add per-pager (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/490)
- Add Filterriffic/Pagy to Repos (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/491)
- Add Pagy to Page Ingests (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/492)
- Add BL View buttons to show page (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/493)
- Add Display filter to index page (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/495)
- Make it easier to figure out why item is not being displayed (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/496)
- Add slug to repo/coll filter selects (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/663)
- Collection Solr view (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/498)

### Fixed
- Button styles (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/494)

## [20230210] (Sprint C23)
### Changed
- Show more records per page on item and collection indexes (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/661)

### Fixed
- a:visited styles on button groups in tables (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/651)
- Dropdown styles on Blacklight search results page (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/652)
- Geographic Location > Collection page filters out all results (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/656)
- Thumbnails in blacklight search results push metadata downward (in Safari) (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/657)
- Breadcrumb on Geographic Locations > Consolidate results is wrong (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/654)
- Fulltext Ingest > Undo does not reindex items (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/655)
- Blacklight Results > Add to Batch functionality does not copy holding institutions or geographic locations (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/653)

## [20230203] (Mid-Sprint C23)
### Changed
- Upgrade to Rails 6 (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/599)
- Upgrade to Blacklight 7 (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/599)
- Redo Items index page using Filterrific (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/439)

## [20230113] (Sprint A23)
### Changed
- Update HTTParty (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/437)

## [20221215] (Mid-Sprint Y22)
### Changed
- EDS Update Rake task: Added error handling for directory cleanup (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/631)
- Year range 9999 indexing refinement (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/632)
- Run legacy link checker (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/628)

## [20221118] (Sprint W22)
### Changed
- Setup Sentry (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/412)
- Missing manifest fields from show page (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/415)
- Try to auto-fire up the webserver on CI/CD deploys (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/623)
- 20221109 sn reindex items button (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/417)
- Fix page ingest bug w/ pages not reindexing and new delete all pages button for item (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/419)
- Solr Item page (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/420)


## [20220926] (Hotfix Sprint T22)
### Changed
- Provide a way to reindex solr rows at a particular id (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/617)
- Small update to reindex (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/407)

### Fixed
- Fix nil bug in index dates (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/616)
- Fix dc_date validation to disallow arrays that contain blanks (previously allowed if the array also contained a valid date) (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/618)


## [20220923] (Sprint S22)
### Changed
- Fix FTP issue with feed_eds rake task (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/613)
- Add incremental update capability to EDS update rake task (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/614)

### Fixed
- Fix code in DateIndexer (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/611)

## [20220908] (Sprint R22)
### Changed
- Automated incremental EDS updates (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/600)

## [20220826] (Sprint Q22)
### Changed
- Date sort doesn't sort by month and day (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/608)

## [20220812] (Sprint P22)
### Fixed
- Filtering remediation actions by field not working (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/607)
- Fix schema that has missing null:false due to incorrect migrations had `nil:false` instead of `null:false` (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/549)

## [20220721] (Mid-Sprint O22)
### Added
- Filteriffic on Remediation Action table (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/385)

### Changed
- Update Blacklight to 6.25 (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/604)

## [20220720] (Sprint N22)
### Added
- Create filters in meta (or reports) for Geographic Locations that have no geonomes match (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/480)

### Changed
- Update Rails and Diffy and add a fix for Blacklight (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/378)
- CRDL Names report (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/603)
- Name remediation fix (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/382)


## [20220621] (Mid-Sprint M22)
### Added
- Add more linking on index, show pages (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/595)

### Changed
- Blacklight Upgrade to 6.24 (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/587)
- Please add online exhibitions to educator resources facet (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/593)
- Remove CRDL Import data service/tasks (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/596)
- Remove AlternateNames Table(deprecated) (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/557)
* Remove CRDL Import data service/tasks (No labels) (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/596)

### Fixed
- No display in prod after updating a name record (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/592)
- New event not displaying (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/591)
- Committing a valid batch item fails if the item it modifies fails geographic locations validation (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/597)


## [20220609] (Sprint L22)
### Fixed
- Holding institutions not updated in Solr when collections change (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/586)

## [20220603] (Sprint K22)
### Changed
- Add missing mediums to educator resources solr column (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/583)
- Add media_thumbnail column to pages (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/584)

### Fixed
- Uploaded feature image does not index correctly (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/563)

## [20220520] (Sprint J22)
### Changed
- CSV importer improvements (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/576)
- Allow local building with specific branch of solr config (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/578)
- add educator_resource_mediums solr field (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/579)

### Fixed
- Fix missing rights statement icon error (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/577)
- Feature still in Solr after removal in meta (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/580)

## [20220506] (Sprint I22)
### Added
- Add Filteriffic to Batches (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/337)
- Add Filteriffic to Page_ingests, Fulltext_ingests (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/342)

### Changed
- Update rails to 5.2.7.1 (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/573)
- Update CSV report/import for Names and Events (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/572)
- Add collection resources to Solr (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/340)
- Hide direct name editing of item names and fix blacklight search (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/341)

## [20220422] (Sprint H22)
### Added
- Create report: holding institution (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/491)

### Changed
- Match names with items when item/event saved. (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/568)
- Update collections on Holding institutions solr column (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/332)
- Update uploads dump file (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/333)

## [20220411] (Sprint G22)
### Added
- Add video to pages (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/316)
- Rake task to convert video csv into page dump json (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/328)
- Holding Insts image report (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/329)

## [20220225] (Sprint F22)
### Added
- Migration to create name slugs (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/540)
- Misc: Feature changes, Uploads dump task, Broken portal selects (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/565)
- Add a check for the uploads dir to the import rake task (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/555)
- Need alternate_name_public/alternate_name_not_public in Name edit form (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/551)
- Create docker environment to replace vagrant for development (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/556)
- Make connections between names/items (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/537)
- re-import corporate name data (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/543)
- Educator resources solr field for crdl (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/559)
- Add "Event" Solr field to items for CRDL (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/561)

### Changed
- Fix label in feature edit page (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/562)
- Remove 'IIIF manifest' label from metadata display when there isn't one (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/564)

## [20220225] (Sprint D22)
### Added
- Add us_states to solr (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/547)
- Create report of all items that are missing holding institutions (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/545)
- Migration to create name slugs (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/540)
- Give CRDL-only items a CRDL-specific IIIF presentation manifest URL (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/550)

### Changed
- Fix people search (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/303)
- Escape HTML symbols on Item > Full Text page (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/552)

## [20220211] (Sprint C22)
### Added
- Add features to solr (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/538)
- Name - add first letter to solr (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/539)
- Add on-save routines for events and items' events (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/531)
- Move subform_helpers directory into app (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/542)

### Changed
- Update ruby to 2.6.9 (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/518)
- Allow manual associating of names with Items (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/532)
- Change logging to warn (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/287)
- Possibly make Item reindex less resource intense by changing Item.has_pages scope (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/290)
- Quality of Life changes to Data Importer (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/291)


## [20220128] (Sprint B22)
### Added
- Add public, portal solr fields in Name, Event tables (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/524)
- Add events selector to item edit page (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/522)
- Add subject/subject personal fields to event (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/525)
- Add items to event show page (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/530)
- Add title solr field for crdl tables (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/279)
- Typeahead suggestions for Names (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/526)
- Match events with items using canned_search (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/523)

### Changed
- Remove Faker (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/527)
- Fix array_lowercase SQL function (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/529)

## [20220114] (Sprint A22)
### Added
- Events and Items relations (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/519)
- Names and Item relation (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/520)
### Changed
- Update README.md (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/262)
- Fix bug that prevented events from being saved. (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/263)
- Fix: Geographic Location typeahead suggestions not working for values like "United States" (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/267)

## [20220107] (Mid Sprint A)
### Added
- Field addition to collection reports (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/509)

## [20211122] (Mid Sprint X)
### Added
- Add Geographic Locations to "All rows for database" report (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/489)
- turn on spatial validation in batch import (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/500)

### Changed
- Change item_slug to record_id on iiif_partner_url report (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/512)
- Tweak Item reindex (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/257)
- Limit "All Rows For Table" report to whitelisted tables (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/258)

## [20211105] (Sprint V)
### Changed
  - Please add a report for IIIF Partner url values (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/507)
  - Deduplication task for Geographic Locations (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/511)

## [20211021] (Sprint U)
### Changed
 - Add CRDL Solr columns (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/473)
 - Provide a way to manually correct geospatial data (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/501)

## [20211012] (Mid-Sprint U Hotfix)
### Changed
 - Fix: Geographic Location lookup fails to normalize separator before search (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/505)

## [20211007] (Mid-Sprint T)
### Changed
 - Update nokogiri (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/497)
 - Show warning for batch items that point to non-existent items (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/498)
 - test CRDL “Name” csv loader to be sure it still works (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/493)
 - change to spatial representation in DPLA export (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/499)

## [20210927] (Mid-Sprint T update)
### Added
 - GeoLocations - Add to Batch importer (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/477)
 - Fix spatial lookup (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/457)
 - Geographic Locations: minor refactors + incremental remediation task (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/494)
 - Add two fields to holding institution records (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/490)
 - How to link to supplementary materials that aren’t hosted within DLG? (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/363)
 - Geographic Locations selector on Item and Collection edit pages (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/488)
 - Point production to GeographicLocations instead of dcterms_spatial (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/487)
 - Add geographic_locations to Batch_Items (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/483)
 - Use Geonames API as fallback to Geographic Location table lookup (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/476)
### Changed 
 - Bug - Fix collection count link on Repository index page (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/486)


## [20210916] (Mid-Sprint S update)

### Changed 
- Fix references to old version of Solr in vagrant provision script
(https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/220)

### Staging
  - Geographic Locations selector on Item and Collection edit pages (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/488)
  - Point production to GeographicLocations instead of dcterms_spatial (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/487)
  - Add geographic locations to holding institution records
(https://gitlab.galileo.usg.edu/DLG/meta/-/issues/490)


## [20210831] (Mid-Sprint R update)

### Changed
- Take "Coverage Spatial" from new Geographic Locations table in production (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/487)
- Export of Geographic Locations to "Coverage Spatial" ignores blank latitudes and longitudes (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/213)
- Fixed bug with collection count link on Repository index page (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/486)
- Upgrade Solr version in development environment (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/215)
- Create Geonames API service (https://gitlab.galileo.usg.edu/DLG/meta/-/merge_requests/206)

## [20210826] (Sprint Q21)

### Added
- Geographic Location relationship to Item (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/469)
- Report: Collections, URLs, and Portals (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/452)
- Add GeoLocations place name array (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/475)
- Add GeoLocations to Solr (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/474)
- Associate Geographic Location with Collections (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/479)

### Changed
- Remove Corporate Entities (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/472)

## [20210812] (Sprint P21)

### Added
- Geographic locations table (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/462)
- Batch import for geographic locations from geonames.org (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/462)
- Remediation script for geographic metadata (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/469)
- Add OCLC number to name data (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/458)
- Alternate names for CRDL (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/464)

### Changed
- Public field changes for CRDL tables (https://gitlab.galileo.usg.edu/DLG/meta/-/issues/455)

## [20210710] (Sprint O21)

### Changed 
- Streamlined data importer https://gitlab.galileo.usg.edu/DLG/meta/-/commit/7631c03cd6db85313d34b1f52244d0fc301c5361 (Multiple other commits)

## [20210710] (Sprint N21)
### Added
- For CRDL: Add CRUD logic to support Event Browse https://gitlab.galileo.usg.edu/DLG/meta/-/issues/443
- For CRDL: Add CRUD logic to support People Browse https://gitlab.galileo.usg.edu/DLG/meta/-/issues/442
- Issue with Holding Institution facet (In Staging) https://gitlab.galileo.usg.edu/DLG/meta/-/issues/453
- Link report, List of harvested urls https://gitlab.galileo.usg.edu/DLG/meta/-/issues/451

### Changed 
- 'Collections' Item counts' report not working https://gitlab.galileo.usg.edu/DLG/meta/-/issues/448

## [20210526] (Sprint J21)
### Added
- Change iiif_presentation_url to iiif_partner_url; Add iif_manifest_url https://gitlab.galileo.usg.edu/DLG/meta/-/issues/446
-Report: Items with pages and iiif images https://gitlab.galileo.usg.edu/DLG/meta/-/issues/440

## [20210507] (Sprint I21)
### Added
- Report: Items with full text, list of items with full text https://gitlab.galileo.usg.edu/DLG/meta/-/issues/438
- Add iiif_presentation flag to turn on/off https://gitlab.galileo.usg.edu/DLG/meta/-/issues/428

### Changed
- Update to Rails 5.2.6
- Implement encrypted secrets file https://gitlab.galileo.usg.edu/DLG/meta/-/issues/422
- Bugfix: Error when trying to add page data for record https://gitlab.galileo.usg.edu/DLG/meta/-/issues/403

## [20210226] (Sprint D21)
### Added
- Add fields to Solr for creating IIIF Manifests https://gitlab.galileo.usg.edu/DLG/meta/-/issues/432
- Data Importer https://gitlab.galileo.usg.edu/DLG/meta/-/issues/415


## [20210212] (Sprint C21)
### Added
- Add iiif_presentation_manifest field to item https://gitlab.galileo.usg.edu/DLG/meta/-/issues/424


## [20210129] (Sprint B21)
### Added
- Add record_id to show page for Collection
 [406](https://gitlab.galileo.usg.edu/DLG/meta/-/issues/406)

 - Added a new field to the collection record in dlgadmin: sensitive_content
 [418](https://gitlab.galileo.usg.edu/DLG/meta/-/issues/418)

### Changed

## [20210115] (Sprint A21)
### Added
- New Reports [414](https://gitlab.galileo.usg.edu/DLG/meta/-/issues/414)

### Changed
- EDS Export: Add a single prolog at top of XML document [416](https://gitlab.galileo.usg.edu/DLG/meta/-/issues/416)

- Bug fix: EDS Export produces prologs for every record [411](https://gitlab.galileo.usg.edu/DLG/meta/-/issues/411)

- Update to Ruby 2.6 [407](https://gitlab.galileo.usg.edu/DLG/meta/-/issues/407)

- On dlgadmin search results page: display portal IDs [413](https://gitlab.galileo.usg.edu/DLG/meta/-/issues/413)


## [20201218] (Sprint Y)
### Added
- Reports Section [409](https://gitlab.galileo.usg.edu/DLG/meta/-/issues/409)

### Changed
- Incorrect notice after page ingest [404](https://gitlab.galileo.usg.edu/DLG/meta/-/issues/404)

## [20201203] (Sprint X)
### Added

### Changed
- Put Rails 5.2 upgrade branch into production [390](https://gitlab.galileo.usg.edu/DLG/meta/-/issues/390)



## [20201117] (Sprint W)
### Added

Unify Vagrant environment with DLG and provide better data [361](https://gitlab.galileo.usg.edu/DLG/meta/-/issues/361)

Add "has full text" indication on results list [389](https://gitlab.galileo.usg.edu/DLG/meta/-/issues/389)

### Changed

Vagrant: Update redis version to match dev/staging/prod [394](https://gitlab.galileo.usg.edu/DLG/meta/-/issues/394)

Bug in Development sample_data.rake causes postgres sequence problems [393](https://gitlab.galileo.usg.edu/DLG/meta/-/issues/393)
