# frozen_string_literal: true

module Exceptions
  class DataImportError < StandardError; end
  class CsvImporterError < StandardError; end
end
