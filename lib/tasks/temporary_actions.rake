# frozen_string_literal: true

desc 'Change local item urls to https'
task local_items_change_url_to_https: :environment do |_, _args|
  $stdout.sync = true
  start = Time.now

  notifier = NotificationService.new allow_slack_failure: true
  notifier.notify 'Starting local_items_change_url_to_https'
  rows = Item.are_local.where("array_to_string(edm_is_shown_at, '||') ILIKE ?", "%http:%").or(Item.are_local.where("array_to_string(edm_is_shown_by, '||') ILIKE ?", "%http:%"))
  total_committed = 0
  all_rows = rows.count

  rows.for_indexing.find_in_batches do |batch|
    batch_start = Time.zone.now

    batch.each do |item|
      item.edm_is_shown_at = item.edm_is_shown_at.map{|url| url.gsub(/\bhttp:/, 'https:')}
      item.edm_is_shown_by = item.edm_is_shown_by.map{|url| url.gsub(/\bhttp:/, 'https:')}
      item.save!(validate: false)
    end

    batch_end = (Time.zone.now - batch_start).round(1)
    total_committed += batch.size
    percent_done = (total_committed.to_f/all_rows * 100.0).round(2)
    total_elapsed_minutes = ((Time.zone.now - start)/60.0).round(1)
    notifier.notify "local_items_change_url_to_https: #{total_committed}/#{all_rows} rows (#{percent_done}%). last id: #{batch.last.id}. batch Solr time: #{batch_end} seconds. total time: #{total_elapsed_minutes} minutes."
  end

  notifier.notify("local_items_change_url_to_https finished in `#{((Time.zone.now - start)/60).round(1)}` minutes")
end
