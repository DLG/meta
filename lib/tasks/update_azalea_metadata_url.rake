require 'rake'

record_id = 'zhw_urnp'
old_domain = 'http://uncleremus'
new_domain = 'http://azalealibraries'

task update_azalea_metadata_url: :environment do
  start_time = Time.now

  counter = update_item_domains(record_id, old_domain, new_domain)

  puts 'Task complete!'
  puts "Items updated:#{counter}. Processing took #{Time.now - start_time} seconds."
end

task revert_azalea_metadata_url: :environment do
  start_time = Time.now

  counter = update_item_domains(record_id, new_domain, old_domain)

  puts 'Task complete!'
  puts "Items updated:#{counter}. Processing took #{Time.now - start_time} seconds."
end

def update_item_domains(record_id, old, new)
  collection = Collection.includes(:items).find_by record_id: record_id
  return  0 unless collection

  counter = 0

  collection.items.each  do |item|
    updated_urls = item.edm_is_shown_at.map do |url|
      url.start_with?(old) ? url.sub(old,new) : url
    end
    if updated_urls.any?
      item.update(edm_is_shown_at: updated_urls)
      counter += 1
    end
  end
  return counter
end