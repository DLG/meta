require 'rake'

desc "Add event to CRDL items based on the subject and subject personal matches"
task add_events_to_crdl_items: :environment do
  items_events_list = compile_items_events(Item.joins(:portals).where("portals.code" => "crdl"))
  items_list        = (items_events_list.pluck(:item_id) + ItemEvent.all.pluck(:item_id)).uniq

  counts = update_items_events(items_events_list)

  Sunspot.index! Item.where(id: items_list) 

  print_items_events_summary(counts)
  puts "#{items_list.size} Items reindexed"
end

def compile_items_events(items)
  list = []

  Event.all.each do |event|
    event.dcterms_subject_matches.each do |match|
      if match =~ /^(.*)[*]$/
        items_events_wildcard(list, "dcterms_subject", items, event, $1)
      else
        items_events(list, "dcterms_subject", items, event, match)
      end
    end
    event.dlg_subject_personal_matches.each do |match|
      if match =~ /^(.*)[*]$/
        items_events_wildcard(list, "dlg_subject_personal", items, event, $1)
      else
        items_events(list, "dlg_subject_personal", items, event, match)
      end
    end
  end

  list
end

def items_events(list, field, items, event, match)
  items.where("array_lowercase(#{field}) @> ARRAY[:q1]", q1: match.downcase).each do |item|
    list << {item_id: item.id, event_id: event.id}
  end
end

def items_events_wildcard(list, field, items, event, match)
  items.where("array_to_string(#{field}, '||') ILIKE :q1", q1: "%#{match}%").each do |item|
    list << {item_id: item.id, event_id: event.id}
  end
end

def update_items_events(list)
  counts = {} # how many items per each event
  new_items_events = list.uniq.map do |attrs|
    counts[attrs[:event_id]] ||= 0
    counts[attrs[:event_id]] += 1
    ItemEvent.new(attrs)
  end
  ItemEvent.delete_all
  ItemEvent.import(new_items_events)
  counts
end

# number of items for each event
def print_items_events_summary(counts)
  by_slug = {}
  counts.keys.each do |event_id|
    event_slug = Event.find(event_id).slug
    by_slug[event_slug] = counts[event_id]
  end
  by_slug.keys.sort.each do |event_slug|
    puts "#{event_slug}: #{by_slug[event_slug]}"
  end
end
