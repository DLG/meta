require 'rake'

desc "Add names to CRDL items based on subject personal matches"
task add_names_to_crdl_items: :environment do

  items_names_list = compile_items_names
  items_list       = (items_names_list.pluck(:item_id) + ItemName.all.pluck(:item_id)).uniq

  puts "#{Time.now} Update #{items_names_list.size} ItemNames"
  update_items_names(items_names_list)

  puts "#{Time.now} Index #{items_list.size} Items"
  Sunspot.index! Item.for_indexing.where(id: items_list)

  puts "#{Time.now} #{items_list.size} Items Indexed"
end

def compile_items_names
  subject_personal = {}

  items = Item.where.not(dlg_subject_personal: [])

  puts "#{Time.now} Compile subject_personal names for #{items.count} Items"
  items.each do |item|
    item.dlg_subject_personal.each do |subject|
      next unless subject

      # we want to drop any sub-subjects that follow double hyphens, e.g.,
      # - Abernathy, Barbara, 1941- --Political activity (note the space following 1941-)
      # - Abernathy, Ralph, 1926-1990--Homes and haunts
      lc_subject = subject.sub(/ ?--.*/,"").downcase
      subject_personal[lc_subject] ||= []
      subject_personal[lc_subject] << item.id
    end
  end

  list = []

  puts "#{Time.now} Compile ItemNames for #{Name.count} Names"
  Name.all.each do |name|
    item_ids = subject_personal[name.authoritative_name.downcase]
    unless item_ids.blank?
      item_ids.each do |item_id|
        list << {item_id: item_id, name_id: name.id}
      end
    end
  end

  list
end

def update_items_names(list)
  new_items_names = list.uniq.map do |attrs|
    ItemName.new(attrs)
  end
  ItemName.delete_all
  ItemName.import(new_items_names)
end
