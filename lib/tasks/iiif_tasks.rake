# frozen_string_literal: true

desc 'Update missing image dimensions on Page items'
task update_page_image_dimensions: :environment do |_, _args|
  notifier = NotificationService.new

  notifier.notify 'Starting update_page_image_dimensions'
  start = Time.zone.now

  begin
    updated_pages = 0
      Page.missing_image_dimensions.each do |page|
        if page.update_image_dimensions
          page.save
          updated_pages += 1
        end
    end

  rescue StandardError => e
    notifier.notify "update_page_image_dimensions failed. exception: #{e}"
  end
  notifier.notify("Updated #{updated_pages} page image dimensions.")
  notifier.notify("update_page_image_dimensions finished in `#{(Time.zone.now - start).round(3)}` seconds")
end



