# frozen_string_literal: true

require 'rake'
require 'open3'
require 'shellwords'

## This job extracts DLG and CRDL records from solr, builds XML files, zips them, and sftps them to EBSCO
# For CRDL, run:
# RAILS_ENV=production bundle exec rake feed_eds[crdl,ir01613a,full,2500]
# For DLG, run
# RAILS_ENV=production bundle exec rake feed_eds[georgia,ir01612a,incremental,2500,2]
# Note, for incremental, substitute "incremental" for "full"
# Incremental is hard coded to check for records updated in the last 2 days.
# Also, periodically remove the eds_feed_* files from /tmp

task(:feed_eds, [:portal_name, :ir_name, :direction, :records_per_file, :days_past] => [:environment]) do |_, args|
  start = Time.zone.now
  KEY_PATH = "/home/gitlab-runner/.ssh/eds_meta_key"
  SFTP_OPTIONS = "-oKexAlgorithms=diffie-hellman-group14-sha1"

  # explicitly state fields to be included in Solr response, and therefore the
  # XML for EDS
  def eds_fields
    %w[id collection_titles_sms dcterms_provenance_display
       dcterms_title_display dcterms_creator_display dcterms_subject_display
       dcterms_description_display edm_is_shown_at_display
       edm_is_shown_by_display dc_date_display dcterms_spatial_display
       dc_format_display dc_right_display dcterms_type_display
       dcterms_language_display dlg_subject_personal_display
       dcterms_bibliographic_citation_display dcterms_identifier_display
       dc_relation_display dcterms_contributor_display
       dcterms_publisher_display dcterms_temporal_display
       dcterms_is_part_of_display dcterms_rights_holder_display
       dlg_local_right_display dcterms_medium_display dcterms_extent_display
       created_at_dts updated_at_dts]
  end

  notifier = NotificationService.new

  if args.count < 4
    notifier.notify("Error:  Wrong number of parameters. Example parameter list: [crdl,ir01613a,full,2500]")
    next
  end

  direction = args[:direction]

  # build path for server file storage
  local_file_storage = File.join(Rails.application.root, 'tmp')

  # build date strings for file path and file name
  today = Time.zone.today
  date_string = "eds_#{args[:portal_name]}_feed_#{today.strftime('%m%d%Y')}"
  run_file_storage = File.join(local_file_storage, date_string)

  # create local directory if needed
  if Dir.exist?(run_file_storage)
    notifier.notify("Error:  Export directory already exists, please remove #{run_file_storage} directory and re-run rake task.")
    next
  else
    Dir.mkdir(run_file_storage)
    # remove update directory from yesterday:
    Dir.glob(File.join(local_file_storage, "eds_#{args[:portal_name]}_feed_*")).each do |dir|
      next unless File.basename(dir) =~ /_([0-9]+)$/
      begin
        date = Date.strptime($1, '%m%d%Y')
      rescue Date::Error
        next
      end
      next unless date < today
      begin
        FileUtils.rm_rf(dir)
      rescue Errno::ENOENT => e
        notifier.notify("WARNING: Deleting earlier update directory failed with error: #{e.message}.")
      end
    end
  end


  # initialize Solr connection object
  solr = Blacklight.default_index.connection

  # set cursorMark variables for while loop below
  last_cursor_mark = ''
  cursor_mark = '*'
  run = 1
  outcomes = []
  num_records = 0

  rows = args[:records_per_file]
  # get days_past from command params or use default
  days_past = if defined?(args) && args[:days_past]
           args[:days_past]
         else
           '2'
         end

  # query Solr until the end of the set is reached
  # Future: to do incremental: use this filter: updated_at_dts:[NOW-7DAY/DAY TO NOW]
  case direction
  when 'full'
    filters = ['display_b:1', 'portals_sms: ' + args[:portal_name], 'class_name_ss: Item']
  when 'incremental'
    filters = ['display_b:1', 'portals_sms: ' + args[:portal_name], 'class_name_ss: Item', "updated_at_dts:[NOW-#{days_past}DAY/DAY TO NOW]"]
  else
    notifier.notify("Error:  Wrong parameter for 'direction'.  Must be either 'full' or 'incremental'.")
    next
  end

  notifier.notify("Starting EDS #{direction} update for #{args[:portal_name]} portal.  Running solr search to gather records.  This could take a while...")

  while last_cursor_mark != cursor_mark
    # do query
    # TODO: catch exception (cursorMark errors?)
    response = solr.post 'select', data: {
      rows: rows,
      sort: 'id asc',
      fq: filters,
      fl: eds_fields,
      cursorMark: cursor_mark
    }

    # cycle cursorMark variables
    next_cursor_mark = response['nextCursorMark']
    last_cursor_mark = cursor_mark
    cursor_mark = next_cursor_mark
    break if last_cursor_mark == next_cursor_mark

    # build file name for this query response data
    set_file_name = File.join(
      run_file_storage,
      "set_#{run}.xml"
    )

    # create next xml file
    file = File.open(set_file_name, 'w')

    # write XML to file line
    response['response']['docs'].each_with_index do |doc, index|
      if index == 0
        file.puts doc.to_xml(root: 'record')
      end
      file.puts doc.to_xml(root: 'record', skip_instruct: true)
      num_records += 1
    end

    file.close

    outcomes << {
        run: run,
        file: file.path
    }

    run += 1
  end

  if outcomes.length == 0
    notifier.notify("No records found for #{args[:portal_name]} portal #{direction} update. Nothing done.")
    next
  else
    notifier.notify "Record export for #{args[:portal_name]} portal complete: `#{outcomes.length}` files exported containing a total of `#{num_records}` records.  Processing files and uploading..."
  end
  # create lookup table (only needed to submit to EBSCO if facet configuration needs to change.)
  # See https://connect.ebsco.com/s/article/EBSCO-Discovery-Service-EDS-Custom-Catalog-Location-Lookup-Table-Overview

  # build file name for this query response data
  lt_file_name = File.join(
      run_file_storage,
      'dlg_lookup.csv'
  )

  #create lookup table file
  lt_file = File.open(lt_file_name, 'w')

  collections = Collection.all
  collections.each do |col|
    lt_file.puts "\"#{col.display_title}\",\"#{col.display_title}\",\"#{col.display_title}\",\"#{col.display_title}\""
  end

  lt_file.close

  # upload procedure

  # zip up the file(s).  Only one file needed if direction is incremental, two if direction is full update
  # see here: https://connect.ebsco.com/s/article/Using-FTP-to-Create-and-Maintain-Records-in-your-EBSCO-Discovery-Service-EDS-Custom-Catalog-Database

  zipfile1_name =  File.join(run_file_storage, "#{args[:portal_name]}-#{direction}-1.tar.gz")
  zipfile2_name =  File.join(run_file_storage, "#{args[:portal_name]}-#{direction}-2.tar.gz")

  if direction == 'full'
    # for full update, the first zipfile should only contain the first xml file
    zip_command1 = "(cd #{run_file_storage} && tar -czvf #{zipfile1_name} set_1.xml)"
  else
    # for incremental update, the first zipfile should contain all xml files
    zip_command1 = "(cd #{run_file_storage} && tar -czvf #{zipfile1_name} *.xml)"
  end

  # second file is only used for "full" update, and should contain all files except the first one (set_1.xml)
  zip_command2 = "(cd #{run_file_storage} && tar --exclude='set_1.xml' -czvf #{zipfile2_name} *.xml)"

  # generate first zipfile:
  _stdout_str, error_str, status = Open3.capture3({}, zip_command1)

  if !status.success?
    notifier.notify("Unable to zip file 1 for #{args[:portal_name]} #{direction} update. #{error_str}")
    next
  end

  # second zipfile (only needed for full update):
  if direction == 'full'
    _stdout_str, error_str, status = Open3.capture3({}, zip_command2)

    if !status.success?
      notifier.notify("Unable to zip file 2 for #{args[:portal_name]} portal full update. #{error_str}")
      next
    end
  end


  # transfer the files

  if direction == 'full'
    # send first file to the "Full" directory for full update:
    sftp_command = "sftp -i #{KEY_PATH} #{SFTP_OPTIONS} #{args[:ir_name]}@sftp.epnet.com:Full <<<$COMMAND$'\n'exit$'\n'"
  else
    sftp_command = "sftp -i #{KEY_PATH} #{SFTP_OPTIONS} #{args[:ir_name]}@sftp.epnet.com:Update <<<$COMMAND$'\n'exit$'\n'"
  end

  _stdout_str, error_str, status = Open3.capture3({'COMMAND' => "put #{zipfile1_name}"}, "bash -c #{Shellwords.escape(sftp_command)}")
  if !status.success?
    notifier.notify("Unable to upload file for #{args[:portal_name]} portal full update. #{error_str}")
    next
  end

  if direction == 'full'
    # For full update, send second file to the "Update" directory
    sftp_command = "sftp -i #{KEY_PATH} #{SFTP_OPTIONS} #{args[:ir_name]}@sftp.epnet.com:Update <<<$COMMAND$'\n'exit$'\n'"
    _stdout_str, error_str, status = Open3.capture3({'COMMAND' => "put #{zipfile2_name}"}, "bash -c #{Shellwords.escape(sftp_command)}")
  end

  if status.success?
    notifier.notify("Successfully uploaded #{args[:portal_name]} portal #{direction} update to EBSCO. Finished in `#{(Time.zone.now - start).round(1)}` seconds")
  else
    notifier.notify("Unable to upload file(s) for #{args[:portal_name]} portal #{direction} update. #{error_str}")
    next
  end

  outcomes.find { |o| !o[:uploaded] }.present?
end
