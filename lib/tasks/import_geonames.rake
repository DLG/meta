# frozen_string_literal: true

desc 'Batch import GeographicLocations from GeoNames.org'
task import_geonames: :environment do |_, _args|
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.notify 'Starting import_geonames'
  start = Time.zone.now
  begin
    local_path = GeonamesImportService.retrieve_usa
    GeonamesImportService.import_usa local_path
    notifier.notify 'Done importing geonames.'
    File.delete local_path
  rescue StandardError => e
    notifier.notify "import_geonames failed. exception: #{e}"
  end

  notifier.notify("import_geonames finished in `#{(Time.zone.now - start).round(3)}` seconds")
end

desc 'Retrieve and load GeographicLocations from GeoNames.org, adding new rows and updating lat/longs on existing rows'
task update_geonames: :environment do |_, _args|
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.notify 'Starting update_geonames'
  start = Time.zone.now
  begin
    local_path = GeonamesImportService.retrieve_usa
    GeonamesImportService.update_usa local_path
    File.delete local_path
  rescue StandardError => e
    notifier.notify "update_geonames failed. exception: #{e}"
  end
  notifier.notify("update_geonames finished in `#{(Time.zone.now - start).round(3)}` seconds")
end

desc 'Retrieve and load GeographicLocations from GeoNames.org, adding new rows and updating ALL metadata on existing rows'
task update_all_geonames_metadata: :environment do |_, _args|
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.notify 'Starting update_all_geonames_metadata'
  start = Time.zone.now
  begin
    local_path = GeonamesImportService.retrieve_usa
    GeonamesImportService.update_usa_overwrite_names local_path
    File.delete local_path
  rescue StandardError => e
    notifier.notify "update_all_geonames_metadata failed. exception: #{e}"
  end
  notifier.notify("update_all_geonames_metadata finished in `#{(Time.zone.now - start).round(3)}` seconds")
end

desc 'Delete all geographic locations and their relations'
task delete_all_geographic_locations: :environment do |_, _args|
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.notify 'Starting delete_all_geographic_locations'
  start = Time.zone.now
  begin
    GeonamesImportService.delete_all_geographic_locations
  rescue StandardError => e
    notifier.notify "delete_all_geographic_locations failed. exception: #{e}"
  end
  notifier.notify("delete_all_geographic_locations finished in `#{(Time.zone.now - start).round(3)}` seconds")
end

desc 'Remediate Item dccoverage_spatial -> geographic_location'
task remediate_item_geography: :environment do |_, _args|
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.notify 'Starting remediate_item_geography'
  start = Time.zone.now
  begin
    GeonamesImportService.remediate_geographic_info Item, notifier
  rescue StandardError => e
    notifier.notify "remediate_item_geography failed. exception: #{e}"
  end

  notifier.notify("remediate_item_geography finished in `#{(Time.zone.now - start).round(3)}` seconds")
end

desc 'Remediate Collection dccoverage_spatial -> geographic_location'
task remediate_collection_geography: :environment do |_, _args|
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.notify 'Starting remediate_collection_geography'
  start = Time.zone.now
  begin
    GeonamesImportService.remediate_geographic_info Collection, notifier
  rescue StandardError => e
    notifier.notify "remediate_collection_geography failed. exception: #{e}"
  end

  notifier.notify("remediate_collection_geography finished in `#{(Time.zone.now - start).round(3)}` seconds")
end

desc 'Remediate dccoverage_spatial -> geographic_location for Items updated in last n hours (default: 24)'
task :remediate_recently_updated_geography, [:hours_ago] => :environment do |_, args|
  args.with_defaults hours_ago: 24
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.notify 'Starting remediate_recently_updated_geography'
  start = Time.zone.now
  begin
    hours = args[:hours_ago].to_f
    cutoff = hours.hours.ago
    items = Item.updated_since(cutoff)
    notifier.notify "#{items.count} items updated in the last #{hours} hours"
    GeonamesImportService.incremental_remediatate_geographic_info items, notifier
    collections = Collection.updated_since(cutoff)
    notifier.notify "#{collections.count} collections updated in the #{hours} hours prior to this script starting"
    GeonamesImportService.incremental_remediatate_geographic_info collections, notifier
  rescue StandardError => e
    notifier.notify "remediate_recently_updated_geography failed. exception: #{e}"
  end

  notifier.notify("remediate_recently_updated_geography finished in `#{(Time.zone.now - start).round(3)}` seconds")
end

desc 'Remediate dccoverage_spatial -> geographic_location for BatchItems updated in last n hours (default: 24)'
task :remediate_recently_updated_batch_item_geography, [:hours_ago] => :environment do |_, args|
  args.with_defaults hours_ago: 24
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.notify 'Starting remediate_recently_updated_batch_item_geography'
  start = Time.zone.now
  begin
    hours = args[:hours_ago].to_f
    cutoff = hours.hours.ago
    items = BatchItem.updated_since(cutoff)
    notifier.notify "#{items.count} batch items updated in the last #{hours} hours"
    GeonamesImportService.incremental_remediatate_geographic_info items, notifier
  rescue StandardError => e
    notifier.notify "remediate_recently_updated_batch_item_geography failed. exception: #{e}"
  end

  notifier.notify("remediate_recently_updated_batch_item_geography finished in `#{(Time.zone.now - start).round(3)}` seconds")
end

task clean_up_duplicate_geolocations: :environment do |_, _args|
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.notify 'Starting clean_up_duplicate_geolocations'
  start = Time.zone.now
  begin
    # Part 1: consolidate any exact duplicates. These should not exist (but a bug created them for a while)
    duplicates = GeographicLocation.select(:code, :geonames_id, GeographicLocation.arel_table[:id].minimum.as('min_id'))
                                   .group(:code, :geonames_id).having('count(*) > 1').to_a
    notifier.notify "#{duplicates.size} sets of duplicates with same code AND Geonames ID found."
    duplicates.each do |group|
      members = GeographicLocation.where(code: group.code, geonames_id: group.geonames_id)
      num_members = members.size
      keeper = members.order(id: :asc).first
      to_cull = members.where.not(id: keeper.id)
      to_cull.each do |geoloc|
        geoloc.consolidate_into keeper
      end
      notifier.notify "Consolidated '#{group.code}', Geonames ID: #{group.geonames_id || '(none)'} " +
                        "(#{num_members} rows) into one row (ID: #{keeper.id})"
    end
    # Part 2: Delete any geolocs with nil Geonames ID whose code matches another geoloc with a non-nil ID.
    # Consolidate their relations into their Geonames-ID-having counterpart
    id_pairs = GeographicLocation.joins('INNER JOIN geographic_locations gl2 ON geographic_locations.code = gl2.code ' +
                               'AND geographic_locations.id <> gl2.id AND geographic_locations.geonames_id is NULL ' +
                               'AND gl2.geonames_id IS NOT NULL')
                      .pluck(:id, 'gl2.id', :code)
    id_pairs.each do |from_id, to_id, code|
      source = GeographicLocation.find_by(id: from_id)
      next if source.nil? # Source geoloc was already deleted earlier in the loop; maybe had multiple matches
      source.consolidate_into(GeographicLocation.find(to_id))
      notifier.notify "Consolidated '#{code}', Geonames ID: (none) (GeographicLocation ID: #{from_id})' " +
                        "into its counterpart with non-nil Geonames ID (GeographicLocation ID: #{to_id})"
    end
  rescue StandardError => e
    notifier.notify "clean_up_duplicate_geolocations failed. exception: #{e}"
  end

  notifier.notify("clean_up_duplicate_geolocations finished in `#{(Time.zone.now - start).round(3)}` seconds")
end
