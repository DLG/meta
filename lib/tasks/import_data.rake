# frozen_string_literal: true

desc 'Retrieve and load the production data dump'
task :import_production_data, [:batch_size] => :environment do |_, args|
  $stdout.sync = true

  notifier = NotificationService.new
  if Rails.env.production?
    message = "Not allowed to run import_production_data in #{Rails.env}."
    notifier.notify(message)
    return
  end

  notifier.notify 'Starting import_production_data'
  start = Time.zone.now

  begin
    notifier.notify 'Starting import sql'
    DataImportService.start(notifier: notifier)
    notifier.notify 'Finished import sql'

    notifier.notify 'Starting Solr reindex'
    SolrService.delete_all
    SolrService.reindex_all batch_size: args[:batch_size].to_i
    notifier.notify 'Finished Solr reindex'

  rescue StandardError => e
    notifier.notify "import_production_data failed. exception: #{e}"
  end

  notifier.notify("import_production_data finished in `#{((Time.zone.now - start)/60).round(1)}` minutes")
end

desc 'Retrieve SQL data dump'
task retrieve_sql_dump: :environment do |_, _args|
  $stdout.sync = true

  notifier = NotificationService.new
  if Rails.env.production?
    message = "Not allowed to run retrieve_sql_dump in #{Rails.env}."
    notifier.notify(message)
    return
  end

  notifier.notify 'Starting retrieve_sql_dump'
  start = Time.zone.now

  begin
    DataImportService.retrieve_sql_dump(notifier: notifier)
  rescue StandardError => e
    notifier.notify "retrieve_sql_dump failed. exception: #{e}"
  end

  notifier.notify("retrieve_sql_dump finished in `#{((Time.zone.now - start)/60).round(1)}` minutes")
end

desc 'Retrieve uploads data dump'
task retrieve_uploads_dump: :environment do |_, _args|
  $stdout.sync = true

  notifier = NotificationService.new
  if Rails.env.production?
    message = "Not allowed to run retrieve_uploads_dump in #{Rails.env}."
    notifier.notify(message)
    return
  end

  notifier.notify 'Starting retrieve_uploads_dump'
  start = Time.zone.now

  begin
    DataImportService.retrieve_uploads_dump(notifier: notifier)
  rescue StandardError => e
    notifier.notify "retrieve_uploads_dump failed. exception: #{e}"
  end

  notifier.notify("retrieve_uploads_dump finished in `#{((Time.zone.now - start)/60).round(1)}` minutes")
end

desc 'Load SQL data dump'
task load_sql_dump: :environment do |_, _args|
  $stdout.sync = true

  notifier = NotificationService.new
  if Rails.env.production?
    message = "Not allowed to run load_sql_dump in #{Rails.env}."
    notifier.notify(message)
    return
  end

  notifier.notify 'Starting load_sql_dump'
  start = Time.zone.now

  begin
    DataImportService.load_sql(notifier: notifier)
  rescue StandardError => e
    notifier.notify "load_sql_dump failed. exception: #{e}"
  end

  notifier.notify("load_sql_dump finished in `#{((Time.zone.now - start)/60).round(1)}` minutes")
end

desc 'Load uploads data dump'
task load_uploads_dump: :environment do |_, _args|
  $stdout.sync = true

  notifier = NotificationService.new
  if Rails.env.production?
    message = "Not allowed to run load_uploads_dump in #{Rails.env}."
    notifier.notify(message)
    return
  end

  notifier.notify 'Starting load_uploads_dump'
  start = Time.zone.now

  begin
    DataImportService.load_uploads(notifier: notifier)
  rescue StandardError => e
    notifier.notify "load_uploads_dump failed. exception: #{e}"
  end

  notifier.notify("load_uploads_dump finished in `#{(Time.zone.now - start).round(1)}` seconds")
end

