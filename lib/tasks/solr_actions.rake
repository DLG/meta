# frozen_string_literal: true

desc 'Delete all solr data'
task solr_delete_all: :environment do |_, _args|
  $stdout.sync = true
  SolrService.delete_all
end

desc 'Delete the solr data for a single table'
task :solr_delete_table, [:table_name] => :environment do |_, args|
  $stdout.sync = true
  SolrService.delete_table args[:table_name]
end

desc 'Delete collections solr data'
task solr_delete_collections: :environment do |_, _args|
  $stdout.sync = true
  SolrService.delete_table 'Collection'
end

desc 'Delete Items solr data'
task solr_delete_items: :environment do |_, _args|
  $stdout.sync = true
  SolrService.delete_table 'Item'
end

desc 'Delete Names solr data'
task solr_delete_names: :environment do |_, _args|
  $stdout.sync = true
  SolrService.delete_table 'Name'
end

desc 'Delete Events solr data'
task solr_delete_events: :environment do |_, _args|
  $stdout.sync = true
  SolrService.delete_table 'Event'
end

desc 'Delete Feature solr data'
task solr_delete_features: :environment do |_, _args|
  $stdout.sync = true
  SolrService.delete_table 'Feature'
end

desc 'Delete HoldingInstitution solr data'
task solr_delete_holding_institutions: :environment do |_, _args|
  $stdout.sync = true
  SolrService.delete_table 'HoldingInstitution'
end

desc 'Reindex all solr data'
task :solr_reindex_all, [:batch_size] => :environment do |_, args|
  $stdout.sync = true
  SolrService.reindex_all(batch_size: args[:batch_size].to_i)
end

desc 'Reindex the solr data for a single table'
task :solr_reindex_table, [:table_name, :starting_id, :batch_size] => :environment do |_, args|
  $stdout.sync = true
  SolrService.reindex args[:table_name], starting_id: args[:starting_id].to_i, batch_size: args[:batch_size].to_i
end

desc 'Reindex all items using batch sizes that adjust based on fulltext size'
task smart_reindex_items: :environment do |_, args|
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.notify "Starting smart_reindex_items. This will reindex all items."
  SolrService.smart_reindex_items notifier: notifier
  notifier.notify "Done with smart_reindex_items."
end

desc 'Reindex Collections solr data'
task solr_reindex_collections: :environment do |_, _args|
  $stdout.sync = true
  SolrService.reindex 'Collection'
end

desc 'Reindex the Items for a given Collection in one batch'
task :legacy_reindex_collection_items,  [:collection_slug] => :environment do |_, args|
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.notify "Starting solr_reindex_collection_items for collection #{args[:collection_slug]}"

  collection = Collection.find_by slug: args[:collection_slug]
  unless collection
    notifier.notify "Collection with slug #{args[:collection_slug]} not found."
    next #Exit
  end
  Sunspot.index  collection.items
  Sunspot.commit
  notifier.notify "Finished reindexing #{collection.items.size} for collection #{args[:collection_slug]}"
end

desc 'Reindex the Items for a given Collection using batch sizes that adjust based on fulltext size'
task :solr_reindex_collection_items,  [:collection_slug] => :environment do |_, args|
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.notify "Starting solr_reindex_collection_items for collection #{args[:collection_slug]}"

  collection = Collection.find_by slug: args[:collection_slug]
  unless collection
    notifier.notify "Collection with slug #{args[:collection_slug]} not found."
    next #Exit
  end
  SolrService.smart_reindex_items collection.items
  Sunspot.commit
  notifier.notify "Finished reindexing #{collection.items.size} for collection #{args[:collection_slug]}"
end


desc 'Reindex Items solr data'
task solr_reindex_items: :environment do |_, _args|
  $stdout.sync = true
  SolrService.reindex 'Item'
end

desc 'Reindex CRDL Items solr data'
task solr_reindex_crdl_items: :environment do |_, _args|
  $stdout.sync = true
  notifier = NotificationService.new
  items = Item.for_portals('crdl').for_indexing
  notifier.notify "Starting reindex of #{items.count} CRDL Items"
  Item.for_portals('crdl').for_indexing.find_in_batches(batch_size: SolrService::REINDEX_BATCH_SIZE) do |batch|
    Sunspot.index! batch
  end
  notifier.notify "Finished solr_reindex_crdl_items"

end

desc 'Reindex Names solr data'
task solr_reindex_names: :environment do |_, _args|
  $stdout.sync = true
  SolrService.reindex 'Name'
end

desc 'Reindex Events solr data'
task solr_reindex_events: :environment do |_, _args|
  $stdout.sync = true
  SolrService.reindex 'Event'
end

desc 'Reindex Feature solr data'
task solr_reindex_features: :environment do |_, _args|
  $stdout.sync = true
  SolrService.reindex 'Feature'
end

desc 'Reindex HoldingInstitution solr data'
task solr_reindex_holding_institutions: :environment do |_, _args|
  $stdout.sync = true
  SolrService.reindex 'HoldingInstitution'
end
