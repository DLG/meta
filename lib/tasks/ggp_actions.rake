# frozen_string_literal: true

desc 'Remediate GGP items where dc_date used "/" incorrectly'
task remediate_ggp_dc_date: :environment do |_, _args|
  $stdout.sync = true
  notifier = NotificationService.new

  batch_size = 500
  affected_item_ids = Item.from(Collection.find_by_slug('ggpd')
                                          .items
                                          .select(:id, 'UNNEST(dc_date) as date'), 'unnested')
                          .where(
                            "unnested.date ~ '^\\d{4}/\\d{1,2}\\M'" # \M in Postgres is similar to \b in other dialects
                          # so this will 1234/56 or 1234/56/78 but not 1234/5678
                          ).pluck :id
  notifier.notify "Will remediate dates for #{affected_item_ids.size} GGP items in batches of #{batch_size}"
  succeeded = 0
  failed = 0
  affected_item_ids.in_groups_of(batch_size, false) do |group|
    Item.where(id: group).each do |item|
      item.dc_date = item.dc_date.map do |date|
        date.gsub(/\b(\d{4})\/(\d{1,2})(\/(\d{1,2}))?\b/) { "#{$1}-#{$2}#{$3 && "-#{$4}"}" }
      end
      if item.save
        succeeded += 1
      else
        failed += 1
      end
    end
    notifier.notify "#{succeeded} items remediated successfully. #{failed} errors."
  end
  notifier.notify 'Done remediating GGP dates.' + (
    failed > 0 ? " #{failed} items failed to save and could not be updated. Note that these items may have failed " +
      "validations completely unrelated to dc_date and the changes made by this script."
      : ''
  )
end

desc 'Match GGP items to serials'
task match_ggp_items_to_serials: :environment do |_, _args|
  $stdout.sync = true
  notifier = NotificationService.new

  path_to_856_lookup = Rails.root.join('tmp/ggp_previous_856s.csv')
  previous_856_lookup = nil
  if File.exist? path_to_856_lookup
    notifier.notify "Will use previous 856s from #{path_to_856_lookup}."
    previous_856_lookup = {}
    CSV.foreach(path_to_856_lookup, headers: true) do |row|
      previous_856_lookup[row['slug']] = row['url'] if row['slug'].present? && row['url'].present?
    end
    notifier.notify "Done reading #{path_to_856_lookup}. Found #{previous_856_lookup.size} items."
  end

  notifier.notify 'Matching existing GGP items to serials...'
  matchmaker = GgpItemMatchingService.new(notifier: notifier, previous_856_lookup: previous_856_lookup)
  matchmaker.match_serials_to_items
  notifier.notify('Done saving item-to-serial matches.')
  item_ids = matchmaker.all_matched_items.sort
  SolrService.smart_reindex_items Item.where(id: item_ids), notifier: notifier
  notifier.notify 'Done reindexing GGP items'
  SolrService.reindex 'serials'
  notifier.notify 'Done matching existing GGP items to serials'
end

desc 'Audit identifiers in GGP items'
task audit_identifiers_in_ggp_items: :environment do |_, _args|
  $stdout.sync = true

  csv_file_path = "#{Rails.root}/tmp/ggp_id_call_number_audit.csv"
  headers = %w[db_id slug dcterms_identifier call_number prefix correction case]
  CSV.open(csv_file_path, 'w', headers: headers, write_headers: true) do |csv|

    coll = Collection.find_by_slug('ggpd')
    item_ids = Item.where(collection_id: coll.id).pluck :id
    item_ids.each do |item_id|
      item = Item.find(item_id)

      external_identifiers = item.external_identifiers
      dcterms_identifier = item.dcterms_identifier
      next if dcterms_identifier.empty?

      found = external_identifiers.find {|h| h['type']=='Call Number'}
      next if found.nil?

      call1 = dcterms_identifier.first
      call2 = found['value']
      next if call1 == call2

      case_cond = ''
      corr_cond = ''
      pref_cond = ''

      if cn_norm(call1) == cn_norm(call2)
        case_cond = 'case'
        corr_cond = 'correction'
      elsif call1 =~ /#{call2}$/
        pref_cond = 'prefix'
      elsif cn_norm(call1) =~ /#{cn_norm(call2)}$/
        case_cond = 'case'
        pref_cond = 'prefix'
        corr_cond = 'correction'
      else
        corr_cond = 'correction'
      end

      # ['db_id', 'slug', 'dcterms_identifier', 'call_number', 'prefix', 'correction', 'case']
      csv << [item.id, item.slug, call1, call2, pref_cond, corr_cond, case_cond]
    end
  end
end

desc 'Add call numbers to GGP items'
task add_call_numbers_ggp_items: :environment do |_, _args|
  $stdout.sync = true
  notifier = NotificationService.new
  notifier.notify 'Adding call numbers to GGP items...'

  updated       = 0
  item_ids      = []
  skipped       = 0
  skipped_slugs = []
  count         = 0
  msg           = ''

  csv_file_path = "#{Rails.root}/tmp/ggp_id_call_number.csv"
  CSV.foreach(csv_file_path, headers: true) do |row|
    count += 1

    slug        = row['id']
    call_number = row['call_number']
    call_number = slug_to_call_number( slug ) if call_number.blank?
    if call_number.blank?
      skipped += 1
      skipped_slugs << slug
      next
    end

    if (item = Item.find_by_slug(slug))
      # note: the keys (e.g., 'type'/'value') must be String (not Symbol)
      external_identifiers = (item.external_identifiers + [{'type' => 'Call Number', 'value' => call_number}]).uniq
      Item.where(id: item.id).update_all(external_identifiers: external_identifiers, dcterms_identifier: [])
      updated += 1
      item_ids << item.id
    else
      skipped += 1
      skipped_slugs << slug
    end

    msg = "#{count}. Call number added to #{updated} items."
    msg += " #{skipped} slugs skipped." if skipped > 0
    if count % 10000 == 0
      notifier.notify msg
    end
  end

  notifier.notify msg
  notifier.notify "Slugs skipped: #{skipped_slugs}"
  notifier.notify 'Finished: Adding call numbers to GGP items...'

  SolrService.smart_reindex_items Item.where(id: item_ids), notifier: notifier
  notifier.notify "Finished: Reindexing #{item_ids.size} GGP items"

end

def cn_norm( call_number )
  call_number.downcase.gsub(/ +/, ' ')
end

def slug_to_call_number( slug )

  return '' unless slug =~ /^[isy]-/
  cn = slug.dup

  cn.sub!(/^[isy]-/,         '')
  cn.sub!(/-belec-p-btext$/, '')
  cn.sub!(/-bfolio$/,        '')
  cn.sub!(/-bcd-hrom$/,      '')
  cn.sub!(/-bdvd-hrom$/,     '')
  cn.sub!(/-baudio-bcd$/,    '')
  cn.sub!(/-bvideo$/,        '')

  cn.gsub!(/-s/, '/')
  cn.gsub!(/-p/, '.')
  cn.gsub!(/-m/, ',')
  cn.gsub!(/-l/, '+')
  cn.gsub!(/-o/, '(')
  cn.gsub!(/-c/, ')')
  cn.gsub!(/-b/, ' ')
  cn.gsub!(/-a/, '&')
  cn.gsub!(/-0/, '#')
  cn.gsub!(/-h/, '-')  # last

  cn.sub!(/ +$/, '')

  cn.upcase!

  # from studying the csv data ...
  cn.sub!(/ SUPPL\.$/, ' Suppl.')
  cn.sub!(/A$/, 'a')

  cn
end

desc 'Delete items from MARC import with bad DO links'
task :delete_bad_marc_import_items, [:marc_import_id] => :environment do |_, args|
  $stdout.sync = true
  marc_import = MarcXmlImport.find args[:marc_import_id]
  raise "You must supply a valid MarcXmlImport ID" unless marc_import

  notifier = NotificationService.new allow_slack_failure: true
  job_desc = "checking DO links for items created by MarcXmlImport #{marc_import.id} (#{marc_import.date_title})"
  notifier.notify "#{job_desc.upcase_first}..."
  notifier.notify 'Items with bad DO links (HTTP status is not 200 after following all redirects) will be deleted.'

  created_items = Item.where id: marc_import.result_records
                                            .filter { |rec| rec['type']=='Item' && rec['created'] }
                                            .map { |rec| rec['id'] }
  total_count = created_items.count
  to_delete = []
  notifier.notify "Inspecting #{total_count} items..."
  created_items.find_each do |item|
    do_link = item.edm_is_shown_by.first
    if do_link.blank?
      # This shouldn't happen for GGP
      notifier.notify "Skipping item #{item.id} (#{item.record_id}) with no DO link"
      next
    end

    begin
      link_status = HTTParty.head(do_link)&.code # follows redirects
      status_message =  "HTTP #{link_status}"
    rescue StandardError => e
      link_status = nil
      status_message = e.class.name # e.g. Errno::ECONNREFUSED
    end
    if link_status != 200
      to_delete << item.id
      notifier.notify "Will delete item #{item.id} (#{item.record_id}). Got #{status_message} from #{do_link}"
    end
  end

  notifier.notify "Deleting #{to_delete.size} items of #{total_count} inspected..."
  Item.where(id: to_delete).destroy_all

  notifier.notify "Done #{job_desc}."
end
