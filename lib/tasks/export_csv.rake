# frozen_string_literal: true

require 'rake'
require 'csv'

task export_csv_for_link_checking: :environment do |_, _args|

  job      = "export_csv_for_link_checking"
  notifier = NotificationService.new

  notifier.notify "Start #{job}"

  BATCH_SIZE = 100_000

  local_tmp   = File.join(Rails.application.root, 'tmp')
  date        = Time.zone.now.strftime('%Y%m%d')
  path_string = "csv_export_#{date}.csv"
  local_file  = File.join(local_tmp, path_string)

  headers = %w[ id public record_id portals edm_is_shown_at edm_is_shown_by ]
  page_count  = (Item.all.count/BATCH_SIZE.to_f).ceil

  CSV.open( local_file ,'w', write_headers: true, headers: headers ) do |csv|
    page = 0
    Item.all.order(id: :asc).includes(:collection).includes(:repository).find_in_batches(batch_size: BATCH_SIZE) do |batch|
      page += 1
      notifier.notify "#{job}: Write page #{page} of #{page_count}"
      batch.each do |i|
        csv << [
          i.id,
          i.display?,
          i.record_id,
          i.portal_codes.join(','),
          i.edm_is_shown_at.first.to_s,
          i.edm_is_shown_by.first.to_s
        ]
      end
    end
  end

  notifier.notify "Finish #{job}"
end
