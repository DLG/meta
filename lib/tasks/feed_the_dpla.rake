# frozen_string_literal: true

require 'rake'

##
# **Task Name**: `feed_the_dpla`
#
# This rake task extracts records from Solr that are marked as DPLA=true,
# builds JSON files, and uploads them to the DPLA S3 bucket specifically for the DLG (Digital Library of Georgia).
#
# **AWS credentials** and the **bucket name** are stored in the encrypted credentials file.
#
# ### Usage:
#
# ```sh
# rake feed_the_dpla[records_per_file,dry_run,force]
# ```
#
# ### Parameters:
#
# - `records_per_file` (Optional):
#   - **Type**: Integer
#   - **Default**: `5000`
#   - **Description**: The number of records to include in each JSON file.
#     Avoid using a number higher than 5000 as it may lead to file sizes that trigger
#     a multipart upload to S3, which will break the MD5 checksum validation in place
#     for each upload (see `DplaS3Service`).
#
# - `dry_run` (Optional):
#   - **Type**: Boolean (`true` or `false`)
#   - **Default**: `false`
#   - **Description**: If set to `true`, the task will perform all operations except uploading files to S3.
#
# - `force` (Optional):
#   - **Type**: Boolean (`true` or `false`)
#   - **Default**: `false`
#   - **Description**: If set to `true`, the task will run immediately, bypassing the scheduled date checks.
#
# ### Description:
#
# This task is designed to run daily but will execute the main operations only on specific days of the year,
# unless the `force` parameter is set to `true`.
#
# **Operations Performed**:
#
# 1. **Date Check**: Determines if the task should run based on the current date or the `force` parameter.
#    - The task runs on specific Mondays before certain significant dates.
#      - **December**: The Monday before Christmas week.
#      - **March, June, September**: The Monday before the last Friday of the month.
#
# 2. **Initialization**:
#    - Creates an instance of `DplaS3Service` for handling file writing and S3 uploads.
#    - Establishes a connection to the Solr index.
#
# 3. **Data Retrieval**:
#    - Queries Solr for records that are flagged for inclusion in DPLA.
#    - Uses cursor-based pagination to efficiently retrieve all matching records.
#
# 4. **Data Processing**:
#    - Processes each record, adjusting fields as necessary.
#    - Writes records to JSON files, ensuring files do not exceed the size limit to avoid multipart uploads.
#
# 5. **Uploading**:
#    - Uploads the generated JSON files to the DPLA S3 bucket.
#    - If in dry run mode, the upload step is skipped.
#
# 6. **Notifications**:
#    - Sends notifications about upcoming runs, completions, and any issues encountered via `NotificationService`.
#
# **Note**: Periodically remove the `set_*` files from `/tmp` to avoid clutter.
#
# ### Examples:
#
# **Run with default parameters**:
#
# ```sh
# rake feed_the_dpla
# ```
#
# **Run with 3000 records per file in dry run mode**:
#
# ```sh
# rake feed_the_dpla[3000,true]
# ```
#
# **Force the task to run immediately, bypassing date checks**:
#
# ```sh
# rake feed_the_dpla[5000,false,true]
# ```
#
task(:feed_the_dpla, [:records_per_file, :dry_run, :force] => [:environment]) do |_, args|
  today = Date.today
  month = today.month

  # Explicitly state fields to be included in Solr response, and therefore the JSON for the DPLA.
  def dpla_fields
    %w[
      id collection_titles_sms dcterms_provenance_display
      dcterms_title_display dcterms_creator_display dcterms_subject_display
      dcterms_description_display edm_is_shown_at_display
      edm_is_shown_by_display dc_date_display dpla_spatial_json_ss
      dc_format_display dc_right_display dcterms_type_display
      dcterms_language_display dlg_subject_personal_display
      dcterms_bibliographic_citation_display dcterms_identifier_display
      dc_relation_display dcterms_contributor_display
      dcterms_publisher_display dcterms_temporal_display
      dcterms_is_part_of_display dcterms_rights_holder_display
      dlg_local_right_display dcterms_medium_display dcterms_extent_display
      iiif_manifest_url_ss created_at_dts updated_at_dts
    ]
  end

  # Calculates the Monday before Christmas week.
  def monday_before_christmas_week(year)
    christmas = Date.new(year, 12, 25)
    monday_of_christmas_week = christmas.beginning_of_week
    if christmas.saturday? || christmas.sunday?
      monday_of_christmas_week
    else
      monday_of_christmas_week.prev_week
    end
  end

  # Calculates the Monday before the last Friday of the month.
  def monday_before_last_friday_of_the_month(today)
    today.end_of_month.prev_occurring(:thursday).prev_occurring(:monday)
  end

  # For testing purposes, you can manually set the month and date:
  # month = 12
  # today = Date.new(2024, 12, 16)

  # Determine the target date based on the month.
  if month == 12
    target_date = monday_before_christmas_week(today.year)
  elsif [3, 6, 9].include?(month)
    target_date = monday_before_last_friday_of_the_month(today)
  end

  notifier = NotificationService.new

  # Parse the force argument.
  force_run = args[:force].to_s.downcase == 'true' if args[:force]
  force_run ||= false

  if today == target_date&.prev_day && !force_run
    notifier.notify "DPLA feed will run tomorrow."
  elsif today == target_date || force_run

    # Build date strings for file path and S3 folder.
    date = Time.zone.now.strftime('%m%d%Y')

    # Initialize S3 uploader.
    dry_run = args[:dry_run].to_s.downcase == 'true' if args[:dry_run]
    dry_run ||= false
    s3 = DplaS3Service.new(date, dry_run: dry_run)

    # Initialize Solr connection object.
    solr = Blacklight.default_index.connection

    # Set cursorMark variables for while loop below.
    last_cursor_mark = ''
    cursor_mark = '*'

    # Get rows from command params or use default.
    rows = if defined?(args) && args[:records_per_file]
             args[:records_per_file]
           else
             '5000'
           end

    # Query Solr until the end of the set is reached.
    while last_cursor_mark != cursor_mark
      # Do query.
      # TODO: Catch exceptions (e.g., cursorMark errors).
      response = solr.post 'select', data: {
        rows: rows,
        sort: 'id asc',
        fq: ['display_b:1', 'dpla_b: 1', 'class_name_ss: Item'],
        fl: dpla_fields,
        cursorMark: cursor_mark
      }

      # Cycle cursorMark variables.
      next_cursor_mark = response['nextCursorMark']
      last_cursor_mark = cursor_mark
      cursor_mark = next_cursor_mark
      break if last_cursor_mark == next_cursor_mark

      # Write JSON to file line by line.
      response['response']['docs'].each do |doc|
        dpla_spatial_json = doc.delete 'dpla_spatial_json_ss'
        if dpla_spatial_json.present?
          doc['dcterms_spatial_display'] = JSON.parse(dpla_spatial_json)
        end
        s3.write_doc doc
      end
    end

    s3.upload_last_file

    upload_count = s3.outcomes.filter { |o| o[:uploaded] }.size
    notifier.notify "DPLA feed complete: `#{upload_count}` of `#{s3.file_count}` files uploaded."
    s3.outcomes.find { |o| !o[:uploaded] }.present?
  end
end